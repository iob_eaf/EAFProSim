# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 09:09:18 2021

@author: Alexander Reimann IOB RWTH Aachen
"""
import numpy as np
import h5py
import pickle
from scipy.interpolate import PchipInterpolator
from numba import njit, types, from_dtype


#@dataclass    
#class InputData: 

float_array = types.float64[:]

@njit(cache=True)
def calculate_UIP_parameters(c,M):
    MM = np.array([M["C"],M["Si"],M["Mn"],M["Cr"],M["P"],M["Al"],M["S"],M["O"]])
    #np.fromiter(map(M.get,["C","Si","Mn","Cr","P","Al","S","O"]),dtype=float,count=8)
    c["melt_UIP"]=c["melt_UIP"]*MM
    c["melt_interaction_T_UIP"] = c["melt_interaction_T_UIP"]*MM
    # Epsilon(i,j)=Epsilon(j,i) however conversion gives different values so
    # Epsilon(i,j) and Epsilon(j,i) is set to mean(Epsilon(i,j),Epsilon(j,i))
    
    for i in range(c["melt_UIP"].shape[0]):
        for j in range(c["melt_UIP"].shape[1]):
            c["melt_UIP"][i,j] = (c["melt_UIP"][i,j]+c["melt_UIP"][j,i])/2
            c["melt_UIP"][j,i] = c["melt_UIP"][i,j]
            c["melt_interaction_T_UIP"][i,j]=(c["melt_interaction_T_UIP"][i,j]+c["melt_interaction_T_UIP"][j,i])/2
            c["melt_interaction_T_UIP"][j,i]=c["melt_interaction_T_UIP"][i,j]
    #return c

@njit(cache=True)
def update_mass_and_molar_fractions(ind,M,X,W,y,c,cp):
    n_st_liq =     (y[ind["m_O_st_liq"]] / M["O"]
                    + y[ind["m_C_st_liq"]] / M["C"]
                    + y[ind["m_Si_st_liq"]] / M["Si"]
                    + y[ind["m_Mn_st_liq"]] / M["Mn"]
                    + y[ind["m_Cr_st_liq"]] / M["Cr"]
                    + y[ind["m_P_st_liq"]] / M["P"]
                    + y[ind["m_Fe_st_liq"]] / M["Fe"])   

    n_reac_liq =   (y[ind["m_O_reac"]] / M["O"]
                    + y[ind["m_C_reac"]] / M["C"]
                    + y[ind["m_Si_reac"]] / M["Si"]
                    + y[ind["m_Mn_reac"]] / M["Mn"]
                    + y[ind["m_Cr_reac"]] / M["Cr"]
                    + y[ind["m_P_reac"]] / M["P"]
                    + y[ind["m_Fe_reac"]] / M["Fe"])  
    
    n_sl_liq = (y[ind["m_CaO_sl_liq"]] / M["CaO"]
                + y[ind["m_MgO_sl_liq"]] / M["MgO"]
                + y[ind["m_Al2O3_sl_liq"]] / M["Al2O3"]
                + y[ind["m_SiO2_sl_liq"]] / M["SiO2"]
                + y[ind["m_MnO_sl_liq"]] / M["MnO"]
                + y[ind["m_Cr2O3_sl_liq"]]/ M["Cr2O3"]
                + y[ind["m_P2O5_sl_liq"]] / M["P2O5"]
                + y[ind["m_CaS_sl_liq"]] / M["CaS"]
                + y[ind["m_FeO_sl_liq"]] / M["FeO"])
    
    n_sl_sol = (y[ind["m_CaO_sl_sol"]] / M["CaO"]
                + y[ind["m_MgO_sl_sol"]] / M["MgO"]
                + y[ind["m_Al2O3_sl_sol"]] / M["Al2O3"]
                + y[ind["m_SiO2_sl_sol"]] / M["SiO2"]
                + y[ind["m_MnO_sl_sol"]] / M["MnO"]
                + y[ind["m_Cr2O3_sl_sol"]]/ M["Cr2O3"]
                + y[ind["m_P2O5_sl_sol"]] / M["P2O5"]
                + y[ind["m_CaS_sl_sol"]] / M["CaS"]
                + y[ind["m_FeO_sl_sol"]] / M["FeO"])
        
    n_gas = (y[ind["m_CO"]] / M["CO"] + y[ind["m_CO2"]] / M["CO2"]
            + y[ind["m_N2"]] / M["N2"] + y[ind["m_O2"]] / M["O2"]
            + y[ind["m_H2"]] / M["H2"] + y[ind["m_H2O"]] / M["H2O"]
            + y[ind["m_CH4"]] / M["CH4"])

    X["O"] = y[ind["m_O_st_liq"]] / M["O"] / n_st_liq
    X["C"] = y[ind["m_C_st_liq"]] / M["C"] / n_st_liq
    X["Si"] = y[ind["m_Si_st_liq"]] / M["Si"] / n_st_liq
    X["Mn"] = y[ind["m_Mn_st_liq"]] / M["Mn"] / n_st_liq
    X["Cr"] = y[ind["m_Cr_st_liq"]] / M["Cr"] / n_st_liq
    X["P"] = y[ind["m_P_st_liq"]] / M["P"] / n_st_liq
    # calculation missing!!
    X["Al"] = 0
    X["S"] = y[ind["m_S_st_liq"]] / M["S"]/ n_st_liq
    X["Fe"] = y[ind["m_Fe_st_liq"]] / M["Fe"]/ n_st_liq

    X["O_reac"] = y[ind["m_O_reac"]] / M["O"] / n_reac_liq
    X["C_reac"] = y[ind["m_C_reac"]] / M["C"] / n_reac_liq
    X["Si_reac"] = y[ind["m_Si_reac"]] / M["Si"] / n_reac_liq
    X["Mn_reac"] = y[ind["m_Mn_reac"]] / M["Mn"] / n_reac_liq
    X["Cr_reac"] = y[ind["m_Cr_reac"]] / M["Cr"] / n_reac_liq
    X["P_reac"] = y[ind["m_P_reac"]] / M["P"] / n_reac_liq
    # calculation missing!!
    X["Al_reac"] = 0
    X["S_reac"] = y[ind["m_S_reac"]] / M["S"]/ n_reac_liq
    X["Fe_reac"] = y[ind["m_Fe_reac"]] / M["Fe"]/ n_reac_liq

    X["CaO"] = y[ind["m_CaO_sl_liq"]] / M["CaO"] / n_sl_liq 
    X["MgO"] = y[ind["m_MgO_sl_liq"]] / M["MgO"] / n_sl_liq
    X["Al2O3"] = y[ind["m_Al2O3_sl_liq"]] / M["Al2O3"] / n_sl_liq 
    X["SiO2"] = y[ind["m_SiO2_sl_liq"]] / M["SiO2"] / n_sl_liq
    X["MnO"] = y[ind["m_MnO_sl_liq"]] / M["MnO"] / n_sl_liq
    X["Cr2O3"] = y[ind["m_Cr2O3_sl_liq"]] / M["Cr2O3"] / n_sl_liq
    X["P2O5"] = y[ind["m_P2O5_sl_liq"]] / M["P2O5"] / n_sl_liq
    X["CaS"] = y[ind["m_CaS_sl_liq"]] / M["CaS"] / n_sl_liq
    X["FeO"] = y[ind["m_FeO_sl_liq"]] / M["FeO"]/ n_sl_liq

    X["CO"] = y[ind["m_CO"]] / M["CO"]/ n_gas
    X["CO2"] = y[ind["m_CO2"]] / M["CO2"]/ n_gas
    X["N2"] = y[ind["m_N2"]] / M["N2"]/ n_gas
    X["O2"] = y[ind["m_O2"]] / M["O2"]/ n_gas
    X["H2"] = y[ind["m_H2"]] / M["H2"]/ n_gas
    X["H2O"] = y[ind["m_H2O"]] / M["H2O"]/ n_gas
    X["CH4"] = y[ind["m_CH4"]] / M["CH4"]/ n_gas

    X["p_CO"] = X["CO"]*(c["p_ambient"]+y[ind["p_furnace"]])

    W["O"] = y[ind["m_O_st_liq"]] / y[ind["m_st_liq"]] * 100
    W["C"] = y[ind["m_C_st_liq"]] / y[ind["m_st_liq"]] * 100
    W["Si"] = y[ind["m_Si_st_liq"]] / y[ind["m_st_liq"]] * 100
    W["Mn"] = y[ind["m_Mn_st_liq"]] / y[ind["m_st_liq"]] * 100
    W["Cr"] = y[ind["m_Cr_st_liq"]] / y[ind["m_st_liq"]] * 100
    W["P"] = y[ind["m_P_st_liq"]] / y[ind["m_st_liq"]] * 100
    # calculation missing!!
    W["Al"] = 0
    W["S"] = y[ind["m_S_st_liq"]] / y[ind["m_st_liq"]] * 100
    W["Fe"] = y[ind["m_Fe_st_liq"]] / y[ind["m_st_liq"]] * 100
    
    m_reac = (y[ind["m_O_reac"]] + y[ind["m_C_reac"]] + y[ind["m_Si_reac"]]
            + y[ind["m_Mn_reac"]] + y[ind["m_Cr_reac"]] + y[ind["m_P_reac"]]
            +  y[ind["m_S_reac"]] + y[ind["m_Fe_reac"]])
    
    W["O_reac"] = y[ind["m_O_reac"]] / m_reac * 100
    W["C_reac"] = y[ind["m_C_reac"]] / m_reac * 100
    W["Si_reac"] = y[ind["m_Si_reac"]] / m_reac * 100
    W["Mn_reac"] = y[ind["m_Mn_reac"]] / m_reac * 100
    W["Cr_reac"] = y[ind["m_Cr_reac"]] / m_reac * 100
    W["P_reac"] = y[ind["m_P_reac"]] / m_reac * 100
    #Al_reac missing in y!!!!
    W["Al_reac"] = 0 / m_reac * 100   
    W["S_reac"] = y[ind["m_S_reac"]] / m_reac * 100
    W["Fe_reac"] = y[ind["m_Fe_reac"]] / m_reac * 100

    W["CaO"] = y[ind["m_CaO_sl_liq"]] / y[ind["m_sl_liq"]] * 100
    W["MgO"] = y[ind["m_MgO_sl_liq"]] / y[ind["m_sl_liq"]] * 100
    W["Al2O3"] = y[ind["m_Al2O3_sl_liq"]] / y[ind["m_sl_liq"]] * 100
    W["SiO2"] = y[ind["m_SiO2_sl_liq"]] / y[ind["m_sl_liq"]] * 100
    W["MnO"] = y[ind["m_MnO_sl_liq"]] / y[ind["m_sl_liq"]] * 100
    W["Cr2O3"] = y[ind["m_Cr2O3_sl_liq"]] / y[ind["m_sl_liq"]] * 100
    W["CaS"] = y[ind["m_CaS_sl_liq"]] / y[ind["m_sl_liq"]] * 100
    W["FeO"] = y[ind["m_FeO_sl_liq"]] / y[ind["m_sl_liq"]] * 100
    W["P2O5"] = y[ind["m_P2O5_sl_liq"]] / y[ind["m_sl_liq"]] * 100
    
    M["sl_liq"] = y[ind["m_sl_liq"]]/ n_sl_liq
    M["sl_sol"] = y[ind["m_sl_sol"]]/ n_sl_sol
    M["gas"]    = y[ind["m_gas"]]/ n_gas

    cp["gas"] = (cp["H2"] * X["H2"] + cp["N2"] * X["N2"] + cp["O2"] * X["O2"] + 
                 cp["CO"] * X["CO"] + cp["CO2"] * X["CO2"] + cp["CH4"] * X["CH4"] +
                 cp["H2O_gas"] * X["H2O"] )


    #return X,W

'''
def update_structarray(obj,fieldnames,new_values):
    # update structered array only before heat
    # dont do this regularly # costly operation!
    
    type_modified = []
    for name, typ in obj.dtype.fields.items():
        if name in fieldnames:
            type_modified.append((name,'f8',new_values[fieldnames.index(name)].shape))
        else:
            type_modified.append((name,typ[0].base.descr[0][1],typ[0].shape))

    new_obj = np.empty(1,dtype=type_modified)
    new_obj = new_obj[0]
    for name, typ in obj.dtype.fields.items():
        if name in fieldnames:
            new_obj[name] = new_values[fieldnames.index(name)]
        else:
            new_obj[name] = obj[name]

    return new_obj
'''

#def __init__(self):
#@njit(cache=True)
def create_InputData(charge_num,datasrc):

    #molar mass in [kg/mol]
    M = np.empty(1, dtype = from_dtype(np.dtype([('C', 'f8'), ('Fe', 'f8'), ('Mn', 'f8'), ('Si', 'f8'), ('P', 'f8'),
                           ('Cr', 'f8'), ('S', 'f8'), ('Al', 'f8'), ('O', 'f8'), ('FeO', 'f8'),
                           ('MnO', 'f8'), ('SiO2', 'f8'), ('P2O5', 'f8'), ('Cr2O3', 'f8'), ('Al2O3', 'f8'),
                           ('CaO', 'f8'), ('MgO', 'f8'), ('CaS', 'f8'), ('H2', 'f8'), ('N2', 'f8'), ('O2', 'f8'),
                           ('CO', 'f8'), ('CO2', 'f8'), ('CH4', 'f8'), ('C9H20', 'f8'), ('H2O', 'f8'), ('air', 'f8'),
                           ('sl_sol', 'f8'), ('sl_liq', 'f8'), ('gas', 'f8')], align=True)))
    M = M[0]

    M["C"] = 0.012011
    M["Fe"] = 0.055850
    M["Mn"] = 0.054938
    M["Si"] = 0.0280855
    M["P"] = 0.0309738
    M["Cr"] = 0.0519961
    M["S"] = 0.032065
    M["Al"] = 0.02698153
    M["O"] = 0.0159994
    #oxides
    M["FeO"] = 0.0718464
    M["MnO"] = 0.0709374
    M["SiO2"] = 0.0600843
    M["P2O5"] = 0.1419445
    M["Cr2O3"] = 0.1519904
    M["Al2O3"] = 0.10196
    M["CaO"] = 0.0560774
    M["MgO"] = 0.0403044
    M["CaS"] = 0.0721430
    #gases
    M["H2"] = 0.0020159
    M["N2"] = 0.0280134
    M["O2"] = 0.0319988
    M["CO"] = 0.0280104
    M["CO2"] = 0.0440098
    M["CH4"] = 0.0160428
    M["C9H20"] = 0.128258
    M["H2O"] = 0.0180153
    M["air"] = 0.02897
    #zones
    M["sl_sol"] = 0.0486
    M["sl_liq"] = 0.0486
    M["gas"] = 0.035
    

    # specific heat capacity in [kJ/(mol*K)]
    cp = np.empty(1, dtype = from_dtype(np.dtype([('C', 'f8'), ('Fe', 'f8'), ('Mn', 'f8'), ('Si', 'f8'), ('P', 'f8'),
                            ('Cr', 'f8'), ('S', 'f8'), ('Al', 'f8'), ('C9H20', 'f8'), ('FeO', 'f8'),
                            ('MnO', 'f8'), ('SiO2', 'f8'), ('P2O5', 'f8'), ('Cr2O3', 'f8'), ('Al2O3', 'f8'),
                            ('CaO', 'f8'), ('MgO', 'f8'), ('CaS', 'f8'), ('H2', 'f8'), ('N2', 'f8'), ('O2', 'f8'),
                            ('CO', 'f8'), ('CO2', 'f8'), ('CH4', 'f8'), ('H2O_gas', 'f8'), ('H2O_liq', 'f8'), ('st_sol', 'f8'),
                            ('st_liq', 'f8'), ('sl_sol', 'f8'), ('sl_liq', 'f8'), ('gas', 'f8'), ('roof', 'f8'), ('wall', 'f8')],align=True)))
    cp = cp[0]

    cp["C"] = 0.008517
    cp["Fe"] = 0.040
    cp["Mn"] = 0.0263
    cp["Si"] = 0.0197
    cp["P"] = 0.0212
    cp["Cr"] = 0.0233
    cp["S"] = 0.02359616
    cp["Al"] = 0.888
    cp["C9H20"] = 0.2833
    #oxides
    cp["FeO"] = 0.048
    cp["MnO"] = 0.0431
    cp["SiO2"] = 0.0448
    cp["P2O5"] = 0.2115
    cp["Cr2O3"] = 0.1097
    cp["Al2O3"] = 0.88
    cp["CaO"] = 0.058
    cp["MgO"] = 0.037
    cp["CaS"] = 0.057
    #gases
    cp["H2"] = 0.0290
    cp["N2"] = 0.0291
    cp["O2"] = 0.0294
    cp["CO"] = 0.0291
    cp["CO2"] = 0.0381
    cp["CH4"] = 0.035
    cp["H2O_gas"] = 0.0342
    cp["H2O_liq"] = 0.075
    # zones
    cp["st_sol"] = 0.039 #0.035 #0.039
    cp["st_liq"] = 0.047 #0.046 #0.047
    cp["sl_sol"] = 0.025
    cp["sl_liq"] = 0.047 #0.046 #0.047
    cp["gas"] = 0.03
    cp["roof"] = 0.95 #0.65
    cp["wall"] = 0.95

    
    # standard enthalpie of formation in [kJ/mol]
    # HINWEIS: Logar Part 2-Anhang --> S.44 making, shaping and treatment of steel - Turkdogan
    # Èinfluss BILDUNGSENTHALPIE in Lösung überprufen?
    dh = np.empty(1, dtype = from_dtype(np.dtype([('C', 'f8'), ('Fe', 'f8'), ('Mn', 'f8'), ('Si', 'f8'), ('P', 'f8'),
                            ('Cr', 'f8'), ('S', 'f8'), ('Al', 'f8'), ('FeO', 'f8'), ('SiO2_solid', 'f8'),
                            ('MnO', 'f8'), ('SiO2', 'f8'), ('P2O5', 'f8'), ('Cr2O3', 'f8'), ('Al2O3', 'f8'),
                            ('CaO', 'f8'), ('CaS', 'f8'), ('CO', 'f8'), ('CO2', 'f8'), ('CH4', 'f8'), 
                            ('C9H20', 'f8'), ('H2O', 'f8'), ('H2O_100', 'f8')],align=True)))
    dh = dh[0]

    dh["C"] = -27 
    dh["Fe"] = 0 
    dh["Mn"] = -20 
    dh["Si"] = -132 
    dh["P"] = -29 
    dh["Cr"] = -42 
    dh["S"] = 0 
    dh["Al"] = 0 
    #oxides
    dh["FeO"] = -243 
    dh["MnO"] = -385 
    dh["SiO2"] = -946 
    dh["P2O5"] = -2940 
    dh["Cr2O3"] = -1142 
    dh["SiO2_solid"] = -45 
    dh["CaO"] = -635.09 
    dh["CaS"] = -482.4 
    dh["Al2O3"] = -1675.3 
    #gases
    dh["CO"] = -117 
    dh["CO2"] = -396 
    dh["CH4"] = -91 
    dh["C9H20"] = -280 
    dh["H2O"] = -247 
    #enthalpy of evaporation of water in [MJ/kg]
    dh["H2O_100"] = 2.256 

    
    p = np.empty(1, dtype=from_dtype(np.dtype([('h_EAF_up', 'f8'), ('h_wall', 'f8'), ('h_wall_unc', 'f8'), ('r_EAF_low', 'f8'), ('r_EAF_up', 'f8'),
                            ('h_arc_min', 'f8'), ('h_el_tip', 'f8'), ('r_hole_init', 'f8'), ('r_el', 'f8'), ('n_el', 'f8'),
                            ('r_heart', 'f8'), ('m_water_el', 'f8'), ('h_el_cool', 'f8'), ('lambda_roof', 'f8'), ('lambda_wall', 'f8'),
                            ('pipe_thick', 'f8'), ('slag_thick', 'f8'), ('h_burner', 'f8'), ('l_flame', 'f8'), ('rho_roof', 'f8'), 
                            ('rho_wall', 'f8'), ('d_roof', 'f8'), ('d_wall', 'f8'),('d_refrac', 'f8'), ('T_vessel_out', 'f8'),
                            ('rho_st_liq', 'f8'),('rho_st_sol', 'f8'),('rho_el', 'f8'),('StartingTimeDelay', 'f8'),('roof_opening_time', 'f8'),
                            ('h_EAF_low', 'f8'),('r_el_rep', 'f8'), ('V_EAF', 'f8'), ('KS', 'f8'), ('Vg', 'f8'), ('m_roof', 'f8'), ('m_wall', 'f8'),
                            ('m_wall_unc', 'f8'),('A_vessel_side', 'f8'),('A_vessel_bottom', 'f8'),('A_vessel_out', 'f8'), ('rho_sl_liq', 'f8'),
                            ('h_melt', 'f8'),('h_scrap', 'f8'),('h_hole', 'f8'),('r_hole', 'f8'), ('h_wall_unc_exp', 'f8'), ('h_arc', 'f8'),
                            ('h_el', 'f8'),('r_arc', 'f8'),('el_tip_ratio', 'f8'),('m_scrap_init', 'f8'), ('rho_scrap_init', 'f8'), ('V_scrap_init', 'f8'),
                            ('l_scrap_flame', 'f8'),('V_gas', 'f8'),('EAF_scrap_fill_level', 'f8'),
                            ('geo_fact', 'f8', (2,)), ('m_hot_heel', 'f8'), ('m_tap', 'f8')],align=True)))
    p = p[0]

    p["h_EAF_up"] = 0 #3.2
    p["h_wall"] = 0 #0.15
    p["h_wall_unc"] = 0 #0.15
    p["r_EAF_low"] = 0 #2.5
    p["r_EAF_up"] = 0 #3.65
    p["h_arc_min"] = 0 #0.1
    p["h_el_tip"] = 0 #1.5
    p["r_hole_init"] = 0 #0.75
    p["r_el"] = 0 #0.35
    p["n_el"] = 0 #1
    p["r_heart"] = 0 #1
    p["m_water_el"] = 0 #0.5
    p["h_el_cool"] = 0 #0.4
    p["lambda_roof"] = 0 #35
    p["lambda_wall"] = 0 #55
    p["pipe_thick"] = 0 #0.03
    p["slag_thick"] = 0 #0.03
    p["h_burner"] = 0 #0.5
    p["l_flame"] = 0 #1
    p["rho_roof"] = 0 #8500
    p["rho_wall"] = 0 #7500
    p["d_roof"] = 0 #0.15
    p["d_wall"] = 0 #0.15
    p["d_refrac"] = 0 #0.95
    p["T_vessel_out"] = 0 #580
    p["rho_st_liq"] = 0 #7850
    p["rho_st_sol"] = 0 #500
    p["rho_el"] = 0 #2100
    p["StartingTimeDelay"] = 0 #5
    p["roof_opening_time"] = 0 #20
    p["h_EAF_low"] = 0 # = r_EAF_up - r_EAF_low
    p["r_el_rep"] = 0 # = np.sqrt(n_el) * r_el
    p["V_EAF"] = 0 # = (np.pi * (r_EAF_up - r_EAF_low) / 3 * (r_EAF_up ** 2
        #    + r_EAF_up * r_EAF_low + r_EAF_low ** 2) + np.pi * r_EAF_up ** 2
        #    * h_EAF_up)
    p["KS"] = 0
    p["Vg"] = 0
    p["m_roof"] = 0 # = np.pi * (r_EAF_up ** 2 - r_heart ** 2) * d_roof * rho_roof
    p["m_wall"] = 0 # = (np.pi * 2 * r_EAF_up * (h_EAF_up - h_wall_unc) * d_wall * rho_wall)
    p["m_wall_unc"] = 0 # = ((np.pi * 2 * r_EAF_up * h_wall_unc + np.pi 
        #         * (r_heart ** 2 - r_el_rep ** 2)) * d_wall * rho_wall)
    p["A_vessel_side"] = 0 # = ((2 **0.5) * (r_EAF_up - r_EAF_low) * np.pi 
        #             *(r_EAF_low + r_EAF_up + (2 ** 0.5) * d_refrac) * 1.01)
    p["A_vessel_bottom"] = 0 # = np.pi * r_EAF_low ** 2
    p["A_vessel_out"] = 0 # = (np.pi * (r_EAF_up ** 2 + (h_EAF_low + d_refrac) ** 2) * 1.5)

    p["rho_sl_liq"] = 0 
    p["h_melt"] = 0 
    p["h_scrap"] = 0 
    p["h_hole"] = 0 
    p["r_hole"] = 0 
    p["h_wall_unc_exp"] = 0 
    p["h_arc"] = 0 
    p["h_el"] = 0 
    p["r_arc"] = 0 
    p["el_tip_ratio"] = 0 
    p["m_scrap_init"] = 0 
    p["rho_scrap_init"] = 0 
    p["V_scrap_init"] = 0 
    p["l_scrap_flame"] = 0 
    p["V_gas"] = 0 
    p["EAF_scrap_fill_level"] = 0 
    p["geo_fact"] = np.zeros(2)  # = [3, 1.5]
    p["m_hot_heel"] = 0    #60000
    p["m_tap"] = 0
    #"h_el_sp": np.float64(4.75)

    #"m_tap": np.float64(145000) #?


    
    m_geo_len = 150
    c = np.empty(1, dtype=from_dtype(np.dtype([('melt_WIP', 'f8', (8,8)), ('melt_UIP', 'f8', (8,8)), ('melt_interaction_T', 'f8', (8,8)), ('melt_interaction_T_UIP', 'f8', (8,8)),
                           ('ln_gamma0', 'f8', (8,)), ('ln_gamma0_T', 'f8', (8,)), ('slag_interaction', 'f8', (8,8)), ('slag_interaction2', 'f8', (8,64)),
                           ('sigma_sb', 'f8'), ('p_ambient', 'f8'), ('p_standard', 'f8'), ('R_gas', 'f8'), ('T_ambient', 'f8'), ('T_ref', 'f8'), ('T_standard', 'f8'),
                           ('T_undercool', 'f8'), ('T_melt_sl', 'f8'), ('T_melt_st', 'f8'), ('cathode_anode_fall', 'f8'), ('field_strength_gas', 'f8'), 
                           ('field_strength_slag', 'f8'), ('arc_conductivity', 'f8'), ('lambda_st', 'f8'), ('lambda_sl', 'f8'), ('lambda_C', 'f8'), 
                           ('g', 'f8'), ('Hu_CH4', 'f8'), ('Hu_H2', 'f8'), ('melt_model', 'i8'), ('slag_model', 'i8'), ('equilibrium_constants', 'f8', (2,8)),
                           ('slag_correction', 'f8', (2,8)), ('equilibrium_factor_RS', 'f8', (8,)), ('equilibrium_factor_cell', 'f8', (8,)), ('ZM_stoech', 'f8', (6,)),
                           ('GGWCoeff', 'f8', (10,45)), ('Ep1_CH4', 'f8', (18,6)), ('Ep1_CO', 'f8', (18,5)), ('temp2_CH4', 'f8', (6,)), ('ps2_CH4', 'f8', (18,)),
                           ('temp2_CO', 'f8', (5,)), ('ps2_CO', 'f8', (18,)), ('Ep', 'f8', (8,)), ('Gas_T', 'f8', (45,)),
                           ('GaMtr_interp0', 'f8', (3,44)), ('GaMtr_interp1', 'f8', (3,44)),('GaMtr_interp2', 'f8', (3,44)),('GaMtr_interp3', 'f8', (3,44)),
                           ('m_geo_interp0', 'f8', (m_geo_len,5)), ('m_geo_interp1', 'f8', (m_geo_len,5)), ('m_geo_interp2', 'f8', (m_geo_len,5)), ('m_geo_interp3', 'f8', (m_geo_len,5)),
                           ('Geo_x1', 'f8', (m_geo_len,)), ('Geo_x6', 'f8', (m_geo_len,)), ('Geo_last_elem', 'i8'), ('ap', 'f8'), 
                           ('NN_slag_act', 'f8', (1287,)), ('NN_x_min_max_slag', 'f8', (2,8)),('NN_steel_act', 'f8', (1448,)), ('NN_x_min_max_steel', 'f8', (2,9)), ('NN_y_min_max_steel', 'f8', (2,8))], align=True)))
    c = c[0]

    c["melt_WIP"] = np.zeros((8,8))
    c["melt_UIP"] = np.zeros((8,8))
    c["melt_interaction_T"] = np.zeros((8,8))
    c["melt_interaction_T_UIP"] = np.zeros((8,8))

    #interaction matrix and infinite dilution activity coefficients for
    #unified interaction parameter formalism (UIP), reference state mole fraction of 1
    #coefficients at infinite dilution

    #C    Si    Mn      Cr     P     AL      S       O
    c["ln_gamma0"] = np.array([-0.619896,0,0.364643,0,0,1.2,0,3.5])
    c["ln_gamma0_T"] = np.array([0,-11763.2,0,0,0,-7596.7, 0,-15280])

    # regular solution model (parameters and algorithm by Ban-Ya, Cr2O2 parameters Xiao)
    # several parameters for Cr2O3 interaction unknown, significant error in
    # Cr2O3 acitivty expected and observed in comparison with other models
    c["slag_interaction"] = np.zeros((8,8))
    # construct matrix for Xk*Xj* (aij + aik +akj)
    c["slag_interaction2"] = np.zeros((8,64))

    c["sigma_sb"] = 5.67e-8
    c["p_ambient"] = 1e5
    c["p_standard"] = 101325
    c["R_gas"] = 8.314   # Gas constant [J/(mol K)],
    c["T_ambient"] = 298
    c["T_ref"] = 298
    c["T_standard"] = 273.15
    c["T_undercool"] = 50#
    c["T_melt_sl"] = 1700#
    c["T_melt_st"] = 1809#
    c["cathode_anode_fall"] = 20
    c["field_strength_gas"] = 0.8
    c["field_strength_slag"] = 0.45
    c["arc_conductivity"] = 5e3
    c["lambda_st"] = 15.4
    c["lambda_sl"] = 12.66
    c["lambda_C"] = 117
    c["g"] = 9.81
    c["Hu_CH4"] = 40 # Heizwert in [MJ/kg]
    c["Hu_H2"] = 119.972 # [MJ/kg]
    
    
    c["melt_model"] = 0
    c["slag_model"] = 1
    
    #equilibrium constants from Conejo 2007 and Graham 2008, Chromium from Ding 2000
    c["equilibrium_constants"] = np.zeros((2,8))
    #  Ca2    Mg2  Al3  Si4    Mn2   Cr3     P5     Fe2 
    c["slag_correction"] = np.zeros((2,8))
    # P      Si       Cr       Al        Fe        Mn      S       C
    # for RS model correction of equilibrium; based on FactSage comparison
    c["equilibrium_factor_RS"] = np.array([1, 10, 20, 0.01, 0.0175, 75, 150, 1])
    # for cell model correction of equilibrium; based on FactSage comparison
    c["equilibrium_factor_cell"] = np.array([0.5, 10, 0.1, 0.01, 0.015, 0.6, 150, 1])
    # for burner_reactions
    c["ZM_stoech"] = np.array([0.200433780940625, 0.250508268140722, 0.333935589156065, 0.267145802180089, 0, 0.111899330013933])
    c["GGWCoeff"] = np.zeros((10,45))

    c["Ep1_CH4"] = np.array([[0,	0,	0,	0,	0,	0],
                            [0,	0.0170,	0.0250,	0.0250,	0.0210,	0.0160],
                            [0,	0.0200,	0.0290,	0.0280,	0.0240,	0.0180],
                            [0,	0.0220,	0.0340,	0.0330,	0.0270,	0.0210],
                            [0,	0.0270,	0.0400,	0.0390,	0.0330,	0.0260],
                            [0,	0.0330,	0.0490,	0.0480,	0.0400,	0.0310],
                            [0,	0.0380,	0.0570,	0.0550,	0.0470,	0.0360],
                            [0,	0.0430,	0.0630,	0.0620,	0.0520,	0.0400],
                            [0,	0.0470,	0.0690,	0.0680,	0.0570,	0.0450],
                            [0,	0.0530,	0.0790,	0.0780,	0.0640,	0.0510],
                            [0,	0.0640,	0.1030,	0.1010,	0.0850,	0.0660],
                            [0,	0.0750,	0.1230,	0.1200,	0.1010,	0.0770],
                            [0,	0.0870,	0.1440,	0.1420,	0.1200,	0.0930],
                            [0,	0.0990,	0.1620,	0.1620,	0.1360,	0.1030],
                            [0,	0.1080,	0.1760,	0.1760,	0.1490,	0.1140],
                            [0,	0.1100,	0.1900,	0.1880,	0.1580,	0.1210],
                            [0,	0.1170,	0.2090,	0.2140,	0.1800,	0.1390],
                            [0,	0.1250,	0.2280,	0.2330,	0.1960,	0.1520]])
    
    c["Ep1_CO"] = np.array([[0,	0,	0,	0,	0],
                        [0,	0.0055,	0.0095,	0.0120,	0.0110],
                        [0,	0.0076,	0.0135,	0.0165,	0.0153],
                        [0,	0.0111,	0.0180,	0.0230,	0.0220],
                        [0,	0.0140,	0.0230,	0.0292,	0.0270],
                        [0,	0.0155,	0.0270,	0.0330,	0.0310],
                        [0,	0.0180,	0.0330,	0.0380,	0.0360],
                        [0,	0.0215,	0.0375,	0.0440,	0.0390],
                        [0,	0.0250,	0.0430,	0.0500,	0.0430],
                        [0,	0.0310,	0.0515,	0.0570,	0.0500],
                        [0,	0.0400,	0.0630,	0.0700,	0.0570],
                        [0,	0.0450,	0.0720,	0.0760,	0.0620],
                        [0,	0.0520,	0.0800,	0.0860,	0.0700],
                        [0,	0.0600,	0.0960,	0.1000,	0.0800],
                        [0,	0.0730,	0.1150,	0.1180,	0.0920],
                        [0,	0.0900,	0.1350,	0.1350,	0.1080],
                        [0,	0.1130,	0.1550,	0.1550,	0.1300],
                        [0,	0.1400,	0.1800,	0.1800,	0.1500]])

    c["temp2_CH4"] = np.array([300,	400,	800,	1200,	1600, 2000])
    c["ps2_CH4"] = np.array([0,	0.0040,	0.0050,	0.0070,	0.0100,	0.0150,	0.0200,	0.0250,	0.0300,	0.0400,	0.0700,	0.1000,	0.1500,	0.2000,	0.2500,	0.3000,	0.4500,	0.6000])
    c["temp2_CO"] = np.array([900,	1000,	1400, 1800,	2200])
    c["ps2_CO"] = np.array([0,	0.0040,	0.0060,	0.0100,	0.0150,	0.0200,	0.0300,	0.0400,	0.0600,	0.1000,	0.2000,	0.3000,	0.5000,	1,	2,	4,	8,	16])
                    
    c["Ep"] = np.array([0.375, 0.4, 0.8, 0.35, 1.0, 0.85, 0.3, 0.85])

    #"GaMtr_interp_dict"] = np.NaN,
    #"GaMtr2_interp_dict"] = np.NaN,
    #"m_geo_interp_dict"] = np.NaN,
    #c["m_geo_interp_array"] = np.zeros((4,8))
    
    c["Geo_x1"] = np.zeros((m_geo_len))
    c["Geo_x6"] = np.zeros((m_geo_len))
    
    c["ap"] = 100000

    #"T_tap_min"] = 1900,
    

    
    # adjust values of self.c
                                #Ca2     Mg2   Al3  Si4     Mn2     Cr3    P5     Fe2 
    c["slag_correction"][0] = [-40880, -23300,  0, 17450, -86860, 74967, 52720, -8540]
    c["slag_correction"][1] = [-4.703, 1.833,   0, 2.820, 51.465, -12.6, -230.706, 7.142]

    c["GGWCoeff"][9] = [298,348,398,448,498,548,598,648,698,748,798,848,898,948,998,
                                1048,1098,1148,1198,1248,1298,1348,1398,1448,1498,1548,1598,
                                1648,1698,1748,1798,1848,1898,1948,1998,2048,2098,2148,2198,2248,2298,2348,2398,2448,2498]
    c["GGWCoeff"][1] = [11.5560275566732,9.17500650593625,7.40361110015688,6.03910305334449,
                                4.95933166027017,4.08631126855819,3.36793113275760,2.76806289680593,
                                2.26088405132752,1.82745934737703,1.45359742529879,1.12846035315697,
                                0.843634803665926,0.592495636245145,0.369760010073108,0.171168303955177,
                                -0.00675158886893613,-0.166855809846797,-0.311513259474617,-0.442703443134819,
                                -0.562091955628898,-0.671089244471493,-0.770896789060332,-0.862543709032347,
                                -0.946916022055283,-1.02478020462461,-1.09680229978103,-1.16356351629469,
                                -1.22557304285091,-1.28327863605092,-1.33707541716842,-1.38731321866256,
                                -1.43430274963813,-1.47832079413968,-1.51961461327058,-1.55840568863204,
                                -1.59489291825591,-1.62925535539376,-1.66165456397838,-1.69223665134315,
                                -1.72113402815221,-1.74846693690597,-1.77434478341720,-1.79886729997016,-1.82212556422349]
    c["GGWCoeff"][2] = [-48.4572433644727,-38.4416729834684,-30.9228864012317,-25.0726707349354,
                                -20.3932602277284,-16.5672017111139,-13.3823413445270,-10.6914401118731,
                                -8.38908367786957,-6.39780607399480,-4.65940243819831,-3.12929575115335,
                                -1.77277209337241,-0.562398031820786,0.523792112829150,1.50359318383141,
                                2.39156951017859,3.19975567238981,3.93818262363078,4.61527688803780,
                                5.23816816187269,5.81292887413615,6.34476262352977,6.83815376951304,
                                7.29698719929136,7.72464497645414,8.12408490870384,8.49790485646843,
                                8.84839570875007,9.17758528631707,9.48727493193205,9.77907016811848,
                                10.0544065132354,10.3145713235185,10.5607223556609,10.7939035861063,
                                11.0150589206874,11.2250437049004,11.4246349237458,11.6145399180342,
                                11.7954039856176,11.9678170099544,12.1323192580358,12.2894064664982,12.4395343157248]
    c["GGWCoeff"][3] = [-36.9012158077996,-29.2666664775322,-23.5192753010748,-19.0335676815909,
                                -15.4339285674582,-12.4808904425557,-10.0144102117694,-7.92337721506713,
                                -6.12819962654205,-4.57034672661777,-3.20580501289952,-2.00083539799638,
                                -0.929137289706484,0.0300976044243587,0.893552122902258,1.67476148778658,
                                2.38481792130966,3.03289986254302,3.62666936415616,4.17257344490298,
                                4.67607620624380,5.14183962966465,5.57386583446944,5.97561006048069,
                                6.35007117723608,6.69986477182952,7.02728260892283,7.33434134017374,
                                7.62282266589916,7.89430665026614,8.15019951476364,8.39175694945593,
                                8.62010376359729,8.83625052937878,9.04110774239032,9.23549789747425,
                                9.42016600243151,9.59578834950668,9.76298035976744,9.92230326669110,
                                10.0742699574654,10.2193500730484,10.3579744746186,10.4905391665280,10.6174087515014]
    
    
    for ij in range(0,3):                                                            
        pcobj = PchipInterpolator(c["GGWCoeff"][9],c["GGWCoeff"][ij+1])
        c["GaMtr_interp0"][ij,:] = pcobj.c[0]
        c["GaMtr_interp1"][ij,:] = pcobj.c[1]
        c["GaMtr_interp2"][ij,:] = pcobj.c[2]
        c["GaMtr_interp3"][ij,:] = pcobj.c[3] 
    
    c["m_geo_interp0"] = np.zeros((m_geo_len,5))
    c["m_geo_interp1"] = np.zeros((m_geo_len,5))
    c["m_geo_interp2"] = np.zeros((m_geo_len,5))
    c["m_geo_interp3"] = np.zeros((m_geo_len,5))
    
    c["Geo_x1"] = np.zeros(m_geo_len)
    c["Geo_x6"] = np.zeros(m_geo_len)
    
    c["Gas_T"] = c["GGWCoeff"][9]   


    #                               P          Si            Cr          Al          Fe            Mn            S             C
    c["equilibrium_constants"][0] = [-19.5924,   -29.23,      -21.7808,    -23.7695,     -6.2999,    -13.12,        2.902,      4.77]
    c["equilibrium_constants"][1] = [3925.45,   71509.8084,  49678.844,  74757.96519,  14553.757,   29494.945,  -12228.93,    2694.21]
    
    # interaction parameters for melt activities (parameters from Sigworth and Elliot)
    # several parameters unknown and set to zero. For low concentrations that
    # are to be expected values close to one (ideal solution behavior) and
    # negligible errors from omitted parameters are expected

    # interaction matrix for Wagner Interaction Parameter Formalism (WIPF)
    # reference state is 1 % weight
    
    #  1 C        2  Si    3  Mn     4  Cr      5  P     6  Al   7  S        8 O
    c["melt_WIP"][0] = [0.0581,   -0.008,   -0.012,   -0.024,    0.051,   0.043,   0.046,    -0.34]
    c["melt_WIP"][1] = [-0.023,    0.089,    0.002,   -0.0003,   0.11,    0.058,   0.056,    -0.23]
    c["melt_WIP"][2] = [-0.07,     0,        0,        0.0042,  -0.0035,  0,      -0.048,    -0.083]
    c["melt_WIP"][3] = [-0.12,    -0.0043,   0.004,   -0.003,   -0.053,   0,       0.062,    -0.14]
    c["melt_WIP"][4] = [0.13,      0.12,     0,       -0.03,     0.062,   0,       0.028,     0.13]
    c["melt_WIP"][5] = [0.091,     0.0056,    0,       0.012,    0,       0.011,   0.03,      11.95]
    c["melt_WIP"][6] = [0.11,      0.063,   -0.026,    0.0396,   0.29,    0.035,  -0.153,    -0.27]
    c["melt_WIP"][7] = [-0.45,    -0.131,   -0.021,   -0.04,     0.07,    7.15,   -0.133,     0.734]

    c["melt_interaction_T"][0,0]=158    # C-C
    c["melt_interaction_T"][1,0]=380    # Si-C
    c["melt_interaction_T"][0,1]=162    # C-Si
    c["melt_interaction_T"][1,1]=34.5   # Si-Si
    c["melt_interaction_T"][5,5]=63     # Al-Al
    c["melt_interaction_T"][7,5]=-20600 # O-Al
    c["melt_interaction_T"][6,3]=-94.2  # S-Cr
    c["melt_interaction_T"][3,6]=-153   # Cr-S
    c["melt_interaction_T"][6,6]=233    # S-S
    c["melt_interaction_T"][5,7]=-34740 # Al-O
    c["melt_interaction_T"][7,7]=-1750  # O-O
    
    #conversion of parameters according to Elliot
    c["melt_UIP"] =  c["melt_WIP"] #/0.245*1e3
    c["melt_interaction_T_UIP"] = c["melt_interaction_T"]/0.245*1e3

    #                               Ca2        Mg2       Al3        Si4       Mn2      Cr3       P5       Fe2 
    c["slag_interaction"][0] = [       0,  -100420,   -154810,   -133890,   -92050,   44235,  -251040,  -31380]; # Ca2
    c["slag_interaction"][1] = [ -100420,        0,    -71130,    -66940,    61920,   28085,   -37660,   33470]; # Mg2
    c["slag_interaction"][2] = [ -154810,   -71130,         0,   -127610,   -83680,  -46210,  -261500,  -41000]; # Al3   
    c["slag_interaction"][3] = [ -133890,   -66940,   -127610,         0,   -75310,  -48975,    83680,  -41840]; # Si4
    c["slag_interaction"][4] = [  -92050,    61920,    -83680,    -75310,        0,       0,   -84940,    7110]; # Mn2   MN-CR3 Unknown!!
    c["slag_interaction"][5] = [   44235,    28085,    -46210,    -48975,        0,       0,        0,       0]; # Cr3   CR-MN,P,FE Unknown!!
    c["slag_interaction"][6] = [ -251040,   -37660,   -261500,     83680,   -84940,       0,        0,  -31380]; # P5    P5-CR3 Unknown!!
    c["slag_interaction"][7] = [  -31380,    33470,    -41000,    -41840,     7110,       0,   -31380,       0]; #Fe2    FE2Cr3 Unknown!!

    n_species = c["slag_interaction"].shape[0]
    for i in range(1,n_species+1):
        for j in range(1,np.power(n_species,2)+1):
            c["slag_interaction2"][i-1,j-1] = (c["slag_interaction"][i-1,int(np.ceil(j/n_species))-1] + c["slag_interaction"][i-1,j-(int(np.ceil(j/n_species))-1)*n_species-1]
                                    -c["slag_interaction"][int(np.ceil(j/n_species))-1,j-(int(np.ceil(j/n_species))-1)*n_species-1] )
            
            
            if i==np.ceil(j/n_species) or i==j-(np.ceil(j/n_species)-1)*n_species or j-(np.ceil(j/n_species)-1)*n_species<= np.ceil(j/n_species):
                c["slag_interaction2"][i-1,j-1]=0

    #initialize NN weights
    c["NN_x_min_max_slag"] = np.zeros((2,8))
    c["NN_slag_act"] = np.zeros(1287)
    c["NN_x_min_max_steel"] = np.zeros((2,9))
    c["NN_y_min_max_steel"] = np.zeros((2,8))
    c["NN_steel_act"] = np.zeros(1448)
    with h5py.File("data/activity_model_parameter.h5", "r") as hdf_in:
        c["NN_x_min_max_slag"]  = np.array(hdf_in.get(f'NN_slag_x_min_max'))
        c["NN_slag_act"]  = np.array(hdf_in.get(f'NN_slag_parameter'))
        c["NN_x_min_max_steel"]  = np.array(hdf_in.get(f'NN_steel_x_min_max'))
        c["NN_y_min_max_steel"]  = np.array(hdf_in.get(f'NN_steel_y_min_max'))
        c["NN_steel_act"]  = np.array(hdf_in.get(f'NN_steel_parameter'))
    
    calculate_UIP_parameters(c,M)
   
    


    Q = np.empty(1, dtype=from_dtype(np.dtype([('el_cooling_heat', 'f8'), ('rad', 'f8',(8,)), ('arc', 'f8'),  ('arc_gas', 'f8'), ('arc_rad', 'f8'),
                           ('arc_diss', 'f8'),('gas_rad', 'f8'), ('balance', 'f8'), ('roof_rad', 'f8'),
                           ('wall_rad', 'f8'), ('st_sol_rad', 'f8'), ('st_liq_rad', 'f8'), ('arc_rad_test', 'f8'), ('el_shaft_rad', 'f8'),
                           ('wall_unc_rad', 'f8'), ('el_tip_rad', 'f8'), ('el_rad', 'f8'), ('gas_chem', 'f8'), ('burn_st_sol', 'f8'),
                           ('burn_gas', 'f8'), ('CO_post', 'f8'), ('st_liq_chem', 'f8'), ('sl_liq_chem', 'f8'), ('st_sol_chem', 'f8'),
                           ('st_sol_st_liq', 'f8'), ('st_sol_sl_sol', 'f8'), ('st_sol_sl_liq', 'f8'), ('st_sol_gas', 'f8'), ('st_sol_wall', 'f8'), ('st_sol_wall_unc', 'f8'),
                           ('st_liq_sl_sol', 'f8'), ('st_liq_sl_liq', 'f8'), ('st_liq_gas', 'f8'), ('st_liq_vessel', 'f8'), ('sl_liq_vessel', 'f8'), ('sl_liq_sl_sol', 'f8'),
                           ('sl_liq_gas', 'f8'), ('sl_sol_vessel', 'f8'), ('gas_roof', 'f8'), ('gas_wall', 'f8'), ('gas_wall_unc', 'f8'), ('roof_water', 'f8'), ('wall_water', 'f8'),
                           ('el_cooling', 'f8'), ('el_gas', 'f8'), ('wall_unc_air', 'f8'), ('wall_unc_st_liq', 'f8'), ('roof', 'f8'), ('wall', 'f8'), ('el', 'f8'), ('st_sol', 'f8'),
                           ('st_liq', 'f8'), ('sl_sol', 'f8'), ('sl_liq', 'f8'), ('gas', 'f8'), ('water', 'f8'), ('vessel', 'f8'), ('wall_unc', 'f8'), ('resolid_st', 'f8'),
                           ('dri', 'f8'), ('aluminium', 'f8')],align=True)))
    Q = Q[0]

    Q["el_cooling_heat"] = 0 
    Q["rad"] = np.zeros(8)

    Q["arc"] = 0 
    Q["arc_gas"] = 0 
    Q["arc_rad"] = 0 
    Q["arc_diss"] = 0 

    Q["gas_rad"] = 0 
    Q["balance"] = 0 
    Q["roof_rad"] = 0 
    Q["wall_rad"] = 0 
    Q["st_sol_rad"] = 0 
    Q["st_liq_rad"] = 0 
    Q["arc_rad_test"] = 0 
    Q["el_shaft_rad"] = 0 
    Q["wall_unc_rad"] = 0 
    Q["el_tip_rad"] = 0 
    Q["el_rad"] = 0 
    Q["gas_chem"] = 0 
    Q["burn_st_sol"] = 0 
    Q["burn_gas"] = 0 
    Q["CO_post"] = 0 
    Q["st_liq_chem"] = 0 
    Q["sl_liq_chem"] = 0 
    Q["st_sol_chem"] = 0 


    Q["st_sol_st_liq"] = 0
    Q["st_sol_sl_sol"] = 0
    Q["st_sol_sl_liq"] = 0
    Q["st_sol_gas"] = 0
    Q["st_sol_wall"] = 0
    Q["st_sol_wall_unc"] = 0

    Q["st_liq_sl_sol"] = 0
    Q["st_liq_sl_liq"] = 0
    Q["st_liq_gas"] = 0
    Q["st_liq_vessel"] = 0
    Q["sl_liq_vessel"] = 0
    Q["sl_liq_sl_sol"] = 0
    Q["sl_liq_gas"] = 0
    Q["sl_sol_vessel"] = 0

    Q["gas_roof"] = 0
    Q["gas_wall"] = 0
    Q["gas_wall_unc"] = 0
    Q["roof_water"] = 0
    Q["wall_water"] = 0

    Q["el_cooling"] = 0
    Q["el_gas"] = 0
    Q["wall_unc_air"] = 0

    Q["wall_unc_st_liq"] = 0
    Q["roof"] = 0
    Q["wall"] = 0
    Q["el"] = 0
    Q["st_sol"] = 0
    Q["st_liq"] = 0 
    Q["sl_sol"] = 0
    Q["sl_liq"] = 0
    Q["gas"] = 0
    Q["water"] = 0
    Q["vessel"] = 0
    Q["wall_unc"] = 0
    Q["resolid_st"] = 0
    
    Q["dri"] = 0

    Q["aluminium"] = 0 

    
    matrix_len = 2000
    field_num = 96
    basket_num = 5
    comp_len = 35
    OpCh = np.empty(1, dtype=from_dtype(np.dtype([('data', '<U5'), ('charge_num', 'i8'), ('current_t', 'f8'), ('low_power_cutofftime', 'f8'), ('P_arc', 'f8'), ('m_O2_lance', 'f8'), ('m_O2_post', 'f8'),
                              ('m_C_inj', 'f8'), ('m_offgas', 'f8'), ('m_leakair', 'f8'), ('m_slag_form', 'f8'), ('m_CH4_burn', 'f8'), ('m_O2_CH4_burn', 'f8'),
                              ('m_H2_burn', 'f8'), ('m_O2_H2_burn', 'f8'), ('m_water_roof', 'f8'), ('m_water_wall', 'f8'), ('U_arc', 'f8'),
                              ('I_arc', 'f8'), ('m_water_roof_roc', 'f8'), ('m_water_wall_roc', 'f8'), ('T_water_roof_roc', 'f8'), ('T_water_wall_roc', 'f8'),
                              ('m_conv_gas', 'f8'), ('m_dri', 'f8'), ('T_dri', 'f8'), ('m_dust', 'f8'), ('basket_number', 'i8'), ('basket_number_diff', 'i8'), ('basket_number_next', 'i8'), ('t_basket', 'f8'), 
                              ('m_dolomite', 'f8'), ('m_chalk', 'f8'), ('m_coal', 'f8',(basket_num,)), ('m_slag', 'f8',(basket_num,)), ('m_st_sol_basket', 'f8',(basket_num,)), ('m_tap_data', 'f8'),
                              ('T_tap_last', 'f8'), ('t_end', 'f8',(basket_num,)), ('t_start', 'f8'), ('m_tap_slag', 'f8'), ('hotheel_comp', 'f8', (comp_len)), ('OpCh_data', 'f8', (matrix_len,field_num)), ('mat_t', 'f8',(matrix_len,)), 
                              ('OpCh_interp0', 'f8',(matrix_len,field_num)), ('OpCh_interp1', 'f8',(matrix_len,field_num)), ('OpCh_interp2', 'f8',(matrix_len,field_num)), ('OpCh_interp3', 'f8',(matrix_len,field_num)),
                              ('T_water_roof_out', 'f8',(matrix_len,)), ('T_water_wall_out', 'f8',(matrix_len,)), ('m_offgas_CO', 'f8',(matrix_len,)), ('m_offgas_CO2', 'f8',(matrix_len,)), ('m_offgas_N2', 'f8',(matrix_len,)), 
                              ('m_offgas_O2', 'f8',(matrix_len,)), ('m_offgas_H2', 'f8',(matrix_len,)), ('m_offgas_H2O', 'f8',(matrix_len,)),
                              ('T_roof_init', 'f8',(basket_num,)),('T_wall_init', 'f8',(basket_num,)),('T_roof_H2O_init', 'f8',(basket_num,)),('T_wall_H2O_init', 'f8',(basket_num,)),
                              ('m_offgas_CH4', 'f8',(matrix_len,)), ('scrap_comp', 'f8' ,(8,)), ('OpCh_last_elem', 'i8'),
                              ('mass_scrap', 'f8',(basket_num,comp_len)),('mass_coal', 'f8',(basket_num,comp_len)),('mass_C_inj', 'f8',(comp_len,)),('mass_dri_inj', 'f8',(comp_len,)),('mass_slagformer_inj', 'f8',(comp_len,))],align=True)))
                              #('scrap_H2O', 'f8',(basket_num,)), ('scrap_Combustables', 'f8',(basket_num,)), 
                              #('scrap_O', 'f8',(basket_num,)), ('scrap_C', 'f8',(basket_num,)), ('scrap_Si', 'f8',(basket_num,)), ('scrap_Mn', 'f8',(basket_num,)), ('scrap_Cr', 'f8',(basket_num,)), ('scrap_P', 'f8',(basket_num,)), ('scrap_Fe', 'f8',(basket_num,)), ('scrap_S', 'f8',(basket_num,)), 
                              #('scrap_Al', 'f8',(basket_num,)), ('scrap_Cu', 'f8',(basket_num,)), ('scrap_Ni', 'f8',(basket_num,)), ('scrap_Mo', 'f8',(basket_num,)), ('scrap_As', 'f8',(basket_num,)), ('scrap_Bi', 'f8',(basket_num,)), ('scrap_Sb', 'f8',(basket_num,)), 
                              #('scrap_Sn', 'f8',(basket_num,)), ('scrap_V', 'f8',(basket_num,)), ('scrap_CaO', 'f8',(basket_num,)), ('scrap_MgO', 'f8',(basket_num,)), ('scrap_Al2O3', 'f8',(basket_num,)), ('scrap_SiO2', 'f8',(basket_num,)), ('scrap_MnO', 'f8',(basket_num,)), 
                              #('scrap_Cr2O3', 'f8',(basket_num,)), ('scrap_P2O5', 'f8',(basket_num,)), ('scrap_FeO', 'f8',(basket_num,)), ('scrap_CaS', 'f8',(basket_num,)), ('coal_H2O', 'f8',(basket_num,)), ('coal_Combustables', 'f8',(basket_num,)), 
                              #('coal_O', 'f8',(basket_num,)), ('coal_C', 'f8',(basket_num,)), ('coal_Si', 'f8',(basket_num,)), ('coal_Mn', 'f8',(basket_num,)), ('coal_Cr', 'f8',(basket_num,)), ('coal_P', 'f8',(basket_num,)), ('coal_Fe', 'f8',(basket_num,)), ('coal_S', 'f8',(basket_num,)), 
                              #('coal_Al', 'f8',(basket_num,)), ('coal_Cu', 'f8',(basket_num,)), ('coal_Ni', 'f8',(basket_num,)), ('coal_Mo', 'f8',(basket_num,)), ('coal_As', 'f8',(basket_num,)), ('coal_Bi', 'f8',(basket_num,)), ('coal_Sb', 'f8',(basket_num,)), 
                              #('coal_Sn', 'f8',(basket_num,)), ('coal_V', 'f8',(basket_num,)), ('coal_CaO', 'f8',(basket_num,)), ('coal_MgO', 'f8',(basket_num,)), ('coal_Al2O3', 'f8',(basket_num,)), ('coal_SiO2', 'f8',(basket_num,)), ('coal_MnO', 'f8',(basket_num,)), 
                              #('coal_Cr2O3', 'f8',(basket_num,)), ('coal_P2O5', 'f8',(basket_num,)), ('coal_FeO', 'f8',(basket_num,)), ('coal_CaS', 'f8',(basket_num,))],align=True)))
    OpCh = OpCh[0]

    OpCh["charge_num"] = int(charge_num)
    OpCh["current_t"] = 0
    OpCh["low_power_cutofftime"] = 0
    OpCh["data"] = datasrc
    OpCh["P_arc"] = 0 
    OpCh["m_O2_lance"] = 0 
    OpCh["m_O2_post"] = 0 
    OpCh["m_C_inj"] = 0 
    OpCh["m_offgas"] = 0 
    OpCh["m_leakair"] = 0 
    OpCh["m_slag_form"] = 0 
    OpCh["m_CH4_burn"] = 0 
    OpCh["m_O2_CH4_burn"] = 0 
    OpCh["m_H2_burn"] = 0 
    OpCh["m_O2_H2_burn"] = 0 
    OpCh["m_water_roof"] = 0 
    OpCh["m_water_wall"] = 0 
    OpCh["U_arc"] = 0 
    OpCh["I_arc"] = 0 
    OpCh["m_water_roof_roc"] = 0 
    OpCh["m_water_wall_roc"] = 0 
    OpCh["T_water_roof_roc"] = 0 
    OpCh["T_water_wall_roc"] = 0 
    OpCh["m_conv_gas"] = 0 
    OpCh["m_dri"] = 0
    OpCh["T_dri"] = 0
    OpCh["m_dust"] = 0

    OpCh["basket_number"] = 0 
    OpCh["basket_number_diff"] = 0 
    OpCh["basket_number_next"] = 0 
    OpCh["t_basket"] = 0 

    OpCh["m_dolomite"] = 0
    OpCh["m_chalk"] = 0
    OpCh["m_coal"] = np.zeros(basket_num)
    OpCh["m_slag"] = np.zeros(basket_num)
    OpCh["m_st_sol_basket"] = np.zeros(basket_num)
    
    OpCh["m_tap_data"] = 0 
    OpCh["T_tap_last"] = 0 
    OpCh["t_end"] = np.zeros(basket_num) 
    OpCh["t_start"] = 0 
    OpCh["m_tap_slag"] = 0 
    OpCh["hotheel_comp"] = np.zeros(comp_len)
    
    OpCh["T_roof_init"] = np.zeros(basket_num) 
    OpCh["T_wall_init"] = np.zeros(basket_num) 
    OpCh["T_roof_H2O_init"] = np.zeros(basket_num) 
    OpCh["T_wall_H2O_init"] = np.zeros(basket_num)

    OpCh["OpCh_data"] = np.zeros((matrix_len,field_num))
    #OpCh_data = np.array([[0,  0.14, 0,   0,   4.5,  0, 0.028, 0.24,   0,    0,   0,    0,   0, 0,  0,  0,  0,  0,  0,  0,  0],
    #                      [3000,2.8,  0.6, 1.3, 4.6,  0, 0.14,  1.25,   100,  100, 100,  100, 0, 750,100,100,100,100,100,100,100]])
    OpCh["mat_t"] = np.empty(matrix_len)
    #"OpCh_interp_dict": np.NaN,
    OpCh["OpCh_interp0"] = np.empty((matrix_len,field_num))
    OpCh["OpCh_interp1"] = np.empty((matrix_len,field_num))
    OpCh["OpCh_interp2"] = np.empty((matrix_len,field_num))
    OpCh["OpCh_interp3"] = np.empty((matrix_len,field_num))
    # for evaluation
    OpCh["T_water_roof_out"] = np.zeros(matrix_len)
    OpCh["T_water_wall_out"] = np.zeros(matrix_len)
    OpCh["m_offgas_CO"] = np.zeros(matrix_len)
    OpCh["m_offgas_CO2"] = np.zeros(matrix_len)
    OpCh["m_offgas_N2"] = np.zeros(matrix_len)
    OpCh["m_offgas_O2"] = np.zeros(matrix_len)
    OpCh["m_offgas_H2"] = np.zeros(matrix_len)
    OpCh["m_offgas_H2O"] = np.zeros(matrix_len)
    OpCh["m_offgas_CH4"] = np.zeros(matrix_len)
    OpCh["OpCh_last_elem"] = matrix_len


    OpCh["mass_scrap"] = np.zeros((basket_num,comp_len))
    OpCh["mass_coal"] = np.zeros((basket_num,comp_len))

    
    OpCh["T_dri"] = 0
    OpCh["m_slag_form"] = 0 
    OpCh["m_C_inj"] = 0 

    OpCh["mass_C_inj"] = np.zeros(comp_len) #currently unused 
    OpCh["mass_dri_inj"] = np.zeros(comp_len)
    OpCh["mass_slagformer_inj"] = np.zeros(comp_len)

    '''
    OpCh["scrap_H2O"] = np.zeros(basket_num)
    OpCh["scrap_Combustables"] = np.zeros(basket_num)
    OpCh["scrap_O"] = np.zeros(basket_num)
    OpCh["scrap_C"] = np.zeros(basket_num)
    OpCh["scrap_Si"] = np.zeros(basket_num)
    OpCh["scrap_Mn"] = np.zeros(basket_num)
    OpCh["scrap_Cr"] = np.zeros(basket_num)
    OpCh["scrap_P"] = np.zeros(basket_num)
    OpCh["scrap_Fe"] = np.zeros(basket_num)
    OpCh["scrap_S"] = np.zeros(basket_num)
    OpCh["scrap_Al"] = np.zeros(basket_num)
    OpCh["scrap_Cu"] = np.zeros(basket_num)
    OpCh["scrap_Ni"] = np.zeros(basket_num)
    OpCh["scrap_Mo"] = np.zeros(basket_num)
    OpCh["scrap_As"] = np.zeros(basket_num)
    OpCh["scrap_Bi"] = np.zeros(basket_num)
    OpCh["scrap_Sb"] = np.zeros(basket_num)
    OpCh["scrap_Sn"] = np.zeros(basket_num)
    OpCh["scrap_V"] = np.zeros(basket_num)
    OpCh["scrap_CaO"] = np.zeros(basket_num)
    OpCh["scrap_MgO"] = np.zeros(basket_num)
    OpCh["scrap_Al2O3"] = np.zeros(basket_num)
    OpCh["scrap_SiO2"] = np.zeros(basket_num)
    OpCh["scrap_MnO"] = np.zeros(basket_num)
    OpCh["scrap_Cr2O3"] = np.zeros(basket_num)
    OpCh["scrap_P2O5"] = np.zeros(basket_num)
    OpCh["scrap_FeO"] = np.zeros(basket_num)
    OpCh["scrap_CaS"] = np.zeros(basket_num)
    
    OpCh["coal_H2O"] = np.zeros(basket_num)
    OpCh["coal_Combustables"] = np.zeros(basket_num)
    OpCh["coal_O"] = np.zeros(basket_num)
    OpCh["coal_C"] = np.zeros(basket_num)
    OpCh["coal_Si"] = np.zeros(basket_num)
    OpCh["coal_Mn"] = np.zeros(basket_num)
    OpCh["coal_Cr"] = np.zeros(basket_num)
    OpCh["coal_P"] = np.zeros(basket_num)
    OpCh["coal_Fe"] = np.zeros(basket_num)
    OpCh["coal_S"] = np.zeros(basket_num)
    OpCh["coal_Al"] = np.zeros(basket_num)
    OpCh["coal_Cu"] = np.zeros(basket_num)
    OpCh["coal_Ni"] = np.zeros(basket_num)
    OpCh["coal_Mo"] = np.zeros(basket_num)
    OpCh["coal_As"] = np.zeros(basket_num)
    OpCh["coal_Bi"] = np.zeros(basket_num)
    OpCh["coal_Sb"] = np.zeros(basket_num)
    OpCh["coal_Sn"] = np.zeros(basket_num)
    OpCh["coal_V"] = np.zeros(basket_num)
    OpCh["coal_CaO"] = np.zeros(basket_num)
    OpCh["coal_MgO"] = np.zeros(basket_num)
    OpCh["coal_Al2O3"] = np.zeros(basket_num)
    OpCh["coal_SiO2"] = np.zeros(basket_num)
    OpCh["coal_MnO"] = np.zeros(basket_num)
    OpCh["coal_Cr2O3"] = np.zeros(basket_num)
    OpCh["coal_P2O5"] = np.zeros(basket_num)
    OpCh["coal_FeO"] = np.zeros(basket_num)
    OpCh["coal_CaS"] = np.zeros(basket_num)
    '''

    #indizes for y and dy
    ind = np.empty(1, dtype=from_dtype(np.dtype([('T_roof', 'i8'), ('T_wall', 'i8'), ('T_roof_water', 'i8'), ('T_wall_water', 'i8'), ('T_wall_unc', 'i8'),
                             ('T_st_sol', 'i8'), ('T_st_liq', 'i8'), ('T_el', 'i8'), ('T_sl_sol', 'i8'), ('T_sl_liq', 'i8'),
                             ('T_gas', 'i8'), ('m_coal', 'i8'), ('m_vol_coal', 'i8'), ('m_comb_scrap', 'i8'), ('m_el', 'i8'),
                             #('m_CaO_st_sol', 'i8'), ('m_MgO_st_sol', 'i8'), ('m_Al2O3_st_sol', 'i8'), ('m_SiO2_st_sol', 'i8'), ('m_FeO_st_sol', 'i8'),
                             ('m_O_st_sol', 'i8'), ('m_C_st_sol', 'i8'), ('m_Si_st_sol', 'i8'), ('m_Mn_st_sol', 'i8'), ('m_Cr_st_sol', 'i8'),
                             ('m_P_st_sol', 'i8'), ('m_Fe_st_sol', 'i8'), ('m_S_st_sol', 'i8'), ('m_st_sol', 'i8'), ('V_st_sol', 'i8'),
                             ('m_O_st_liq', 'i8'), ('m_C_st_liq', 'i8'), ('m_Si_st_liq', 'i8'), ('m_Mn_st_liq', 'i8'), ('m_Cr_st_liq', 'i8'),
                             ('m_P_st_liq', 'i8'), ('m_Fe_st_liq', 'i8'), ('m_S_st_liq', 'i8'), ('m_st_liq', 'i8'), ('h_slag', 'i8'),
                             ('m_O_reac', 'i8'), ('m_C_reac', 'i8'), ('m_Si_reac', 'i8'), ('m_Mn_reac', 'i8'), ('m_Cr_reac', 'i8'),
                             ('m_P_reac', 'i8'), ('m_Fe_reac', 'i8'), ('m_S_reac', 'i8'), ('m_resolid', 'i8'), ('m_CaO_sl_sol', 'i8'),
                             ('m_MgO_sl_sol', 'i8'), ('m_Al2O3_sl_sol', 'i8'), ('m_SiO2_sl_sol', 'i8'), ('m_MnO_sl_sol', 'i8'), ('m_Cr2O3_sl_sol', 'i8'),
                             ('m_P2O5_sl_sol', 'i8'), ('m_FeO_sl_sol', 'i8'), ('m_CaS_sl_sol', 'i8'), ('m_sl_sol', 'i8'), ('m_CaO_sl_liq', 'i8'),
                             ('m_MgO_sl_liq', 'i8'), ('m_Al2O3_sl_liq', 'i8'), ('m_SiO2_sl_liq', 'i8'), ('m_MnO_sl_liq', 'i8'), ('m_Cr2O3_sl_liq', 'i8'),
                             ('m_P2O5_sl_liq', 'i8'), ('m_FeO_sl_liq', 'i8'), ('m_CaS_sl_liq', 'i8'), ('m_sl_liq', 'i8'), ('m_CO', 'i8'),
                             ('m_CO2', 'i8'), ('m_N2', 'i8'), ('m_O2', 'i8'), ('m_H2', 'i8'), ('m_H2O', 'i8'), ('m_CH4', 'i8'), ('m_gas', 'i8'), 
                             ('m_leak_air', 'i8'), ('p_furnace', 'i8'),
                             #('event_time', 'i8'), ('event_trigger', 'i8'),
                             ('comp_H2O', 'i8'), ('comp_Combustables', 'i8'), ('comp_O', 'i8'), ('comp_C', 'i8'), ('comp_Si', 'i8'), 
                             ('comp_Mn', 'i8'), ('comp_Cr', 'i8'), ('comp_P', 'i8'), ('comp_Fe', 'i8'), ('comp_S', 'i8'), 
                             ('comp_Al', 'i8'), ('comp_Cu', 'i8'), ('comp_Ni', 'i8'), ('comp_Mo', 'i8'), ('comp_As', 'i8'), 
                             ('comp_Bi', 'i8'), ('comp_Sb', 'i8'), ('comp_Sn', 'i8'), ('comp_V', 'i8'), ('comp_CaO', 'i8'), 
                             ('comp_MgO', 'i8'), ('comp_Al2O3', 'i8'), ('comp_SiO2', 'i8'), ('comp_MnO', 'i8'), ('comp_Cr2O3', 'i8'), 
                             ('comp_P2O5', 'i8'), ('comp_FeO', 'i8'), ('comp_CaS', 'i8')],align=True)))
    ind = ind[0]

    ind["T_roof"] = 1
    ind["T_wall"] = 2
    ind["T_roof_water"] = 3
    ind["T_wall_water"] = 4
    ind["T_wall_unc"] = 5
    ind["T_st_sol"] = 6
    ind["T_st_liq"] = 7
    ind["T_el"] = 8
    ind["T_sl_sol"] = 9
    ind["T_sl_liq"] = 10
    ind["T_gas"] = 11
    ind["m_coal"] = 12
    ind["m_vol_coal"] = 13
    ind["m_comb_scrap"] = 14
    ind["m_el"] = 15
    ind["m_O_st_sol"] = 16
    ind["m_C_st_sol"] = 17
    ind["m_Si_st_sol"] = 18
    ind["m_Mn_st_sol"] = 19
    ind["m_Cr_st_sol"] = 20
    ind["m_P_st_sol"] = 21
    ind["m_Fe_st_sol"] = 22
    ind["m_S_st_sol"] = 23
    ind["m_st_sol"] = 24
    ind["V_st_sol"] = 25
    ind["m_O_st_liq"] = 26
    ind["m_C_st_liq"] = 27
    ind["m_Si_st_liq"] = 28
    ind["m_Mn_st_liq"] = 29
    ind["m_Cr_st_liq"] = 30
    ind["m_P_st_liq"] = 31
    ind["m_Fe_st_liq"] = 32
    ind["m_S_st_liq"] = 33
    ind["m_st_liq"] = 34
    ind["h_slag"] = 35
    ind["m_O_reac"] = 36
    ind["m_C_reac"] = 37
    ind["m_Si_reac"] = 38
    ind["m_Mn_reac"] = 39
    ind["m_Cr_reac"] = 40
    ind["m_P_reac"] = 41
    ind["m_Fe_reac"] = 42
    ind["m_S_reac"] = 43
    ind["m_resolid"] = 44
    ind["m_CaO_sl_sol"] = 45
    ind["m_MgO_sl_sol"] = 46
    ind["m_Al2O3_sl_sol"] = 47
    ind["m_SiO2_sl_sol"] = 48
    ind["m_MnO_sl_sol"] = 49
    ind["m_Cr2O3_sl_sol"] = 50
    ind["m_P2O5_sl_sol"] = 51
    ind["m_FeO_sl_sol"] = 52
    ind["m_CaS_sl_sol"] = 53
    ind["m_sl_sol"] = 54
    ind["m_CaO_sl_liq"] = 55
    ind["m_MgO_sl_liq"] = 56
    ind["m_Al2O3_sl_liq"] = 57
    ind["m_SiO2_sl_liq"] = 58
    ind["m_MnO_sl_liq"] = 59
    ind["m_Cr2O3_sl_liq"] = 60
    ind["m_P2O5_sl_liq"] = 61
    ind["m_FeO_sl_liq"] = 62
    ind["m_CaS_sl_liq"] = 63
    ind["m_sl_liq"] = 64
    ind["m_CO"] = 65
    ind["m_CO2"] = 66
    ind["m_N2"] = 67
    ind["m_O2"] = 68
    ind["m_H2"] = 69
    ind["m_H2O"] = 70
    ind["m_CH4"] = 71
    ind["m_gas"] = 72
    ind["m_leak_air"] = 73
    ind["p_furnace"] = 74

    ind["comp_H2O"] = 0
    ind["comp_Combustables"] = 1
    ind["comp_O"] = 2
    ind["comp_C"] = 3
    ind["comp_Si"] = 4
    ind["comp_Mn"] = 5
    ind["comp_Cr"] = 6
    ind["comp_P"] = 7
    ind["comp_Fe"] = 8
    ind["comp_S"] = 9
    ind["comp_Al"] = 10
    ind["comp_Cu"] = 11
    ind["comp_Ni"] = 12
    ind["comp_Mo"] = 13
    ind["comp_As"] = 14
    ind["comp_Bi"] = 15
    ind["comp_Sb"] = 16
    ind["comp_Sn"] = 17
    ind["comp_V"] = 18
    ind["comp_CaO"] = 19
    ind["comp_MgO"] = 20
    ind["comp_Al2O3"] = 21
    ind["comp_SiO2"] = 22
    ind["comp_MnO"] = 23
    ind["comp_Cr2O3"] = 24
    ind["comp_P2O5"] = 25
    ind["comp_FeO"] = 26
    ind["comp_CaS"] = 27


    X = np.empty(1, dtype=from_dtype(np.dtype([('O', 'f8'), ('C', 'f8'), ('Si', 'f8'), ('Mn', 'f8'), ('Cr', 'f8'), ('Al', 'f8'), ('P', 'f8'), ('Fe', 'f8'), ('S', 'f8'),
                                               ('O_reac', 'f8'), ('C_reac', 'f8'), ('Si_reac', 'f8'), ('Mn_reac', 'f8'), ('Cr_reac', 'f8'), ('Al_reac', 'f8'), ('P_reac', 'f8'), ('Fe_reac', 'f8'), ('S_reac', 'f8'),
                                               ('CaO', 'f8'), ('MgO', 'f8'), ('Al2O3', 'f8'), ('SiO2', 'f8'), ('MnO', 'f8'), ('Cr2O3', 'f8'), ('P2O5', 'f8'), ('FeO', 'f8'),
                                               ('CaS', 'f8'), ('CO', 'f8'), ('CO2', 'f8'), ('N2', 'f8'), ('O2', 'f8'), ('H2', 'f8'), ('H2O', 'f8'), ('CH4', 'f8'), ('p_CO', 'f8')],align=True)))
    X = X[0]

    X["O"] = 0
    X["C"] = 0
    X["Si"] = 0
    X["Mn"] = 0
    X["Cr"] = 0
    X["Al"] = 0
    X["P"] = 0
    X["Fe"] = 0
    X["S"] = 0
    
    X["O_reac"] = 0
    X["C_reac"] = 0
    X["Si_reac"] = 0
    X["Mn_reac"] = 0
    X["Cr_reac"] = 0
    X["Al_reac"] = 0
    X["P_reac"] = 0
    X["Fe_reac"] = 0
    X["S_reac"] = 0
    
    X["CaO"] = 0
    X["MgO"] = 0
    X["Al2O3"] = 0
    X["SiO2"] = 0
    X["MnO"] = 0
    X["Cr2O3"] = 0
    X["P2O5"] = 0
    X["FeO"] = 0
    X["CaS"] = 0
    X["CO"] = 0
    X["CO2"] = 0
    X["N2"] = 0
    X["O2"] = 0
    X["H2"] = 0
    X["H2O"] = 0
    X["CH4"] = 0
    X["p_CO"] = 0
  
  
    W = np.empty(1, dtype=from_dtype(np.dtype([('O', 'f8'), ('C', 'f8'), ('Si', 'f8'), ('Mn', 'f8'), ('Cr', 'f8'), ('Al', 'f8'), ('P', 'f8'), ('Fe', 'f8'), ('S', 'f8'),
                           ('O_reac', 'f8'), ('C_reac', 'f8'), ('Si_reac', 'f8'), ('Mn_reac', 'f8'), ('Cr_reac', 'f8'), ('Al_reac', 'f8'), ('P_reac', 'f8'), ('Fe_reac', 'f8'), ('S_reac', 'f8'),
                           ('CaO', 'f8'), ('MgO', 'f8'), ('Al2O3', 'f8'), ('SiO2', 'f8'), ('MnO', 'f8'), ('Cr2O3', 'f8'), ('P2O5', 'f8'), ('FeO', 'f8'), ('CaS', 'f8'),
                           ('CO', 'f8'), ('CO2', 'f8'), ('N2', 'f8'), ('O2', 'f8'), ('H2', 'f8'), ('H2O', 'f8'), ('CH4', 'f8'),
                           ('O_eq', 'f8'), ('C_eq', 'f8'), ('Si_eq', 'f8'), ('Mn_eq', 'f8'), ('Cr_eq', 'f8'), ('P_eq', 'f8'), ('Fe_eq', 'f8'), ('S_eq', 'f8'),
                           ('O_eq_C', 'f8'), ('C_eq_C', 'f8'), ('Si_eq_C', 'f8'), ('Mn_eq_C', 'f8'), ('Cr_eq_C', 'f8'), ('P_eq_C', 'f8'), ('Fe_eq_C', 'f8'), ('S_eq_C', 'f8'),
                           ('O_eq_O', 'f8'), ('C_eq_O', 'f8'), ('Si_eq_O', 'f8'), ('Mn_eq_O', 'f8'), ('Cr_eq_O', 'f8'), ('P_eq_O', 'f8'), ('Fe_eq_O', 'f8'), ('S_eq_O', 'f8'),
                           ],align=True)))
    W = W[0]

    W["O"] = 0
    W["C"] = 0
    W["Si"] = 0
    W["Mn"] = 0
    W["Cr"] = 0
    W["Al"] = 0
    W["P"] = 0
    W["Fe"] = 0
    W["S"] = 0
    
    W["O_reac"] = 0
    W["C_reac"] = 0
    W["Si_reac"] = 0
    W["Mn_reac"] = 0
    W["Cr_reac"] = 0
    W["Al_reac"] = 0
    W["P_reac"] = 0
    W["Fe_reac"] = 0
    W["S_reac"] = 0
    
    W["CaO"] = 0
    W["MgO"] = 0
    W["Al2O3"] = 0
    W["SiO2"] = 0
    W["MnO"] = 0
    W["Cr2O3"] = 0
    W["P2O5"] = 0
    W["FeO"] = 0
    W["CaS"] = 0
    
    W["CO"] = 0
    W["CO2"] = 0
    W["N2"] = 0
    W["O2"] = 0
    W["H2"] = 0
    W["H2O"] = 0
    W["CH4"] = 0
    
    W["O_eq"] = 0
    W["C_eq"] = 0
    W["Si_eq"] = 0
    W["Mn_eq"] = 0
    W["Cr_eq"] = 0
    W["P_eq"] = 0
    W["Fe_eq"] = 0
    W["S_eq"] = 0
    
    W["O_eq_C"] = 0
    W["C_eq_C"] = 0
    W["Si_eq_C"] = 0
    W["Mn_eq_C"] = 0
    W["Cr_eq_C"] = 0
    W["P_eq_C"] = 0
    W["Fe_eq_C"] = 0
    W["S_eq_C"] = 0
    
    W["O_eq_O"] = 0
    W["C_eq_O"] = 0
    W["Si_eq_O"] = 0
    W["Mn_eq_O"] = 0
    W["Cr_eq_O"] = 0
    W["P_eq_O"] = 0
    W["Fe_eq_O"] = 0
    W["S_eq_O"] = 0



    K = np.empty(1, dtype=from_dtype(np.dtype([('melt_exposed', 'f8'), ('wall_unc_exposed', 'f8'), ('m_resolid', 'f8'), ('T_resolid', 'f8'), ('resolid_influence', 'f8'), ('burn', 'f8'), ('alpha_el_cool', 'f8'), ('alpha_vessel_air', 'f8'), ('C_inj_loss', 'f8'),
                           ('refrac', 'f8'), ('pr', 'f8'), ('pr1', 'f8'), ('pr2', 'f8'), ('pr3', 'f8'), ('pr4', 'f8'), ('convective_air', 'f8'), ('leak_air_coal_burn', 'f8'), ('C_inj_diss', 'f8'), ('C_inj_char', 'f8'),
                           ('CO_slag_CO2', 'f8'), ('reac_fact', 'f8'), ('CO_C_CO2', 'f8'), ('O2_lance_diss', 'f8'), ('O2_post_CO', 'f8'), ('FeO_O2', 'f8'), ('FeO_CO2', 'f8'), ('FeO_O2_post', 'f8'),
                           ('slag_height_fact', 'f8'), ('slag_influence', 'f8'), ('slag_T_reduc', 'f8'), ('coal_burn_charging', 'f8'), ('conv_gas', 'f8'), ('combustibles_st_sol', 'f8'), ('reactor_melt_mass', 'f8'),
                           ('wall', 'f8',(3,)), ('therm', 'f8',(11,)), ('area', 'f8',(5,)), ('arc_power_distribution', 'f8',(4,)), ('post', 'f8'), ('slag', 'f8'), ('xV_part_2', 'f8'), ('xV_part_3', 'f8'), ('xV_part_4', 'f8'),
                           ('xV_burn_1', 'f8'), ('xV_burn_2', 'f8'), ('xV_burn_3', 'f8'), ('xV_burn_4', 'f8'), ('xV_burn_5', 'f8'), ('xV_burn_6', 'f8')],align=True)))
    K = K[0]

    K["melt_exposed"] = 0
    K["wall_unc_exposed"] = 0
    K["m_resolid"] = 0 #50e3
    K["T_resolid"] = 0 #50
    K["resolid_influence"] = 0 #0.5
    K["burn"] = 0 #0.7
    K["alpha_el_cool"] = 0 #8.5
    K["alpha_vessel_air"] =  0#40
    K["C_inj_loss"] = 0 #0.05
    K["refrac"] = 0 #0.005
    K["pr"] = 0 #0.2
    K["pr1"] = 2
    K["pr2"] = 1
    K["pr3"] = 1
    K["pr4"] = 3
    K["convective_air"] = 0 #2
    K["leak_air_coal_burn"] = 0 #0.1
    K["C_inj_diss"] = 0 #0.06
    K["C_inj_char"] = 0 #0.3
    K["CO_slag_CO2"] = 0 #0.175
    K["reac_fact"] = 0 #25
    K["CO_C_CO2"] = 0 #0.1
    K["O2_lance_diss"] = 0 #0.025
    K["O2_post_CO"] = 0 #0.05
    K["FeO_O2"] = 0 #0.5
    K["FeO_CO2"] = 0 #0.25
    K["FeO_O2_post"] = 0 #0.2
    K["slag_height_fact"] = 0 #8
    K["slag_influence"] = 0 #0.9
    K["slag_T_reduc"] = 0 #75
    K["coal_burn_charging"] = 0 #0.01
    K["conv_gas"] = 0 #0.01 # set in Excel
    K["combustibles_st_sol"] = 0 #0.003
    K["reactor_melt_mass"] = 0 #1000
    
    K["wall"] = np.zeros(3) # = [2.5, 0.0125, 0.04]
    K["therm"] = np.zeros(11) # = [0.165, 0.05, np.NaN, 12, 0.5, 0.1, 25, 2e-4, 0.05, 0.05, 0.15]
    K["area"] = np.zeros(5) # = [np.NaN, 0.1, 0.01, 0.09, 0.12]
    K["arc_power_distribution"] = np.zeros(4) # = [0.3, 0.075, 0.6, 0.025]
    
    
    K["post"] = 0
    K["slag"] = 0
    
    # for burner_reactions
    K["xV_part_2"] = 0.8
    K["xV_part_3"] = 0.06
    K["xV_part_4"] = 0.13
    K["xV_burn_1"] = 0 #0.55
    K["xV_burn_2"] = 0 #0.3
    K["xV_burn_3"] = 0 #0.15
    K["xV_burn_4"] = 0 #0
    K["xV_burn_5"] = 0 #0
    K["xV_burn_6"] = 1
    

    
    kd = np.empty(1, dtype=from_dtype(np.dtype([('comb', 'f8'), ('vol', 'f8'), ('el_tip', 'f8'), ('el_side', 'f8'), ('C_char_burn', 'f8'), ('C_char_reac', 'f8'),
                              ('C_char_diss', 'f8'), ('O_1', 'f8'), ('C_1', 'f8'), ('C_2', 'f8'), ('Si_1', 'f8'), ('Si_2', 'f8'),
                              ('Si_3', 'f8'), ('Mn_1', 'f8'), ('Mn_2', 'f8'), ('Mn_3', 'f8'), ('Cr_1', 'f8'), ('Cr_2', 'f8'), ('Cr_3', 'f8'),
                              ('P_1', 'f8'), ('P_2', 'f8'), ('P_3', 'f8'), ('Fe_1', 'f8'), ('Fe_2', 'f8'), ('Fe_3', 'f8'), ('S', 'f8'),
                              ('CO_post', 'f8'), ('H2_post', 'f8'), ('CH4_post', 'f8'), ('H2O_diss', 'f8'), ('gas', 'f8', (4,))],align=True)))
    kd = kd[0]

    kd["comb"] = 0 #0.028
    kd["vol"] = 0 #0.01
    kd["el_tip"] = 0 #0.013
    kd["el_side"] = 0 #10
    kd["C_char_burn"] = 0 #5
    kd["C_char_reac"] = 0 #0.015
    kd["C_char_diss"] = 0 #0.1 * 6e-5
    kd["O_1"] = 0 #5
    kd["C_1"] = 0 #7.5
    kd["C_2"] = 0 #12
    kd["Si_1"] = 0 #10
    kd["Si_2"] = 0 #4
    kd["Si_3"] = 0 #1.5
    kd["Mn_1"] = 0 #15
    kd["Mn_2"] = 0 #10
    kd["Mn_3"] = 0 #0.05
    kd["Cr_1"] = 0 #0.5
    kd["Cr_2"] = 0 #10
    kd["Cr_3"] = 0 # 0.02
    kd["P_1"] = 0 #0.1
    kd["P_2"] = 0 #11
    kd["P_3"] = 0 #0.5
    kd["Fe_1"] = 0 #2e-4
    kd["Fe_2"] = 0 #0.6
    kd["Fe_3"] = 0 #0.35
    kd["S"] = 0 #0.2
    kd["CO_post"] = 0 #12
    kd["H2_post"] = 0 #0.1
    kd["CH4_post"] = 0 #5
    kd["H2O_diss"] = 0 #0.01
    kd["gas"] = np.zeros(4) # = np.array([0, 2, 0.0005, 0.2]) 
    


    comp = np.empty(1, dtype=from_dtype(np.dtype([('C_char_C_fix', 'f8'), ('C_char_C_vol', 'f8'), ('C_char_H2', 'f8'), ('C_char_N2', 'f8'), ('C_char_O2', 'f8'), 
                              ('C_char_H2O', 'f8'), ('C_char_lhv', 'f8'), ('C_inj_C_fix', 'f8'), ('C_inj_C_vol', 'f8'), ('C_inj_H2', 'f8'), 
                              ('C_inj_N2', 'f8'), ('C_inj_O2', 'f8'), ('C_inj_H2O', 'f8'), ('C_inj_lhv', 'f8'), ('gas_CH4', 'f8'), 
                              ('gas_N2', 'f8'), ('gas_H2', 'f8'), ('O2_O2', 'f8'), ('O2_N2', 'f8'), ('air_O2', 'f8'), ('air_N2', 'f8'), 
                              ('chalk_CaO', 'f8'), ('chalk_MgO', 'f8'), ('chalk_Al2O3', 'f8'), ('chalk_SiO2', 'f8'), ('chalk_MnO', 'f8'),
                              ('chalk_Cr2O3', 'f8'), ('chalk_P2O5', 'f8'), ('chalk_CaS', 'f8'), ('chalk_FeO', 'f8'),
                              ('dolomite_CaO', 'f8'), ('dolomite_MgO', 'f8'), ('dolomite_Al2O3', 'f8'), ('dolomite_SiO2', 'f8'), ('dolomite_MnO', 'f8'),
                              ('dolomite_Cr2O3', 'f8'), ('dolomite_P2O5', 'f8'), ('dolomite_CaS', 'f8'), ('dolomite_FeO', 'f8'),
                              ('dust_CaO', 'f8'), ('dust_MgO', 'f8'), ('dust_Al2O3', 'f8'), ('dust_SiO2', 'f8'), ('dust_MnO', 'f8'), ('dust_Cr2O3', 'f8'),
                              ('dust_P2O5', 'f8'), ('dust_CaS', 'f8'), ('dust_FeO', 'f8'), ('dust_C', 'f8'),
                              ('refrac_CaO', 'f8'), ('refrac_MgO', 'f8'), ('slag_tap', 'f8', (9,)), ('steel_tap', 'f8', (8,)),
                              ('steel_start', 'f8', (8,)), ('slag', 'f8', (6,)), ('burn_CH4', 'f8'), ('burn_H2', 'f8'), ('dri_O', 'f8'), ('dri_C', 'f8'),
                              ('dri_Si', 'f8'), ('dri_Mn', 'f8'), ('dri_Cr', 'f8'), ('dri_P', 'f8'), ('dri_S', 'f8'), ('dri_Fe', 'f8')],align=True)))

    comp = comp[0]

    comp["C_char_C_fix"] = 0#0.85
    comp["C_char_C_vol"] = 0 #0
    comp["C_char_H2"] = 0 #0.03
    comp["C_char_N2"] = 0 #0.05
    comp["C_char_O2"] = 0 #0
    comp["C_char_H2O"] = 0 #0.06
    comp["C_char_lhv"] = 0 #3.35e4
    
    comp["C_inj_C_fix"] = 0 #0.875
    comp["C_inj_C_vol"] = 0 #0
    comp["C_inj_H2"] = 0 #0.01
    comp["C_inj_N2"] = 0 #0
    comp["C_inj_O2"] = 0 #0
    comp["C_inj_H2O"] = 0 #0.02
    comp["C_inj_lhv"] = 0 #31800
    
    comp["gas_CH4"] = 0 #0.8
    comp["gas_N2"] = 0 #0.2
    comp["gas_H2"] = 0 #0
    
    comp["O2_O2"] = 0 #0.95
    comp["O2_N2"] = 0 #0.026
    
    comp["air_O2"] = 0 #0.23
    comp["air_N2"] = 0 #0.77

    comp["chalk_CaO"] = 0 
    comp["chalk_MgO"] = 0 
    comp["chalk_Al2O3"] = 0 
    comp["chalk_SiO2"] = 0 
    comp["chalk_MnO"] = 0 
    comp["chalk_Cr2O3"] = 0 
    comp["chalk_P2O5"] = 0 
    comp["chalk_CaS"] = 0 
    comp["chalk_FeO"] = 0
    
    comp["dolomite_CaO"] = 0 
    comp["dolomite_MgO"] = 0 
    comp["dolomite_Al2O3"] = 0 
    comp["dolomite_SiO2"] = 0 
    comp["dolomite_MnO"] = 0 
    comp["dolomite_Cr2O3"] = 0 
    comp["dolomite_P2O5"] = 0 
    comp["dolomite_CaS"] = 0 
    comp["dolomite_FeO"] = 0
    
    comp["dust_CaO"] = 0
    comp["dust_MgO"] = 0
    comp["dust_Al2O3"] = 0
    comp["dust_SiO2"] = 0
    comp["dust_MnO"] = 0
    comp["dust_Cr2O3"] = 0
    comp["dust_P2O5"] = 0
    comp["dust_CaS"] = 0
    comp["dust_FeO"] = 0
    comp["dust_C"] = 0

    comp["refrac_CaO"] = 0 #0.1
    comp["refrac_MgO"] = 0 #0.9

    comp["slag_tap"] = np.zeros(9) 
    comp["steel_tap"] = np.zeros(8) 
    comp["steel_start"] = np.zeros(8)  

    comp["slag"] = np.zeros(6) 
    comp["burn_CH4"] = 0
    comp["burn_H2"] = 0
    
    comp["dri_O"] = 0
    comp["dri_C"] = 0
    comp["dri_Si"] = 0
    comp["dri_Mn"] = 0
    comp["dri_Cr"] = 0
    comp["dri_P"] = 0
    comp["dri_S"] = 0
    comp["dri_Fe"] = 0


    iv = np.empty(1, dtype = from_dtype(np.dtype([('m_steam', 'f8'), ('m_water_in', 'f8'), ('C_conv_gas', 'f8'), ('C_inj_av', 'f8'), ('T_el_tip', 'f8'), 
                                                  ('dz_gas', 'f8', (8,8)), ('VF', 'f8', (8,8)), ('J', 'f8', (8,)), ('eps_gas', 'f8'), ('abs_gas', 'f8', (8,)), 
                                                  ('transm', 'f8', (8,)), ('burner', 'f8', (6,)), ('T_prev', 'f8'), ('m_FeO_burn_O2', 'f8'), 
                                                  ('m_FeO_burn_CO2', 'f8'), ('m_FeO_post_O2', 'f8'), ('m_FeO_sum', 'f8'), ('iteration_count','i8')], align=True)))
    iv = iv[0]

    iv["m_steam"] = 0 
    iv["m_water_in"] = 0 
    iv["C_conv_gas"] = 0 
    iv["C_inj_av"] = 0 
    iv["T_el_tip"] = 0 
    iv["dz_gas"] = np.zeros((8,8))
    iv["VF"] = np.zeros((8,8))
    iv["J"] = np.zeros(8)
    iv["eps_gas"] = 0 
    iv["abs_gas"] = np.zeros(8)
    iv["transm"] = np.zeros(8)
    iv["burner"] = np.zeros(6)
    iv["T_prev"] = 0 

    iv["m_FeO_burn_O2"] = 0
    iv["m_FeO_burn_CO2"] = 0
    iv["m_FeO_post_O2"] = 0
    iv["m_FeO_sum"] = 0

    iv["iteration_count"] = 0



    DH_T = np.empty(1, dtype=from_dtype(np.dtype([('a', 'f8'), ('b', 'f8'), ('c', 'f8'), ('d', 'f8'), ('e', 'f8'), ('f', 'f8'),
                              ('g', 'f8'), ('h', 'f8'), ('i', 'f8'), ('j', 'f8'), ('k', 'f8'), ('l', 'f8'),
                              ('m', 'f8'), ('n', 'f8'), ('o', 'f8'), ('p', 'f8'), ('q', 'f8'), ('r', 'f8'),
                              ('s', 'f8'), ('t', 'f8'), ('u', 'f8'), ('v', 'f8'), ('w', 'f8'), ('x', 'f8'),
                              ('y', 'f8'), ('z', 'f8')],align=True)))
    DH_T = DH_T[0]

    DH_T["a"] = 0
    DH_T["b"] = 0
    DH_T["c"] = 0
    DH_T["d"] = 0
    DH_T["e"] = 0
    DH_T["f"] = 0
    DH_T["g"] = 0
    DH_T["h"] = 0
    DH_T["i"] = 0
    DH_T["j"] = 0
    DH_T["k"] = 0
    DH_T["l"] = 0
    DH_T["m"] = 0
    DH_T["n"] = 0
    DH_T["o"] = 0
    DH_T["p"] = 0
    DH_T["q"] = 0
    DH_T["r"] = 0
    DH_T["s"] = 0
    DH_T["t"] = 0
    DH_T["u"] = 0
    DH_T["v"] = 0
    DH_T["w"] = 0
    DH_T["x"] = 0
    DH_T["y"] = 0
    DH_T["z"] = 0
    
    

    QM = np.empty(1, dtype=from_dtype(np.dtype([('C_inj', 'f8'), ('C_diss', 'f8'), ('O2_lance', 'f8'), ('O2_post', 'f8'), ('burner', 'f8'), ('leak_air', 'f8'),
                            ('leak_H2O', 'f8'), ('conv_gas', 'f8'), ('resolid', 'f8'), ('slag_form', 'f8'), ('dri', 'f8'), ('dust', 'f8'), ('FeO_burners', 'f8'),
                            ('st_liq_O', 'f8'), ('st_liq_C', 'f8'), ('st_liq_Si', 'f8'), ('st_liq_Mn', 'f8'), ('st_liq_Cr', 'f8'), ('st_liq_P', 'f8'),
                            ('st_liq_Fe', 'f8'), ('st_liq_S', 'f8'), ('sl_liq_CaO', 'f8'), ('sl_liq_MgO', 'f8'), ('sl_liq_Al2O3', 'f8'), ('sl_liq_SiO2', 'f8'),
                            ('sl_liq_MnO', 'f8'), ('sl_liq_Cr2O3', 'f8'), ('sl_liq_P2O5', 'f8'), ('sl_liq_FeO', 'f8'), ('sl_liq_CaS', 'f8'),
                            ('gas_CO', 'f8'), ('gas_CO2', 'f8'), ('gas_N2', 'f8'), ('gas_O2', 'f8'), ('gas_H2', 'f8'), ('gas_H2O', 'f8'), ('gas_CH4', 'f8'),
                            ('st_sol_melt', 'f8'), ('st_liq_melt', 'f8'), ('sl_sol_melt', 'f8'), ('sl_liq_melt', 'f8'), ('gas', 'f8'), ('st_sol', 'f8'),
                            ('sl_sol', 'f8'), ('gas_cp_change', 'f8'), ('st_liq', 'f8'), ('sl_liq', 'f8')],align=True)))


    QM = QM[0]
    QM["C_inj"] = 0
    QM["C_diss"] = 0
    QM["O2_lance"] = 0
    QM["O2_post"] = 0
    QM["burner"] = 0
    QM["leak_air"] = 0
    QM["leak_H2O"] = 0
    QM["conv_gas"] = 0
    QM["resolid"] = 0
    QM["slag_form"] = 0
    QM["dri"] = 0
    QM["dust"] = 0
    QM["FeO_burners"] = 0
    QM["st_liq_O"] = 0
    QM["st_liq_C"] = 0
    QM["st_liq_Si"] = 0
    QM["st_liq_Mn"] = 0
    QM["st_liq_Cr"] = 0
    QM["st_liq_P"] = 0
    QM["st_liq_Fe"] = 0
    QM["st_liq_S"] = 0
    QM["sl_liq_CaO"] = 0
    QM["sl_liq_MgO"] = 0
    QM["sl_liq_Al2O3"] = 0
    QM["sl_liq_SiO2"] = 0
    QM["sl_liq_MnO"] = 0
    QM["sl_liq_Cr2O3"] = 0
    QM["sl_liq_P2O5"] = 0
    QM["sl_liq_FeO"] = 0
    QM["sl_liq_CaS"] = 0
    QM["gas_CO"] = 0
    QM["gas_CO2"] = 0
    QM["gas_N2"] = 0
    QM["gas_O2"] = 0
    QM["gas_H2"] = 0
    QM["gas_H2O"] = 0
    QM["gas_CH4"] = 0
    QM["st_sol_melt"] = 0
    QM["st_liq_melt"] = 0
    QM["sl_sol_melt"] = 0
    QM["sl_liq_melt"] = 0
    QM["gas"] = 0
    QM["st_sol"] = 0
    QM["sl_sol"] = 0
    QM["gas_cp_change"] = 0
    QM["st_liq"] = 0
    QM["sl_liq"] = 0

    

    #molar volumes
    V = np.empty(1, dtype=from_dtype(np.dtype([('CaO', 'f8'), ('MgO', 'f8'), ('FeO', 'f8'), ('MnO', 'f8'), ('Cr2O3', 'f8')],align=True)))
    V = V[0]
    V["CaO"] = 20.7000
    V["MgO"] = 16.1000
    V["FeO"] = 15.8000
    V["MnO"] = 15.6000
    V["Cr2O3"] = 29.2000
    
    
    # evaluation properties
    eval = np.empty(1, dtype=from_dtype(np.dtype([('CO_extraction', 'f8'), ('CO2_extraction', 'f8'), ('N2_extraction', 'f8'), ('O2_extraction', 'f8'), ('H2_extraction', 'f8'),
                                                  ('H2O_extraction', 'f8'), ('CH4_extraction', 'f8'), ('air_extraction', 'f8'), ('refractory_material', 'f8'), ('a', 'f8'),
                                                  ('b', 'f8'), ('c', 'f8'), ('d', 'f8'), ('e', 'f8'), ('f', 'f8'), ('g', 'f8'), ('h', 'f8'), ('i', 'f8'), ('j', 'f8'),
                                                  ('k', 'f8'), ('l', 'f8'), ('m', 'f8'), ('n_1', 'f8'), ('n_2', 'f8'), ('n_3', 'f8'), ('q', 'f8'), ('r', 'f8'), ('s', 'f8'),
                                                  ('t', 'f8'), ('u', 'f8'), ('v', 'f8'), ('w', 'f8'), ('x', 'f8'), ('z', 'f8'), ('m_resolid_st', 'f8')], align=True)))
    eval = eval[0]
    eval["CO_extraction"] = 0
    eval["CO2_extraction"] = 0
    eval["N2_extraction"] = 0
    eval["O2_extraction"] = 0
    eval["H2_extraction"] = 0
    eval["H2O_extraction"] = 0
    eval["CH4_extraction"] = 0
    eval["air_extraction"] = 0
    eval["refractory_material"] = 0
    eval["a"] = 0
    eval["b"] = 0
    eval["c"] = 0
    eval["d"] = 0
    eval["e"] = 0
    eval["f"] = 0
    eval["g"] = 0
    eval["h"] = 0
    eval["i"] = 0
    eval["j"] = 0
    eval["k"] = 0
    eval["l"] = 0
    eval["m"] = 0
    eval["n_1"] = 0
    eval["n_2"] = 0
    eval["n_3"] = 0
    eval["q"] = 0
    eval["r"] = 0
    eval["s"] = 0
    eval["t"] = 0
    eval["u"] = 0
    eval["v"] = 0
    eval["w"] = 0
    eval["x"] = 0
    eval["z"] = 0
    eval["m_resolid_st"] = 0
    
    
 
    return M, cp, dh, p, c, Q, OpCh, ind, X, W, K, kd, comp, iv, DH_T, QM, V, eval
