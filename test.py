# https://chat.openai.com/share/3ccccf20-39c0-430f-abb6-59f2364270d6
import multiprocessing as mp
import threading
import time
import paho.mqtt.client as mqtt
import sys
import os
import json
import numpy as np
import pickle
import base64

# Append system path to include your custom module directory
sys.path.append('../EAFProsim')
import io_tools as iot
from EAF_start import EAF_parallel
from EAF_save_results import EAF_save_results
from mass_balance import mass_balance

def worker_initializer():
    global start_time
    start_time = time.perf_counter()

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("your/topic/here")  # Subscribe to the topic where jobs are published

def on_message(client, userdata, msg):
    # Assuming the message payload is directly usable as job data
    job_data = json.loads(msg.payload)
    userdata.put(job_data)  # Put the job data into the queue

def do_task(client_id, response_topic, data, process_lock):
    elapsed_time = time.perf_counter() - start_time
    print(f"Worker {mp.current_process().name} started processing after {elapsed_time:.4f} seconds.")
    print(f"Processing {data} for client {client_id}")

def process_tasks(task_queue):
    while True:
        job = task_queue.get()
        if job is None:  # Shutdown signal for the worker
            break
        do_task(*job)

def setup_mqtt_client(task_queue):
    client = mqtt.Client(userdata=task_queue)
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect("mqtt_broker_address", 1883, 60)  # Connect to your MQTT broker
    client.loop_forever()  # Start a network loop

if __name__ == "__main__":
    task_queue = mp.Queue()
    num_processes = 32
    pool = mp.Pool(processes=num_processes, initializer=worker_initializer)

    # Start the MQTT client in a separate thread
    mqtt_thread = threading.Thread(target=setup_mqtt_client, args=(task_queue,))
    mqtt_thread.start()

    # Use workers to process tasks from the queue
    workers = [mp.Process(target=process_tasks, args=(task_queue,)) for _ in range(num_processes)]
    for worker in workers:
        worker.start()

    for worker in workers:
        worker.join()  # This will wait indefinitely unless a specific shutdown condition is sent

    pool.close()
    pool.join()
    mqtt_thread.join()  # Ensure MQTT thread also closes cleanly
