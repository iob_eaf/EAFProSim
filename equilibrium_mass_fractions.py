# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 14:53:40 2021

@author: Alexander Reimann IOB RWTH Aachen
"""
import numpy as np
from cell_model import cell_model
from numba import njit


@njit(cache=True)
def nn_slag_activity(input,NN_weights):
    y_pred = np.zeros(len(input)-1)

    coeff1 = np.reshape(NN_weights[0:640],(8,80))
    coeff2 = np.reshape(NN_weights[640:1200],(80,7))
    biases1 = np.reshape(NN_weights[1200:1280],(80,))
    biases2 = NN_weights[1280:1287]
    L1 = np.maximum(np.sum(coeff1.T * input,1) + biases1,0)
    y_pred = np.sum(coeff2.T * L1,1) + biases2
    y_pred = np.maximum(y_pred,1e-8)
    return y_pred

@njit(cache=True)
def nn_steel_activity(input,NN_weights):
    y_pred = np.zeros(len(input)-1)

    coeff1 = np.reshape(NN_weights[0:720],(9,80))
    coeff2 = np.reshape(NN_weights[720:1360],(80,8))
    biases1 = np.reshape(NN_weights[1360:1440],(80,))
    biases2 = NN_weights[1440:1520]
    L1 = np.maximum(np.sum(coeff1.T * input,1) + biases1,0)
    y_pred = np.sum(coeff2.T * L1,1) + biases2
    y_pred = np.maximum(y_pred,1e-8)
    return y_pred

@njit(cache=True)
def oxygen_saturation(mass_fraction,c_melt_WIP,c_melt_interaction_T,c_equilibrium_constants,Temperature):
    #interaction parameters for current temperature
    melt_inter = c_melt_WIP + c_melt_interaction_T/Temperature
    mass_fraction[7]=0 #set oxygen to zero
    #activity dependence for all other trace elements
    log_f_O=np.dot(melt_inter[7,:],mass_fraction[0:8])
    K_O=np.exp(-c_equilibrium_constants[0,4]-c_equilibrium_constants[1,4]/Temperature)
    
    '''
    # this is the smart way to calculate the equilibrium concentration
    # however numba does not support scipy lambertw function
    # in a future release add manual calculation of the first branch of lambertw
    tr=(10**(-log_f_O))*K_O*melt_inter[7,7]*np.log(10)
    O_sat=np.real(lambertw(tr))/(melt_inter[7,7]*np.log(10))
    '''

    mass_perc_O = np.arange(0,20,0.01)
    # Equilibrium constant at 1 atm O2 partial pressure and different oxygen concentrations
    K_O_test = mass_perc_O*np.power(10,(log_f_O+melt_inter[7,7]*mass_perc_O))
    #mask = np.ones(len(K_O_test))
    #mask[np.where(K_O_test == np.max(K_O_test))[0][0]:] = False
    K_O_test_red = K_O_test[0:np.where(K_O_test == np.max(K_O_test))[0][0]]
    O_sat=mass_perc_O[np.where(np.abs(K_O_test_red-K_O) == min(np.abs(K_O_test_red-K_O)))[0][0]]

    return O_sat


# Wagner interaction parameter formalism (WIPF)
@njit(cache=True)
def steel_activity_WIP(mass_fraction, c_melt_WIP, c_melt_interaction_T, Temperature):
    #interaction parameters for current temperature
    
    melt_inter=c_melt_WIP + c_melt_interaction_T/Temperature; 
    log_f=np.dot(melt_inter,mass_fraction[0:8])
    fact=(10**log_f) # activity coefficients
    melt_activity=fact*mass_fraction[0:8] # activity of solutes (1% weight reference state)
    melt_activity = np.append(melt_activity,1) # activity for Fe
    fact = np.append(fact,1)
    #print ("fact", fact, "melt_activity", melt_activity, "log_f", log_f, "mass_fraction_melt", mass_fraction_melt, "melt_inter", melt_inter)
    #numpy ist row major --> berechnung nochmal umstellen für geschwindigkeit
    #rearrange order: --> slow
    melt_activity=melt_activity[np.array([4,1,3,5,8,2,6,0,7])]
    fact=fact[np.array([4,1,3,5,8,2,6,0,7])]

    return melt_activity, fact


#unified interaction parameter formalism (UIP)
@njit(cache=True)
def steel_activity_UIP(mass_fraction, c_melt_UIP, c_melt_interaction_T_UIP, c_ln_gamma0, c_ln_gamma0_T, MM, Temperature):
    # interaction parameters for current temperature
    melt_inter = c_melt_UIP + c_melt_interaction_T_UIP/Temperature
    # infinite dilution activity parameters for current temperature
    ln_gamma0 = c_ln_gamma0 + c_ln_gamma0_T/Temperature
    mole_fraction = mass_fraction/MM
    mole_fraction =(mole_fraction/np.sum(mole_fraction)) # convert mass to mole fractions
    mole_fraction_Fe = mole_fraction[8]
    ln_gamma = np.dot(melt_inter,mole_fraction[0:8]) 
    #mole_fraction = mole_fraction_1D[...,np.newaxis]
    ln_Fe = -0.5*np.sum(np.dot(((melt_inter*mole_fraction[0:8]).T),mole_fraction[0:8]))    
    ln_gamma = (ln_gamma+ln_gamma0+ln_Fe) # solutes
    gamma = np.exp(ln_gamma); # activity coefficients of solutes
    melt_activity = gamma*mole_fraction[0:8] #activity of solutes (pure liquid reference state) 
    melt_activity = melt_activity/MM[8]/np.exp(ln_gamma0)*MM[0:8]*100; # activity of solutes 1% weight reference state
    melt_activity = np.append(melt_activity,np.exp(ln_Fe)*mole_fraction_Fe*100) #activity for Fe

    fact=melt_activity/(mass_fraction) # activity coefficients with 1% weight reference state
    fact[np.where(mass_fraction == 0)[0]] = 1

    melt_activity=melt_activity[np.array([4,1,3,5,8,2,6,0,7])]
    fact=fact[np.array([4,1,3,5,8,2,6,0,7])]
    return melt_activity,fact
    #P Si Cr Al Fe Mn S C O

@njit(cache=True)
def rs_model(mole_fraction_slag, c_slag_correction, c_slag_interaction, c_slag_interaction2, c_R_gas, Temperature):
    #Ca2        Mg2       Al3        Si4       Mn2      Cr3       P5       Fe2 
    slag_mole_frac = mole_fraction_slag[np.array([6,7,3,1,5,2,0,4])]*np.array([1, 1, 2, 1, 1, 2, 2, 1])
    slag_mole_frac=slag_mole_frac/np.sum(slag_mole_frac)  # molar fraction of cations in slag

    # activities in steel and slag and equilibrium constants
    ln_g = (c_slag_correction[0,:]+c_slag_correction[1,:]*Temperature).T  # correction energy for regular solution state (compared to solid reference state)
    ln_g = ln_g + np.dot(c_slag_interaction,(slag_mole_frac**2)) # sum Xj^2 * aij
                   
    slag_mole_matrix = slag_mole_frac*slag_mole_frac.reshape(8, 1)
    nx = slag_mole_frac.size*slag_mole_frac.size
    ln_g = ln_g + np.dot(c_slag_interaction2,slag_mole_matrix.reshape(nx,1)).T # sum XkXj *(aij + aik -akj)   
    gamma=np.exp(ln_g/c_R_gas/Temperature) # slag activities
    slag_act=gamma[0]*slag_mole_frac # sort activities
    
    #rearrange back into original order
    slag_act = slag_act[np.array([6,3,5,2,7,4,1,0])]
    return slag_act 


@njit(cache=True)
def equilibrium_mass_fractions(y, c, ind, X, W, M):

    T_liquid_scrap = y[ind["T_st_liq"]]
    r_P		= y[ind["p_furnace"]]	#relative pressure inside the EAF
    p_CO    = (X["CO"]*(c["p_ambient"]+r_P))/1.013125e5 #partial pressure CO in atm
    #calculate activities
    
    stoech_fact = np.zeros((3,7),dtype=np.float64)
    stoech_fact[0] = [2, 1, 2, 2, 1, 1, 1]
    stoech_fact[1] = [5, 2, 3, 3, 1, 1, 1]
    stoech_fact[2] = [5/2, 2/1, 3/2, 3/2, 1, 1, 1]
    
    #molar mass of melt species
    MM_st = np.array([M["C"],M["Si"],M["Mn"],M["Cr"],M["P"],M["Al"],M["S"],M["O"],M["Fe"]])
    #composition of melt for activity calculation
    mass_fraction_melt = np.array([W["C"],W["Si"],W["Mn"],W["Cr"],W["P"],W["Al"],W["S"],W["O"],W["Fe"]])

    #composition of interphase for activity calculation
    mass_fraction_inter = np.array([W["C_reac"],W["Si_reac"],W["Mn_reac"],W["Cr_reac"],W["P_reac"],W["Al_reac"],W["S_reac"],W["O_reac"],W["Fe_reac"]])
 
    #carbon mass percent at carbon saturation (Deo 1993, chapter 2)
    C_sat=0.56+2.57e-3*T_liquid_scrap
    #oxygen mass percent at oxygen saturation (equilibrium with FeO)
    O_sat=oxygen_saturation(mass_fraction_melt, c["melt_WIP"], c["melt_interaction_T"], c["equilibrium_constants"], T_liquid_scrap)
    #composition of melt wit oxygen at saturation for activity calculation at oxygen injection site
    mass_fraction_melt[7] = O_sat   


    #molar mass of slag species
    #original sorting "P","Si","Cr","Al","Fe","Mn","S","C","O"
    #MM_sl = np.array([M["CaO"],M["MgO"],M["Al2O3"],M["SiO2"],M["MnO"],M["Cr2O3"],M["P2O5"],M["FeO"]])
    #composition of slag for activity calculation
    mass_fraction_slag = np.array([W["CaO"],W["MgO"],W["Al2O3"],W["SiO2"],W["MnO"],W["Cr2O3"],W["P2O5"],W["FeO"]])
    #composition of slag for activity calculation (CaS treated as CaO)
    mass_fraction_slag[0] = mass_fraction_slag[0] + W["CaS"] * M["CaS"]/M["CaO"]
    

    mole_fraction_slag = np.array([X["P2O5"],X["SiO2"],X["Cr2O3"],X["Al2O3"],X["FeO"],X["MnO"],X["CaO"],X["MgO"]])
    mole_fraction_slag[6] = mole_fraction_slag[6] + X["CaS"]

    if c["slag_model"] == 1:
        slag_act = cell_model(mole_fraction_slag, c["R_gas"], T_liquid_scrap) # activities of slag species form cell model
        equi_factor = c["equilibrium_factor_cell"].copy() #  correction of equilibrium
    elif c["slag_model"] == 2:
        # experimental model DO NOT USE WHEN RUNNING CALCULATIONS
        # P2O5 not considered!
        mole_fraction_slag_nn = mole_fraction_slag
        mole_fraction_slag_nn = mole_fraction_slag_nn/np.sum(mole_fraction_slag_nn)

        input = np.zeros(8)
        input[0] = T_liquid_scrap
        input[1:] = mole_fraction_slag_nn[1:]
        input = (input - c["NN_x_min_max_slag"][0]) / (c["NN_x_min_max_slag"][1]-c["NN_x_min_max_slag"][0])

        slag_act = np.zeros(8)
        slag_act[1:] = nn_slag_activity(input,c["NN_slag_act"]) # activities of slag species form cell model

        # P2O5 from rs model
        slag_act_rs = rs_model(mole_fraction_slag, c["slag_correction"], c["slag_interaction"], c["slag_interaction2"], c["R_gas"], T_liquid_scrap) # activities of slag species from regular solution modelc["equi_factor=c["equi_factor_cell
        slag_act[0] = slag_act_rs[0]
        #slag_act[0] = 0
        
        equi_factor = np.ones(len(c["equilibrium_factor_RS"]))
        equi_factor[0] = c["equilibrium_factor_RS"][0]

    else:
        slag_act = rs_model(mole_fraction_slag, c["slag_correction"], c["slag_interaction"], c["slag_interaction2"], c["R_gas"], T_liquid_scrap) # activities of slag species from regular solution modelc["equi_factor=c["equi_factor_cell
        equi_factor = c["equilibrium_factor_RS"].copy() #  correction of equilibrium

    
    #slag_act = rs_model(mass_fraction_slag,MM_sl, c["slag_correction"], c["slag_interaction"], c["slag_interaction2"], c["R_gas"], T_liquid_scrap)
    #equi_factor = np.array([1,1,1,1,1,1,1,1])
    #equi_factor = c["equilibrium_factor_RS"]
    
    #rearrange slag activity to match DGL
    #activities in slag Si Mn Cr P Fe Ca Al
    #slag_act_sorted = slag_act[[3, 4, 5, 6, 7, 0, 2]]

    #c["melt_model"] = 1
    if c["melt_model"] == 1:
        [melt_act,f] = steel_activity_UIP(mass_fraction_melt, c["melt_UIP"], c["melt_interaction_T_UIP"], c["ln_gamma0"], c["ln_gamma0_T"], MM_st, T_liquid_scrap); # activity of melt species from UIP
        [melt_act_inter,f_inter] = steel_activity_UIP(mass_fraction_inter, c["melt_UIP"], c["melt_interaction_T_UIP"], c["ln_gamma0"], c["ln_gamma0_T"], MM_st, T_liquid_scrap); # activity of melt species at interface from UIP 
    # elif c["melt_model == 2:
    #     [melt_act,f]=steel_activity_Eps(mass_fraction_melt,inp,T_liquid_scrap); # activity of melt species from EPS
    #     [melt_act_inter,f_inter]=steel_activity_Eps(mass_fraction_inter,inp,T_liquid_scrap); # activity of melt species at interface from EPS
    elif c["melt_model"] == 3: 
        [melt_act_P,f_P] = steel_activity_UIP(mass_fraction_melt, c["melt_UIP"], c["melt_interaction_T_UIP"], c["ln_gamma0"], c["ln_gamma0_T"], MM_st, T_liquid_scrap); # activity of melt species from UIP
        [melt_act_inter_P,f_inter_P] = steel_activity_UIP(mass_fraction_inter, c["melt_UIP"], c["melt_interaction_T_UIP"], c["ln_gamma0"], c["ln_gamma0_T"], MM_st, T_liquid_scrap); # activity of melt species at interface from UIP 
        
        input = np.zeros(9)
        input[0] = T_liquid_scrap
        #np.array([W["C"],W["Si"],W["Mn"],W["Cr"],W["P"],W["Al"],W["S"],W["O"],W["Fe"]])
        #mole_fraction_steel = np.array([X["Fe"],X["Si"],X["Cr"],X["Al"],X["Mn"],X["S"],X["C"],X["O"]])
        mole_fraction_steel = mass_fraction_melt/MM_st
        mole_fraction_steel =(mole_fraction_steel/np.sum(mole_fraction_steel)) # convert mass to mole fractions
        input[1:] = mole_fraction_steel[np.array([8,1,3,5,2,6,0,7])]
        input = (input - c["NN_x_min_max_steel"][0]) / (c["NN_x_min_max_steel"][1]-c["NN_x_min_max_steel"][0])
        melt_act = nn_steel_activity(input,c["NN_steel_act"])
        #melt_act = melt_act[np.array([0,6,2,3,0,4,5,1,7])]*100
        melt_act = (melt_act * (c["NN_y_min_max_steel"][1]-c["NN_y_min_max_steel"][0]) + c["NN_y_min_max_steel"][0]) * 100
        melt_act = np.append(melt_act_P[0],melt_act)
        #mass_fraction_steel = np.array([W["P"],W["Si"],W["Cr"],W["Al"],W["Fe"],W["Mn"],W["S"],W["C"],W["O"]])#/100
        f = melt_act/mass_fraction_melt[np.array([4,1,3,5,8,2,6,0,7])]
        f[np.where(mass_fraction_melt[np.array([4,1,3,5,8,2,6,0,7])] <= 0)[0]] = 1

        input = np.zeros(9)
        input[0] = T_liquid_scrap
        #mole_fraction_reac = np.array([X["Fe_reac"],X["Si_reac"],X["Cr_reac"],X["Al_reac"],X["Mn_reac"],X["S_reac"],X["C_reac"],X["O_reac"]])
        mole_fraction_inter = mass_fraction_inter/MM_st
        mole_fraction_inter =(mole_fraction_inter/np.sum(mole_fraction_inter)) # convert mass to mole fractions
        input[1:] = mole_fraction_inter[np.array([8,1,3,5,2,6,0,7])]
        input = (input - c["NN_x_min_max_steel"][0]) / (c["NN_x_min_max_steel"][1]-c["NN_x_min_max_steel"][0])
        melt_act_inter = nn_steel_activity(input,c["NN_steel_act"])
        #melt_act_inter = melt_act_inter[np.array([0,6,2,3,0,4,5,1,7])]*100
        melt_act_inter = (melt_act_inter * (c["NN_y_min_max_steel"][1]-c["NN_y_min_max_steel"][0]) + c["NN_y_min_max_steel"][0]) * 100
        #melt_act_inter[0] = melt_act_inter_P[0]
        melt_act_inter = np.append(melt_act_inter_P[0],melt_act_inter)
        #mass_fraction_inter = np.array([W["P_reac"],W["Si_reac"],W["Cr_reac"],W["Al_reac"],W["Fe_reac"],W["Mn_reac"],W["S_reac"],W["C_reac"],W["O_reac"]])#/100
        f_inter = melt_act_inter/mass_fraction_inter[np.array([4,1,3,5,8,2,6,0,7])]
        f_inter[np.where(mass_fraction_inter[np.array([4,1,3,5,8,2,6,0,7])] <= 0)[0]] = 1

    else:
        [melt_act,f] = steel_activity_WIP(mass_fraction_melt, c["melt_WIP"], c["melt_interaction_T"], T_liquid_scrap); # activity of melt species from WIPF
        [melt_act_inter,f_inter] = steel_activity_WIP(mass_fraction_inter, c["melt_WIP"], c["melt_interaction_T"], T_liquid_scrap); # activity of melt species at interface from WIPF


    #rearrange melt activity to match DGL
    # P Si Cr Al Fe Mn S C O
    #melt_act_sorted = melt_act[[4, 1, 3, 5, 8, 2, 6, 0, 7]]
    #f_sorted = f[[4, 1, 3, 5, 8, 2, 6, 0, 7]]

    equi_constants = c["equilibrium_constants"][0] + c["equilibrium_constants"][1]/T_liquid_scrap
    equi_constants = np.exp(equi_constants) # equilibrium constants for current temperature

    #calculate equillibrium melt concentrations
    #melt_act(9)=melt_act(9)+1e-10; % avoid division by zero
    #equilibrium mass fractions (%) in melt at interface
    #if (melt_act_inter[8],stoech_fact[2,0] < 0):
    #    W["P_eq = (slag_act[0]/equi_constants[0]/(melt_act_inter[8]**stoech_fact[2,0]))/f_inter[0]/equi_factor[0]
    

    #prevent division by zero 
    #if melt_act[8]<=0.001:#==0:
    #    melt_act[8] = 0.001
    #if melt_act_inter[8]<=0.001:#==0:
    #    melt_act_inter[8] = 0.001
    
    melt_act[8] = np.max(np.array([melt_act[8],0.001]))
    melt_act_inter[8] = np.max(np.array([melt_act_inter[8],0.001]))


    W["P_eq"] = (slag_act[0]/equi_constants[0]/(melt_act_inter[8]**stoech_fact[2,0]))/f_inter[0]/equi_factor[0]
    W["Si_eq"] = (slag_act[1]/equi_constants[1]/(melt_act_inter[8]**stoech_fact[2,1]))/f_inter[1]/equi_factor[1]
    W["Cr_eq"] = (slag_act[2]/equi_constants[2]/(melt_act_inter[8]**stoech_fact[2,2]))/f_inter[2]/equi_factor[2]
    #W.Al_eq=(slag_act(4)/equi_constants(4)/melt_act(9)^stoech_fact(3,4))/f(4)/c["equi_factor(4);
    W["Fe_eq"] = (slag_act[4]/equi_constants[4]/(melt_act_inter[8]**stoech_fact[2,4]))/f_inter[4]/equi_factor[4]
    W["Mn_eq"] = (slag_act[5]/equi_constants[5]/(melt_act_inter[8]**stoech_fact[2,5]))/f_inter[5]/equi_factor[5]
    W["S_eq"] = W["CaS"]/M["CaS"]*melt_act_inter[8]/equi_constants[6]/(mass_fraction_slag[0]/M["CaO"])/f_inter[6]/equi_factor[6]
    W["C_eq"] = p_CO/equi_constants[7]/(melt_act_inter[8]**stoech_fact[2,6])/f_inter[7]/equi_factor[7]
    
    #melt_act(10)=melt_act(9); % save for testing;
    #equilibrium mass fractions (%) in melt for zone of oxygen injection

    #influence on other melt activities from increased oxygen not considered
    W["P_eq_O"] = (slag_act[0]/equi_constants[0]/(melt_act[8]**stoech_fact[2,0]))/f[0]/equi_factor[0]
    W["Si_eq_O"] = (slag_act[1]/equi_constants[1]/(melt_act[8]**stoech_fact[2,1]))/f[1]/equi_factor[1]
    W["Cr_eq_O"] = (slag_act[2]/equi_constants[2]/(melt_act[8]**stoech_fact[2,2]))/f[2]/equi_factor[2]
    #W.Al_eq_O=(slag_act(4)/equi_constants(4)/melt_act(9)^stoech_fact(3,4))/f(4)/c["equi_factor(4);
    W["Fe_eq_O"] = (slag_act[4]/equi_constants[4]/(melt_act[8]**stoech_fact[2,4]))/f[4]/equi_factor[4]
    W["Mn_eq_O"] = (slag_act[5]/equi_constants[5]/(melt_act[8]**stoech_fact[2,5]))/f[5]/equi_factor[5]
    #W.C_eq_O=p_CO/equi_constants(8)/melt_act(9)^stoech_fact(3,7)/f(8)/c["equi_factor(8);
    W["C_eq_O"] = 1/equi_constants[7]/(melt_act[8]**stoech_fact[2,6])/f[7]/equi_factor[7] # CO partial pressure at carbon reaction zone assumed to be one atmosphere


    #equilibrium mass fractions (%) in melt for zone of carbon injection
    #melt activity coefficients are assumed to be equal to interface zone 
    
    melt_act[8] = ((1/equi_constants[7]/f_inter[7]/C_sat/equi_factor[7])**(1/stoech_fact[2,6])) # oxygen activity assuming CO partial pressure of 1 atm (CO buble in slag) and carbon activity of 1 (carbon saturation from carbon particle in buble)
    W["P_eq_C"] = (slag_act[0]/equi_constants[0]/(melt_act[8]**stoech_fact[2,0]))/f_inter[0]/equi_factor[0]
    W["Si_eq_C"] = (slag_act[1]/equi_constants[1]/(melt_act[8]**stoech_fact[2,1]))/f_inter[1]/equi_factor[1]
    W["Cr_eq_C"] = (slag_act[2]/equi_constants[2]/(melt_act[8]**stoech_fact[2,2]))/f_inter[2]/equi_factor[2]
    #W.Al_eq_C=(slag_act(4)/equi_constants(4)/melt_act(9)^stoech_fact(3,4))/f(4)/c["equi_factor(4);
    W["Fe_eq_C"] = min(100,(slag_act[4]/equi_constants[4]/(melt_act[8]**stoech_fact[2,4]))/f_inter[4]/equi_factor[4])
    W["Mn_eq_C"] = (slag_act[5]/equi_constants[5]/(melt_act[8]**stoech_fact[2,5]))/f_inter[5]/equi_factor[5]

    #constraints (reactions only allowed in one direction
    W["C_eq"] = min(W["C_eq"],W["C"]) # no carburization of melt from CO gas
    W["C_eq_O"] = min(W["C_eq_O"],W["C"])
    W["Si_eq_O"] = min(W["Si_eq_O"],W["Si"]) # no backwards reaction in zone of oxygen injection (not necessarily in contact with slag)
    W["Cr_eq_O"] = min(W["Cr_eq_O"],W["Cr"])
    W["Mn_eq_O"] = min(W["Mn_eq_O"],W["Mn"])
    W["Fe_eq_O"] = min(W["Fe_eq_O"],W["Fe"])
    W["P_eq_O"] = min(W["P_eq_O"],W["P"])

# Epsilon Formalism
@njit(cache=True) #, fastmath=True
def melt_act_sorted(mass_fraction_melt, c_melt_UIP, c_melt_interaction_T_UIP, c_ln_gamma0, c_ln_gamma0_T, MM ,T_liquid_scrap):
    melt_inter = c_melt_UIP + c_melt_interaction_T_UIP /T_liquid_scrap    
    ln_gamma0 = c_ln_gamma0 + c_ln_gamma0_T /T_liquid_scrap 

    #MM = M.get_fields(["C","Si","Mn","Cr","P","Al","S","O","Fe"]).T 
    mass_fraction_melt = np.add(mass_fraction_melt,[1e-10,1e-10,1e-10,1e-10,1e-10,1e-10,1e-10,1e-10,1e-10]) 
    mole_fraction = mass_fraction_melt/MM.T
    mole_fraction=(mole_fraction/np.sum(mole_fraction)) 
    mole_fraction_Fe = mole_fraction[8] 
    log_min_mole = np.log(1-mole_fraction) 

    sum1=np.zeros(5)  
    for i in range(0,len(melt_inter)): 
        sum1= sum1 + melt_inter[(i,i)]*(mole_fraction[i]+log_min_mole[i])
        for k in range(0,len(melt_inter)): 
            if k==i:
                continue
            else:
                sum1[2]=sum1[2]+melt_inter[(i,k)]*mole_fraction[i]*mole_fraction[k]*(1+log_min_mole[k]/mole_fraction[k]+1/(1-mole_fraction[k]))
                sum1[4]=sum1[4]+melt_inter[(i,k)]*np.square(mole_fraction[i])*np.square(mole_fraction[k])*(1/(1-mole_fraction[i])+1/(1-mole_fraction[k])+mole_fraction[i]/(2*np.square(1-mole_fraction[i])-1))
    sum1[4]=-sum1[4] 
    for j in range(0,(len(melt_inter)-1)):
        for k in range((j+1),len(melt_inter)): 
            sum1[1]=sum1[1]+melt_inter[(j,k)]*mole_fraction[j]*mole_fraction[k]*(1+log_min_mole[j]/mole_fraction[j]+log_min_mole[k]/mole_fraction[k])
            sum1[3]=sum1[3]+melt_inter[(j,k)]*np.square(mole_fraction[j])*np.square(mole_fraction[k])*(1/(1-mole_fraction[j])+1/(1-mole_fraction[k])-1)

    sum1[1]= -sum1[1]
    sum1[3]= 0.5*sum1[3]
    ln_Fe=np.sum(sum1)

    ln_gamma=np.zeros(len(melt_inter)) 
    for i in  range(0,len(melt_inter)):
        ln_gamma[i]=ln_Fe+ln_gamma0[i]-melt_inter[i,i]*log_min_mole[i]
        for k in range(0,len(melt_inter)):
            if k==i:
                continue
            else:
                ln_gamma[i]=ln_gamma[i]-melt_inter[(i,k)]*mole_fraction[k]*(1+log_min_mole[k]/mole_fraction[k]-1/(1-mole_fraction[i]))
                ln_gamma[i]=ln_gamma[i]+melt_inter[(i,k)]*np.square(mole_fraction[k])*mole_fraction[i]*(1/(1-mole_fraction[i])+1/(1-mole_fraction[k])+0.5*mole_fraction[i]/np.square(1-mole_fraction[i])-1)
    gamma= np.exp(ln_gamma) 

    melt_activity = gamma*mole_fraction[0:8] 

    melt_activity = melt_activity/MM[8]/np.exp(ln_gamma0)*MM[0:8]*100 
    melt_activity= np.append(melt_activity,(np.exp(ln_Fe)*mole_fraction_Fe*100))
    fact=melt_activity/mass_fraction_melt

    
    fact[np.where((mass_fraction_melt) <= 0.0001)] = 1  #Korrektur?

    idx= [4,1,3,5,8,2,6,0,7]
    melt_activity = melt_activity[idx]

    fact=fact[idx]
    return[melt_activity,fact]





# %% internal functions to calculate melt activities

# % Epsilon formalism
#     function [melt_act_sorted,f_sorted]=steel_activity_Eps(mass_perc,c,T,M)
#         melt_inter=c["melt_inter_temp2+c["melt_inter_T2./T; % interaction parameters for current temperature
#         ln_gamma0=c["ln_gamma0_temp+c["ln_gamma0_temp_T/T; % infinite dilution activity parameters for current temperature
#         mass_perc=mass_perc+1e-10; % avoid division by 0
#         mole_fraction=mass_perc["/[M.C M.Si M.Mn M.Cr M.P M.Al M.S M.O2/2 M.Fe];
#         mole_fraction=mole_fraction/sum(mole_fraction); % convert mass to mole fractions
#         mole_fraction_Fe=mole_fraction(9);
#         mole_fraction(9)=[];
#         log_min_mole=log(1-mole_fraction);
        
#         sum1=zeros(1,5);
#         for i=1:length(melt_inter) % factors for activity of solvent
#             sum1=sum1+melt_inter(i,i)*(mole_fraction(i)+log_min_mole(i));
#             for k=1:length(melt_inter)
#                 if k==i
#                     continue
#                 else
#                     sum1(3)=sum1(3)+melt_inter(i,k)*mole_fraction(i)*mole_fraction(k)*(1+log_min_mole(k)/mole_fraction(k)+1/(1-mole_fraction(k)));
#                     sum1(5)=sum1(5)+melt_inter(i,k)*mole_fraction(i)^2*mole_fraction(k)^2*(1/(1-mole_fraction(i))+1/(1-mole_fraction(k))+mole_fraction(i)/(2*(1-mole_fraction(i))^2)-1);
#                 end
#             end
#         end
#         sum1(5)=-sum1(5);
#         for j=1:length(melt_inter-1) % factors for activity of solvent
#             for k=i+1:length(melt_inter)
#                 sum1(2)=sum1(2)+melt_inter(j,k)*mole_fraction(j)*mole_fraction(k)*(1+log_min_mole(j)/mole_fraction(j)+log_min_mole(k)/mole_fraction(k));
#                 sum1(4)=sum1(4)+melt_inter(j,k)*mole_fraction(j)^2*mole_fraction(k)^2*(1/(1-mole_fraction(j))+1/(1-mole_fraction(k))-1);
#             end
#         end
#         sum1(2)=-sum1(2);
#         sum1(4)=0.5*sum1(4);
#         ln_Fe=sum(sum1); % solvent activity
        
#         ln_gamma=zeros(1,length(melt_inter));
#         for i=1:length(melt_inter) % activity of solutes
#             ln_gamma(i)=ln_Fe+ln_gamma0(i)-melt_inter(i,i)*log_min_mole(i);
#             for k=1:length(melt_inter)
#                 if k==i
#                     continue
#                 else
#                     ln_gamma(i)=ln_gamma(i)-melt_inter(i,k)*mole_fraction(k)*(1+log_min_mole(k)/mole_fraction(k)-1/(1-mole_fraction(i)));
#                     ln_gamma(i)=ln_gamma(i)+melt_inter(i,k)*mole_fraction(k)^2*mole_fraction(i)*(1/(1-mole_fraction(i))+1/(1-mole_fraction(k))+0.5*mole_fraction(i)/(1-mole_fraction(i))^2-1);
#                 end
#             end
#         end
        
#         gamma=exp(ln_gamma); % activity coefficients of solutes
#         melt_activity=gamma'.*mole_fraction'; % activity of solutes (pure liquid reference state)
#         melt_activity=melt_activity/M.Fe./exp(ln_gamma0)'.*[M.C M.Si M.Mn M.Cr M.P M.Al M.S M.O2/2]'*100; % activity of solutes 1% weight reference state
#         melt_activity(9)=exp(ln_Fe)*mole_fraction_Fe*100; % activity for Fe
#         fact=melt_activity./mass_perc'; % activity coefficients with 1% weight reference state
#         fact(melt_activity==0)=1;
        
#         melt_act_sorted=melt_activity([5 2  4  6  9  3 7 1 8]);
#         % P Si Cr Al Fe Mn S C O
#         f_sorted=fact([5 2 4 6 9 3 7 1 8]);
#         end
