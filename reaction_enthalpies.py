# -*- coding: utf-8 -*-
"""
Created on Fri Apr 16 14:38:08 2021

@author: hay
"""
from numba import njit

@njit(cache=True)
def reaction_enthalpies(DH_T,M,dh,cp,ind,c,iv,comp,y,dy,x,K_FeO_CO2):
   # Enthalpy from chemical reactions
    # KW; positive value is energy released (exhothermic reactions)
    
    # 1(a)  Fe + 1/2 O2 --> FeO             Fe oxidation
    # 2(b)  FeO + C --> Fe + CO             FeO reduction with carbon
    # 3(c)  Si + O2 --> SiO2                Si oxidation
    # 4(d)  SiO2 + C --> Si + 2CO           SiO2 reduction with carbon
    # 5(e)  Mn + 1/2 O --> MnO              Mn oxidation
    # 6(f)  MnO + C --> Mn + CO             MnO reduction with carbon
    # 7(g)  C + 1/2 O2 --> CO               C oxidation from coal and dissolved carbon
    # 8(h)  CO + 1/2 O2 --> CO2             CO oxidation in gas phase
    # 9(i)  C + O2 --> CO2                  C oxidation to CO2 dissolved carbon
    # 10(j) 2Cr + 1.5 O2 --> Cr2O3          Cr oxidation
    # 11(k) Cr2O3 + 3C   --> 2Cr + 3CO      Cr2O3 reduction with carbon
    # 12(l) 2P + 2.5O2--> P2O5              P oxidation
    # 13(m) P2O5 + 5C--> 2P + 5CO           P2O5 reduction with carbon
    # 14(n) CH4 + 2 O2 --> CO2 + 2 H2O      CH4 combustion       
    # 15(o) C+1/2 O2 --> C0                 electrode oxidation
    # 16(p) C9H20 --> 9 C + 10 H2           dissociation of combustibles
    # 17(q) CO + H2O --> CO2 + H2           water gas shift homogenous
    # 18(r) C + CO2 --> 2CO                 boduard 
    # 19(s) C + H2O --> CO + H2             water gas shit heterogenous
    # 20(t) H2 + 1/2 O2 --> H2O             hydrogen combustion
    # 21(u) S + CaO --> CaS + O             sulphur slag reaction
    # 22(v) 2CO --> CO2 + C                 correction for reduction of slag not by C but CO (fraction of lanced coal reacting to CO2 instead of CO)
    # 23(w) 2Fe + O2 --> 2FeO               reaction in burner flame
    # 24(x) Fe + CO2 --> FeO + CO           reaction in burner flame
    # 25(y) Gasification of volatile coal components
    # 26(z) H2O --> H2 + 1/2 O2             dissociation of water 
    # enthalpy released through reactions [kW]
    
    DH_T["a"] = (x[7][1]+x[7][2])/M["Fe"]*(dh["FeO"]-dh["Fe"]+cp["FeO"]*(y[ind["T_sl_liq"]]-c["T_ref"])-(cp["Fe"]+0.5*cp["O2"])*(y[ind["T_st_liq"]]-c["T_ref"]))                           # Fe + O = FeO
     
    DH_T["b"] = -x[7][3]/M["Fe"]*(dh["Fe"]+dh["CO"]-dh["C"]-dh["FeO"]+cp["Fe"]*(y[ind["T_st_liq"]]-c["T_ref"])+cp["CO"]*(y[ind["T_gas"]]-c["T_ref"])- 
                                cp["C"]*(y[ind["T_st_liq"]]-c["T_ref"])-cp["FeO"]*(y[ind["T_sl_liq"]]-c["T_ref"]))                                                                     # FeO + C = Fe + CO 
     
    DH_T["c"] = (x[3][1]+x[3][2])/M["Si"]*(dh["SiO2"]-dh["Si"]+cp["SiO2"]*(y[ind["T_sl_liq"]]-c["T_ref"])-(cp["Si"]+cp["O2"])*(y[ind["T_st_liq"]]-c["T_ref"]))                      # Si + O = SiO2
     
    DH_T["d"] = -x[3][3]/M["Si"]*(dh["Si"]+2*dh["CO"]-2*dh["C"]-dh["SiO2"]+cp["Si"]*(y[ind["T_st_liq"]]-c["T_ref"])+2*cp["CO"]*(y[ind["T_gas"]]-c["T_ref"])-2* 
                                cp["C"]*(y[ind["T_st_liq"]]-c["T_ref"])-cp["SiO2"]*(y[ind["T_sl_liq"]]-c["T_ref"]))                                                                 # SiO2 + C = Si + 2CO 
    
    DH_T["e"] = (x[4][1]+x[4][2])/M["Mn"]*(dh["MnO"]-dh["Mn"]+cp["MnO"]*(y[ind["T_sl_liq"]]-c["T_ref"])-(cp["Mn"]+0.5*cp["O2"])*(y[ind["T_st_liq"]]-c["T_ref"]))                      # Mn + O = MnO
    
    DH_T["f"] = -x[4][3]/M["Mn"]*(dh["Mn"]+dh["CO"]-dh["C"]-dh["MnO"]+cp["Mn"]*(y[ind["T_st_liq"]]-c["T_ref"])+cp["CO"]*(y[ind["T_gas"]]-c["T_ref"])- 
                                cp["C"]*(y[ind["T_st_liq"]]-c["T_ref"])-cp["MnO"]*(y[ind["T_sl_liq"]]-c["T_ref"]))                                                                   # MnO + C = Mn + CO 
    
    DH_T["g"] = (x[1][1]+x[2][1]+x[2][2]-iv["C_conv_gas"])/M["C"]*(dh["CO"]-dh["C"]+cp["CO"]*(y[ind["T_gas"]]-c["T_ref"]))- \
                    (x[1][1]-iv["C_conv_gas"])/M["C"]*(cp["C"]*(c["T_ambient"]-c["T_ref"])+0.5*cp["O2"]*(y[ind["T_gas"]]-c["T_ref"]))- \
                        (x[2][1]+x[2][2])/M["C"]*(cp["C"]*+0.5*cp["O2"])*(y[ind["T_st_liq"]]-c["T_ref"])                                                                               # C + 1/2O2 = CO
     
    DH_T["h"] = (x[9][3])/M["CO"]*(dh["CO2"]-dh["CO"]+(cp["CO2"]-cp["CO"]-0.5*cp["O2"])*(y[ind["T_gas"]]-c["T_ref"]))                                                              # CO + 1/2O2 = CO2
    
    DH_T["i"] = x[2][5]/M["C"]*((dh["CO2"]-dh["C"])+(cp["CO2"]*(y[ind["T_gas"]]-c["T_ref"])-(cp["C"]+cp["O2"])*(y[ind["T_st_liq"]]-c["T_ref"])))                                 # C + O2 = CO2
     
    DH_T["j"] = (x[5][1]+x[5][2])/M["Cr"]*(dh["Cr2O3"]-2*dh["Cr"]+cp["Cr2O3"]*(y[ind["T_sl_liq"]]-c["T_ref"])-(2*cp["Cr"]+1.5*cp["O2"])*(y[ind["T_st_liq"]]-c["T_ref"]))       # 2Cr + 3/2 O2 = Cr2O3
    
    DH_T["k"] = -x[5][3]/M["Cr"]*(2*dh["Cr"]+3*dh["CO"]-3*dh["C"]-dh["Cr2O3"]+2*dh["Cr"]*(y[ind["T_st_liq"]]-c["T_ref"])+3*cp["CO"]*(y[ind["T_gas"]]-c["T_ref"])- \
                                3*cp["C"]*(y[ind["T_st_liq"]]-c["T_ref"])-cp["Cr2O3"]*(y[ind["T_sl_liq"]]-c["T_ref"]))                                                                # Cr2O3 + C = 2Cr + 3CO 
                 
    DH_T["l"] = (x[6][1]+x[6][2])/M["P"]*(dh["P2O5"]-2*dh["P"]+cp["P2O5"]*(y[ind["T_sl_liq"]]-c["T_ref"])-(2*cp["P"]+2.5*cp["O2"])*(y[ind["T_st_liq"]]-c["T_ref"]))            # 2P + 5/2 O2 = P2O5
    
    DH_T["m"] = -x[6][3]/M["P"]*(2*dh["P"]+5*dh["CO"]-5*dh["C"]-dh["P2O5"]+2*cp["P"]*(y[ind["T_st_liq"]]-c["T_ref"])+5*cp["CO"]*(y[ind["T_gas"]]-c["T_ref"])- \
                                   5*cp["C"]*(y[ind["T_st_liq"]]-c["T_ref"])-cp["P2O5"]*(y[ind["T_sl_liq"]]-c["T_ref"]))                                                              # P2O5 + C = 2P + 5CO
    
    DH_T_n_1 = x[15][4]/M["CH4"]*((dh["CO2"]+2*dh["H2O"]-dh["CH4"])+(cp["CO2"]+2*cp["H2O_gas"]-cp["CH4"]-2*cp["O2"])*(y[ind["T_gas"]]-c["T_ref"]))                                  # CH4 + 2O2 = CO2 + 2H2O    
    DH_T_n_2= (x[15][5]+x[15][8])/M["CH4"]*((dh["CO"]+2*dh["H2O"]-dh["CH4"])+(cp["CO"]+2*cp["H2O_gas"]-cp["CH4"]-3/2*cp["O2"])*(y[ind["T_gas"]]-c["T_ref"]))                       # CH4 + 3/2 O2 = CO + H2O    
    DH_T_n_3 = x[15][6]/M["CH4"]*((dh["CO2"]-dh["CH4"])+(cp["CO2"]+2*cp["H2"]-cp["CH4"]-cp["O2"])*(y[ind["T_gas"]]-c["T_ref"]))                                                    # CH4 + O2 = CO2 + 2H2    
    DH_T["n"] = DH_T_n_1+DH_T_n_2+DH_T_n_3                                                                                                                                                 # Sum of CH4 reactions
    
    DH_T["o"] = dy[ind["m_el"]]/M["C"]*((dh["CO"]-dh["C"])+(cp["CO"]-0.5*cp["O2"])*(y[ind["T_gas"]]-c["T_ref"])-cp["C"]*(y[ind["T_el"]]-c["T_ref"]))                          # electrode consumption for combustion to CO
    
    DH_T["p"] = dy[ind["m_comb_scrap"]]/M["C9H20"]*(9*dh["C"]-dh["C9H20"]+10*cp["H2"]*(y[ind["T_gas"]]-c["T_ref"])+(9*cp["C"]-cp["C9H20"])*(c["T_ambient"]-c["T_ref"]))           # combustibles from dissociation to C and H2
    
    DH_T["q"] = x[9][5]/M["CO"]*((dh["CO2"]-dh["CO"]-dh["H2O"])+(cp["CO2"]+cp["H2"]-cp["CO"]-cp["H2O_gas"])*(y[ind["T_gas"]]-c["T_ref"]))                                          # CO + H2O = CO2 + H2 (hom. water gas)
    
    DH_T["r"] = x[10][5]/M["CO2"]*(2*dh["CO"]-dh["C"]-dh["CO2"]+(2*cp["CO"]-cp["CO2"])*(y[ind["T_gas"]]-c["T_ref"])-cp["C"]*(c["T_ambient"]-c["T_ref"]))                          # C + CO2 = 2CO (Boduard)
    
    DH_T["s"] = x[14][8]/M["H2O"]*(dh["CO"]-dh["C"]-dh["H2O"]+(cp["CO"]+cp["H2"]-cp["H2O_gas"])*(y[ind["T_gas"]]-c["T_ref"])-cp["C"]*(c["T_ambient"]-c["T_ref"]))                  # C + H2O = CO + H2 (het. water gas)
    
    DH_T["t"] = x[13][4]/M["H2"]*((dh["H2O"])+(cp["H2O_gas"]-cp["O2"]*0.5-cp["H2"])*(y[ind["T_gas"]]-c["T_ref"]))                                                                      # H2 + 1/2 O2 = H2O
    
    DH_T["u"] = -x[8][9]/M["O2"]*2*(dh["CaS"]-dh["CaO"]-dh["S"]+(cp["CaS"]-cp["CaO"])*(y[ind["T_sl_liq"]]-c["T_ref"])+(cp["O2"]/2-cp["S"])*(y[ind["T_st_liq"]]-c["T_ref"]))     # CaO + S --> CaS + O
    
    DH_T["v"] = -x[10][9]/M["C"]*(dh["CO2"]+dh["C"]-2*dh["CO"]+(cp["CO2"]-2*cp["CO"])*(y[ind["T_gas"]]-c["T_ref"])+cp["C"]*(y[ind["T_st_liq"]]-c["T_ref"]))                    # 2CO --> CO2 + C (correction for slag reactions forming CO2 not CO)
    
    DH_T["w"] = -(iv["m_FeO_burn_O2"]+iv["m_FeO_post_O2"])/2/M["FeO"]*(2*dh["FeO"]-2*dh["Fe"]+2*cp["FeO"]* \
        (y[ind["T_sl_liq"]]-c["T_ref"])-2*cp["Fe"]*(y[ind["T_st_liq"]]-c["T_ref"])-cp["O2"]*(y[ind["T_gas"]]-c["T_ref"]))                                                            # 2Fe + O2 = 2FeO (burner reaction) (melting and heating of Fe already in QM)
    
    DH_T["x"] = -iv["m_FeO_burn_CO2"] /M["FeO"]*(dh["FeO"]+dh["CO"]-dh["Fe"]-dh["CO2"]+ \
        cp["FeO"]*(y[ind["T_sl_liq"]]-c["T_ref"])-cp["Fe"]*(y[ind["T_st_liq"]]-c["T_ref"])-(cp["CO2"]-cp["CO"])*(y[ind["T_gas"]]-c["T_ref"]))                                      # Fe + CO2 = FeO + CO (burner reaction) (melting and heating of Fe already in QM)
    
    C_reac = iv["C_inj_av"] + iv["C_conv_gas"] 
    DH_T_y_cor =[0,0,0,0,0,0,0,0]
    DH_T_y_cor[0] = (-dy[ind["m_vol_coal"]]/(1-comp["C_char_C_fix"])*comp["C_char_C_fix"]+C_reac)/M["C"]*(dh["C"]-dh["CO2"])                                                                   # correction for unburnt C
    DH_T_y_cor[1] = (-dy[ind["m_vol_coal"]]/(1-comp["C_char_C_fix"])*comp["C_char_C_vol"]+C_reac*comp["C_inj_C_vol"]/comp["C_inj_C_fix"])/M["C"]*(dh["CO"]-dh["CO2"])                            # correction for unburnt CO
    DH_T_y_cor[2] = (-dy[ind["m_vol_coal"]]/(1-comp["C_char_C_fix"])*comp["C_char_H2"]+C_reac*comp["C_inj_H2"]/comp["C_inj_C_fix"])/M["H2"]*(-dh["H2O"])                                          # correction for unburnt H2
    DH_T_y_cor[3] = (-dy[ind["m_vol_coal"]]/(1-comp["C_char_C_fix"])*comp["C_char_H2O"]+ 
                         C_reac*comp["C_inj_H2O"]/comp["C_inj_C_fix"])/M["H2O"]*(cp["H2O_liq"]*(373.15-c["T_ref"])+cp["H2O_gas"]*(y[ind["T_gas"]]-373.15))                                  # correction for heating of H2O
    DH_T_y_cor[4] = (-dy[ind["m_vol_coal"]]/(1-comp["C_char_C_fix"])*comp["C_char_H2O"]+C_reac*comp["C_inj_H2O"]/comp["C_inj_C_fix"])*dh["H2O_100"]/1000                                           # correction for evaporation of H2O
    DH_T_y_cor[5] = (-dy[ind["m_vol_coal"]]/(1-comp["C_char_C_fix"])*comp["C_char_C_vol"]+C_reac*comp["C_inj_C_vol"]/comp["C_inj_C_fix"])/M["C"]*cp["CO"]*(y[ind["T_gas"]]-c["T_ref"])          # correction for heating of CO
    DH_T_y_cor[6] = (-dy[ind["m_vol_coal"]]/(1-comp["C_char_C_fix"])*comp["C_char_H2"]+C_reac*comp["C_inj_H2"]/comp["C_inj_C_fix"])/M["H2"]*cp["H2"]*(y[ind["T_gas"]]-c["T_ref"])               # correction for heating of H2
    DH_T_y_cor[7] = (-dy[ind["m_vol_coal"]]/(1-comp["C_char_C_fix"])*comp["C_char_N2"]+C_reac*comp["C_inj_N2"]/comp["C_inj_C_fix"])/M["N2"]*cp["N2"]*(y[ind["T_gas"]]-c["T_ref"])               # correction for heating of N2
    DH_T["y"] = -dy[ind["m_vol_coal"]]/(1-comp["C_char_C_fix"])*comp["C_char_lhv"]-sum(DH_T_y_cor)+C_reac*comp["C_inj_lhv"]/comp["C_inj_C_fix"]                                                     # enthalpy from gas emission from volatile coal components
    DH_T["z"] = -x[14][5]/M["H2O"]*(dh["H2O"]+(cp["H2O_gas"]-cp["O2"]*0.5-cp["H2"])*(y[ind["T_gas"]]-c["T_ref"]))                                                                          # dissociation of water H2O = H2 + 1/2 O2  
    
