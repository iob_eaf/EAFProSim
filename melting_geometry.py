"""
Created on Tue Apr 27 13:36:49 2021

@author: schuettensack
"""
import numpy as np
from scipy.interpolate import PchipInterpolator

def melting_geometry(p, V_scrap_max, m_scrap_charged, m_st_liq, injected_mass):
    ## calculation of melting geometry
    

    if V_scrap_max>p["V_EAF"] *0.7 :  
        V_scrap_max=p["V_EAF"] *0.7 - 1e-10                                                # if calculated scrap volume is too high set to furnace volume to avoid numerical inconsistencies
    
    p["rho_st_sol"]=m_scrap_charged/V_scrap_max                                      # calculated scrap density acutally used
    
    V_lSc = m_st_liq/p["rho_st_liq"]                                               # initial liquid melt volume
    m_st_liq_end = m_st_liq+m_scrap_charged+injected_mass+5000                     # liquid melt mass at the end
    V_lSc_end = m_st_liq_end/p["rho_st_liq"]                                       # liquid melt volume at the end of melting
    
    V_max_cone=np.pi*(p["r_EAF_up"]-p["r_EAF_low"])/3*(p["r_EAF_up"]**2+p["r_EAF_up"]*p["r_EAF_low"]+p["r_EAF_low"]**2)
    h_cone_it=np.arange(0,p["r_EAF_up"]-p["r_EAF_low"]+0.01,0.01)
    V_cone_it=np.pi*h_cone_it/3*((p["r_EAF_low"]+h_cone_it)**2+(p["r_EAF_low"]+h_cone_it)*p["r_EAF_low"]+p["r_EAF_low"]**2)
    h_rest=np.arange(0.1,p["h_EAF_up"],0.3)
    V_rest=V_max_cone+h_rest*np.pi*p["r_EAF_up"]**2
    h_rest=h_rest+p["h_EAF_low"]
    volume_it = np.zeros([2,(len(h_cone_it)+len(h_rest))])
    volume_it[0,:]=np.concatenate((h_cone_it,h_rest))
    volume_it[1,:]=np.concatenate((V_cone_it,V_rest))                          # total volume of EAF for given height
    
    # function to get bath height dependent on actual volume liquid scrap
    fVol = PchipInterpolator(volume_it[1], volume_it[0])
    
    
    ## cylinder geometry
    # Geometry based on Hernandez 2019 and Meier 2016 with cylindrical furnace,
    # single electrode, bottom vessel as cone frustrum with 45° angle. Melting
    # geometry with initially flat scrap surface, boring down of electrode with
    # constant radius of hole until melt surface is reached. After reaching of
    # melt surface radius of hole incrases and scrap height decreases. The
    # ratio of height decrease and radius increase can be determined by two
    # parameters.
    if V_scrap_max>V_max_cone:                                                   # calculation of initial scrap height
        h_scrap_init=(V_scrap_max-V_max_cone)/(np.pi*p["r_EAF_up"]**2)+(p["r_EAF_up"]-p["r_EAF_low"])
    else:
        h_scrap_init=fVol(V_scrap_max)
    
    a_range=np.arange(0.0, V_scrap_max+0.5, 0.5)
    h_bath=fVol(V_lSc)                                                         # initial bath height
    
    V = np.empty((len(a_range),9))
    V[:] = np.nan
    
    for i in range(0,len(a_range)):
        V_molten=a_range[i]
        h_hole=V_molten/np.pi/p["r_hole_init"]**2                                # depth of initial hole from electrode boring into scrap with constant radius
        h_bath=fVol(V_lSc+V_molten*p["rho_st_sol"]/p["rho_st_liq"])
        if h_scrap_init >= h_bath and h_hole<=h_scrap_init-h_bath:             # calculation for upper scrap level still over melt level && boring not completed
            V[i,0]=V_scrap_max-V_molten                                          # rest Volume of solid scrap
            V[i,1]=h_hole                                                      # height of the cone frustum
            V[i,2]=p["r_hole_init"]                                                # inner radius of cone frustum
            V[i,3]=p["r_hole_init"]                                                # outer radius of cone frustum
            V[i,4]=V_molten*p["rho_st_sol"]                                      # melted mass of m_sSc
            V[i,5]=V_lSc+V[i,4]/p["rho_st_liq"]                                 # Actual V_lSc
            V[i,6]=V[i,0]*p["rho_st_sol"]                                        # rest m_sSc
            V[i,7]=h_bath                                                      # corresponding bath height to actual V_lSc
            V[i,8]=h_scrap_init                                                # scrap height
            V_hole=V_molten                                                    # volume of hole from electrode boring down
        elif i==0:                                                             # if initial scrap height is already lower than melt height give three sample points for geometry
            V[0:3,0]=np.multiply(V_scrap_max,[1, 0.5, 0.01])
            V[0:3,1]=[0, 0, 0]
            V[0:3,2]=np.multiply(p["r_EAF_up"],[1, 1, 1])
            V[0:3,3]=np.multiply(p["r_EAF_up"],[1, 1, 1])
            V[0:3,4]=np.multiply(m_scrap_charged,[0, 0.5, 1])
            V[0:3,5]=V_lSc+np.multiply((m_scrap_charged/p["rho_st_liq"]),[0, 0.5, 1])
            V[0:3,6]=np.multiply(m_scrap_charged,[1, 0.5, 0])
            V[0:3,7]=fVol(V[0:3,5])
            V[0:3,8]=fVol(V[0:3,0])
        else:                                                                  # boring phase completed
            i=i-1
            break
    #V = V[~np.isnan(V).all(axis=1)]
    rad_inc=np.arange(p["r_hole_init"]+0.01,p["r_EAF_up"],0.05)                   # increase in hole diameter
    h_red=p["geo_fact"][0]*(rad_inc-p["r_hole_init"])**p["geo_fact"][1]          # reduction in scrap height as function of increased radius
    #rad_inc = np.linspace(p["r_hole_init"] + 0.01, p["r_EAF_up"] - 0.01, 40)                  # increase in hole diameter
    #h_red = h_scrap_init - ((p["r_EAF_up"]-rad_inc)/(p["r_EAF_up"]-p["r_hole_init"]))**p["geo_fact"][0]*(h_scrap_init)  # reduction in scrap height as function of increased radius
    V_new = V[~np.isnan(V).any(axis=1), :]
    
    h_bath_init = h_bath

    if V_new[-1,0]>0 and ~np.isnan(V_new[-1,0]):                                     # if scrap was initially above bath height continue with melt down
        for j in range(0,len(rad_inc)):
            V_molten=V_hole+np.pi*(rad_inc[j]**2-p["r_hole_init"]**2)*(h_scrap_init-h_bath_init)+np.pi*(p["r_EAF_up"]**2-rad_inc[j]**2)*h_red[j] # volume of scrap molten for given radius and height
            h_scrap=h_scrap_init-h_red[j]                                      # new scrap height
            h_bath=fVol(V_lSc+V_molten*p["rho_st_sol"]/p["rho_st_liq"])            # new bath height
            if h_scrap>h_bath:
                V[j+i+1,0]=V_scrap_max-V_molten                                  # rest Volume of solid scrap
                V[j+i+1,1]=h_scrap-h_bath                                      # height of the cone frustum
                V[j+i+1,2]=rad_inc[j]                                          # inner radius of cone frustum
                V[j+i+1,3]=rad_inc[j]                                          # outer radius of cone frustum
                V[j+i+1,4]=V_molten*p["rho_st_sol"]                              # melted mass of m_sSc
                V[j+i+1,5]=V_lSc+V[j+i+1,4]/p["rho_st_liq"]                     # Actual V_lSc
                V[j+i+1,6]=V[j+i+1,0]*p["rho_st_sol"]                           # rest m_sSc
                V[j+i+1,7]=h_bath                                              # corresponding bath height to actual V_lSc
                V[j+i+1,8]=h_scrap                                             # scrap height
            else:                                                              # break if scrap height has reached bath level otherwise continue until hole radius has reached EAF radius
                break
   
    V = V[~np.isnan(V).any(axis=1), :]                                         # deleting rows where row is completely empty
    V = V[~np.all(V==0, axis=1)]                                               # deleting rows where row is completely zero
    # setting final row for interpolation
    V = np.append(V, [[0,0,p["r_EAF_up"],p["r_EAF_up"],m_scrap_charged,V_lSc_end,0,fVol(V_lSc_end),0]], axis=0)
    
    V_mod = np.insert(V, 0, V[0], axis=0)
    V_mod[0,0] = V_scrap_max*1.1
    V_mod[0,4] = (V_scrap_max-V_mod[0,0])*p["rho_st_sol"]
    V_mod[0,5] = V_lSc+V_mod[0,4]/p["rho_st_liq"]
    V_mod[0,6] = V_mod[0,0]*p["rho_st_sol"]

    return V_mod

