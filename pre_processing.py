import numpy as np
import pandas as pd
from scipy import signal
from scipy.interpolate import PchipInterpolator
import math

def pre_processing(y, OpCh, c, M, K, p, cp, ind, iv, dh,cfg=None):

    if cfg is None: cfg = {}
    cfg.setdefault('OpCh.T_tap_last',1900)

    matrix = OpCh["OpCh_data"]
    matrix = matrix[~np.all(matrix == 0, axis=1)]
    #correct unreasonable values for electric energy supply
    matrix[:,1] = np.maximum(matrix[:,1],0.01)
    matrix[:,2] = np.maximum(matrix[:,2],10)
    matrix[:,3] = np.maximum(matrix[:,3],0.01)
    
    cooling_roof_rate = matrix[:,12]
    cooling_wall_rate = matrix[:,13]
    cooling_roof_temp_in = matrix[:,14]
    cooling_wall_temp_in = matrix[:,15]
    # outlet temperature for inital values of each basket
    cooling_roof_temp_out = matrix[:,20]
    cooling_wall_temp_out = matrix[:,21]
    
    num_charged_baskets = int(max(matrix[:,11]))
    # preallocation based on number of charged baskets
    ni = np.zeros(num_charged_baskets+1, dtype=int)
    ni_alt = np.zeros(num_charged_baskets, dtype=int)
    LowPowerSection = np.zeros(num_charged_baskets+1)

    # use combined filter of p_arc > 0.5 or m_o2_jet > 0.05
    relevant_opch = np.logical_or(matrix[:,1]>0.5,matrix[:,4]>0.5)
    # cutting of relevant data
    # find begining of electric energy input as starting point for charge
    ni[0] = next(x for (x, val) in enumerate(relevant_opch) if val == True)
    OpCh["low_power_cutofftime"] = ni[0] * (matrix[ni[0],0] - matrix[ni[0]-1,0])
    # move starting point to earlier time to account for roof closing etc.
    if (next((x for (x, val) in enumerate(list(reversed(matrix[:,0]))) if val <= (matrix[ni[0],0] - p["StartingTimeDelay"] - p["roof_opening_time"])), None)) is not None:
        ni[0] = matrix.shape[0] - 1 - next(x for (x, val) in enumerate(list(reversed(matrix[:,0]))) if val <= (matrix[ni[0],0] - p["StartingTimeDelay"] - p["roof_opening_time"]))
    
    ni_alt[0] = ni[0]
    # find ending of electric energy input
    ni[num_charged_baskets] = matrix.shape[0] - 1 - next(x for (x, val) in enumerate(list(reversed(relevant_opch))) if val == True)
    #ni[num_charged_baskets] = [i for i in range(np.shape(matrix)[0]) if matrix[i,1] >= 0.5][-1]
    # move end point to back to account for roof closing etc.
    ni[num_charged_baskets] = next(x for (x, val) in enumerate(matrix[:,0]) if val >= matrix[ni[num_charged_baskets],0] + p["StartingTimeDelay"] + p["roof_opening_time"])

    for idx_basket in range(1, num_charged_baskets):
        ni[idx_basket] = next(x for (x, val) in enumerate(matrix[ni[0]+1:,11]) if val == idx_basket+1) + ni[0] + 1
        ni_alt[idx_basket] = ni[idx_basket] + 1 + next(x for (x, val) in enumerate(matrix[ni[idx_basket]:,1]) if val > 1)
        ni_alt[idx_basket] = matrix.shape[0] - 1 - next(x for (x, val) in enumerate(list(reversed(matrix[:,0]))) if val <= (matrix[ni_alt[idx_basket],0] - p["StartingTimeDelay"]))
        if ni_alt[idx_basket] < ni[idx_basket]:
            ni_alt[idx_basket] = ni[idx_basket]
    
    LowPowerSection[0] = len(np.where(matrix[ni[0]:ni[1],1] < 0.5)[0])
    for idx_basket in range(1, num_charged_baskets):
        LowPowerSection[idx_basket] = ni_alt[idx_basket] - ni[idx_basket] - 1
    LowPowerSection[-1] = len(np.where(matrix[ni_alt[-1]:ni[-1],1] < 0.5)[0])
    #terminate_long_charging = max(LowPowerSection)

    #Adjusting time and removing entries before first basket is charged or
    # electric energy input begins (therefor before first entry used in simulation

    if ni[0] > 1:
        delta_time = matrix[ni[0],0] - matrix[ni[0]-1,0]
        for k in range(ni[0],matrix.shape[0]):
            matrix[k,0] = matrix[k,0] - matrix[ni[0]-1,0] - delta_time
        matrix = np.delete(matrix, slice(0,ni[0]), axis=0)
    t_end = np.zeros(num_charged_baskets)
    for idx_basket in range(0, num_charged_baskets-1):
        # second basket charging time
        t_end[idx_basket] = matrix[round((ni[idx_basket+1] + ni_alt[idx_basket+1] + 2) / 2) - ni[0], 0]
    t_end[-1] = matrix[ni[-1] - ni[0], 0]

    OpCh["t_end"][0:num_charged_baskets] = t_end


    # filter data for smoothing
    # filter properties; defines the filter intensity
    facfilt = np.ones(79)
    facfilt[0:21] = [1, 10, 10, 4, 4, 4, 4, 4, 4, 4, 4, 1, 7, 7, 4, 4, 4, 4, 4, 4, 4]#, 1, 1, 1, 1, 1, 1, 1, 1]
    # filter intensity set for 5 second measurement intervals, adjustment for differing intervals
    facfilt = np.around([i * 5 / np.mean(np.diff(matrix[:,0])) for i in facfilt], 0).astype(int)
    # if c.fast...
    # warning occurs when 6 is used as a filter parameter, cause unknown
    # facfilt(facfilt==6)=5
    # no filter for matrix(0) = time

    for pk in range(1,79):
        if pk == 11:
            # no filter for matrix(10)= number of scrap basket
            continue
        matrix[:,pk] = signal.filtfilt(np.ones(facfilt[pk]) / facfilt[pk], [1, 0], matrix[:,pk])

    ### Reference data if no data is read
    OpCh["T_tap_last"] = cfg['OpCh.T_tap_last']

    OpCh["m_tap_data"] = p["m_tap"]

    # initial temperatures of cooling water at outlet and inner sides of wall and roof
    T_roof_H2O_init = np.zeros(num_charged_baskets)
    T_wall_H2O_init = np.zeros(num_charged_baskets)

    T_roof_H2O_init[0] = cooling_roof_temp_out[ni[0]]
    T_wall_H2O_init[0] = cooling_wall_temp_out[ni[0]]
    for idx_basket in range(1, num_charged_baskets):
        T_roof_H2O_init[idx_basket] = cooling_roof_temp_out[round((ni[idx_basket] + ni_alt[idx_basket]) / 2)]
        T_wall_H2O_init[idx_basket] = cooling_wall_temp_out[round((ni[idx_basket] + ni_alt[idx_basket]) / 2)]

    T_roof_init = np.zeros(num_charged_baskets)
    T_wall_init = np.zeros(num_charged_baskets)

    T_roof_init[0] = (T_roof_H2O_init[0] + cooling_roof_rate[ni[0]] * cp["H2O_liq"] / M["H2O"] * 1E3 *
                    (T_roof_H2O_init[0] - cooling_roof_temp_in[ni[0]]) * p["pipe_thick"] /
                    (p["lambda_roof"] * np.pi * (p["r_EAF_up"] ** 2 - p["r_heart"] ** 2)))
    
    T_wall_init[0] = (T_wall_H2O_init[0] + cooling_wall_rate[ni[0]] * cp["H2O_liq"] / M["H2O"] * 1E3 *
                    (T_wall_H2O_init[0] - cooling_wall_temp_in[ni[0]]) * (p["pipe_thick"] + p["slag_thick"]) /
                    (p["lambda_wall"] * np.pi * 2 *p["r_EAF_up"] * (p["h_EAF_up"] - p["h_EAF_low"])))

    for idx_basket in range(1, num_charged_baskets):
        T_roof_init[idx_basket] = (T_roof_H2O_init[idx_basket] + cooling_roof_rate[round((ni[idx_basket] + ni_alt[idx_basket]) / 2)] * cp["H2O_liq"] / M["H2O"] * 1E3 *
                                (T_roof_H2O_init[idx_basket] - cooling_roof_temp_in[round((ni[idx_basket] + ni_alt[idx_basket]) / 2)]) * p["pipe_thick"] /
                                (p["lambda_roof"] * np.pi * (p["r_EAF_up"] ** 2 - p["r_heart"] ** 2)))
        T_wall_init[idx_basket] = (T_wall_H2O_init[idx_basket] + cooling_wall_rate[round((ni[idx_basket] + ni_alt[idx_basket]) / 2)] * cp["H2O_liq"] / M["H2O"] * 1E3 *
                                (T_wall_H2O_init[idx_basket] - cooling_wall_temp_in[round((ni[idx_basket] + ni_alt[idx_basket]) / 2)]) * (p["pipe_thick"] + p["slag_thick"]) /
                                (p["lambda_wall"] * np.pi * 2 *p["r_EAF_up"] * (p["h_EAF_up"] - p["h_EAF_low"])))

    # Integration stop time for initaial two scrap baskets
    tstop = np.zeros(num_charged_baskets)
    tstop[0] = t_end[0]
    for idx_basket in range(1, num_charged_baskets):
        tstop[idx_basket] = t_end[idx_basket] - t_end[idx_basket - 1]

    OpCh["m_coal"] = np.sum(OpCh["mass_coal"],axis=1)
    
    # calculate average coal composition for each basket
    for i in range(0, len(OpCh["m_coal"])):
        if OpCh["m_coal"][i] == 0:
            OpCh["m_coal"][i] = 1E-10 # add .001 kg to each entry of m_coal, to avoid division by zero
    
    OpCh["m_coal"] = OpCh["m_coal"] * (1 - K["coal_burn_charging"]) 


    # Water cooling and wall initial Temperatures [K]
    OpCh["T_roof_init"][0:num_charged_baskets] = T_roof_init
    OpCh["T_wall_init"][0:num_charged_baskets] = T_wall_init
    OpCh["T_roof_H2O_init"][0:num_charged_baskets] = T_roof_H2O_init
    OpCh["T_wall_H2O_init"][0:num_charged_baskets] = T_wall_H2O_init

    OpCh_count = matrix.shape[0]
    OpCh["OpCh_last_elem"] = OpCh_count
    OpCh["OpCh_data"][0:matrix.shape[0],0:matrix.shape[1]] = matrix
    OpCh["mat_t"] = OpCh["OpCh_data"][:,0]
 

    #Interpolation OpCh
    for ij in range(1,matrix.shape[1]):                                                            
        pcobj = PchipInterpolator(matrix[:,0],matrix[:,ij])
        #OpCh_interp_dict[ij] = np.transpose(pcobj.c)
        OpCh["OpCh_interp0"][:OpCh_count-1,ij] = pcobj.c[0]
        OpCh["OpCh_interp1"][:OpCh_count-1,ij] = pcobj.c[1]
        OpCh["OpCh_interp2"][:OpCh_count-1,ij] = pcobj.c[2]
        OpCh["OpCh_interp3"][:OpCh_count-1,ij] = pcobj.c[3]
    
    
    ## assigning initial conditions
    y[ind["T_gas"]] = c["T_ambient"] + 150
    y[ind["T_st_sol"]] = c["T_ambient"]
    y[ind["T_sl_sol"]] = c["T_ambient"]
    OpCh["m_st_sol_basket"] =  (OpCh["mass_scrap"][:,ind["comp_O"]] + OpCh["mass_scrap"][:,ind["comp_C"]] + OpCh["mass_scrap"][:,ind["comp_Si"]] + OpCh["mass_scrap"][:,ind["comp_Mn"]] + 
                                OpCh["mass_scrap"][:,ind["comp_Cr"]] + OpCh["mass_scrap"][:,ind["comp_P"]] + OpCh["mass_scrap"][:,ind["comp_Fe"]] + OpCh["mass_scrap"][:,ind["comp_S"]])

    OpCh["m_slag"] =  (OpCh["mass_scrap"][:,ind["comp_CaO"]] + OpCh["mass_scrap"][:,ind["comp_MgO"]] + OpCh["mass_scrap"][:,ind["comp_Al2O3"]] + OpCh["mass_scrap"][:,ind["comp_SiO2"]] + 
                        OpCh["mass_scrap"][:,ind["comp_MnO"]] + OpCh["mass_scrap"][:,ind["comp_Cr2O3"]] + OpCh["mass_scrap"][:,ind["comp_P2O5"]] + OpCh["mass_scrap"][:,ind["comp_FeO"]])

    ######## comp_sl dummy werte wieder entfernen!!!! ##########
    y[ind["m_sl_liq"]] = 1
    comp_sl = np.array([0.6, 0.312, 0.072499, 0.0090008, 0.003, 0.0000001, 0.0035, 0.0000001, 0.0000001]) 
    
    y[ind["m_CaO_sl_liq"]:ind["m_CaS_sl_liq"]+1] = comp_sl * y[ind["m_sl_liq"]]

    
    if all(OpCh["hotheel_comp"][2:9] == 0):#np.isnan(OpCh["hotheel_comp"][2:9]).any():
        y[ind["m_O_st_liq"]] = (OpCh["mass_scrap"][0,ind["comp_O"]] / OpCh["m_st_sol_basket"][0]) * p["m_hot_heel"]
        y[ind["m_C_st_liq"]] = (OpCh["mass_scrap"][0,ind["comp_C"]] / OpCh["m_st_sol_basket"][0]) * p["m_hot_heel"]
        y[ind["m_Si_st_liq"]] = (OpCh["mass_scrap"][0,ind["comp_Si"]] / OpCh["m_st_sol_basket"][0]) * p["m_hot_heel"]
        y[ind["m_Mn_st_liq"]] = (OpCh["mass_scrap"][0,ind["comp_Mn"]] / OpCh["m_st_sol_basket"][0]) * p["m_hot_heel"]
        y[ind["m_Cr_st_liq"]] = (OpCh["mass_scrap"][0,ind["comp_Cr"]] / OpCh["m_st_sol_basket"][0]) * p["m_hot_heel"]
        y[ind["m_P_st_liq"]] = (OpCh["mass_scrap"][0,ind["comp_P"]] / OpCh["m_st_sol_basket"][0]) * p["m_hot_heel"]
        y[ind["m_Fe_st_liq"]] = (OpCh["mass_scrap"][0,ind["comp_Fe"]] / OpCh["m_st_sol_basket"][0]) * p["m_hot_heel"]
        y[ind["m_S_st_liq"]] = (OpCh["mass_scrap"][0,ind["comp_S"]] / OpCh["m_st_sol_basket"][0]) * p["m_hot_heel"]
    else:
        y[ind["m_O_st_liq"]] =  OpCh["hotheel_comp"][2] * p["m_hot_heel"]
        y[ind["m_C_st_liq"]] =  OpCh["hotheel_comp"][3] * p["m_hot_heel"]
        y[ind["m_Si_st_liq"]] = OpCh["hotheel_comp"][4] * p["m_hot_heel"]
        y[ind["m_Mn_st_liq"]] = OpCh["hotheel_comp"][5] * p["m_hot_heel"]
        y[ind["m_Cr_st_liq"]] = OpCh["hotheel_comp"][6] * p["m_hot_heel"]
        y[ind["m_P_st_liq"]] =  OpCh["hotheel_comp"][7] * p["m_hot_heel"]
        y[ind["m_Fe_st_liq"]] = OpCh["hotheel_comp"][8] * p["m_hot_heel"]
        y[ind["m_S_st_liq"]] =  OpCh["hotheel_comp"][9] * p["m_hot_heel"]

    y[ind["m_st_liq"]] = np.sum(y[ind["m_O_st_liq"]:ind["m_S_st_liq"]+1])
    ## assigning initial conditions

    y[ind["T_st_liq"]] = OpCh["T_tap_last"]

    y[ind["T_el"]] = 1000

    y[ind["T_sl_liq"]] = c["T_melt_sl"] + 50
    y[ind["T_wall_unc"]] = 1600

    y[ind["m_coal"]] = 1 #?
    y[ind["m_vol_coal"]] = 0.01
    y[ind["m_el"]] = math.pi * p["r_el"] ** 2 * (p["h_el_cool"] + p["h_EAF_low"] + p["h_EAF_up"]) * p["n_el"] * p["rho_el"]
    y[ind["m_sl_liq"]] = 1
    y[ind["m_gas"]] = c["ap"] * (p["V_EAF"] - y[ind["m_st_liq"]] / p["rho_st_liq"] - OpCh["m_st_sol_basket"][0] / p["rho_st_liq"]) * M["air"] / (c["R_gas"] * y[ind["T_gas"]])
    y[ind["m_resolid"]] = 1

    y[ind["p_furnace"]] = 0
    y[ind["m_O_reac"]:ind["m_S_reac"]+1] = 0

    y[ind["V_st_sol"]] = 0
    y[ind["h_slag"]] = 0.0001
    y[ind["m_leak_air"]] = 0
    
    iv["T_prev"] = 0

    OpCh["t_basket"] = OpCh["t_end"][0]
    #Aluminium addition reacts directly
    T_melt_Al = 660 + 273
    m_Al_ges = y[ind["m_Al2O3_sl_sol"]]/M["Al2O3"]*M["Al"]*0 # in kg
    t_end = OpCh["mat_t"][-1]
    m_Al = ((2*m_Al_ges)/t_end) - ((2*m_Al_ges)/(t_end**2)) * OpCh["mat_t"]
    m_O2 = (m_Al/M["Al"])*(3/2)*M["O2"]
    x_Al2O3 = (m_Al + m_O2)/M["Al2O3"]
    Q_Al2O3 = -x_Al2O3 * (dh["Al2O3"]-2*dh["Al"]+cp["Al2O3"]*(T_melt_Al-c["T_ref"])-(2*cp["Al"]+3*cp["O2"])*(T_melt_Al-c["T_ref"]))/1000

    OpCh["OpCh_data"][:,21] = Q_Al2O3
    
    