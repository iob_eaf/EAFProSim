import numpy as np

def setUserCustomParameters(cfg,hdf,**kwargs):

    parameters = {}
    for key, value in kwargs.items():
        parameters[key] = value
        
    # change solver for ivp_solve - if wanted or needed
    # on some systems there are problems using 'BDF' with Numba 
    # Switch to 'Radau' in this case because it's faster than 'LSODA'
    cfg['__ODESOLVER__'] = 'BDF' # 'BDF','Radau','LSODA'

    # example: tapping mass
    '''
    cfg['p.m_tap'] = 141000 # kg
    print("="*64)
    print("set cfg['p.m_tap'] = %.1f kg" % cfg['p.m_tap'])
    print("="*64)
    '''
    # # # example: time series roof cooling water inlet temperature [K] 
    #tm    = hdf.get(f'{para["charge_list"][0]}/cyclic/timesteps') # original time
    #val   = np.zeros(tm.shape) + 301    # new value is 301 K for every timestep
    #name  = "Temperature water roof in" # used as headline for excel output
    #unit  = "K"                         # used as unit for excel output
    #hdf.set(f'{para["charge_list"][0]}/cyclic/cooling_roof_temperature_inlet',val,name,unit)


