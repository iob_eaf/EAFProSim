import sys
import copy

from EAF_diff_eq import EAF_diff_eq
import numpy as np
from scipy.integrate import solve_ivp
from scipy.interpolate import PchipInterpolator
from tqdm import tqdm
from init_cond import init_cond
#from melting_geometry import melting_geometry



def EAF_sim_batch(y, M, cp, dh, p, c, Q, OpCh, ind, X, W, iv, K, kd, comp, DH_T, QM, V, eval, show_pbar, resh, cfg=None):

    #print("run EAF_sim_batch.py (probably re-compile solve_ivp by numba)...")

    ts = []
    ys = []
    t = 0
    num_baskets = sum(OpCh["t_end"]>0)

    OpCh["basket_number_next"] = OpCh["OpCh_data"][1, 9] + 1
    basket_idx = 1

    pbar = None
    if show_pbar:
        pbar = tqdm(total=OpCh["t_end"][num_baskets-1], unit='steps', desc=f'Charge {OpCh["charge_num"]}', file=sys.stdout)

    jacsparse = np.ones((y.shape[0],y.shape[0]))

    if cfg is None: cfg = {}
    cfg.setdefault('__ODESOLVER__','BDF')
    if cfg['__ODESOLVER__'] != 'BDF': print(); print("solve_ivp: solver changed to '%s' ..." % cfg['__ODESOLVER__'])
    
    while True:        
        
        dy_2 = solve_ivp(EAF_diff_eq, [t, OpCh["t_end"][basket_idx-1]], y, method=cfg['__ODESOLVER__'], jac_sparsity = None,#jacsparse
                        args=(ind, K, c, p, OpCh, iv, cp, M, comp, kd, Q, X, W, V, dh, QM, DH_T, eval, resh, False), max_step=1)#False #pbar max_step=1

        ts.append(dy_2.t)
        ys.append(dy_2.y)
        
        if pbar is not None and ts[0][-1] > pbar.n:
            pbar.update(int(ts[0][-1] - pbar.n))

        if dy_2.status == 0 and basket_idx<num_baskets:
            t = dy_2.t[-1]
            y = dy_2.y[:, -1].copy()

            i = int(OpCh["basket_number_next"] - 1)
            OpCh["t_basket"] = OpCh["t_end"][i] + 0.1
            OpCh["basket_number_next"] = OpCh["basket_number_next"] + 1
            init_cond(y=y, OpCh=OpCh, M=M, p=p, comp=comp, c=c, cp=cp, K=K, ind=ind)

        else:
            ts = np.concatenate(ts, axis= None)
            ys = np.concatenate(ys, axis= 1)
            #np.save('../Angaben/result_QM_zero_neu',ys)
            #np.save('../Angaben/time_QM_zero_neu',ts)
            break

        basket_idx += 1

    if show_pbar:
        if pbar is not None:
            pbar.update(int(ts[-1] - pbar.n))
            pbar.close()

    return ts, ys, resh




