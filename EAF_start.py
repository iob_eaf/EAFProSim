import argparse
import os
import time
import traceback
import warnings
from functools import partial
from multiprocessing import Pool, cpu_count
import sys
import numpy as np
from tqdm import tqdm
import platform

from EAF_sim_batch import EAF_sim_batch
from InputData import create_InputData
from parameter import parameter
from init_cond import init_cond
from data_input import data_input
from pre_processing import pre_processing
from EAF_save_results import EAF_save_results
from waitbar import logger
from mass_balance import mass_balance

from numba.typed import Dict
from numba.core import types

from io_tools import hdf5Wrapper, cfgDict, xls2cfg, markSection, sysexit

def EAF_parallel(charge_num, y, datasrc, show_pbar, logger, cfg=None, hdf=None):
    # redirect warnings
    warnings.showwarning = logger.redirect_warning
    


    #try:
    M, cp, dh, p, c, Q, OpCh, ind, X, W, K, kd, comp, iv, DH_T, QM, V, eval = create_InputData(charge_num, datasrc)
    parameter(M=M, p=p, comp=comp, c=c, K=K, kd=kd, cfg=cfg)
    data_input(ind=ind, OpCh=OpCh, comp=comp, K=K, hdf=hdf)
    pre_processing(y=y, OpCh=OpCh, c=c, M=M, K=K, p=p, cp=cp, ind=ind, iv=iv, dh=dh, cfg=cfg)
    init_cond(y=y, OpCh=OpCh, M=M, p=p, comp=comp, c=c, cp=cp, K=K, ind=ind)

    resh = Dict.empty(
        key_type=types.unicode_type,
        value_type=types.float64[:],
    )
    
    dy = EAF_sim_batch(y=y, M=M, cp=cp, dh=dh, c=c, p=p, Q=Q, OpCh=OpCh, ind=ind, X=X, W=W, iv=iv,
                    K=K, kd=kd, comp=comp, DH_T=DH_T, QM=QM, V=V, eval=eval, show_pbar=show_pbar,resh=resh,cfg=cfg)

    #except Exception:
    #    logger.redirect_exception(traceback.format_exc(), f"\nError occurred in solving charge {charge_num}\n")
    #    return None, charge_num
    
    return dy, charge_num


# =============================================================
if __name__ == '__main__':

    #markSection("EAF_START")    

    # =====================================================================================
    # commandline parameters
    # INFO: the dummy data set contains three charges
    # INFO: the charge numbers are: 1, 2 and 3
    argparser = argparse.ArgumentParser()
    argparser.add_argument("-d", "--data", type=str, default="EAFProSim")
    argparser.add_argument("-fch", "--first_charge", type=int, default=1) # set the charge number of the first charge
    argparser.add_argument("-lch", "--last_charge", type=int, default=1) # set the charge number of the last charge; if only one charge is calculated first and last charge are the same
    argparser.add_argument("-rsp", "--results_path", type=str, default=r"results/results_01.hdf5")
    argparser.add_argument("-err", "--error_path", type=str, default=r"error.log")
    argparser.add_argument("-np", "--num_processes", type=int, default=int(cpu_count()/2))
    argparser.add_argument("-u", "--user", choices=["default","FILE"],help="select type of user code",default='default')
    argparser.add_argument("-hdf", "--hdf", help="set hdf filename for FILE mode",default='./data/dummy_data.h5')
    argparser.add_argument("-cfg", "--cfg", help="set cfg base filename for FILE mode",default='./EAFPro_Parameter.xlsx')
    argparser.add_argument("-she", "--sheet", help="set xlsx sheet if cfg is Excel",default='EAF_properties')
    argparser.add_argument("-sep", "--sep", help="set csv file separator",default=';')
    argparser.add_argument("-nn", "--nonumba", help="disable numba",action='store_true')

    args, unknown = argparser.parse_known_args()

    # doesn't work if numba imports have been executed before 
    # --> set on commandline: set NUMBA_DISABLE_JIT=1 or export NUMBA_DISABLE_JIT=1
    if args.nonumba:        
        # os.environ['NUMBA_DISABLE_JIT'] = '1' # if problems with stability
        print("NUMBA has to be disabled via commandline:  ...")
        print("    set NUMBA_DISABLE_JIT=1    or    export NUMBA_DISABLE_JIT=1")
        sys.exit()

    # =====================================================================================
    # initialize data

    log = logger(args.error_path)    
    y   = np.zeros(75)

    charge_list  = np.arange(args.first_charge, args.last_charge + 1)
    results_path = os.path.abspath(args.results_path)
    
    # =====================================================================================
    # JUST READ PURE DATA HERE
    # read data from Excel and h5
    # read timeseries

    if args.user=="FILE":  # read only user data
        #print("read settings and data from file ...")
        hdfFile  = args.hdf
        cfgFile  = args.cfg        
        cfgSheet = args.sheet        
    else:                  # dummy_data
        #print("read settings and data from default ...")
        hdfFile  = './data/dummy_data.h5'
        cfgFile  = "./EAFPro_Parameter.xlsx"
        cfgSheet = args.sheet

    # generate dictionary of parameters
    cfg  = cfgDict("parameter",xls2cfg(cfgFile,xlssheet=cfgSheet))

    # generate hdf Dictionary from hdf file
    hdf = hdf5Wrapper(hdfFile)

    # =====================================================================================
    # USER CODE TO MODIFY DATA - START
    if args.user == 'FILE':
        pass # take data a read from files
    else: # simple example for USER code
        #print("do USER_customization ...")
        from USER_customization import setUserCustomParameters
        setUserCustomParameters(cfg,hdf,charge_list=charge_list)
    
    # USER CODE TO MODIFY DATA - END
    # =====================================================================================

    start_time = time.time()

    if len(charge_list) == 1:
        dy_charge_list = [EAF_parallel(charge_list[0], y=y, datasrc=args.data,
                                       show_pbar=True, logger=log, cfg=cfg, hdf=hdf)]
    else:
        dy_charge_list = []
        for chrg in charge_list:
            y = np.zeros(80)
            dy_charge_list.append(EAF_parallel(chrg, y=y, datasrc=args.data,
                                       show_pbar=True, logger=log, cfg=cfg, hdf=hdf))
    
    # remove charges and dy which threw error
    valid_dy_charges = [dy_charge for dy_charge in dy_charge_list if dy_charge[0] is not None]
    valid_dy_charges = list(map(list, zip(*valid_dy_charges)))
    valid_dy_list = valid_dy_charges[0]
    valid_charge_list = valid_dy_charges[1]
    ts, ys, results = map(list, zip(*valid_dy_list))

    print(f"Total no. of valid charges: {len(valid_charge_list)}/{len(charge_list)}")
    if len(valid_charge_list) != len(charge_list):
        print(f"Check {args.error_path} for charges with errors")

    # save result to h5 file
    # if path is None --> generate io.BytesIO() object
    h5_obj = EAF_save_results(None, args.data, valid_charge_list, ts=ts, ys=ys, results=results)
    with open(results_path, 'wb') as file:  
        h5_obj.seek(0)
        file.write(h5_obj.getvalue())

    # calculate mass balance from hdf5 results file
    h5_obj.seek(0)        
    mass_balance(dataFile=h5_obj, valid_charge_list=valid_charge_list)

    try:    h5_obj.close()
    except: pass

    print(f"Total program time: {round(time.time() - start_time, 2)}s")

    #markSection("EAF_END")    
