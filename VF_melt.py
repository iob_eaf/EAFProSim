# -*- coding: utf-8 -*-
"""
Created on Thu Apr  1 08:46:02 2021

@author: schuettensack
"""
import numpy as np
from numba import njit

@njit(cache=True)
def VF_melt(p):
    # calculations from Minning (AAIAJ, 1979, Vol. 17, issue 3 and 12) using
    # ring segments to roof (as rings) and wall (as cylinder surface) with
    # intercepting cylinder (electrode) in center. Radii and height of wall
    # shaded are erstimated from maximum and minimum radius/height that can be
    # seen from element, see http://www.thermalradiation.net/sectionc/C-54.html
    # and http://www.thermalradiation.net/sectionc/C-56.html for sources and
    # drawings
    

    num_elem=100                                                               # number of elements
    
    # vector or radius increasing from electrode to cone radius
    vect1=np.arange(p["r_el_rep"], p["r_hole"]+(p["r_hole"]-p["r_el_rep"])/(num_elem) , (p["r_hole"]-p["r_el_rep"])/(num_elem-1) )
    np.round(vect1, 5, vect1)
    p["r_hole"] = np.round(p["r_hole"], 5)
    vect_sqr=vect1**2                                                          # vector squared for later use
    
    if p["h_hole"]==0:
         r_out_max=np.ones(num_elem)*p["r_EAF_up"]
    else:
        r_out_max=vect1+(p["r_hole"]-vect1)/p["h_hole"]*(p["h_el"]+p["h_arc"])                 # maximum radius of roof not blocked looking outside (away from electrode)

    l2=np.sqrt(p["r_hole"]**2-p["r_el_rep"]**2)
    l1=np.sqrt(vect_sqr-p["r_el_rep"]**2)
    if p["h_hole"]==0:
        r_out_max2=np.ones(num_elem)*p["r_EAF_up"]
    else:
        r_out_max2=np.sqrt(((l2+l1)/p["h_hole"]*(p["h_arc"]+p["h_el"])-l1)**2+p["r_el_rep"]**2)   # maximum radius of roof not blocked looking along the side of the electrode
    
    r_out_heart=np.minimum(r_out_max,p["r_heart"]) 
    r_out_roof=np.minimum(r_out_max,p["r_EAF_up"])
    r_out_heart2=np.minimum(r_out_max2,p["r_heart"])
    r_out_roof2=np.minimum(r_out_max2,p["r_EAF_up"])
    RO_roof=(r_out_roof2+r_out_roof)/2                                         # maximum radii for roof and heart as average from smaller and larger radius
    RO_heart=(r_out_heart2+r_out_heart)/2
    cos_vect=np.arccos(p["r_el_rep"]/vect1)

    #if (any(np.isnan(np.arccos(p["r_el_rep/r_out_heart)))):
    #    stop = 1

    omega_heart=cos_vect+np.arccos(p["r_el_rep"]/r_out_heart)                        
    omega_roof=cos_vect+np.arccos(p["r_el_rep"]/r_out_roof)
    phi_heart=cos_vect+np.arccos(p["r_el_rep"]/p["r_el_rep"])
    phi_roof=cos_vect+np.arccos(p["r_el_rep"]/p["r_heart"])
    # RC=p["r_el_rep
    RI_heart=p["r_el_rep"]
    RI_roof=p["r_heart"]
    # RO_heart=r_out_heart
    # RO_roof=r_out_roof
    h=p["h_el"]+p["h_arc"]
    RO_roof_sqr=RO_roof**2
    RO_heart_sqr=RO_heart**2
    
    F_test_roof=((omega_roof-phi_roof)/2/np.pi+((RO_roof_sqr-vect_sqr-h**2)/(np.pi*np.sqrt((RO_roof_sqr+vect_sqr+h**2)**2-4*vect_sqr*RO_roof_sqr)))*
        np.arctan(np.sqrt((RO_roof_sqr+vect_sqr+h**2-2*vect1*RO_roof)/(RO_roof_sqr+vect_sqr+h**2+2*vect1*RO_roof))*np.tan(omega_roof/2))-
        (RI_roof**2-vect_sqr-h**2)/(np.pi*np.sqrt((RI_roof**2+vect_sqr+h**2)**2-4*vect_sqr*RI_roof**2))*
        np.arctan(np.sqrt((RI_roof**2+vect_sqr+h**2-2*vect1*RI_roof)/(RI_roof**2+vect_sqr+h**2+2*vect1*RI_roof))*np.tan(phi_roof/2)))
    # roof from roof heart (inside radius) to maximum radius (either from
    # shading or outer EAF diameter)
    
    F_test_heart=((omega_heart-phi_heart)/2/np.pi+((RO_heart_sqr-vect_sqr-h**2)/(np.pi*np.sqrt((RO_heart_sqr+vect_sqr+h**2)**2-4*vect_sqr*RO_heart_sqr)))*
        np.arctan(np.sqrt((RO_heart_sqr+vect_sqr+h**2-2*vect1*RO_heart)/(RO_heart_sqr+vect_sqr+h**2+2*vect1*RO_heart))*np.tan(omega_heart/2))-
        (RI_heart**2-vect_sqr-h**2)/(np.pi*np.sqrt((RI_heart**2+vect_sqr+h**2)**2-4*vect_sqr*RI_heart**2))*
        np.arctan(np.sqrt((RI_heart**2+vect_sqr+h**2-2*vect1*RI_heart)/(RI_heart**2+vect_sqr+h**2+2*vect1*RI_heart))*np.tan(phi_heart/2)))
    # roof heart from eletrode (inner radius) to maximum radius (either from
    # shading or radius of roof heart)
    
    VF = np.zeros((4,9))
    VF[3,0]=np.sum(F_test_roof)/num_elem
    VF[3,8]=np.sum(F_test_heart)/num_elem
    
    ##
    
    # vector with increasing radius from electrode to cone
    vect1=np.arange(p["r_el_rep"],p["r_hole"]+(p["r_hole"]-p["r_el_rep"])/(num_elem),(p["r_hole"]-p["r_el_rep"])/(num_elem-1))
    l3=np.sqrt(p["r_EAF_up"]**2-p["r_el_rep"]**2)
    l2=np.sqrt(p["r_hole"]**2-p["r_el_rep"]**2)
    l1=np.sqrt(vect1**2-p["r_el_rep"]**2)
    h_min=(l1+l3)/(l1+l2)*p["h_hole"]                                             # lowest point on wall not blocked from view
    h_min2=p["h_hole"]*(p["r_EAF_up"]-vect1)/(p["r_hole"]-vect1+1e-10)
    
    h_min_wall=np.minimum(h_min,p["h_arc"]+p["h_el"])                                # second calculation with lower boundary from different trajectory
    h_min_wall2=np.minimum(h_min2,p["h_arc"]+p["h_el"])
    h_min_wall=(h_min_wall+h_min_wall2)/2
    h_min_uncooled=np.minimum(h_min,p["h_wall_unc_exp"]+p["h_hole"])
    h_min_uncooled2=np.minimum(h_min2,p["h_wall_unc_exp"]+p["h_hole"])
    h_min_uncooled=(h_min_uncooled+h_min_uncooled2)/2
    
    RO=p["r_EAF_up"]
    R1=p["r_el_rep"]
    omega=np.arccos(R1/vect1)+np.arccos(R1/RO)
    tan_omega=np.tan(omega/2)
    vect_sqr=vect1**2
    atan1=np.arctan((RO+vect1)/(RO-vect1)*tan_omega)
    h=p["h_arc"]+p["h_el"]                                                          # from melt to top of wall
    h_sqr=h**2
    F_test_tot_wall=(1/np.pi*atan1-
        (RO**2-vect_sqr-h_sqr)/(np.pi*np.sqrt((RO**2+vect_sqr+h_sqr)**2-4*vect_sqr*RO**2))*
        np.arctan(np.sqrt((RO**2+vect_sqr+h_sqr)**2-4*vect_sqr*RO**2)/(RO**2+vect_sqr+h_sqr-2*vect1*RO)*tan_omega))
    
    h=h_min_wall                                                               # from melt to top of shaded wall
    h_sqr=h**2
    F_test_shade_wall=(1/np.pi*atan1-
        (RO**2-vect_sqr-h_sqr)/(np.pi*np.sqrt((RO**2+vect_sqr+h_sqr)**2-4*vect_sqr*RO**2))*
        np.arctan(np.sqrt((RO**2+vect_sqr+h_sqr)**2-4*vect_sqr*RO**2)/(RO**2+vect_sqr+h_sqr-2*vect1*RO)*tan_omega))
    
    F_test_wall=F_test_tot_wall-F_test_shade_wall                              # from melt to unshaded wall as total wall minus shaded wall
    
    h=p["h_wall_unc_exp"]+p["h_hole"]                                             # from melt to top of exposed uncooled wall
    h_sqr=h**2
    F_test_tot_uncooled=(1/np.pi*atan1-
        (RO**2-vect_sqr-h_sqr)/(np.pi*np.sqrt((RO**2+vect_sqr+h_sqr)**2-4*vect_sqr*RO**2))*
        np.arctan(np.sqrt((RO**2+vect_sqr+h_sqr)**2-4*vect_sqr*RO**2)/(RO**2+vect_sqr+h_sqr-2*vect1*RO)*tan_omega))
    
    h=h_min_uncooled                                                           # from melt to top of shaded exposed uncooled wall
    h_sqr=h**2
    F_test_shade_uncooled=(1/np.pi*atan1-
        (RO**2-vect_sqr-h_sqr)/(np.pi*np.sqrt((RO**2+vect_sqr+h_sqr)**2-4*vect_sqr*RO**2))*
        np.arctan(np.sqrt((RO**2+vect_sqr+h_sqr)**2-4*vect_sqr*RO**2)/(RO**2+vect_sqr+h_sqr-2*vect1*RO)*tan_omega))
    
    #F_test_shade_uncooled[np.isnan(F_test_shade_uncooled)] = 0
    F_test_uncooled=F_test_tot_uncooled-F_test_shade_uncooled                  # melt to uncooled wall as total uncooled wall minus shaded uncooled wall
    
    F_test_wall=F_test_wall-F_test_uncooled                                    # melt to wall as total unshaded wall minus unshaded uncooled wall
    
    #F_test_wall[np.isnan(F_test_wall)] = 0
    
    VF[3,1]=np.sum(F_test_wall)/num_elem
    VF[3,6]=np.sum(F_test_uncooled)/num_elem

    return VF
