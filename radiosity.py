import numpy as np
from numba import njit

@njit(cache=True)  #, fastmath=True
def emissivity(T, T_gas, phase, p, c, X):
    ap = 0.998                                                                 #absolute pressure
    d_h = 2*p["r_EAF_up"] #diameter of cylinder inner EAF
    H = p["h_wall"]+0.5*p["h_hole"]*(p["r_hole"]/p["r_EAF_up"]) #height of cylinder inner EAF
    S_gl = 0.9*d_h/(1+0.5*d_h/H) #equal layer thickness: for circular cylinder
    
    T_w = T
    T_g = T
    
    p_H2O = X["H2O"]*ap*(T_w/T_gas)
    if X["H2O"] < 0.001:
        Ep_H2O = 0
    else:
        f = (1.785-0.39*p_H2O*S_gl+0.2436*(p_H2O*S_gl)**2)*p_H2O*S_gl
        g = 1+(0.11923/(0.137+(p_H2O*S_gl)**0.79))*(10**(-3)*T_g-0.273-0.99/(0.495+(p_H2O*S_gl)**4))**2
        Ep_Inf = 0.747-0.168*(10**(-3))*T_g
        Ep_H2O = Ep_Inf*(1-np.exp(-f*g))
    
    p_CO2 = X["CO2"]*ap*(T_w/T_gas)        
    
    if X["CO2"] < 0.001:
        Ep_CO2 = 0
    else:
        a_Inf = np.array([0.2520,0.10,-0.0955,-0.0303])
        bb = np.array([0.1166,0.0658,-0.0535,-0.0806])
        cc = np.array([0.04,0.0245,0.0130,0.0816])
        dd = np.array([0.477,1.712,0.115,0.691])
        mm = np.array([1.542,0.250,2.450,0.130])
        nn = np.array([0.802,0.715,1.076,0.495])
        aa = (bb*(p_CO2*S_gl)**nn)/(cc+(p_CO2*S_gl)**nn)+(a_Inf-bb)*((p_CO2*S_gl)**mm)/(dd+(p_CO2*S_gl)**mm)
        Gama =(1273-T_g)/1000
        Ep_CO2 = aa[0]+aa[1]*Gama+aa[2]*(Gama**2)+aa[3]*(Gama**3)
        
    if Ep_CO2 < 0.001:
        Ep_H2O_CO2 = Ep_H2O
    else:
        if Ep_H2O < 0.001:
            Ep_H2O_CO2 = Ep_CO2
        else:
            pS = p_H2O*S_gl+p_CO2*S_gl
            z_1 = p_CO2*S_gl/pS
            f_k = 1+((0.25*pS)/(0.11*pS))*(1-z_1)*np.log(1-z_1)
            Ep_H2O_CO2 = (Ep_H2O+Ep_CO2)*f_k
    
    p_CO = X["CO"]*ap*(T_w/T_gas)
    T_g_star = (9/5)*T_g   #[R]-[K]
    pS_CO = (1/1.01325)*(1/0.3048)*p_CO*S_gl; #[atmft]-[barm]
    
    if (pS_CO<0.000 or T_g_star<900): #No data
        Ep_CO = 0
    else:
        if T_g_star>2200: #No data
            T_g_star = 2200
        if pS_CO>16: #No data
            pS_CO = 16
    
        Ep=c["Ep1_CO"]
        temp = c["temp2_CO"]
        ps = c["ps2_CO"]
        a=T_g_star
        b=pS_CO
    
        help1=0
        for i in range(1,len(temp)):
            if a<=temp[i]:
                help1=i
                break
            
        help2=0
        for i in range (1,len(ps)):
            if b<=ps[i]:
                help2=i
                break
    
        help3 = ((a-temp[help1-1])/(temp[help1]-temp[help1-1])*Ep[help2-1,help1])+((temp[help1]-a)/(temp[help1]-temp[help1-1])*Ep[help2-1,help1-1])
        help4 = ((a-temp[help1-1])/(temp[help1]-temp[help1-1])*Ep[help2,help1])+((temp[help1]-a)/(temp[help1]-temp[help1-1])*Ep[help2,help1-1])
        Ep_CO = ((b-ps[help2-1])/(ps[help2]-ps[help2-1])*help4)+((ps[help2]-b)/(ps[help2]-ps[help2-1])*help3)    
    
    pS_CH4 = X["CH4"]*ap*(T_w/T_gas)

    if (pS_CH4<0.000 or T_g<300):
        Ep_CH4=0
    else:
        if pS_CH4>0.6:
            pS_CH4=0.6

        if T_g>2000:
            T_g=2000


    #Ep_CH4 = interp.CH4(T_g,pS_CH4);
    
    
        Ep=c["Ep1_CH4"]
        temp = c["temp2_CH4"]
        ps = c["ps2_CH4"]
        a=T_g
        b=pS_CH4
    
        help1=0
        for i in range(1,len(temp)):
            if a<=temp[i]:
                help1=i
                break
    
        help2=0
        for i in range(1,len(ps)):
            if b<=ps[i]:
                help2=i
                break

        
        help3 = ((a-temp[help1-1])/(temp[help1]-temp[help1-1])*Ep[help2-1,help1])+((temp[help1]-a)/(temp[help1]-temp[help1-1])*Ep[help2-1,help1-1])
        help4 = ((a-temp[help1-1])/(temp[help1]-temp[help1-1])*Ep[help2,help1])+((temp[help1]-a)/(temp[help1]-temp[help1-1])*Ep[help2,help1-1])
        Ep_CH4 = ((b-ps[help2-1])/(ps[help2]-ps[help2-1])*help4)+((ps[help2]-b)/(ps[help2]-ps[help2-1])*help3)


    Ep_g = Ep_H2O_CO2+Ep_CO+Ep_CH4  
    
    #Adsorption
    
    Av_H2O = Ep_H2O*(T_gas/T_w)**(0.45)
    Av_CO2 = Ep_CO2*(T_gas/T_w)**(0.65)
    Av_CO  = Ep_CO
    Av_CH4 = Ep_CH4
    
    if Ep_CO2==0:
        Av_H2O_CO2 = Av_H2O
    else:
        if Ep_H2O==0:
            Av_H2O_CO2 = Av_CO2
        else:
            pS = p_H2O*S_gl+p_CO2*S_gl
            z_1 = p_CO2*S_gl/pS
            f_k = 1+((0.25*pS)/(0.11*pS))*(1-z_1)*np.log(1-z_1)
            Av_H2O_CO2 = (Av_H2O+Av_CO2)*f_k

    Av_g = Av_H2O_CO2 + Av_CO +Av_CH4

    #Emissivity of the dust Hofer: 3.2.4.3.2.1.--------------------------------
    #data from VDI heat atlas Kd6 example 4(increased dust load)
    Q_abs = 0.9
    Q_rstr = 0.025
    B_st = 0.2*(p["h_scrap"]/p["h_EAF_up"])**2+0.001
    Rho_st = 5600
    d_p = 500e-6
    
    #f_1=thyp(B_st,0.005,2000,1);        #factor wether to choose calculation for low dust load
    #f_1 = 0 for B_st<0.005 and f_1=1 for B_st>0.005
    f_1 = ((np.tanh((B_st-0.005)*2000)+1)*0.5)**1
        

    #Ep_st = 1- exp(-Phi_st); only valid if the Phi_st<0.5
    Alpha = (1+2*Q_rstr/Q_abs)**(1/2)
    Beta = (Alpha-1)/(Alpha+1)
    Phi_st = 3*Q_abs*B_st*S_gl/(2*Rho_st*d_p)*Alpha
    # if B_st<0.005kg/m³ then following equation is valid, otherwise total
    # gas emissivity and absorption must be calculated together
    Ep_st = (1-Beta)*(1-np.exp(-Phi_st))/(1+Beta*np.exp(-Phi_st))                    # High dust load


# Output-------------------------------------------------------------------


    Ep_g_st_1 = (Ep_g+Ep_st-Ep_g*Ep_st)*(1-f_1)                                          # total gas and dust emissivity
    Av_g_st_1 = (Av_g+Ep_st-Av_g*Ep_st)*(1-f_1)

    K_emi_g = -np.log(max(1-Ep_g,0))/S_gl
    K_abs_g = -np.log(max(1-Av_g,0))/S_gl

    Phi_emi_g_st = (3/(2*Rho_st*d_p)*Q_abs*B_st+K_emi_g)*S_gl*Alpha
    Phi_abs_g_st = (3/(2*Rho_st*d_p)*Q_abs*B_st+K_abs_g)*S_gl*Alpha
    Ep_g_st_2 = ((1-Beta)*(1-np.exp(-Phi_emi_g_st))/(1+Beta*np.exp(-Phi_emi_g_st)))*f_1
    Av_g_st_2 = ((1-Beta)*(1-np.exp(-Phi_abs_g_st))/(1+Beta*np.exp(-Phi_abs_g_st)))*f_1

    Ep_g_st = Ep_g_st_1+Ep_g_st_2
    Av_g_st = Av_g_st_1+Av_g_st_2

    if phase == 1:
        eps_A = Ep_g_st
    if phase == 2:
        eps_A = Av_g_st


    
    return eps_A

@njit(cache=True) #, fastmath=True
def radiosity(p, c, X, iv, ind, Q, K, y, A):

    iv["J"] = np.zeros(8)
    iv["eps_gas"] = 0
    iv["transm"] = np.zeros(8)
    iv["abs_gas"] = np.zeros(8)

    ## radiosity, emissivity and absorption
    
    # numbers of phases or walls corresponding to View Factor calculation,
    # J, eps and abs
    
    # 0: roof
    # 1: wall
    # 2: solid scrap
    # 3: liquid scrap
    # 4: arc
    # 5: electrode shaft
    # 6: uncooled wall and roof heart
    # 7: electrode tip

    T_arc = (Q["arc_rad"]*1e6/(c["sigma_sb"]*c["Ep"][4]*A[4]))**(1/4)
    #print(T_arc)
    
    T= np.zeros(8,dtype=np.float64)
    T[0] = y[ind["T_roof"]] #Temp(1)
    T[1] = y[ind["T_wall"]] #Temp(2)
    T[2] = y[ind["T_st_sol"]] #Temp(3)
    T[3] = y[ind["T_st_liq"]] - y[ind["h_slag"]] * K["slag_T_reduc"] # reduce melt surface temperature by .75k per cm of foaming slag height (Temp(4))
    if T[3] <= 10:
        T[3] = 10
    T[4] = 2800    #set temporarily for calculation of absorptivity because emissivity cannot be calculated for higher T (Temp(5))
    T[5] =  y[ind["T_el"]]#temperature of electrode shaft
    T[6] = y[ind["T_wall_unc"]] # uncooled wall (Temp7)
    T[7] = iv["T_el_tip"] #temperature of electrode tip

    
    # absorption: 1-6 according to appropriate wall
    #phase = 2 for absorption coefficient; 1 for emissivity
    for i in range(0,8):
        iv["abs_gas"][i] = emissivity(T=T[i], T_gas=y[ind["T_gas"]], phase=int(2), p=p, c=c, X=X)
    
    iv["transm"] = 1-iv["abs_gas"]
    iv["eps_gas"] = emissivity(T=y[ind["T_gas"]], T_gas=y[ind["T_gas"]], phase=int(1), p=p, c=c, X=X)
    T[4]=T_arc
    #T_array = T #np.array(T)
    
    E = c["Ep"]*(T**4)*c["sigma_sb"]                                                  # emission of each surface 
    E_gas = (np.ones(8)-c["Ep"])*iv["eps_gas"]*c["sigma_sb"]*y[ind["T_gas"]]**4                                   # emission of the gas reflected by each surface

    t_Jk_Fik = np.zeros((8,8))                                                          # matrix for irradiation
    e_mat = np.eye(8)                                                             # unit matrix

    for j in range(0,8):
        for k in range(0,8):
            t_Jk_Fik[j,k]=iv["transm"][k]*(c["Ep"][j]-1)*iv["VF"][j,k]+e_mat[j,k]             # matrix for irradiation
    
    iv["J"] = np.linalg.lstsq(t_Jk_Fik,(E+E_gas))[0]  #,rcond=None
