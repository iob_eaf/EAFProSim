import numpy as np
import h5py
from scipy import signal
from scipy.interpolate import PchipInterpolator



def data_input(ind, OpCh, comp, K, dataFile='data/dummy_data.h5', hdf=None):#dummy_data

    OffGasFlowAdaptFactor = 1
    
    if hdf is None: hdf = h5py.File(dataFile, 'r')
    
    # time [s]
    t = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/timesteps'))
    # Number of basket charged
    basket = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/num_baskets'))
    # Arc Power [MW]
    P_wirk = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/electrical_power'))
    # arc voltage [V]
    arc_voltage = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/arc_voltage'))
    # arc current [kA]
    arc_current = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/arc_current'))
    # O2 rate [kg/s]
    O2_rate = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/oxygen_flowrate_jetbox_lance'))
    # O2 for postcombustion [kg/s]
    O2_post_comb = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/oxygen_flowrate_postcombustion'))
    # CH4 rate [kg/s]
    CH4_rate = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/naturalgas_flowrate_cumulative'))
    # H2 rate [kg/s]
    H2_rate = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/hydrogen_flowrate_cumulative'))
    # O2 for CH4 combustion rate [kg/s]
    O2_comb_rate = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/oxygen_flowrate_burner'))
    # Off-gas extractio rate [kg/s]
    Off_gas_rate = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/offgas_flowrate')) * OffGasFlowAdaptFactor
    # roof cooling water flow [kg/s]
    cooling_roof_rate = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/cooling_roof_flowrate'))
    # wall cooling water flow [kg/s]
    cooling_wall_rate = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/cooling_wall_flowrate'))
    # wall cooling water inlet temperature [K]
    cooling_wall_temp_in = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/cooling_wall_temperature_inlet'))
    # roof cooling water inlet temperature [K]
    cooling_roof_temp_in = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/cooling_roof_temperature_inlet'))
    # roof cooling water outlet temperature [K]
    # for validation
    cooling_roof_temp_out = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/cooling_roof_temperature_outlet'))
    # wall cooling water outlet temperature [K]
    # for validation
    cooling_wall_temp_out = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/cooling_wall_temperature_outlet'))
    # C injection rate [kg/s]
    C_inj_rate = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/injection_carbon_flowrate'))
    C_inj_material = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/injection_carbon_material'))
    C_inj_material = C_inj_material.tolist().decode('utf-8')
    
    #values for convejor air and leakair
    conv_gas_rate = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/conv_gas'))
    leakair_rate = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/leakair_flowrate'))
    
    dust_inj_rate = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/injection_dust_flowrate'))
    if None in dust_inj_rate:
        dust_inj_rate = np.zeros(len(t))
    dust_inj_material = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/injection_dust_material'))
    if dust_inj_material == None:
        dust_inj_material = ""
    else:
        dust_inj_material = dust_inj_material.tolist().decode('utf-8')

    mass_scrap_basket = np.array(hdf.get(f'{OpCh["charge_num"]}/basket/mass_scrap_basket'))
    basket_number = np.array(hdf.get(f'{OpCh["charge_num"]}/basket/basket_number'))
    basket_material = np.array(hdf.get(f'{OpCh["charge_num"]}/basket/basket_material'))
    for i in range(0, len(basket_material)):
        basket_material[i] = basket_material[i].decode('utf-8')#.replace(" ", "")
    
    elements = np.array(hdf.get(f'{OpCh["charge_num"]}/comp/header'))
    for i in range(0, len(elements)):
        elements[i] = elements[i].decode('utf-8')
    ind["comp_H2O"] = np.where(elements=='H2O')[0][0]
    ind["comp_Combustables"] = np.where(elements=='Combustables')[0][0]
    ind["comp_O"] = np.where(elements=='O')[0][0]
    ind["comp_C"] = np.where(elements=='C')[0][0]
    ind["comp_Si"] = np.where(elements=='Si')[0][0]
    ind["comp_Mn"] = np.where(elements=='Mn')[0][0]
    ind["comp_Cr"] = np.where(elements=='Cr')[0][0]
    ind["comp_P"] = np.where(elements=='P')[0][0]
    ind["comp_Fe"] = np.where(elements=='Fe')[0][0]
    ind["comp_S"] = np.where(elements=='S')[0][0]
    ind["comp_Al"] = np.where(elements=='Al')[0][0]
    ind["comp_Cu"] = np.where(elements=='Cu')[0][0]
    ind["comp_Ni"] = np.where(elements=='Ni')[0][0]
    ind["comp_Mo"] = np.where(elements=='Mo')[0][0]
    ind["comp_As"] = np.where(elements=='As')[0][0]
    ind["comp_Bi"] = np.where(elements=='Bi')[0][0]
    ind["comp_Sb"] = np.where(elements=='Sb')[0][0]
    ind["comp_Sn"] = np.where(elements=='Sn')[0][0]
    ind["comp_V"] = np.where(elements=='V')[0][0]
    ind["comp_CaO"] = np.where(elements=='CaO')[0][0]
    ind["comp_MgO"] = np.where(elements=='MgO')[0][0]
    ind["comp_Al2O3"] = np.where(elements=='Al2O3')[0][0]
    ind["comp_SiO2"] = np.where(elements=='SiO2')[0][0]
    ind["comp_MnO"] = np.where(elements=='MnO')[0][0]
    ind["comp_Cr2O3"] = np.where(elements=='Cr2O3')[0][0]
    ind["comp_P2O5"] = np.where(elements=='P2O5')[0][0]
    ind["comp_FeO"] = np.where(elements=='FeO')[0][0]
    ind["comp_CaS"] = np.where(elements=='CaS')[0][0]
    
    materials = np.array(hdf.get(f'{OpCh["charge_num"]}/comp/material'))
    for i in range(0, len(materials)):
        materials[i] = materials[i].decode('utf-8')#.replace(" ", "")

    #for el in materials:
    for idx, el in enumerate(basket_material):
        a = np.array(hdf.get(f'{OpCh["charge_num"]}/comp/{el}'))
        if a[np.where(elements=="C")].item() >= 0.5:
            for i in range(0, len(elements)):
                try:
                    OpCh[f"mass_coal"][basket_number[idx]-1][ind[f"comp_{elements[i]}"]] = OpCh[f"mass_coal"][basket_number[idx]-1][ind[f"comp_{elements[i]}"]] + a[i] * mass_scrap_basket[idx]
                except:
                    pass
        else:
            for i in range(0, len(elements)):
                try:
                    OpCh[f"mass_scrap"][basket_number[idx]-1][ind[f"comp_{elements[i]}"]] = OpCh[f"mass_scrap"][basket_number[idx]-1][ind[f"comp_{elements[i]}"]] + a[i] * mass_scrap_basket[idx]
                except:
                    pass
    
    hotheel_comp = np.array(hdf.get(f'{OpCh["charge_num"]}/basket/hotheel_composition'))
    if not None in hotheel_comp:
        for i in range(0,len(elements)):
            OpCh["hotheel_comp"][ind[f"comp_{elements[i]}"]] = hotheel_comp[ind[f"comp_{elements[i]}"]]
    
    b = np.array(hdf.get(f'{OpCh["charge_num"]}/comp/{C_inj_material}'))
    comp["C_inj_C_fix"] = b[np.where(elements=='C')].item()
    comp["C_inj_C_vol"] = b[np.where(elements=='Combustables')].item()
    comp["C_inj_H2"] = 0
    comp["C_inj_N2"] = 0
    comp["C_inj_O2"] = b[np.where(elements=='O')].item()
    comp["C_inj_H2O"] = b[np.where(elements=='H2O')].item()
    
    # chalk addition rate [kg/s]
    chalk_inj_rate = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/injection_chalk_flowrate'))
    chalk_inj_material = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/injection_chalk_material'))
    chalk_inj_material = chalk_inj_material.tolist().decode('utf-8')
    
    # dolomite overall addition rate [kg/s]
    dolomite_inj_rate = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/injection_dolomite_flowrate'))
    dolomite_inj_material = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/injection_dolomite_material'))
    dolomite_inj_material = dolomite_inj_material.tolist().decode('utf-8')
    # dri addition rate [kg/s]
    dri_inj_rate = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/injection_dri_flowrate'))
    dri_inj_material = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/injection_dri_material'))
    dri_inj_material = dri_inj_material.tolist().decode('utf-8')
    dri_temp = np.array(hdf.get(f'{OpCh["charge_num"]}/cyclic/injection_dri_temperature'))
    if None in dri_inj_rate:
        dri_inj_rate = np.zeros(len(t))
    elif dri_temp.any() == None:
        dri_temp = np.ones(len(t)) * 293
    
    
    dri_comp = np.array(hdf.get(f'{OpCh["charge_num"]}/comp/{dri_inj_material}'))
    dri_inj = np.zeros((len(t),35))
    if not None in dri_comp:
        for i in range(0, len(elements)):
            try:
                dri_inj[:,ind[f"comp_{elements[i]}"]] = dri_inj[:,ind[f"comp_{elements[i]}"]] + dri_comp[i] * dri_inj_rate
            except:
                pass
        

    # slagformer addition rate for each species [kg/s]
    chalk_comp = np.array(hdf.get(f'{OpCh["charge_num"]}/comp/{chalk_inj_material}'))
    dolo_comp = np.array(hdf.get(f'{OpCh["charge_num"]}/comp/{dolomite_inj_material}'))
    slagformer_inj = np.zeros((len(t),35))
    if not None in chalk_comp:
        for i in range(0, len(elements)):
            try:
                slagformer_inj[:,ind[f"comp_{elements[i]}"]] = slagformer_inj[:,ind[f"comp_{elements[i]}"]] + chalk_comp[i] * chalk_inj_rate
            except:
                pass

    if not None in dolo_comp:
        for i in range(0, len(elements)):
            try:
                slagformer_inj[:,ind[f"comp_{elements[i]}"]] = slagformer_inj[:,ind[f"comp_{elements[i]}"]] + dolo_comp[i] * dolomite_inj_rate
            except:
                pass
    
    if dust_inj_material == "":
        comp["dust_CaO"] = 0
        comp["dust_MgO"] = 0
        comp["dust_Al2O3"] = 0
        comp["dust_SiO2"] = 0
        comp["dust_MnO"] = 0
        comp["dust_Cr2O3"] = 0
        comp["dust_P2O5"] = 0
        comp["dust_CaS"] = 0
        comp["dust_FeO"] = 0
        comp["dust_C"] = 0
    else:
        dust = np.array(hdf.get(f'{OpCh["charge_num"]}/comp/{dust_inj_material}'))
        comp["dust_CaO"] = dust[np.where(elements=='CaO')].item()
        comp["dust_MgO"] = dust[np.where(elements=='MgO')].item()
        comp["dust_Al2O3"] = dust[np.where(elements=='Al2O3')].item()
        comp["dust_SiO2"] = dust[np.where(elements=='SiO2')].item()
        comp["dust_MnO"] = dust[np.where(elements=='MnO')].item()
        comp["dust_Cr2O3"] = dust[np.where(elements=='Cr2O3')].item()
        comp["dust_P2O5"] = dust[np.where(elements=='P2O5')].item()
        comp["dust_CaS"] = dust[np.where(elements=='CaS')].item()
        comp["dust_FeO"] = dust[np.where(elements=='FeO')].item()
        comp["dust_C"] = dust[np.where(elements=='C')].item()
        
    
    hdf.close()

    matrix = np.zeros([len(t), 96])
    matrix[:,0] = t - t[0]
    matrix[:,1] = P_wirk
    matrix[:,2] = arc_voltage
    matrix[:,3] = arc_current

    matrix[:,4] = O2_rate
    matrix[:,5] = O2_post_comb
    matrix[:,6] = C_inj_rate
    matrix[:,7] = Off_gas_rate
    if not None in leakair_rate:
        matrix[:,8] = leakair_rate
    else:
        matrix[:,8] = np.zeros(len(t))
    matrix[:,9] = CH4_rate
    matrix[:,10] = O2_comb_rate
    matrix[:,11] = basket
    matrix[:,12] = cooling_roof_rate
    matrix[:,13] = cooling_wall_rate
    matrix[:,14] = cooling_roof_temp_in
    matrix[:,15] = cooling_wall_temp_in

    # roof cooling water flow rate rate of change [kg/s²]
    matrix[:,16] = np.gradient(matrix[:,12], matrix[:,0])
    # wall cooling water flow rate rate of change [kg/s²]
    matrix[:,17] = np.gradient(matrix[:,13], matrix[:,0])
    # roof cooling water inlet temperature rate rate of change [K/s]
    matrix[:,18] = np.gradient(matrix[:,14], matrix[:,0])
    # wall cooling water inlet temperature rate rate of change [K/s]
    matrix[:,19] = np.gradient(matrix[:,15], matrix[:,0])
     
    # water outlet temperatur for setting initial condition of each basket
    matrix[:,20] = cooling_roof_temp_out
    matrix[:,21] = cooling_wall_temp_out
    
    # coal lance conveyor gas mass flow [kg/s] ATTENTION: is used for partial direct combustion of coal; other conveyor gases (lime, dust) have to be defined separately!
    # if no value for coal lance conveyer gas
    if not None in conv_gas_rate:
        matrix[:,22] = conv_gas_rate
    else:
        matrix[:,22] = matrix[:,4] * K["conv_gas"]

    matrix[:,24] = dri_temp
    matrix[:,25] = dust_inj_rate
    matrix[:,26:26+35] = dri_inj
    matrix[:,26+35:26+2*35] = slagformer_inj

    OpCh["OpCh_data"][0:matrix.shape[0],0:matrix.shape[1]] = matrix
    #OpCh["OpCh_data"] = matrix
