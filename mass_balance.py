"""
Created around November 2023

@author: Lilly Schüttensack
"""


import os
import sys
import inspect
currentdir  =  os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir  =  os.path.dirname(currentdir)
sys.path.insert(0, parentdir) 
import pandas as pd
#from InputData import InputData 
import numpy as np
#from termcolor import colored # not needed, just for display
import h5py



#inp = InputData()

# Results of the simulation
#python_y = pd.read_excel(r'DebuggingSolve_IVP.xlsx', sheet_name = "y_output")
#python_OpCh  =  pd.read_excel(r'DebuggingSolve_IVP.xlsx', sheet_name = "OpCh_output")
#python_iv  =  pd.read_excel(r'DebuggingSolve_IVP.xlsx', sheet_name = "iv_output")
def mass_balance(dataFile, valid_charge_list):
    hdf = h5py.File(dataFile, 'r')
    
    for charge in valid_charge_list:
        t = np.array(hdf.get(f'{charge}/t'))
        m_CH4_burn = np.array(hdf.get(f'{charge}/OpCh/m_CH4_burn'))
        m_C_inj = np.array(hdf.get(f'{charge}/OpCh/m_C_inj'))
        m_O2_post = np.array(hdf.get(f'{charge}/OpCh/m_O2_post'))
        m_O2_lance = np.array(hdf.get(f'{charge}/OpCh/m_O2_lance'))
        m_O2_burn = np.array(hdf.get(f'{charge}/OpCh/m_O2_CH4_burn')) + np.array(hdf.get(f'{charge}/OpCh/m_O2_H2_burn'))
        m_conv_gas = np.array(hdf.get(f'{charge}/OpCh/m_conv_gas'))
        m_slag_form = np.array(hdf.get(f'{charge}/OpCh/m_slag_form'))
        basket = np.array(hdf.get(f'{charge}/OpCh/basket_number'))
        max_basket = int(np.max(basket))
        m_water_el = np.array(hdf.get(f'{charge}/iv/m_water_in'))
        m_coal = np.array(hdf.get(f'{charge}/y/m_coal'))
        m_chalk = np.array(hdf.get(f'{charge}/OpCh/m_chalk'))
        m_dolomite = np.array(hdf.get(f'{charge}/OpCh/m_dolomite'))
        mass_dri_inj_O = np.array(hdf.get(f'{charge}/OpCh/mass_dri_inj_O'))
        mass_dri_inj_C = np.array(hdf.get(f'{charge}/OpCh/mass_dri_inj_C'))
        mass_dri_inj_Si = np.array(hdf.get(f'{charge}/OpCh/mass_dri_inj_Si'))
        mass_dri_inj_Mn = np.array(hdf.get(f'{charge}/OpCh/mass_dri_inj_Mn'))
        mass_dri_inj_Cr = np.array(hdf.get(f'{charge}/OpCh/mass_dri_inj_Cr'))
        mass_dri_inj_P = np.array(hdf.get(f'{charge}/OpCh/mass_dri_inj_P'))
        mass_dri_inj_S = np.array(hdf.get(f'{charge}/OpCh/mass_dri_inj_S'))
        mass_dri_inj_Fe = np.array(hdf.get(f'{charge}/OpCh/mass_dri_inj_Fe'))
        # st_liq
        m_C_st_liq = np.array(hdf.get(f'{charge}/y/m_C_st_liq'))
        m_Si_st_liq = np.array(hdf.get(f'{charge}/y/m_Si_st_liq'))
        m_Mn_st_liq = np.array(hdf.get(f'{charge}/y/m_Mn_st_liq'))
        m_Cr_st_liq = np.array(hdf.get(f'{charge}/y/m_Cr_st_liq'))
        m_P_st_liq = np.array(hdf.get(f'{charge}/y/m_P_st_liq'))
        m_S_st_liq = np.array(hdf.get(f'{charge}/y/m_S_st_liq'))
        m_O_st_liq = np.array(hdf.get(f'{charge}/y/m_O_st_liq'))
        m_Fe_st_liq = np.array(hdf.get(f'{charge}/y/m_Fe_st_liq'))
        # st_sol
        m_C_st_sol = np.array(hdf.get(f'{charge}/y/m_C_st_sol'))
        m_Si_st_sol = np.array(hdf.get(f'{charge}/y/m_Si_st_sol'))
        m_Mn_st_sol = np.array(hdf.get(f'{charge}/y/m_Mn_st_sol'))
        m_Cr_st_sol = np.array(hdf.get(f'{charge}/y/m_Cr_st_sol'))
        m_P_st_sol = np.array(hdf.get(f'{charge}/y/m_P_st_sol'))
        m_S_st_sol = np.array(hdf.get(f'{charge}/y/m_S_st_sol'))
        m_Fe_st_sol = np.array(hdf.get(f'{charge}/y/m_Fe_st_sol'))
        m_O_st_sol = np.array(hdf.get(f'{charge}/y/m_O_st_sol'))
        m_st_sol = np.array(hdf.get(f'{charge}/y/m_st_sol'))
        # sl_liq
        m_SiO2_sl_liq = np.array(hdf.get(f'{charge}/y/m_SiO2_sl_liq'))
        m_MnO_sl_liq = np.array(hdf.get(f'{charge}/y/m_MnO_sl_liq'))
        m_Cr2O3_sl_liq = np.array(hdf.get(f'{charge}/y/m_Cr2O3_sl_liq'))
        m_P2O5_sl_liq = np.array(hdf.get(f'{charge}/y/m_P2O5_sl_liq'))
        m_CaS_sl_liq = np.array(hdf.get(f'{charge}/y/m_CaS_sl_liq'))
        m_FeO_sl_liq = np.array(hdf.get(f'{charge}/y/m_FeO_sl_liq'))
        m_CaO_sl_liq = np.array(hdf.get(f'{charge}/y/m_CaO_sl_liq'))
        m_MgO_sl_liq = np.array(hdf.get(f'{charge}/y/m_MgO_sl_liq'))
        m_Al2O3_sl_liq = np.array(hdf.get(f'{charge}/y/m_Al2O3_sl_liq'))
        # sl_sol
        m_SiO2_sl_sol = np.array(hdf.get(f'{charge}/y/m_SiO2_sl_sol'))
        m_MnO_sl_sol = np.array(hdf.get(f'{charge}/y/m_MnO_sl_sol'))
        m_Cr2O3_sl_sol = np.array(hdf.get(f'{charge}/y/m_Cr2O3_sl_sol'))
        m_P2O5_sl_sol = np.array(hdf.get(f'{charge}/y/m_P2O5_sl_sol'))
        m_CaS_sl_sol = np.array(hdf.get(f'{charge}/y/m_CaS_sl_sol'))
        m_FeO_sl_sol = np.array(hdf.get(f'{charge}/y/m_FeO_sl_sol'))
        m_CaO_sl_sol = np.array(hdf.get(f'{charge}/y/m_CaO_sl_sol'))
        m_MgO_sl_sol = np.array(hdf.get(f'{charge}/y/m_MgO_sl_sol'))
        m_Al2O3_sl_sol = np.array(hdf.get(f'{charge}/y/m_Al2O3_sl_sol'))
        #gas
        m_CO = np.array(hdf.get(f'{charge}/y/m_CO'))
        m_CO2 = np.array(hdf.get(f'{charge}/y/m_CO2'))
        m_CH4 = np.array(hdf.get(f'{charge}/y/m_CH4'))
        m_N2 = np.array(hdf.get(f'{charge}/y/m_N2'))
        m_O2 = np.array(hdf.get(f'{charge}/y/m_O2'))
        m_H2O = np.array(hdf.get(f'{charge}/y/m_H2O'))
        m_H2 = np.array(hdf.get(f'{charge}/y/m_H2'))
        
        m_resolid = np.array(hdf.get(f'{charge}/y/m_resolid'))
        m_el = np.array(hdf.get(f'{charge}/y/m_el'))
        m_comb_scrap = np.array(hdf.get(f'{charge}/y/m_comb_scrap'))
        
        m_vol_coal = np.array(hdf.get(f'{charge}/y/m_vol_coal'))
        C_inj_C_fix = np.array(hdf.get(f'{charge}/comp/C_inj_C_fix'))
        C_inj_C_vol = np.array(hdf.get(f'{charge}/comp/C_inj_C_vol'))
        C_inj_N2 = np.array(hdf.get(f'{charge}/comp/C_inj_N2'))
        C_inj_O2 = np.array(hdf.get(f'{charge}/comp/C_inj_O2'))
        C_inj_H2O = np.array(hdf.get(f'{charge}/comp/C_inj_H2O'))
        C_inj_H2 = np.array(hdf.get(f'{charge}/comp/C_inj_H2'))
        C_char_C_fix = np.array(hdf.get(f'{charge}/comp/C_char_C_fix'))
        C_char_C_vol = np.array(hdf.get(f'{charge}/comp/C_char_C_vol'))
        C_char_N2 = np.array(hdf.get(f'{charge}/comp/C_char_N2'))
        C_char_O2 = np.array(hdf.get(f'{charge}/comp/C_char_O2'))
        C_char_H2O = np.array(hdf.get(f'{charge}/comp/C_char_H2O'))
        C_char_H2 = np.array(hdf.get(f'{charge}/comp/C_char_H2'))
        air_O2 = np.array(hdf.get(f'{charge}/comp/air_O2'))
        air_N2 = np.array(hdf.get(f'{charge}/comp/air_N2'))
        gas_CH4 = np.array(hdf.get(f'{charge}/comp/gas_CH4'))
        gas_N2 = np.array(hdf.get(f'{charge}/comp/gas_N2'))
        O2_N2 = np.array(hdf.get(f'{charge}/comp/O2_N2'))
        O2_O2 = np.array(hdf.get(f'{charge}/comp/O2_O2'))
        chalk_CaO = np.array(hdf.get(f'{charge}/comp/chalk_CaO'))
        chalk_MgO = np.array(hdf.get(f'{charge}/comp/chalk_MgO'))
        chalk_Al2O3 = np.array(hdf.get(f'{charge}/comp/chalk_Al2O3'))
        chalk_SiO2 = np.array(hdf.get(f'{charge}/comp/chalk_SiO2'))
        chalk_MnO = np.array(hdf.get(f'{charge}/comp/chalk_MnO'))
        chalk_Cr2O3 = np.array(hdf.get(f'{charge}/comp/chalk_Cr2O3'))
        chalk_P2O5 = np.array(hdf.get(f'{charge}/comp/chalk_P2O5'))
        chalk_CaS = np.array(hdf.get(f'{charge}/comp/chalk_CaS'))
        chalk_FeO = np.array(hdf.get(f'{charge}/comp/chalk_FeO'))
        dolomite_CaO = np.array(hdf.get(f'{charge}/comp/dolomite_CaO'))
        dolomite_MgO = np.array(hdf.get(f'{charge}/comp/dolomite_MgO'))
        dolomite_Al2O3 = np.array(hdf.get(f'{charge}/comp/dolomite_Al2O3'))
        dolomite_SiO2 = np.array(hdf.get(f'{charge}/comp/dolomite_SiO2'))
        dolomite_MnO = np.array(hdf.get(f'{charge}/comp/dolomite_MnO'))
        dolomite_Cr2O3 = np.array(hdf.get(f'{charge}/comp/dolomite_Cr2O3'))
        dolomite_P2O5 = np.array(hdf.get(f'{charge}/comp/dolomite_P2O5'))
        dolomite_CaS = np.array(hdf.get(f'{charge}/comp/dolomite_CaS'))
        dolomite_FeO = np.array(hdf.get(f'{charge}/comp/dolomite_FeO'))
        
        CO_extraction = np.array(hdf.get(f'{charge}/eval/CO_extraction'))
        CO2_extraction = np.array(hdf.get(f'{charge}/eval/CO2_extraction'))
        N2_extraction = np.array(hdf.get(f'{charge}/eval/N2_extraction'))
        O2_extraction = np.array(hdf.get(f'{charge}/eval/O2_extraction'))
        H2_extraction = np.array(hdf.get(f'{charge}/eval/H2_extraction'))
        H2O_extraction = np.array(hdf.get(f'{charge}/eval/H2O_extraction'))
        CH4_extraction = np.array(hdf.get(f'{charge}/eval/CH4_extraction'))
        air_extraction = np.array(hdf.get(f'{charge}/eval/air_extraction'))
        refractory_material = np.array(hdf.get(f'{charge}/eval/refractory_material'))
        refrac_CaO = np.array(hdf.get(f'{charge}/comp/refrac_CaO'))
        refrac_MgO = np.array(hdf.get(f'{charge}/comp/refrac_MgO'))
        refrac_Al2O3 = 0
        
        t_basket = np.array(hdf.get(f'{charge}/OpCh/t_basket'))
        
        charge_pos_end = list(np.zeros(max_basket)) #t_end_pos
        charge_pos_begin = list(np.zeros(max_basket))
        for i in range(0,max_basket):
            charge_pos_begin[i] = np.where(t_basket==np.unique(t_basket)[i])[0][0]
            charge_pos_end[i] = np.where(t_basket==np.unique(t_basket)[i])[0][-1]
        
        
        
        # Molar Masses
        M_C = 0.012011
        M_Fe = 0.055850
        M_Mn = 0.054938
        M_Si = 0.0280855
        M_P = 0.0309738
        M_Cr = 0.0519961
        M_S = 0.032065
        M_Al = 0.02698153
        M_O = 0.0159994
        #oxides
        M_FeO = 0.0718464
        M_MnO = 0.0709374
        M_SiO2 = 0.0600843
        M_P2O5 = 0.1419445
        M_Cr2O3 = 0.1519904
        M_Al2O3 = 0.10196
        M_CaO = 0.0560774
        M_MgO = 0.0403044
        M_CaS = 0.0721430
        M_Ca = M_CaO - M_O
        #gases
        M_H2 = 0.0020159
        M_N2 = 0.0280134
        M_O2 = 0.0319988
        M_CO = 0.0280104
        M_CO2 = 0.0440098
        M_CH4 = 0.0160428
        M_C9H20 = 0.128258
        M_H2O = 0.0180153
        M_air = 0.02897
        
        total_CH4 = np.trapz(m_CH4_burn, t) #m_CH4_burn
        total_C_rate = np.trapz(m_C_inj, t) # m_C_inj
        total_O2_post = np.trapz(m_O2_post,t) # m_O2_post
        total_O2_rate = np.trapz(m_O2_lance, t) # m_O2_lance
        total_O2_CH4 = np.trapz(m_O2_burn, t)  # m_O2_burn
        total_conv_gas= np.trapz(m_conv_gas, t) # m_conv_gas
        '''
        total_slag_former = np.zeros(max_basket)
        for i in range(0,max_basket):
            if i == 0:
                total_slag_former[i] = np.trapz(m_slag_form[0:charge_pos_end[i]], t[0:charge_pos_end[i]])
            else:
                total_slag_former[i] = np.trapz(m_slag_form[charge_pos_end[i-1]:charge_pos_end[i]], t[charge_pos_end[i-1]:charge_pos_end[i]])
        '''
        total_refrac = np.zeros(3)
        total_refrac[0] = refrac_CaO[0] * (refractory_material[0] - np.trapz(refractory_material, t))
        total_refrac[1] = refrac_MgO[0] * (refractory_material[0] - np.trapz(refractory_material, t))
        total_refrac[2] = refrac_Al2O3 * (refractory_material[0] - np.trapz(refractory_material, t))
        
        ######## Input ##########
        
        #H20 input through electrode cooling
        H2O_el = np.trapz(m_water_el,t)
        
        # sucked in leak air
        O2_rate_of_change = np.trapz(air_extraction, t) * air_O2[0]
        N2_rate_of_change = np.trapz(air_extraction, t) * air_N2[0]
        
        m_balance = np.zeros((14,4))
        
        #### C ####
        m_balance[0,0] = (np.sum(m_coal[charge_pos_begin]) + np.sum(m_C_st_liq[charge_pos_begin]) + np.sum(m_C_st_sol[charge_pos_begin])
                          #- (np.sum(m_resolid[charge_pos_begin] * (m_C_st_sol[charge_pos_begin]/m_st_sol[charge_pos_begin])))
                          + np.sum(m_el[charge_pos_begin]) + total_CH4 / M_CH4 * M_C + total_C_rate * (1 + C_inj_C_vol[0] / C_inj_C_fix[0])
                          + np.sum(m_comb_scrap[charge_pos_begin]) / M_C9H20 * M_C * 9 + np.sum(m_CO[charge_pos_begin]) *  M_C / M_CO
                          + np.sum(m_CO2[charge_pos_begin]) * M_C / M_CO2 +  np.sum(m_CH4[charge_pos_begin]) * M_C / M_CH4
                          + np.dot((C_char_C_vol[charge_pos_begin]) / (1 - C_char_C_fix[charge_pos_begin]), m_vol_coal[charge_pos_begin])
                          + np.trapz(mass_dri_inj_C, t))
        
        #### Si ####
        m_balance[1,0] = (np.sum(m_Si_st_liq[charge_pos_begin]) + np.sum(m_Si_st_sol[charge_pos_begin])
                          #- (np.sum(m_resolid[charge_pos_begin] * (m_Si_st_sol[charge_pos_begin]/m_st_sol[charge_pos_begin])))
                          + (np.sum(m_SiO2_sl_sol[charge_pos_begin]) + np.sum(m_SiO2_sl_liq[charge_pos_begin])) / M_SiO2 * M_Si
                          #+ (np.trapz(m_chalk, t) * chalk_SiO2[0] + np.trapz(m_dolomite, t) * dolomite_SiO2[0]) / M_SiO2 * M_Si)
                          + np.trapz(mass_dri_inj_Si, t))
        #### Mn ####
        m_balance[2,0] = (np.sum(m_Mn_st_liq[charge_pos_begin]) + np.sum(m_Mn_st_sol[charge_pos_begin])
                          #- (np.sum(m_resolid[charge_pos_begin] * (m_Mn_st_sol[charge_pos_begin]/m_st_sol[charge_pos_begin])))
                          + (np.sum(m_MnO_sl_sol[charge_pos_begin]) + np.sum(m_MnO_sl_liq[charge_pos_begin])) / M_MnO * M_Mn
                          #+ (np.trapz(m_chalk, t) * chalk_MnO[0] + np.trapz(m_dolomite, t) * dolomite_MnO[0]) / M_MnO * M_Mn)
                          + np.trapz(mass_dri_inj_Mn, t))
        #### Cr ####
        m_balance[3,0] = (np.sum(m_Cr_st_liq[charge_pos_begin]) + np.sum(m_Cr_st_sol[charge_pos_begin])
                          #- (np.sum(m_resolid[charge_pos_begin] * (m_Cr_st_sol[charge_pos_begin]/m_st_sol[charge_pos_begin])))
                          + (np.sum(m_Cr2O3_sl_sol[charge_pos_begin]) + np.sum(m_Cr2O3_sl_liq[charge_pos_begin])) / M_Cr2O3 * M_Cr * 2
                          #+ (np.trapz(m_chalk, t) * chalk_Cr2O3[0] + np.trapz(m_dolomite, t) * dolomite_Cr2O3[0]) / M_Cr2O3 * M_Cr * 2)
                          + np.trapz(mass_dri_inj_Cr, t))
        #### P ####
        m_balance[4,0] = (np.sum(m_P_st_liq[charge_pos_begin]) + np.sum(m_P_st_sol[charge_pos_begin])
                          #- (np.sum(m_resolid[charge_pos_begin] * (m_P_st_sol[charge_pos_begin]/m_st_sol[charge_pos_begin])))
                          + (np.sum(m_P2O5_sl_sol[charge_pos_begin]) + np.sum(m_P2O5_sl_liq[charge_pos_begin])) / M_P2O5 * M_P * 2
                          #+ (np.trapz(m_chalk, t) * chalk_P2O5[0] + np.trapz(m_dolomite, t) * dolomite_P2O5[0]) / M_P2O5 * M_P * 2)
                          + np.trapz(mass_dri_inj_P, t))
        #### S ####
        m_balance[12,0] = (np.sum(m_S_st_liq[charge_pos_begin]) + np.sum(m_S_st_sol[charge_pos_begin])
                           #- (np.sum(m_resolid[charge_pos_begin] * (m_S_st_sol[charge_pos_begin]/m_st_sol[charge_pos_begin])))
                           + (np.sum(m_CaS_sl_sol[charge_pos_begin]) + np.sum(m_CaS_sl_liq[charge_pos_begin])) / M_CaS * M_S
                           #+ (np.trapz(m_chalk, t) * chalk_CaS[0] + np.trapz(m_dolomite, t) * dolomite_CaS[0]) / M_CaS * M_S)
                           + np.trapz(mass_dri_inj_S, t))
        #### Fe ####
        m_balance[5,0] = (np.sum(m_Fe_st_liq[charge_pos_begin]) + np.sum(m_Fe_st_sol[charge_pos_begin])
                          #- (np.sum(m_resolid[charge_pos_begin] * (m_Fe_st_sol[charge_pos_begin]/m_st_sol[charge_pos_begin]))) + np.sum(m_resolid[charge_pos_begin])
                          + (np.sum(m_FeO_sl_sol[charge_pos_begin]) + np.sum(m_FeO_sl_liq[charge_pos_begin])) / M_FeO * M_Fe
                          #+ (np.trapz(m_chalk, t) * chalk_FeO[0] + np.trapz(m_dolomite, t) * dolomite_FeO[0]) / M_FeO * M_Fe)
                          + np.trapz(mass_dri_inj_Fe, t))
        #### N2 ####
        m_balance[6,0] = (np.sum(m_N2[charge_pos_begin]) + N2_rate_of_change + total_CH4 / gas_CH4[0] * gas_N2[0]
                          + np.dot((C_char_N2[charge_pos_begin]) / (1 - C_char_C_fix[charge_pos_begin]), m_vol_coal[charge_pos_begin])
                          + (total_O2_post + total_O2_rate + total_O2_CH4) / O2_O2[0] * O2_N2[0]
                          + total_C_rate / C_inj_C_fix[0] * C_inj_N2[0] + total_conv_gas * air_N2[0])
        #### O2 ####
        m_balance[7,0] = (total_O2_post + total_O2_rate + total_O2_CH4 + O2_rate_of_change + np.sum(m_O_st_liq[charge_pos_begin])
                          + np.sum(m_O_st_sol[charge_pos_begin]) #- (np.sum(m_resolid[charge_pos_begin] * (m_O_st_sol[charge_pos_begin]/m_st_sol[charge_pos_begin])))
                          + np.sum(m_O2[charge_pos_begin]) + total_refrac[0] / M_CaO * M_O2 / 2
                          + np.sum((m_CaO_sl_liq[charge_pos_begin] + m_CaO_sl_sol[charge_pos_begin]) / M_CaO / 2 * M_O2) # + np.trapz(m_chalk, t) * chalk_CaO[0] + np.trapz(m_dolomite, t) * dolomite_CaO[0]
                          + np.sum((m_SiO2_sl_liq[charge_pos_begin] + m_SiO2_sl_sol[charge_pos_begin]) / M_SiO2 / 2 * 2 * M_O2) # + np.trapz(m_chalk, t) * chalk_SiO2[0] + np.trapz(m_dolomite, t) * dolomite_SiO2[0]
                          + np.sum((m_MnO_sl_liq[charge_pos_begin] + m_MnO_sl_sol[charge_pos_begin]) / M_MnO / 2 * M_O2) # + np.trapz(m_chalk, t) * chalk_MnO[0] + np.trapz(m_dolomite, t) * dolomite_MnO[0]
                          + np.sum((m_Cr2O3_sl_liq[charge_pos_begin] + m_Cr2O3_sl_sol[charge_pos_begin]) / M_Cr2O3 / 2 * 3 * M_O2) # + np.trapz(m_chalk, t) * chalk_Cr2O3[0] + np.trapz(m_dolomite, t) * dolomite_Cr2O3[0]
                          + np.sum((m_P2O5_sl_liq[charge_pos_begin] + m_P2O5_sl_sol[charge_pos_begin]) / M_P2O5 / 2 * 5 * M_O2) # + np.trapz(m_chalk, t) * chalk_P2O5[0] + np.trapz(m_dolomite, t) * dolomite_P2O5[0]
                          + np.sum((m_FeO_sl_liq[charge_pos_begin] + m_FeO_sl_sol[charge_pos_begin]) / M_FeO / 2 * M_O2) # + np.trapz(m_chalk, t) * chalk_FeO[0] + np.trapz(m_dolomite, t) * dolomite_FeO[0]
                          + H2O_el / M_H2O * M_O2 / 2
                          + np.dot((C_char_O2[charge_pos_begin] + C_char_H2O[charge_pos_begin] * M_O2 / 2 / M_H2O) / (1 - C_char_C_fix[charge_pos_begin]), m_vol_coal[charge_pos_begin])
                          + np.sum(m_CO[charge_pos_begin]) * M_O2 / (M_O2 + 2 * M_C) + np.sum(m_CO2[charge_pos_begin] * M_O2 / M_CO2)
                          + np.sum(m_H2O[charge_pos_begin] * M_O2 / M_H2O) + total_C_rate / C_inj_C_fix[0] * (C_inj_O2[0] + C_inj_H2O[0] * M_O2 / (2 * M_H2O))
                          + total_conv_gas * air_O2[0] + np.trapz(mass_dri_inj_O, t))
        #### MgO ####
        m_balance[9,0] = (np.sum(m_MgO_sl_liq[charge_pos_begin]) + total_refrac[1] + np.sum(m_MgO_sl_sol[charge_pos_begin]))
                         # + np.trapz(m_chalk, t) * chalk_MgO[0] + np.trapz(m_dolomite, t) * dolomite_MgO[0])
        #### Al2O3 ####
        m_balance[10,0] = (np.sum(m_Al2O3_sl_liq[charge_pos_begin]) + total_refrac[2] + np.sum(m_Al2O3_sl_sol[charge_pos_begin]))
                          # + np.trapz(m_chalk, t) * chalk_Al2O3[0] + np.trapz(m_dolomite, t) * dolomite_Al2O3[0])
        #### Ca ####
        m_balance[8,0] = ((np.sum(m_CaO_sl_liq[charge_pos_begin]) + np.sum(m_CaO_sl_sol[charge_pos_begin]) + total_refrac[0]) * M_Ca / M_CaO 
                          #+ (np.trapz(m_chalk, t) * chalk_CaO[0] + np.trapz(m_dolomite, t) * dolomite_CaO[0]) * M_Ca / M_CaO 
                          + (np.sum(m_CaS_sl_liq[charge_pos_begin]) + np.sum(m_CaS_sl_sol[charge_pos_begin])) * M_Ca / M_CaS)
                          #+ (np.trapz(m_chalk, t) * chalk_CaS[0] + np.trapz(m_dolomite, t) * dolomite_CaS[0]) * M_Ca / M_CaS)
        #### H ####
        m_balance[11,0] = (np.sum(m_H2[charge_pos_begin]) + total_CH4 / M_CH4 * M_H2 / 2 * 4
                           + (np.sum(m_H2O[charge_pos_begin]) + H2O_el) / M_H2O * M_H2
                           + np.sum(m_comb_scrap[charge_pos_begin]) / M_C9H20 * M_H2 * 20 / 2
                           + np.sum(m_CH4[charge_pos_begin]) * 2 * M_H2 / M_CH4
                           + np.dot((C_char_H2[charge_pos_begin] + C_char_H2O[charge_pos_begin] * M_H2 / M_H2O) / (1 - C_char_C_fix[charge_pos_begin]), m_vol_coal[charge_pos_begin])
                           + total_C_rate / C_inj_C_fix[0] * (C_inj_H2[0] + C_inj_H2O[0] * M_H2 / M_H2O))
        
        #### sum ####
        m_balance[13,0] = sum(m_balance[:,0])
        
        ######## Output ##########
        
        #### C ####
        m_balance[0,1] = (np.sum(m_coal[charge_pos_end]) + np.sum(m_C_st_liq[charge_pos_end]) + np.sum(m_C_st_sol[charge_pos_end])
                          #- (np.sum(m_resolid[charge_pos_end] * (m_C_st_sol[charge_pos_end]/m_st_sol[charge_pos_end])))
                          + np.sum(m_el[charge_pos_end]) + np.sum(m_comb_scrap[charge_pos_end]) / M_C9H20 * M_C * 9
                          + abs(-np.trapz(CO_extraction, t) - np.sum(m_CO[charge_pos_end])) / M_CO * M_C
                          + abs(-np.trapz(CO2_extraction, t) - np.sum(m_CO2[charge_pos_end])) / M_CO2 * M_C
                          + abs(-np.trapz(CH4_extraction, t) - np.sum(m_CH4[charge_pos_end])) / M_CH4 * M_C
                          + np.dot((C_char_C_vol[charge_pos_end]) / (1 - C_char_C_fix[charge_pos_end]), m_vol_coal[charge_pos_end]))
        #### Si ####
        m_balance[1,1] = (np.sum(m_Si_st_liq[charge_pos_end]) + np.sum(m_Si_st_sol[charge_pos_end])
                          #- (np.sum(m_resolid[charge_pos_end] * (m_Si_st_sol[charge_pos_end]/m_st_sol[charge_pos_end])))
                          + np.sum(m_SiO2_sl_sol[charge_pos_end]) / M_SiO2 * M_Si + np.sum(m_SiO2_sl_liq[charge_pos_end]) / M_SiO2 * M_Si)
        #### Mn ####
        m_balance[2,1] = (np.sum(m_Mn_st_liq[charge_pos_end]) + np.sum(m_Mn_st_sol[charge_pos_end])
                          #- (np.sum(m_resolid[charge_pos_end] * (m_Mn_st_sol[charge_pos_end]/m_st_sol[charge_pos_end])))
                          + np.sum(m_MnO_sl_sol[charge_pos_end]) / M_MnO * M_Mn + np.sum(m_MnO_sl_liq[charge_pos_end]) / M_MnO * M_Mn)
        #### Cr ####
        m_balance[3,1] = (np.sum(m_Cr_st_liq[charge_pos_end]) + np.sum(m_Cr_st_sol[charge_pos_end])
                          #- (np.sum(m_resolid[charge_pos_end] * (m_Cr_st_sol[charge_pos_end]/m_st_sol[charge_pos_end])))
                          + np.sum(m_Cr2O3_sl_sol[charge_pos_end]) / M_Cr2O3 * M_Cr * 2 + np.sum(m_Cr2O3_sl_liq[charge_pos_end]) / M_Cr2O3 * M_Cr * 2)
        #### P ####
        m_balance[4,1] = (np.sum(m_P_st_liq[charge_pos_end]) + np.sum(m_P_st_sol[charge_pos_end])
                          #- (np.sum(m_resolid[charge_pos_end] * (m_P_st_sol[charge_pos_end]/m_st_sol[charge_pos_end])))
                          + np.sum(m_P2O5_sl_sol[charge_pos_end]) / M_P2O5 * M_P * 2 + np.sum(m_P2O5_sl_liq[charge_pos_end]) / M_P2O5 * M_P * 2)
        #### S ####
        m_balance[12,1] = (np.sum(m_S_st_liq[charge_pos_end]) + np.sum(m_S_st_sol[charge_pos_end])
                           #- (np.sum(m_resolid[charge_pos_end] * (m_S_st_sol[charge_pos_end]/m_st_sol[charge_pos_end])))
                           + np.sum(m_CaS_sl_sol[charge_pos_end]) / M_CaS * M_S + np.sum(m_CaS_sl_liq[charge_pos_end]) / M_CaS * M_S)
        #### Fe ####
        m_balance[5,1] = (np.sum(m_Fe_st_liq[charge_pos_end]) + np.sum(m_Fe_st_sol[charge_pos_end])
                          #- (np.sum(m_resolid[charge_pos_end] * (m_Fe_st_sol[charge_pos_end]/m_st_sol[charge_pos_end]))) + np.sum(m_resolid[charge_pos_end])
                          + np.sum(m_FeO_sl_sol[charge_pos_end]) / M_FeO * M_Fe + np.sum(m_FeO_sl_liq[charge_pos_end]) / M_FeO * M_Fe)
        #### N2 ####
        m_balance[6,1] = (np.sum(m_N2[charge_pos_end]) + np.trapz(N2_extraction, t)
                          + np.dot((C_char_N2[charge_pos_end]) / (1 - C_char_C_fix[charge_pos_end]), m_vol_coal[charge_pos_end]))
        #### O2 ####
        m_balance[7,1] = (np.sum(m_O2[charge_pos_end]) + np.trapz(O2_extraction, t)
                          + np.sum(m_O_st_liq[charge_pos_end]) + np.sum(m_O_st_sol[charge_pos_end])
                          #- (np.sum(m_resolid[charge_pos_end] * (m_O_st_sol[charge_pos_end]/m_st_sol[charge_pos_end])))
                          + np.sum((m_CaO_sl_liq[charge_pos_end] + m_CaO_sl_sol[charge_pos_end]) / M_CaO / 2 * M_O2)
                          + np.sum((m_SiO2_sl_liq[charge_pos_end] + m_SiO2_sl_sol[charge_pos_end]) / M_SiO2 / 2 * 2 * M_O2)
                          + np.sum((m_MnO_sl_liq[charge_pos_end] + m_MnO_sl_sol[charge_pos_end]) / M_MnO / 2 * M_O2)
                          + np.sum((m_Cr2O3_sl_liq[charge_pos_end] + m_Cr2O3_sl_sol[charge_pos_end]) / M_Cr2O3 / 2 * 3 * M_O2)
                          + np.sum((m_P2O5_sl_liq[charge_pos_end] + m_P2O5_sl_sol[charge_pos_end]) / M_P2O5 / 2 * 5 * M_O2)
                          + np.sum((m_FeO_sl_liq[charge_pos_end] + m_FeO_sl_sol[charge_pos_end]) / M_FeO / 2 * M_O2)
                          + abs(-np.trapz(CO_extraction, t) - np.sum(m_CO[charge_pos_end])) / M_CO / 2 * M_O2
                          + abs(-np.trapz(CO2_extraction, t) - np.sum(m_CO2[charge_pos_end])) / M_CO2 * M_O2
                          + abs(-np.trapz(H2O_extraction, t) - np.sum(m_H2O[charge_pos_end])) / M_H2O / 2 * M_O2
                          + np.dot((C_char_O2[charge_pos_end] + C_char_H2O[charge_pos_end] * M_O2 / 2 / M_H2O) / (1 - C_char_C_fix[charge_pos_end]), m_vol_coal[charge_pos_end]))
        #### MgO ####
        m_balance[9,1] = np.sum(m_MgO_sl_liq[charge_pos_end]) + np.sum(m_MgO_sl_sol[charge_pos_end]) # + Sl_former_out
        #### Al2O3 ####
        m_balance[10,1] = np.sum(m_Al2O3_sl_liq[charge_pos_end]) + np.sum(m_Al2O3_sl_sol[charge_pos_end]) # + Sl_former_out
        #### Ca ####
        m_balance[8,1] = ((np.sum(m_CaO_sl_liq[charge_pos_end]) + np.sum(m_CaO_sl_sol[charge_pos_end])) * M_Ca / M_CaO
                          + (np.sum(m_CaS_sl_liq[charge_pos_end]) + np.sum(m_CaS_sl_sol[charge_pos_end])) * M_Ca / M_CaS)
        #### H2 ####
        m_balance[11,1] = (np.sum(m_H2[charge_pos_end]) + np.trapz(H2_extraction, t)
                           + abs(-np.trapz(H2O_extraction, t) - np.sum(m_H2O[charge_pos_end])) / M_H2O * M_H2
                           + abs(-np.trapz(CH4_extraction, t) - np.sum(m_CH4[charge_pos_end])) / M_CH4 / 2 * 4 * M_H2
                           + np.sum(m_comb_scrap[charge_pos_end]) / M_C9H20 * M_H2 * 20 / 2
                           + np.dot((C_char_H2[charge_pos_end] + C_char_H2O[charge_pos_end] * M_H2 / M_H2O) / (1 - C_char_C_fix[charge_pos_end]), m_vol_coal[charge_pos_end]))
        
        #### sum ####
        m_balance[13,1] = sum(m_balance[:,1])
        
        
        for i in range(0,14):
            m_balance[i,2] = m_balance[i,1] - m_balance[i,0]                    # difference = output - input
            m_balance[i,3] = (m_balance[i,1] / m_balance[i,0] - 1) * 100        # ratio
            
        
        Header = [ "Input  ", "Output  ", " Difference", "Ratio " ]
        Side = [ "1)  C", "2)  Si", "3)  Mn", "4)  Cr", "5)  P", "6)  Fe", "7)  N2", "8)  O2", "9)  Ca", "10) MgO", "11) Al2O3", "12) H2", "13) S", "14) Total" ]
        np.set_printoptions(suppress=True)
        print (f"-------------------------------------------------------------------\n                   Mass Balance for Charge {charge} \n-------------------------------------------------------------------")
        
        print(pd.DataFrame(m_balance, index= Side, columns=Header))

        #print("Done")
    
    hdf.close()

#mass_balance(dataFile='results/results_01.hdf5', valid_charge_list=[1])
