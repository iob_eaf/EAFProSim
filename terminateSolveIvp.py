#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 15 09:35:56 2021

@author: zohren
"""

#def terminateSolveIvp (t,y,inp,resh):
#    help_var = max(m_sSc_min-y[inp.ind.m_st_sol],T_tap_min-y[inp.ind.T_st_liq])
#    return (help_var)


def terminateSolveIvp(t, y, inp, resh, classes_to_exclude, pbar=None):
    help_var = inp.OpCh.t_basket - t
    return (help_var)

terminateSolveIvp.terminal = True
#terminateSolveIvp.direction = -1


    