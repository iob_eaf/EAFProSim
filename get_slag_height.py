# -*- coding: utf-8 -*-
"""
Created on Wed Apr 21 14:00:09 2021

@author: Zohren
"""
#from InputData import InputData
import math
from numba import njit

@njit(cache=True)
def get_slag_height(X,p,M,V,c,y_h_slag,x_9_2):


    # constant temperature as calcualted slag zone temperatures are unreliable
    T_slag = 1850
    # replace with liquid slag temperature when better temperatures are calcualted!!
    # use constant temperature since slag temperature is currently unreliable 
    # and slag height is too sensitive towards slag temperature

    #This calculation of the slag heigth does not provide good calculation;
    # heigth sometimes negative or only very small values
    
    Gama_Sio2_a=260
    Gama_Cao_a=625
    Gama_Al2O3_a=655
    Gama_MgO_a=635
    Gama_MnO_a=530
    Gama_Cr2O3_a=-1248*X["Cr2O3"]+8735*X["Cr2O3"]**2
    Gama_FeO_a=645
    
    norm = 1
    X_G=(X["SiO2"]+X["P2O5"])/norm
    X_M=(X["CaO"]+X["MgO"]+X["FeO"]+X["MnO"])/norm
    X_A = (X["Al2O3"]+X["Cr2O3"])/norm
    
    alfa = X_M/(X_M+X_A)
    
    B0 = 13.8+39.9355*alfa-44.049*alfa**2
    B1 = 30.481-117.15*alfa+129.9978*alfa**2
    B2 = -40.9429+234.0486*alfa-300.04*alfa**2
    B3 = 60.7619-153.9276*alfa+211.1616*alfa**2
    
    B = B0+B1*X_G+B2*X_G**2+B3*X_G**3
    
    A_vis = math.exp(-(0.29*B+11.57))
    
    Mu = 0.1*A_vis*T_slag*math.exp(1000*B/T_slag) # viscosity and parameters as per urbain model as published by Kekkonen
    
    p["rho_sl_liq"] = (X["SiO2"]*M["SiO2"]+X["CaO"]*M["CaO"]+X["Al2O3"]*M["Al2O3"]+X["MgO"]*M["MgO"]+X["FeO"]*M["FeO"]+X["MnO"]*M["MnO"])*1000000/ \
            ((X["CaO"]*V["CaO"]+X["MgO"]*V["MgO"]+X["FeO"]*V["FeO"]+X["MnO"]*V["MnO"]+X["Cr2O3"]*V["Cr2O3"]+ \
              X["SiO2"]*(19.55+7.966*X["SiO2"])+X["Al2O3"]*(28.31+32*X["Al2O3"]-31.3*X["Al2O3"]**2))* \
                 (1+(T_slag-1773)*10**-4))
    
    #surface tension
    Gama = ((Gama_Sio2_a*X["SiO2"]+Gama_Cao_a*X["CaO"]+Gama_Al2O3_a*X["Al2O3"]+Gama_MgO_a*X["MgO"]+ \
             Gama_MnO_a*X["MnO"]+Gama_Cr2O3_a+Gama_FeO_a*X["FeO"])+ \
                (0.031*X["SiO2"]-0.094*X["CaO"]-0.177*X["Al2O3"]-0.13*X["MgO"]+0.1*X["FeO"]-0.1*X["MnO"]+0.125*X["Cr2O3"])*(T_slag-1773))*10**-3
    
    p["KS"] = 359*Mu/math.sqrt(p["rho_sl_liq"]*c["g"]*Gama) #Slag index % Jiang and Fruehan 1991
    
    p["Vg"] = ((1/M["CO"])*abs(x_9_2))*c["R_gas"]*T_slag/((y_h_slag+c["p_ambient"])*math.pi*p["r_EAF_up"]**2)    #Superficial gas velocity [m/s] Logar
    
    #return p
