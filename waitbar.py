import datetime
import warnings
import os

def get_datetime():
    now = datetime.now().strftime("%d.%m.%Y %H:%M:%S")
    return now

class logger(object):
    log_file = None

    def __init__(self, log_file):
        self.log_file = log_file

        if os.path.exists(log_file):
            os.remove(log_file)

    def redirect_warning(self, message, category, filename, lineno, *args):
        with open(self.log_file, 'a') as error_stream:
            error_stream.write(warnings.formatwarning(message, category, filename, lineno))

    def redirect_exception(self, exception, message='\n'):
        with open(self.log_file, 'a') as error_stream:
            error_stream.write(message)
            error_stream.write(exception)