"""
necessary io tools for better handling of EAFProsim standard behaviour
"""

import numpy as np
import sys, os, csv
import chardet
from openpyxl import Workbook, load_workbook
from openpyxl.styles import Font, PatternFill
from openpyxl.utils import get_column_letter
import re
import h5py
import time
import uuid

from colorama import Fore, Back, Style, init as cinit
cinit(autoreset=True)

# ================================================================================
# some shortcuts to shorten code
CFS = Fore.RESET
CFY = Fore.LIGHTYELLOW_EX
CFG = Fore.LIGHTGREEN_EX
CFR = Fore.LIGHTRED_EX
CFC = Fore.LIGHTCYAN_EX
CFM = Fore.LIGHTMAGENTA_EX

CBS = Back.RESET
CBY = Back.YELLOW
CBG = Back.GREEN
CBR = Back.RED
CBC = Back.CYAN
CBM = Back.MAGENTA

# =============================================================================    
import traceback
def getTraceback(ex=None):
    """
    collect traceback information after Exception

    Parameters:
    - ex (str): Exception info from exctem Exception as ex

    Returns:
    - str:      formatted Exception information
    """
    
    if ex is None: ex = ""

    e0 = list(sys.exc_info())
    e0.append('\n'.join([str(tb) for tb in traceback.extract_tb(e0[2])]))
    e0.append(traceback.format_exc())
    e1 = '<'+'='*80+'\n' \
        + str(ex) + '\n' \
        +'\n'.join([str(e) for e in e0]) \
        +'\n'+'='*80+'>'
    return e1

# =============================================================================    
# auffälliger Marker beim Printen
def markSection(text,file=sys.__stdout__,fore="",back="",reset=""):
    """
    mark output section on bulk printing (calls print)

    Parameters:
    - text (str):       text to output
    - file (stream):    output file stream (default: stdout)
    - fore (COL):       colorama color for foreground
    - back (COL):       colorama color for background
    - reset (COL):      colorama color resetter

    Returns:
    - nothing
    """

    # Große Start-Visualisierung
    COL = back+fore

    print()
    print(COL+"   "+" "*128+"   ")
    print(COL+" # "+"="*128+" # ")
    print(COL+" # "+" "*128+" # ")
    print(COL+" # "+f"{text:^{128}}"+" # ")
    print(COL+" # "+" "*128+" # ")
    print(COL+" # "+"="*128+" # ")
    print(COL+"   "+" "*128+"   ")
    print(reset)

# =============================================================================    
import inspect

def sysexit(info=""):
    """
    sys.exit() with information of module and code line of calling

    Parameters:
    - info (str):   information to print on sys.exit()

    Returns:
    - nothing
    """

    # make sure to be printable
    info = str(info)

    # Getting the current frame
    current_frame = inspect.currentframe()
    
    # Getting the caller's frame
    caller_frame = current_frame.f_back

    # Extracting information about the file and line number
    file_name   = caller_frame.f_code.co_filename
    line_number = caller_frame.f_lineno

    # print info
    print()
    print("--- "+info)

    # Printing the file name and line number    
    print(f"--- Exiting from {file_name} at line {line_number}")

    # Exiting the program
    sys.exit()

# =============================================================================    
# check if array strictly is ascending
def checkAscending(arr,id='',exit=False,verbose=False):
    """
    check if array is strictly ascending

    Parameters:
    - arr (np.array):   array to check
    - id (str):         id to show
    - exit (bool):      exit on error (default: False)
    - verbose (bool):   -

    Returns:
    - bool:             True or False
    - int:              index of first violation
    """

    diff = np.diff(arr)
    err  = np.any(diff < 0)
    if err:
        errIdx = np.argmax(diff < 0)
        if verbose:
            errIdx  = np.argmax(diff < 0)
            context = 2
            strt    = max(0, errIdx - context)
            end     = min(len(arr), errIdx + context + 1)
            if len(id)>0: id = "'"+id+"'"
            arr2 = ['...'] + [str(i)+": %.4f" % arr[i] for i in range(strt,end)] + ['...']
            print(f"ERROR: array {id} not strictly ascending --> {arr2}")

        if exit:
            sysexit(f"ERROR: array {id} not strictly ascending --> {arr2}")
    else:
        errIdx = None
    
    return err, errIdx

# =============================================================================

def outsep(text,file=sys.__stdout__,length=64,ssep="=",esep="=",start=False,end=False,color=CFC):
    """
    print formatted separator block for bulk printing

    Parameters:
    - text (str):       text to print
    - file (stream):    output file stream (default: stdout)
    - length (int):     length of horizontal bar
    - ssep (char):      charakter for start bar
    - esep (char):      charakter for end bar
    - start (bool):     is start separator block
    - end (bool):       is end separator block
    - color (COL):      colorama color for foreground (default: cyan)

    Returns:
    - nothing
    """

    # make sure to be printable
    text = str(text)

    if not hasattr(outsep, "last_text"):
        outsep.last_text = ""
        outsep.timer     = {}

    if (start or outsep.last_text != text) and not end:
        outsep.timer[text] = time.time()
        print(color+ssep*length,file=file)
        if  start:            
            print(color+text,"START",file=file)
            print(color+'-'*length,file=file)
        else:      
            print(color+text,file=file)
            print(color+esep*length,file=file)
            print(file=file)
    else:
        print(color+'-'*length,file=file)
        t0 = time.time()
        print(color+text,"DONE","%.4f sec" % (t0-outsep.timer.get(text,t0+1.)),file=file)
        outsep.timer[text] = None
        print(color+esep*length,file=file)
        print()

    outsep.last_text = text    
    print(Style.RESET_ALL,end="",file=sys.__stdout__)

# =============================================================================    
# wrapper for HDF5
# this gives the chance of pure reading timeseries from HDF5-DB
# and manipulating the data before first (pre-)processing    
class hdf5Wrapper:
    '''
    wrapper for hdf5 type (adapter)

    Methods:
    - __init__(self, infile=None):              initialize on file or empty
    - saveHDF5(self,outfile,pattern=".*"):      save with path pattern to file
    - getPaths(self,pattern='.*'):              get all paths with given pattern
    - set(self,path,data,name=None,unit="-"):   set entry with full path, value, name and unit
    - remove(self,fullpath):                    remove existing path
    - get(self,path,default=None):              get path with default value
    - modified(self,path):                      check if path was modified after init or reset
    - all_modified(self,pattern=".*"):          get all modified pathes with given pattern
    - saveExcel(self,xlFile,refcol,pattern='1/cyclic/'):
                                                save object to hdf5 file,
                                                refcol is path of time column,
                                                pattern is pattern to export

    Example:
    - my_hdf = hdf5Wrapper('./data/dummy_data.h5')
    '''

    def __init__(self, infile=None):
                
        # outsep("__init__ hdf5Wrapper",start=True)

        self.data   = {}
        self.mod    = {}
        self.units  = {}
        self.shorts = {}
        self.paths  = []

        # TODO: file doesn't exist
        if infile is not None:
            try:
                self.filepath = infile
                hdf5          = h5py.File(infile,'r')        

                hdf5.visit(lambda name: self.paths.append(name) if isinstance(hdf5[name], h5py.Dataset) else None)
                
                for p in self.paths: 
                    self.data[p]   = np.array(hdf5.get(p))
                    self.mod[p]    = False
                    self.units[p]  = '-'
                    self.shorts[p] = p
                
                hdf5.close()
            except:
                print(f"ERROR reading hdf5 file '{infile}'")
                sysexit()

        # outsep("__init__ hdf5Wrapper",end=True)
    
    # save to HDF5 database
    def saveHDF5(self,outfile,pattern=".*"):
        fp  = h5py.File(outfile, "w")
        reg = re.compile(pattern)                        
        groups =  {}        

        for path in self.paths:
            if not reg.search(path): continue
            
            struct = path.split("/")
            gr     = groups
            
            if len(struct)>1: id = struct[0]            
            else:             id = "__general__"
            gr[id] = gr.get(id,{})
            if gr[id] == {}: gr[id] = {'__dg__':fp.create_group(id)}
            gr = gr[id]
            dg = gr['__dg__']

            for id in struct[1:-1]: 
                gr[id] = gr.get(id,{})
                if gr[id] == {}: gr[id] = {'__dg__':dg.create_group(id)}                                
                gr = gr[id]
                dg = gr['__dg__']
                
            id     = struct[-1]

            try:
                gr[id] = dg.create_dataset(id, data=self.data[path])
                # gr[id] = dg.create_dataset(id, data=self.data[path], compression='gzip', compression_opts=5)
            except Exception as ex:
                print(f"saveHDF5: error on setting path '{path}'- '{ex}' ...")
                sysexit("abort due to exception")

        fp.close()        

    # get all available pathes    
    def getPaths(self,pattern='.*'):
        reg = re.compile(pattern)
        res = [path for path in self.paths if reg.search(path)]
        return res

    # e.g. hdf.set(f'{inp.OpCh.charge_num}/timesteps',the_data_array,'time','s')
    def set(self,path,data,name=None,unit="-"):
        self.data[path]   = data
        self.mod[path]    = True
        self.units[path]  = unit
        self.shorts[path] = name if name is not None else path
        if not path in self.paths: self.paths.append(path)

    # remove existing path
    def remove(self,fullpath):
        if fullpath in self.paths:
            self.data[fullpath]   = None
            self.mod[fullpath]    = None
            self.units[fullpath]  = None
            self.shorts[fullpath] = None
            self.paths.remove(fullpath)

    # e.g. hdf.get(f'{inp.OpCh.charge_num}/timesteps')
    def get(self,path,default=None):
        if path in self.data.keys():
            return self.data[path]
        else:
            return default
        
    # for compatibility with h5py
    def close(self):
        pass

    # check if entry was modified or not
    def modified(self,path):
        return self.mod.get(path,'None')
    
    # show all modified entries
    def all_modified(self,pattern=".*"):
        reg = re.compile(pattern)
        mod = []        
        for key in self.paths:
            if reg.search(key):
                # if isinstance(self.data[key],str) or\
                #    isinstance(self.data[key],bytes) or\
                #    isinstance(self.data[key],bytearray):
                #     val = [self.data[key]]
                # else:
                #     val = self.data[key]
                # print("Type of '%s'" % key,type(val))
                if not isinstance(self.data[key],str):
                    mod.append("'%s': %s, shape %s" % (key,self.modified(key),self.data[key].shape))
                else:
                    mod.append("'%s': %s, shape %s" % (key,self.modified(key),self.data[key]))
        return mod

    # save to Excel
    def saveExcel(self,xlFile,refcol,pattern='1/cyclic/'):
        
        wb       = Workbook()
        # wb.title = "one heat" # later overwritten
        ws       = wb.active        
        ws.title = re.sub("[^a-zA-Z0-9]","_",pattern)[0:31]
        print("generate Excel sheet %s from pattern %s ..." % (ws.title,pattern))
        
        reg     = re.compile(pattern)
        paths   = [path for path in self.getPaths() if reg.search(path)]
        names   = [self.shorts.get(path,path) for path in paths]
        ordered = sorted(zip(names,paths))
        paths   = [x for _, x in ordered]
        idx     = paths.index(refcol) 
        paths.pop(idx)
        paths.insert(0,refcol)
        reflen  = len(self.data[refcol])        

        cols  = [self.shorts.get(path,path) for path in paths if self.data[path].shape != () and len(self.data[path])==reflen]
        units = [self.units.get(path,"-") for path in paths if self.data[path].shape != () and len(self.data[path])==reflen]
        data  = np.array([self.data[path] for path in paths if self.data[path].shape != () and len(self.data[path])==reflen])
        
        ws.append(cols)
        ws.append(units)        
        for d in data.T.tolist():
            ws.append(d)

        wb.save(xlFile)

# =============================================================================    
# extended dictionary

class cfgDict(dict):
    """
    extended dictionary with additional functions

    Methods:
    - lock():           lock entries and deny overwriting
    - unlock():         allow overwriting of entries
    - modified():       check if a special key was modified after init or reset
    - modified_all():   give back all modified keys
    - check_modified(): check all values according to a given pattern
    - read_reset():     reset observation of reading of keys
    - check_read():     check keys if read or not after init or reset
    - save_Excel():     save dictionary to excel as key - value columns (and to CSV if requested)
    - compare():        compare to other dict
    """
    
    def __init__(self,name,*args,**kwargs):
        super().__init__(*args, **kwargs)

        self.name = name
        self.mod  = {}        
        self.read = {}
        for key in self.keys():
            self.mod[key]  = False
            self.read[key] = False

        self.overwrite = True # Standard Dictionary Verhalten            

    # allow / deny overwriting of existing keys
    def lock(self):   self.overwrite = False
    def unlock(self): self.overwrite = True

    def __call__(self,key):
        return '%s["%s"] = %s' % (self.name,key,self.get(key,None))
    
    def __getitem__(self,key,default=None): # Zugriff per [key]
        self.read[key] = True
        return super().get(key,default)

    def __setitem__(self, key, value): # Zugriff per [key]
        if self.get(key,None) is not None and not self.overwrite:
            print(Fore.LIGHTRED_EX+"Dict  '%s'  item  '%s'  would be overwritten during lock() ..." % (self.name,key))

            current_frame = inspect.currentframe()
            caller_frame = current_frame.f_back            
            file_name = caller_frame.f_code.co_filename
            line_number = caller_frame.f_lineno            

            sysexit(file_name,line_number) 

        super().__setitem__(key, value)
        self.mod[key] = True

    # pruefen ob modifiziert oder nicht
    def modified(self,path):
        return self.mod.get(path,'None')
    
    # alle modifizierten anzeigen
    def all_modified(self,pattern=".*"):
        reg = re.compile(pattern)
        mod = []
        for key in self.keys():
            if reg.search(key):
                mod.append("'%s': %s" % (key,self.modified(key)))
        return mod
    
    def read_reset(self):
        for key in self.keys():
            self.read[key] = False

    # alle anzeigen, die einem pattern entsprechen
    def check_read(self,pattern=".*"):
        reg = re.compile(pattern)
        mod = []
        for key in self.keys():
            if reg.search(key):
                mod.append([key,self.read.get(key,'None')])
        return mod

    def get(self,key,default=None):
        self.read[key] = True
        return super().get(key,default)

    def saveExcel(self,xlFile,keycol=3,valcol=4,modcol=5,saveCSV=False):
        wb       = Workbook()
        ws       = wb.active
        ws.title = "EAF_properties"        

        font = Font(bold=True)
        fill = PatternFill(fill_type='solid', fgColor='FFFF00')

        for i in range(1,keycol):
            cell = ws.cell(row=1, column=i)
            cell.value = "dummy"
        cell = ws.cell(row=1, column=keycol)
        cell.value = "identifier"; cell.font = font; cell.fill = fill
        cell = ws.cell(row=1, column=valcol)
        cell.value = "value"; cell.font = font; cell.fill = fill        
        cell = ws.cell(row=1, column=modcol)
        cell.value = "modified"; cell.font = font; cell.fill = fill        
        max_key = 0
        # max_val = 0
        if None in self: del self[None]
        for row,key in enumerate(sorted(self.keys()),start=2):
            ws.cell(row=row, column=keycol).value = key            
            if self.modified(key):                 
                ws.cell(row=row, column=valcol).value = self[key]
                ws.cell(row=row, column=modcol).value = 'mod'
            else:
                ws.cell(row=row, column=valcol).value = self[key]
            max_key = max(max_key,len(key))
            
            # max_val = max(max_val,len(self[key]))

        column_letter = get_column_letter(keycol)
        ws.column_dimensions[column_letter].width = max_key
        # column_letter = get_column_letter(valcol)
        # ws.column_dimensions[column_letter].width = max_val

        wb.save(xlFile)

        if saveCSV:
            # write additionally to csv
            csvFile, _ = os.path.splitext(xlFile)
            csvFile   += ".csv"
            # Create a new CSV file and write the Excel data
            with open(csvFile, 'w', newline='', encoding='utf-8') as csv_file:
                writer = csv.writer(csv_file,delimiter=";")
                for row in ws.iter_rows(values_only=True):
                    writer.writerow(row)

    def compare(self,dict2,eps=1.e-8,verbose=False):
        diff = False
        for key in self.keys():
            try:
                if (abs(self[key]-dict2[key]))>eps:
                    print("COMPARE DICT: difference self['%s'] == %f != %f == dict2['%s'] ..." % (key,self[key],dict2[key],key))
                    diff = True
                if verbose:
                    print("COMPARE DICT: difference self['%s'] == %f == %f == dict2['%s'] ..." % (key,self[key],dict2[key],key))
            except:
                try:
                    if self[key] != dict2[key]:
                        print("COMPARE DICT: difference self['%s'] == %s != %s == dict2['%s'] ..." % (key,self[key],dict2[key],key))
                        diff = True
                    if verbose:
                        print("COMPARE DICT: difference self['%s'] == %s == %s == dict2['%s'] ..." % (key,self[key],dict2[key],key))
                except:
                    print("COMPARE DICT: error for key '%s' ...")
                    diff = True
        return diff
    
# =============================================================================    
# read parameter in different ways

# from excel
def xls2cfg(xlsfile,xlssheet,skip=0,namecol=3,valcol=4,unitcol=2,comcol=1,full=False,verbose=False):

    '''
    read config data from xlsx to dict (usually Parameters.xlsx)

    namecol=3 and valcol=4 as default for compatibility with Parameters.xlsx
    
    Parameters:
    - xlsxfile (str):   name of xlsx file with parameters
    - xlsxsheet (str):  sheet with parameters
    - skip (int):       top lines to skip
    - namecol (int):    name column (default 3)
    - valcol (int):     value column (default 4)
    - unitcol (int):    column with units (default 2)
    - comcol (int):     colum with comments (default 1)
    - full (bool):      read units and comments or only key-values
    - verbose (bool):   -

    Returns:
    - cfg (dict):      dictionary with config data (value or [value,unit,comment])

    '''
    
    #outsep("xls2cfg",start=True)

    # Load in the workbook
    if verbose: print("load_workbook")
    wb = load_workbook(xlsfile)

    # Get the sheet
    if verbose: print("select sheet %s" % xlssheet)
    sheet = wb[xlssheet]

    # Create an empty dictionary to store the data
    cfg = {}

    # Loop over the rows in the sheet
    if verbose: print("read lines - set dict")

    for row in sheet.iter_rows(min_row=skip+1, values_only=True):  # Skip header row with min_row=3
        try:
            key     = row[namecol-1]
            value   = row[valcol-1]
            unit    = str(row[unitcol-1]).strip()
            comment = str(row[comcol-1]).strip()

            if key is not None: 
                key = str(key).strip()
            else:
                if full == True:
                    key = str(uuid.uuid4()).split('-')[-1]

            if verbose: print(f"found key {key}")

            # Add the key-value pair to the dictionary
            if full == False: 
                cfg[key] = value
            else:
                cfg[key] = (value,unit,comment)

        except Exception as ex:
            if verbose: print(f"Exception {ex}")

    try:
        try:    
            del cfg[None]        
            for key in cfg.keys():
                if key.startswith('model'):  # 'model variable'
                    #print("DELETE key '%s'" % key)
                    try:    del cfg[key]
                    except: pass
        except: 
            pass
    except Exception as ex:
        print("ERROR:",ex)
        print(cfg)
        sysexit()

    #outsep("xls2cfg",end=True)

    return cfg

# =============================================================================    
# export from excel with ';' separator as default
def csv2cfg(csvfile,sep=';',namecol=3,valcol=4,verbose=False):

    '''
    read parameter fom csv file (probably exported from Parameters.xlsx)

    Parameters:
    - csvfile (str):    filename of csv file
    - sep (str):        separator in csv file
    - namecol (int):    name column (default 3)
    - valcol (int):     value column (default 4)
    - verbose (bool):   -

    Returns-
    - cfg (dict):       dictionary with config data
    '''
    
    #outsep("csv2cfg",start=True)

    # check coding
    with open(csvfile, 'rb') as f:
        charset = chardet.detect(f.read())
        print("file '%s' is encoded '%s' ..." % (csvfile,charset['encoding']))

    # Load in the workbook
    fp = open(csvfile,"r",encoding=charset['encoding'])
    lines = fp.readlines()
    fp.close()

    # Create an empty dictionary to store the data
    cfg = {}

    # Loop over the rows in the sheet
    for line in lines:

        if len(line)>1:# and (re.search("^;.*",line) is None): # geht nur bei Original
            items = line.strip().split(sep)
            try:
                key   = items[namecol-1]        # Column 3 has index 2
                value = float(items[valcol-1])  # Column 4 has index 4
                
                # Add the key-value pair to the dictionary
                cfg[key] = value
            except Exception as ex:
                print("Warning: csv2cfg cannot convert '%s' to float (%s) ..." % (line.strip(),str(ex)))

    #outsep("csv2cfg",end=True)

    return cfg   

# =============================================================================    
# formatted config file
def txt2cfg(filename):
    '''
    read parameters from formatted text file 
    
    generated from Parameters.txt by parameters_xlsx2txt.py

    name = value # unit # comment
    try to keep units and comments

    Parameters:
    - filename (str):   the file to read

    Returns:
    - dict: dictionary with data from file
    '''

    #outsep("txt2cfg",start=True)

    error = False
    with open(filename,"r",encoding='utf-8') as fp:
        lines = fp.readlines()
        cfg = {'__unit__':{},'__comment__':{}}
        for line in lines:
            line  = line.strip()
            if len(line)>0 and not line.startswith('#'):
                items = line.split('#')
                try:
                    key,val = items[0].split(' =')
                    key = key.strip()
                    cfg[key]                = float(val.strip())
                    cfg['__unit__'][key]    = items[1].strip()
                    cfg['__comment__'][key] = items[2].strip()
                except Exception as ex:
                    print("ERROR: cannot parse >>%s<< %s" % (line,ex))
                    error = True

    if error: sys.exit()

    #outsep("txt2cfg",end=True)

    return cfg            

# =============================================================================    
# build and setup an MQTT logging handler

import logging

class MqttLoggingHandler(logging.Handler):
    """
    handler to redicrect logging to MQTT

    Init Parameters:
    - mqtt_client:  paho-mqtt client object
    - topic:        topic to send the logging

    Returns:
    - logger

    Test using Mosquitto:
    - mosquitto_sub.exe -t eafprosim/#
    """

    def __init__(self, mqtt_client, topic="eafprosim/logger"):
        super().__init__()
        self.mqtt_client = mqtt_client
        self.topic = topic

    def emit(self,record):
        # Use the MQTT client to publish the log record
        msg = self.format(record)
        self.mqtt_client.publish(self.topic, msg, qos=1)


def getMqttLogger(mqtt_client,topic='eafprosim/logger'):
    """
    generate MQTT logging handler

    Parameters:
    - mqtt_client ():   paho-mqtt client
    - topic (str):      logging topic

    Returns:
    - logger

    Test using Mosquitto:
    - mosquitto_sub.exe -t eafprosim/#

    """

    try:
        # Set up logging
        logger = logging.getLogger('mqtt_logger')
        logger.setLevel(logging.INFO)
        mqtt_handler = MqttLoggingHandler(mqtt_client,topic)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        mqtt_handler.setFormatter(formatter)
        logger.addHandler(mqtt_handler)

        # Set up redirect_warning to use mqtt_handler
        def redirect_warning(message, category, filename, lineno, file=None, line=None):
            log_msg = f"{filename}:{lineno}: {category.__name__}: {message}"
            mqtt_handler.emit(logging.makeLogRecord({"msg": log_msg, "levelno": logging.WARNING, "levelname": "WARNING"}))
        
        logger.redirect_warning = redirect_warning
        logger.mqtt_client      = mqtt_client

        err = False

    except Exception as ex:
        err = type('ErrorClass', (object,), {'msg': getTraceback(ex)})() # on-the-fly-class
        logger = logging.getLogger('console_logger')
        logger.setLevel(logging.INFO)
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
        logger.addHandler(console_handler)

    return logger, err

# ================================================================================
# redirect sys.stdout und sys.stderr
class MqttRedirector(object):
    """
    MQTT print stream handler

    Init Parameters:
    - mqtt_client ():   paho-mqtt client
    - topic (str):      topic to print

    Returns:
    - object:   object to be assigned to sys.stdout or sys.stderr
    """

    def __init__(self, mqtt_client, topic):
        self.mqtt_client = mqtt_client
        self.topic       = topic

    def write(self, message):
        if message.rstrip() != "":
            # Publish to MQTT
            self.mqtt_client.publish(self.topic,message.rstrip(),qos=1)

    def flush(self):
        pass  # Might be needed depending on your MQTT client's behavior

# ================================================================================
def showMyFunctions(file=sys.stdout):
    """
    show all defined functions, global variables and classes in the calling module

    Parameters:
    - file (filestream):    file to print to (default: stdout)

    Returnd:
    nothing
    """

    current_frame = inspect.currentframe()
    caller_module = inspect.getmodule(current_frame.f_back)
    if caller_module is None: caller_module = inspect.getmodule(current_frame)

    # ========================================
    # header
    print(CFR+f"="*80+CFS,file=file)
    print(CFR+f"Definitions in module {caller_module.__file__}:"+CFS,file=file)
    print(CFR+f"="*80+CFS,file=file)
    print(file=file)

    # ========================================
    # module docstring
    module_docstring = caller_module.__doc__
    print(CFC+f"="*80+CFS,file=file)
    print(CFC+f"Module Docstring:"+CFS,file=file)
    if module_docstring:
        print(module_docstring.strip(), file=file)  # .strip() removes leading/trailing whitespace
    else:
        print("None",file=file)
    print(CFC+f"="*80+CFS,file=file)
    print(file=file)

    # ========================================
    # global variables
    countVar = 0
    print(CFY+"="*80+CFS,file=file)
    print(CFY + "Global Variables:" + CFS, file=file)
    print(file=file)
    global_vars = {name: obj for name, obj in caller_module.__dict__.items()
                   if not (name.startswith('__') and name.endswith('__'))  # Exclude built-ins
                   and not inspect.ismodule(obj) 
                   and not inspect.isfunction(obj) 
                   and not inspect.isclass(obj)
                   }
    for name, value in global_vars.items():
        countVar += 1
        print(f"    {name}: {value}", file=file)
    print(CFY+"="*80+CFS,file=file)
    print(file=file)

    # ========================================
    # classes
    countClass = 0
    print(CFC+"="*80+CFS,file=file)
    print(CFC + "Classes defined in module:" + CFS, file=file)
    print(file=file)
    classes_list = [obj for name, obj in inspect.getmembers(caller_module, inspect.isclass)
                    if inspect.getsourcefile(obj) == caller_module.__file__]
    for cls in classes_list:
        countClass += 1
        print(CFY+f"Class name: {cls.__name__}", file=file)
        print(CFG+f"Docstring:"+CFS,file=file)
        print(f"{cls.__doc__ or 'No docstring provided.'}", file=file)
        print(file=file)
    print(CFC+"="*80+CFS,file=file)
    print(file=file)

    # ========================================
    # module functions

    countFun = 0

    # List all functions defined in the current module
    functions_list = [obj for name, obj in inspect.getmembers(caller_module, inspect.isfunction)
                      if inspect.getsourcefile(obj) == caller_module.__file__]
    
    # Print the names of the functions
    for func in functions_list:
        countFun += 1
        print(CFY+"="*80+CFS,file=file)
        print(CFY+f"Function name: {func.__name__}{inspect.signature(func)}"+CFS,file=file)
        print(CFG+f"Docstring:"+CFS,file=file)
        print(f"{func.__doc__}",file=file)
        print(CFY+"="*80+CFS,file=file)
        print(file=file)

    print(CFG+"Found:"+CFS,file=file)
    print(f"    functions: {countFun}",file=file)
    print(f"    classes:   {countClass}",file=file)
    print(f"    variables: {countVar}",file=file)
    print(file=file)

    print(CFR+f"="*80+CFS,file=file)
    print(CFR+f"DONE definitions in module {caller_module.__file__}"+CFS,file=file)
    print(CFR+f"="*80+CFS,file=file)
    print(file=file)
    Style.RESET_ALL

# =============================================================================    
if __name__ == '__main__':

    # filename = 'EAFPro_Parameter.txt'
    # cfg = txt2cfg(filename)
    # print(cfg)
   
    print("module %s cannot be called ..." % sys.argv[0])

    showMyFunctions()
