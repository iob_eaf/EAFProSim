# ================================================================================
# execute the actual simulation
# this runs as SEPARATE PARALLEL PROCESS !!
# no global data can be used !
def on_thread_connect(client, userdata, flags, reason_code, properties):
    print("do_simulation: client connected to MQTT ...")    

def on_thread_disconnect(client, userdata, flags, reason_code, properties):
    print("do_simulation: client disconnected from MQTT ...")    

# ================================================================================
def do_simulation(client_id,response_topic,data,config,lock):
    """
    concurrent wrapper for calling EAF_start

    Parameters:
    - client_id (str):      id of client, arbitrary but for 
    - response_topic (str): main topic to send stderr, stdout, log and results
    - data (dict):          dictionary with job data 
    - lock (Lock):          lock for concurrent access to info files

    Returns:
    - via MQTT:             stderr, stdout, log and results    
    """

    # ============================================================
    # LAZY imports für kürzere Startup-Zeit

    # ========================================
    # general imports
    import sys
    import json
    import paho.mqtt.client as mqtt
    import time
    import numpy as np
    import pickle
    import base64

    # ========================================
    # EAFProsim imports
    import io_tools as iot
    from EAF_start import EAF_parallel
    from EAF_save_results import EAF_save_results
    from mass_balance import mass_balance


    # ============================================================

    config = type("",(),config)
    
    with lock: iot.outsep("do_simulation",start=True)

    with lock: print(f"do_simulation: MQTT - {config.mqtt_broker_ip}:{config.mqtt_broker_port}",flush=True)

    # connect to broker - send also data back on Exception
    local_client = mqtt.Client(callback_api_version=mqtt.CallbackAPIVersion.VERSION2,
                         client_id=f"EAFProsim_Client_{client_id}") # must be unique !!
    local_client.on_connect    = on_thread_connect
    local_client.on_disconnect = on_thread_disconnect
    local_client.connect(config.mqtt_broker_ip, port=config.mqtt_broker_port)
    print(f"do_simulation: connection established ...",flush=True)

    try:

        # ========================================
        # prepare io

        response_topic = response_topic.strip("/") # make sure right formatting of topic
        
        # logger
        with lock: print(f"do_simulation: redirect logger to MQTT")
        log, err = iot.getMqttLogger(mqtt_client=local_client,
                                     topic=response_topic+"/log") # redirect logger to MQTT
        if err: 
            with lock: print(err.msg)

        # redirect print zu mqtt
        with lock: print(f"do_simulation: redirect stdout and stderr to MQTT")
        sys.stdout = iot.MqttRedirector(mqtt_client=local_client,topic=response_topic+"/stdout")
        sys.sterr  = iot.MqttRedirector(mqtt_client=local_client,topic=response_topic+"/stderr")

        # topic für ergebnisse
        results_topic = response_topic+"/results"

        # ========================================
        # process data here
        # basically adapted EAF_start.py
        # ========================================

        y_results = np.zeros(75)
        charge    = data['charge']

        # ========================================
        # here the magic happens

        # process hdf5 from io.bytesIO() file type
        with lock: print(f"do_simulation: client {client_id} start processing hdf ...",file=sys.__stdout__)
        h5_obj = pickle.loads(base64.b64decode(data['hdf'].encode('utf-8')))
        hdf    = iot.hdf5Wrapper(h5_obj)
        
        # config file dictionary als generisches Dict, neues cfgDict erzeugen
        with lock: print(f"do_simulation: client {client_id} start processing cfg ...",file=sys.__stdout__)
        cfg = iot.cfgDict("parameter",json.loads(data['cfg']))

        # save if demanded
        if data['store'] == True:
            with lock: print("do_simulation: save hdf from hdf5Wrapper() ...")
            hdf.saveHDF5(f"{config.data_dir}/MQTT_inp_{client_id}.h5")      # dir should com via data[]

            with lock: print("do_simulation: save cfg from cfgDict() wrapper ...")
            cfg.saveExcel(f"{config.data_dir}/MQTT_inp_{client_id}.xlsx") # dir should com via data[]

        # ========================================
        # now run EAF_parallel
            
        with lock: print(f"do_simulation: call EAF_parallel ...",file=sys.__stdout__)

        start_time  = time.time()
        data_source = f"EAFProsim_{client_id}"

        ys, _ = EAF_parallel(
                            charge,
                            y         = y_results, 
                            datasrc   = data_source,
                            show_pbar = False, 
                            logger    = log, 
                            cfg       = cfg, 
                            hdf       = hdf)
        with lock: print(f"do_simulation: call EAF_parallel done ...",file=sys.__stdout__)

        if ys is None: 
            with lock: print(f"RESULT not valid",file=sys.__stdout__)
            error  = True
            h5_ret = base64.b64encode(json.dumps({"result":"Heat result not valid"})).decode('utf-8')
        
        else:
            # ========================================
            # TODO: here some more magic happens
            
            ts, ys, results = ys
            # resDict = Dict.empty(key_type=types.int64,value_type=types.float64)            
            # resDict[0] = results
            
            h5_obj = EAF_save_results(None, data_source, [0], ts=ts, ys=ys, results=[results])
            if data['store'] == True:
                with lock: print("do_simulation: save results from io.bytesIO() ...")
                h5_obj.seek(0)
                with open(f"{config.res_dir}/MQTT_res_{client_id}.h5", 'wb') as file: 
                    file.write(h5_obj.getvalue())

            h5_obj.seek(0)
            mass_balance(dataFile=h5_obj, valid_charge_list=[0])

            with lock: print(f"Total program time: {round(time.time() - start_time, 2)}s")

            h5_obj.seek(0)
            h5_ret = base64.b64encode(h5_obj.getvalue()).decode('utf-8')
            error  = False

        # ========================================
        # process data here done
        # ========================================

        # ========================================
        # prepare result data

        return_object         = {"client_id":client_id,
                                 "error":error,
                                 "t_tap_target": data.get('t_tap_target',-1),
                                 "m_tap_target": data.get('m_tap_target',-1),
                                 "user_data":    data.get('user_data',None)}
        return_object['data'] = h5_ret

        # ========================================
        # return result data

        with lock: 
            print(f"do_simulation: send {iot.CFG+client_id+iot.CFS} results back on topic {results_topic} (error: {error})...",file=sys.__stdout__)

        data_json = json.dumps(return_object)
        local_client.publish(results_topic, data_json,qos=1)
        with lock: print(f"do_simulation: send done ...",file=sys.__stdout__)
        with lock:
            with open(config.job_log,"a") as fp:
                print(f"{time.ctime()}: job {client_id} finished and published",file=fp)
        
        time.sleep(1.0)

        # reset io streams
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__        
    
    except Exception as ex:
        print(f"do_simulation: {iot.CFR+client_id+iot.CFS} caused Exception ...")
        data_json = json.dumps({"client_id": client_id,
                                "error":     True,
                                "data":      iot.getTraceback(ex)})
        local_client.publish(results_topic, data_json,qos=1)
        with lock:
            with open(config.job_log,"a") as fp:
                print(f"{time.ctime()}: job {client_id} finished with EXCEPTION, no data returned",file=fp)

    local_client.disconnect()
    with lock: iot.outsep("do_simulation",end=True)
