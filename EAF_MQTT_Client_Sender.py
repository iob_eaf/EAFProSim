"""

MQTT based data preparatioion and call of simulation
SENDER and RECEIVER separated because of corruption of messages

- all input data for simulation have to be provided via MQTT, i.e. nothing is read via files
- all output data from simulation is sent via MQTT
    - "basetopic/.../stdout"
    - "basetopic/.../stderr"
    - "basetopic/.../results"

"""

# ================================================================================

import json
import paho.mqtt.client as mqtt
import uuid
import base64
import pickle
import time
from io import BytesIO
import subprocess

import io_tools as iot

# ================================================================================
# local mqtt broker (e.g. Eclipse Mosquitto)

mqtt_broker_ip    = "localhost"
mqtt_broker_port  = 1883
EAFProsim_topic   = "EAFPROSIM/runconcurrent"
RETURN_base_topic = "EAFPROSIM/response"

# ================================================================================
# generate data and send it to EAFProsim in a separate process, because of 
# stability issues / conflicts with paho-mqtt  
mqtt_is_connected = {}
def on_thread_connect(client, userdata, flags, reason_code, properties):
    mqtt_is_connected[userdata['client_id']] = True
    print(iot.CFY+"# "+"="*64+iot.CFS)
    print(iot.CFY+"parallelPepareAndSend: parallel client connected to MQTT ..."+iot.CFS)
    print(iot.CFY+"# "+"="*64+iot.CFS)

def on_thread_publish(client, userdata, flags, reason_code, properties):
    print(iot.CFY+"# "+"="*64+iot.CFS)
    print(iot.CFY+"parallelPepareAndSend: parallel client successfully published to MQTT ..."+iot.CFS)
    print(iot.CFY+"# "+"="*64+iot.CFS)
    client.disconnect()

def on_thread_disconnect(client, userdata, flags, reason_code, properties):
    print(iot.CFY+"# "+"="*64+iot.CFS)
    print(iot.CFY+"parallelPepareAndSend: parallel client disconnected from MQTT ..."+iot.CFS)
    print(iot.CFY+"# "+"="*64+iot.CFS)

def pepareAndSend(client_id):

    # timeseries and heat data as io.BytesIO() object
    with open('./data/dummy_data.h5',"rb") as fp:
        fbytes = fp.read()
        h5_obj = BytesIO(fbytes)

    # basic config parameters
    cfgFile  = "./data/EAFPro_Parameter.xlsx"
    cfgSheet = "EAF_properties"
    cfg      = iot.cfgDict("parameter",iot.xls2cfg(cfgFile,xlssheet=cfgSheet,skip=3)) # default settings

    # ============================================================
    # modify default cfg and generate random hdf if necessary
    
    charge = 1

    # ============================================================
    # prepare data for sending

    data = {
        "client_id":        client_id,
        "response_topic":   RETURN_base_topic+"_"+str(topic_postfix)+f"/{client_id}",
        "hdf":              base64.b64encode(pickle.dumps(h5_obj)).decode('utf-8'), # serialize
        "cfg":              json.dumps(dict(cfg)),                                  # serialize
        "charge":           charge,
        "t_tap_target":     1666.66, # unknown for example data
        "store":            False
    }
    data_json = json.dumps(data).encode("utf-8")

    # ============================================================
    # send data to EAFProsim

    client = mqtt.Client(callback_api_version=mqtt.CallbackAPIVersion.VERSION2,
                         client_id=f"EAFProsim_Client_{client_id}")
    client.on_connect    = on_thread_connect
    client.on_disconnect = on_thread_disconnect
    client.on_publish    = on_thread_publish
    
    # make sure that connection to mqtt broker is establishe before sending data
    client.user_data_set({'client_id':client_id})
    mqtt_is_connected[client_id] = False
    client.loop_start()
    client.connect(mqtt_broker_ip, port=mqtt_broker_port)
    while not mqtt_is_connected[client_id]: time.sleep(0.1)
    client.publish(EAFProsim_topic, data_json, qos=1)
    client.loop_stop()

    print(f"Message sent to '{EAFProsim_topic}': {data_json[:128]} ... {data_json[-128:]}")

# ================================================================================
if __name__ == '__main__':

    print()

    # ================================================================================
    # start receiver as separate window

    topic_postfix = str(uuid.uuid4()).split('-')[-1]
    cmd = f'start cmd.exe /k python EAF_MQTT_Client_Receiver.py -n 1 -t {topic_postfix}'
    receiver = subprocess.Popen(cmd, shell=True)

    with open("output/jobs.log","w") as fp: 
        print(f"START {time.ctime()}",file=fp)
        print(file=fp)

    # ================================================================================
    # send job to simulator

    start_time = time.time()

    client_id = f"{1:06d}"
    iot.outsep(f"client_id: {client_id}")

    pepareAndSend(client_id)
    with open("jobs.log","a") as fp: 
        print(f"{time.ctime()}: started job '{client_id}'",file=fp)
    
    print("done ...")