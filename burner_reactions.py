# -*- coding: utf-8 -*-
"""
Created on Thu Apr 15 10:04:43 2021

@author: schuettensack
"""
import numpy as np
from tan_hyp import tan_hyp
from numba import njit


@njit(cache=True)
def burner_reactions(p,c,ind,M,K,OpCh,iv,kd,comp,y):
    ## Burners and gas regions for gas reactions

    xV_O2=1-0.1*p["l_scrap_flame"]/p["l_flame"]
    
    m_C_3       = y[ind["m_coal"]]*0.2*tan_hyp(y[ind["m_coal"]],4,5,1)
    m_C_4       = y[ind["m_coal"]]*0.2*tan_hyp(y[ind["m_coal"]],4,5,1)              # C taking part in gas reactions

    n_CO_2   = float(y[ind["m_CO"]]/M["CO"]*K["xV_part_2"])                        # CO in region 2 [mol]
    n_CO2_2  = float(y[ind["m_CO2"]]/M["CO2"]*K["xV_part_2"])                      # CO2 in region 2 [mol]
    n_N2_2   = float(y[ind["m_N2"]]/M["N2"]*K["xV_part_2"])                        # N2 in region 2 [mol]
    n_O2_2   = float(y[ind["m_O2"]]/M["O2"]*K["xV_part_2"])                        # O2 in region 2 [mol]
    n_H2_2   = float(y[ind["m_H2"]]/M["H2"]*K["xV_part_2"])                        # H2 in region 2 [mol]
    n_H2O_2  = float(y[ind["m_H2O"]]/M["H2O"]*K["xV_part_2"])                      # H2O in region 2 [mol]
    n_CH4_2  = float(y[ind["m_CH4"]]/M["CH4"]*K["xV_part_2"])                      # CH4 in region 2 [mol]
    
    n_CO_3   = float(y[ind["m_CO"]]/M["CO"]*K["xV_part_3"])                        # CO in region 3 [mol]
    n_CO2_3  = float(y[ind["m_CO2"]]/M["CO2"]*K["xV_part_3"])                      # CO2 in region 3 [mol]
    n_N2_3   = float(y[ind["m_N2"]]/M["N2"]*K["xV_part_3"])                        # N2 in region 3 [mol]
    n_O2_3   = float(y[ind["m_O2"]]/M["O2"]*K["xV_part_3"])                        # O2 in region 3 [mol]
    n_H2_3   = float(y[ind["m_H2"]]/M["H2"]*K["xV_part_3"])                        # H2 in region 3 [mol]
    n_H2O_3  = float(y[ind["m_H2O"]]/M["H2O"]*K["xV_part_3"])                      # H2O in region 3 [mol]
    n_CH4_3  = float(y[ind["m_CH4"]]/M["CH4"]*K["xV_part_3"])                      # CH4 in region 3 [mol]
    n_C_3    = float(m_C_3/M["C"]*K["xV_part_3"])                                   # C in region 3 [mol]
    
    n_CO_4   = float(y[ind["m_CO"]]/M["CO"]*K["xV_part_4"])                        # CO in region 4 [mol]
    n_CO2_4  = float(y[ind["m_CO2"]]/M["CO2"]*K["xV_part_4"])                      # CO2 in region 4 [mol]
    n_N2_4   = float(y[ind["m_N2"]]/M["N2"]*K["xV_part_4"])                        # N2 in region 4 [mol]
    n_O2_4   = float(y[ind["m_O2"]]/M["O2"]*K["xV_part_4"])                        # O2 in region 4 [mol]
    n_H2_4   = float(y[ind["m_H2"]]/M["H2"]*K["xV_part_4"])                        # H2 in region 4 [mol]
    n_H2O_4  = float(y[ind["m_H2O"]]/M["H2O"]*K["xV_part_4"])                      # H2O in region 4 [mol]
    n_CH4_4  = float(y[ind["m_CH4"]]/M["CH4"]*K["xV_part_4"])                      # CH4 in region 4 [mol]
    n_C_4    = float(m_C_4/M["C"]*K["xV_part_4"])                                   # C in region 4 [mol]
    
    # Region 1 - Gas Burners
    ZM_eff = OpCh["m_CH4_burn"]/(OpCh["m_O2_CH4_burn"]*xV_O2+OpCh["m_CH4_burn"]+1e-8)       # mixture fraction
    ZM_eff_H2 = OpCh["m_H2_burn"]/(OpCh["m_O2_H2_burn"]*xV_O2+OpCh["m_H2_burn"]+1e-8)
    
    reac = np.zeros((6,2))
    
    # CH4 + 2O2 = CO2 + 2H2O
    reac[0,0] = OpCh["m_CH4_burn"]*K["xV_burn_1"]/M["CH4"]
    reac[0,1] = OpCh["m_O2_CH4_burn"]*xV_O2*K["xV_burn_1"]/M["O2"]/2
    #tan_hyp(ZM_eff-c["ZM_stoech[0] ##je nach dem 1 oder 0, ob die Reaktion vollständig(0) oder nicht(1) --> Methan oder Sauerstoff gehen in Gasphase über
    iv["burner"][0] = reac[0,0]+tan_hyp(ZM_eff-c["ZM_stoech"][0],0,10**4,1)*(reac[0,1]-reac[0,0])*(0.7+0.3*tan_hyp(y[ind["V_st_sol"]]/p["V_scrap_init"],0.7,-20,0.6))
    
    # CH4 + 3/2O2 = CO + 2H2O
    reac[1,0] = OpCh["m_CH4_burn"]*K["xV_burn_2"]/M["CH4"]
    reac[1,1] = OpCh["m_O2_CH4_burn"]*xV_O2*K["xV_burn_2"]/M["O2"]*2/3
    iv["burner"][1] = reac[1,0]+tan_hyp(ZM_eff-c["ZM_stoech"][1],0,10**4,1)*(reac[1,1]-reac[1,0])*(0.7+0.3*tan_hyp(y[ind["V_st_sol"]]/p["V_scrap_init"],0.7,-20,0.6))
    
    #CH4 + O2 = CO2 + 2H2
    reac[2,0] = OpCh["m_CH4_burn"]*K["xV_burn_3"]/M["CH4"]
    reac[2,1] = OpCh["m_O2_CH4_burn"]*xV_O2*K["xV_burn_3"]/M["O2"]
    
    ### Muss das so sein ab hier keine Klammer um die ersten Einträge?
    iv["burner"][2] = reac[2,0]+tan_hyp(ZM_eff-c["ZM_stoech"][2],0,10**4,1)*(reac[2,1]-reac[2,0])*(0.7+0.3*tan_hyp(y[ind["V_st_sol"]]/p["V_scrap_init"],0.7,-20,0.6))
    
    #CH4 + CO2 = CO + 3H2
    xV_CO2 = 0.001
    reac[3,0] = OpCh["m_CH4_burn"]*K["xV_burn_4"]/M["CH4"]
    reac[3,1] = y[ind["m_CO2"]]*xV_CO2*K["xV_burn_4"]/M["CO2"]
    iv["burner"][3] = reac[3,0]+tan_hyp(ZM_eff-c["ZM_stoech"][3],0,10**4,1)*(reac[3,1]-reac[3,0])*(0.7+0.3*tan_hyp(y[ind["V_st_sol"]]/p["V_scrap_init"],0.7,-20,0.6))
    
    #CH4 = C + 2H2
    reac[4,0] = OpCh["m_CH4_burn"]*K["xV_burn_5"]/M["CH4"]
    iv["burner"][4] = reac[4,0]+tan_hyp(ZM_eff-c["ZM_stoech"][4],0,10**4,1)*(reac[4,1]-reac[4,0])*(0.7+0.3*tan_hyp(y[ind["V_st_sol"]]/p["V_scrap_init"],0.7,-20,0.6))
    
    # H2 + 1/2O2 = H2O
    # noch einfügen: OpCh["m_H2_burn
    # noch ändern: K["xV_burn_6, c["ZM_stoech[5]
    reac[5,0] = OpCh["m_H2_burn"]*K["xV_burn_6"]/M["H2"]
    reac[5,1] = OpCh["m_O2_H2_burn"]*xV_O2*K["xV_burn_6"]/M["O2"]*2
    iv["burner"][5] = reac[5,0]+tan_hyp(ZM_eff_H2-c["ZM_stoech"][5],0,10**4,1)*(reac[5,1]-reac[5,0])*(0.7+0.3*tan_hyp(y[ind["V_st_sol"]]/p["V_scrap_init"],0.7,-20,0.6))

    #interpolator2 = PchipInterpolator(c["GGWCoeff[9],c["GGWCoeff[1])
    #kc_R2 = interpolator2(y[ind["T_gas])
    #kc_R2 = c["GaMtr_interp_dict[1](y[ind["T_gas"]])
    #interpolator3 = PchipInterpolator(c["GGWCoeff[9],c["GGWCoeff[2])
    #interpolator4 = PchipInterpolator(c["GGWCoeff[9],c["GGWCoeff[3])
    #kc_R3 = interpolator3((y[ind["T_gas]+y[ind["T_st_liq])/2)
    #kc_R4 = interpolator4((y[ind["T_gas]+y[ind["T_st_liq])/2)

    itpi = np.where(c["Gas_T"]<=y[ind["T_gas"]])[0][-1] #tuple that includes a numpy array with all indexes for which condition holds
    xx0 = (y[ind["T_gas"]]-c["Gas_T"][itpi])
    kc_R2 = c["GaMtr_interp0"][0,itpi]*xx0**3 + c["GaMtr_interp1"][0,itpi]*xx0**2 + c["GaMtr_interp2"][0,itpi]*xx0 + c["GaMtr_interp3"][0,itpi]

    meantemp = (y[ind["T_gas"]]+y[ind["T_st_liq"]])/2
    itpi = np.where(c["Gas_T"]<=meantemp)[0][-1] #tuple that includes a numpy array with all indexes for which condition holds
    xx0 = (meantemp-c["Gas_T"][itpi])
    kc_R3 = c["GaMtr_interp0"][1,itpi]*xx0**3 + c["GaMtr_interp1"][1,itpi]*xx0**2 + c["GaMtr_interp2"][1,itpi]*xx0 + c["GaMtr_interp3"][1,itpi]
    kc_R4 = c["GaMtr_interp0"][2,itpi]*xx0**3 + c["GaMtr_interp1"][2,itpi]*xx0**2 + c["GaMtr_interp2"][2,itpi]*xx0 + c["GaMtr_interp3"][2,itpi]

    #        [CO CO2 N2 O2 H2 H2O CH4 C]
    nu_R2  = np.array([-1,1,0,0,1,-1,0,0])                                     # homogeneous water-gas-shift reaction
    nu_R3  = np.array([2,-1,0,0,0,0,0,-1])                                     # Boudouard reaction
    nu_R4  = np.array([1,0,0,0,1,-1,0,-1])                                     # heterogeneous water-gas-shift reaction
    notCondensed = np.array([1,1,1,1,1,1,1,0])
    
    # homogeneous water-gas-shift reaction
    Z_2      = np.array([n_CO_2,n_CO2_2,n_N2_2,n_O2_2,n_H2_2,n_H2O_2,n_CH4_2,0]) # current compostion of gas phase
    Z2       = Z_2/sum(Z_2)                                                    # standardized compostion of gas phase for 1 mol
    num2     = np.prod(Z2**(np.absolute(np.minimum(nu_R2,0))*notCondensed))
    den2     = np.prod(Z2**(np.maximum(nu_R2,0)*notCondensed))
    dK_2     = num2*np.exp(kc_R2)-den2                                         # effective change for 1 mol

    facReact2    = dK_2*kd["gas"][1]                                          # effective change
    iv["dz_gas"][1] = (n_CO_2+tan_hyp(dK_2,0,-10000,1)*(n_CO2_2-n_CO_2))*nu_R2*facReact2       # effective molar change for reaction [mol/s] for every species
    
    # Boudouard reaction
    Z_3      = np.array([n_CO_3,n_CO2_3,n_N2_3,n_O2_3,n_H2_3,n_H2O_3,n_CH4_3,n_C_3]) # current compostion of gas phase
    Z3       = Z_3/sum(Z_3[np.where(notCondensed==1)])                         # standardized compostion of gas phase for 1 mol
    num3     = np.prod(Z3**(np.absolute(np.minimum(nu_R3,0))*notCondensed))
    den3     = np.prod(Z3**(np.maximum(nu_R3,0)*notCondensed))
    dK_3     = num3*np.exp(kc_R3)-den3                                         # effective change for 1 mol
    
    n_hin_3 = min(n_CO2_3,n_C_3)
    gas3 = max(kd["gas"][2]/0.18*(y[ind["m_vol_coal"]]/(y[ind["m_coal"]]+y[ind["m_vol_coal"]]+1))*(1-comp["C_char_C_vol"]),0.0003)
    facReact3    = dK_3*gas3                                                   # effective change
    iv["dz_gas"][2] = (n_hin_3+tan_hyp(dK_3,0,-10000,1)*(n_CO_3/2-n_hin_3))*nu_R3*facReact3*tan_hyp(y[ind["T_gas"]],600,0.1,1)       # effective molar change for reaction [mol/s] for every species
    
    # heterogenous water-gas-shift reaction
    Z_4      = np.array([n_CO_4,n_CO2_4,n_N2_4,n_O2_4,n_H2_4,n_H2O_4,n_CH4_4,n_C_4]) # current compostion of gas phase
    Z4       = Z_4/sum(Z_4[np.where(notCondensed==1)])                         # standardized compostion of gas phase for 1 mol
    num4     = np.prod(Z4**(np.absolute(np.minimum(nu_R4,0))*notCondensed))
    den4     = np.prod(Z4**(np.maximum(nu_R4,0)*notCondensed))
    dK_4     = num4*np.exp(kc_R4)-den4                                         # effective change for 1 mol
    
    n_hin_4 = min(n_H2O_4,n_C_4)
    facReact4    = dK_4*kd["gas"][3]                                          # effective change
    iv["dz_gas"][3] = (n_hin_4+tan_hyp(dK_4,0,-10000,1)*(n_CO_4-n_hin_4))*nu_R4*facReact4       # effective molar change for reaction [mol/s] for every species
