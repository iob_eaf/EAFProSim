# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 09:22:03 2021

@author: hay
"""
import numpy as np
from tan_hyp import tan_hyp
from numba import njit

@njit(cache=True)
def electrode_cooling(p, P_arc, iv, Q_el_cooling_heat, cp_H2O_liq, M_H2O, K_alpha_el_cool, dh_H2O_100, y_T_el):
    
    m_water_el = p["m_water_el"] * tan_hyp(P_arc,5,2,1)
    
    # maximum temperature at electrode tip
    T_el_max = max(3.127 * y_T_el - 389.7, y_T_el)
    
    # average temperature for radiation at electrode tip (tip assumed to be
    # ending where electrode reaches its average temperature) 
    iv["T_el_tip"] = (((T_el_max ** 4 + y_T_el ** 4 ) / 2) ** 0.25)
    
    # temperature at height of cooling
    T_el_cool = 159.4 + 0.5174 * y_T_el
    
    # heat for heating of water from 25°C to 100°C
    Q_el_cooling_heat = m_water_el * cp_H2O_liq / M_H2O * 75
    
    # length needed to provide heat for heating to 100°C
    L_min = (Q_el_cooling_heat / (K_alpha_el_cool * 2 * np.pi
             * p["r_el"] * p["n_el"] * (T_el_cool - 335.65)))
    
    # legth to fully heat and evaporate all cooling water 
    L_max = (L_min + m_water_el * dh_H2O_100 * 1e3 / (K_alpha_el_cool 
            * 2 * np.pi * p["r_el"] * p["n_el"] * ( T_el_cool - 373.15)))
             
    # legth at which Leidenfrost effect occurs
    L_leid = ((p["h_el_cool"] + p["h_EAF_low"] + p["h_EAF_up"])
             * (1 - (-2.765 * 1e9 * y_T_el ** -3.265
            + 7.262) /7.2))
    
    L_cool = max(L_min,(min(L_max,L_leid)))
    
    # amount of steam generated from electrode cooling
    iv["m_steam"] = ((K_alpha_el_cool * np.pi * 2 * p["r_el"] * p["n_el"]
                * L_cool * (T_el_cool - 373.15 ) - Q_el_cooling_heat) 
               / (dh_H2O_100 * 1e3))
    
    m_water_rest = m_water_el - iv["m_steam"]
    
    ratio_water_in = min(1, L_cool/p["h_el_cool"])
    
    iv["m_water_in"] = m_water_rest * ratio_water_in
