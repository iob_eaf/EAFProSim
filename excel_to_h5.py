import pandas as pd
import h5py
import numpy as np


with h5py.File("data/dummy_data.h5", "w") as f:
    #for sheet in xls.sheet_names:
        #df = pd.read_excel(xls, sheet)
        #df = df.tail(-1)
        #df = df.apply(pd.to_numeric)
        #charge = sheet.replace('Charge', '')
        
    b = ["Charge_1","Charge_2","Charge_3"]
    for name in b:
        xls = pd.ExcelFile(f'data/{name}.xlsx')
    
        df = pd.read_excel(xls, "3 Cyclic Input", header=2)
        df1 = pd.read_excel(xls, "2 Baskets", header=2)
        df2 = pd.read_excel(xls, "1 Composition", header=2)
        df2 = df2.dropna(subset=['Material ID'])
        df2 = df2.fillna(0)
        
        charge = name.replace('Charge_', '')
        charge_group = f.create_group(str(charge))
        comp = charge_group.create_group("comp")
        basket = charge_group.create_group("basket")
        cyclic = charge_group.create_group("cyclic")
        
        ### cyclic data
        # time [s]
        t = cyclic.create_dataset("timesteps", data=df["Time [s]"].values)
        # Number of basket charged
        baskets = cyclic.create_dataset("num_baskets", data=df["Basket"].values)
        # Arc Power [MW]
        P_wirk = cyclic.create_dataset("electrical_power", data=df["Arc Power [MW]"].values)
        # arc voltage [V]
        arc_voltage = cyclic.create_dataset("arc_voltage", data=df["Arc Voltage [V]"].values)
        # arc current [kA]
        arc_current = cyclic.create_dataset("arc_current", data=df["Arc Current [kA]"].values)
        # O2 rate [kg/s]
        O2_rate = cyclic.create_dataset("oxygen_flowrate_jetbox_lance", data=df["O2 rate [kg/s]"].values)
        # O2 for postcombustion [kg/s]
        O2_post_comb = cyclic.create_dataset("oxygen_flowrate_postcombustion", data=df["O2 rate for post- combustion [kg/s]"].values)
        # CH4 rate [kg/s]
        CH4_rate = cyclic.create_dataset("naturalgas_flowrate_cumulative", data=df["CH4 [kg/s]"].values)
        # H2 rate [kg/s]
        H2_rate = cyclic.create_dataset("hydrogen_flowrate_cumulative", data=df["H2 [kg/s]"].values)
        # O2 for CH4 combustion rate [kg/s]
        O2_comb_rate = cyclic.create_dataset("oxygen_flowrate_burner", data=df["O2 [kg/s]"].values)
        # Off-gas extractio rate [kg/s]
        Off_gas_rate = cyclic.create_dataset("offgas_flowrate", data=df["mass flowrate [kg/s]"].values)
        # roof cooling water flow [kg/s]
        cooling_roof_rate = cyclic.create_dataset("cooling_roof_flowrate", data=df["cooling water flowrate roof [kg/s]"].values)
        # wall cooling water flow [kg/s]
        cooling_wall_rate = cyclic.create_dataset("cooling_wall_flowrate", data=df["cooling water flowrate wall [kg/s]"].values)
        # roof cooling water inlet temperature [K]
        cooling_roof_temp_in = cyclic.create_dataset("cooling_roof_temperature_inlet", data=df["Temperature water roof ingoing [K]"].values)
        # wall cooling water inlet temperature [K]
        cooling_wall_temp_in = cyclic.create_dataset("cooling_wall_temperature_inlet", data=df["Temperature water wall ingoing [K]"].values)
        # roof cooling water outlet temperature [K]
        cooling_roof_temp_out = cyclic.create_dataset("cooling_roof_temperature_outlet", data=df["Temperature water roof outgoing [K]"].values)
        # wall cooling water outlet temperature [K]
        cooling_wall_temp_out = cyclic.create_dataset("cooling_wall_temperature_outlet", data=df["Temperature water wall outgoing [K]"].values)
        # C injection rate [kg/s]
        C_inj_rate = cyclic.create_dataset("injection_carbon_flowrate", data=df["Carbon 1"].values)
        C_inj_material = cyclic.create_dataset("injection_carbon_material", data="Carbon 1")
        # chalk addition rate [kg/s]
        chalk_inj_rate = cyclic.create_dataset("injection_chalk_flowrate", data=df["Chalk"].values)
        chalk_inj_material = cyclic.create_dataset("injection_chalk_material", data="Chalk")
        # dolomite addition rate [kg/s]
        dolomite_inj_rate = cyclic.create_dataset("injection_dolomite_flowrate", data=df["Dolomite"].values)
        dolomite_inj_material = cyclic.create_dataset("injection_dolomite_material", data="Dolomite")
        # dri addition rate [kg/s]
        dri_inj_rate = cyclic.create_dataset("injection_dri_flowrate", data=df["DRI 2"].values)
        dri_inj_material = cyclic.create_dataset("injection_dri_material", data="DRI 2")
        dri_inj_temp = cyclic.create_dataset("injection_dri_temperature", data=df["DRI 2 Temperature [K]"].values)

        # basket data
        mass_scrap_basket = basket.create_dataset("mass_scrap_basket", data=df1["Mass [kg]"].values)
        number = basket.create_dataset("basket_number", data=df1["Basket number"].values)
        material = basket.create_dataset("basket_material", data=df1["Material ID"].values)
        position = basket.create_dataset("basket_position", data=df1["Position in Basket - bottom up (currently not in use)"].values)

        # composition data
        #mat = df1["Material ID"].values
        material_id = comp.create_dataset("material", data=df2["Material ID"].values)
        header = comp.create_dataset("header", data=df2.columns.tolist()[4:-1])
        for mat in df2["Material ID"].values:
            a = comp.create_dataset(str(mat), data=df2.loc[df2["Material ID"] == mat].values[0][4:-1].astype(np.float64))#data=df2.loc[df2["Material ID"] == mat].values[0].astype(str).tolist())


'''
with h5py.File('data/dummy_data_test.h5', 'r') as hdf:
    a = list(hdf.keys())
    t = np.array(hdf.get('3/timesteps'))
    x = np.array(hdf.get('1/naturalgas_flowrate_cumulative'))
    print(t)
'''