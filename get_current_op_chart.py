# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 15:36:48 2021

@author: hay
"""
import numpy as np
from numba import njit

@njit(cache=True)
def get_current_op_chart(OpCh, iv, comp, c, M, Q, t, dop):

    t_last = OpCh["OpCh_last_elem"]
    itpi = np.where(OpCh["mat_t"][0:t_last]<=t+iv["T_prev"])[0][-1] #tuple that includes a numpy array with all indexes for which condition holds

    xx0 = ((t+iv["T_prev"])-OpCh["mat_t"][itpi])
    OpCh_t = OpCh["OpCh_interp0"][itpi,1:]*xx0**3 + OpCh["OpCh_interp1"][itpi,1:]*xx0**2 + OpCh["OpCh_interp2"][itpi,1:]*xx0 + OpCh["OpCh_interp3"][itpi,1:]
    


    OpCh["P_arc"] = OpCh_t[0]
    OpCh["U_arc"] = OpCh_t[1]
    OpCh["I_arc"] = OpCh_t[2]

    OpCh["m_O2_lance"] = OpCh_t[3] * comp["O2_O2"]
    OpCh["m_O2_post"] = OpCh_t[4] * comp["O2_O2"]
    OpCh["m_C_inj"] = OpCh_t[5]
    OpCh["m_offgas"] = OpCh_t[6] /dop
    #OpCh["m_leakair"] = OpCh_t[7] # unused
    
    OpCh["m_CH4_burn"] = (comp["burn_CH4"] * OpCh_t[8] * c["Hu_CH4"]) / (comp["burn_CH4"] * c["Hu_CH4"] + comp["burn_H2"] * c["Hu_H2"]) #OpCh_t[8]
    OpCh["m_H2_burn"] = (comp["burn_H2"] * OpCh_t[8] * c["Hu_CH4"]) / (comp["burn_CH4"] * c["Hu_CH4"] + comp["burn_H2"] * c["Hu_H2"])
    OpCh["m_O2_CH4_burn"] = OpCh["m_CH4_burn"] * (OpCh_t[9] / (OpCh_t[8]+1e-9)) * comp["O2_O2"]  #OpCh_t[9]
    rho_CH4 = c["p_ambient"] * (M["CH4"] * comp["gas_CH4"] + M["N2"] * comp["gas_N2"]) / (c["R_gas"] * c["T_ambient"])
    rho_H2 = c["p_ambient"] * M["H2"] / (c["R_gas"] * c["T_ambient"])
    OpCh["m_O2_H2_burn"] = OpCh["m_H2_burn"] * 0.25 * (OpCh_t[9] / (OpCh_t[8]+1e-9)) * (rho_CH4 / rho_H2) * comp["O2_O2"]

    OpCh["basket_number_diff"] = OpCh_t[10] - OpCh["basket_number"]
    OpCh["basket_number"] = OpCh_t[10]
    
    OpCh["m_water_roof"] = OpCh_t[11]
    OpCh["m_water_wall"] = OpCh_t[12]
    OpCh["m_water_roof_roc"] = OpCh_t[15]
    OpCh["m_water_wall_roc"] = OpCh_t[16]
    OpCh["T_water_roof_roc"] = OpCh_t[17]
    OpCh["T_water_wall_roc"] = OpCh_t[18]

    OpCh["m_conv_gas"] = OpCh_t[21]
    Q["aluminium"] = OpCh_t[22]

    OpCh["T_dri"] = OpCh_t[23]
    OpCh["m_dust"] = OpCh_t[24]
    OpCh["mass_dri_inj"] = OpCh_t[25:25+35]#[25:25+28]
    OpCh["mass_slagformer_inj"] = OpCh_t[25+35:25+2*35]#
    #OpCh["m_dri"] = OpCh_t[21]
    
    
