import bak.EAF_main as EAF_main
import pickle
import io_tools
import sys
import numpy as np
import os
import platform

# =============================================================================    

from fastapi import FastAPI
from fastapi.responses import HTMLResponse

# =============================================================================

eafapp = FastAPI()

# =============================================================================
# Define a route for the root URL ("/") --> block GET call
@eafapp.get("/")
async def read_root():
    as_html = """
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title>EAFProsim API - Instructions</title>
        <style>
            body {
                font-family: Arial, Helvetica, sans-serif;
            }
        </style>
    </head>
    <body>
        <h1>EAFProsim API</h1>
        <p>Data has to be submitted via POST to <a href="/eafprosim">/eafprosim</a>. Data has to be a dictionary.</p>
        <br>
        <p> Dictionary:</p>
        <ul>
            <li><strong>jobdata:</strong> pickled dictionary</li>
        </ul>
        <br>
        <p> Unpickled dictionary in "jobdata":</p>
        <ul>
            <li><strong>key1:</strong> ... </li>
        </ul>

    </body>
    </html>
    """
    logger, err = io_tools.getMqttLogger()
    if not err: logger.info("access via GET to localhost:8000/eafprosim")
    else:       print(err.msg)

    response = HTMLResponse(content=as_html)
    response.headers["Content-Type"] = "text/html"
    return response

# =============================================================================
# Define a route for data submission ("/eafprosim") using POST
@eafapp.post("/eafprosim")
async def submit_data(data: dict):
    try:
        # unpickle actual data
        data_dict = pickle.loads(data['jobdata'])

        # ==================================
        # process data here
        # ==================================

        logger, err = io_tools.getMqttLogger()
        if err: print(err.msg)

        # Example log message
        for i in range(5):
            logger.info(f"{i:2d} This is a test log message")
    
        resdata = pickle.dumps({"empty":"dict"})

        # ==================================
        # process data here done
        # ==================================

        # return result data
        return {"message": "Data submitted successfully","keys":data_dict.keys(),"resdata":resdata}
    
    except Exception as ex:
        return {"message": "Data submission ERROR","error":ex}

# =============================================================================    
# EAF_main beinhaltet def EAF_parallel(...)
# hier ist alles, was in EAF_start.py im Block "if __name__=='__main__'" steht
# also die gesamte Vorbereitung der Daten
# das muss für jeden Aufruf der Rest-Schnittstelle separat erfolgen
def setupEAF_main():
    
    retobj = type('_',(object,),{})() # leeres Objekt erzeugen

    # redirect logging zu mqtt
    log, err = io_tools.getMqttLogger()
    if not err: log.info("start EAFProSim with redirected logger ...")
    else:       print(err.msg)
    retobj.log = log

    # redirect print zu mqtt
    sys.stdout = io_tools.MqttRedirector(log.mqtt_client,'eafprosim/stdout')
    sys.sterr  = io_tools.MqttRedirector(log.mqtt_client,'eafprosim/stderr')

    # progress bar deaktivieren    
    retobj.show_pbar=False

    # ===============================================================
    # 
    # ab hier __main__ routine
    #
    # ===============================================================

    # Speicher für Ergebnisse
    y   = np.zeros(75)

    # welche Chargen 
    charge_list  = np.arange(1,2) # nur eine charge über REST

    # wohin sollen Ergebnisse gespeichert werden
    results_path = os.path.abspath(args.results_path) # TODO: redirect to memory/objects

    _pf = platform.system()
    
    # es gibt nur einen USER, da daten über REST kommen
    
    # cfgFile = "./EAFPro_Parameter.xlsx"
    if _pf == 'Linux':
        # read parameter: directly reading of xlsx leads to problems on LINUX - therefore csv
        # cfg  = cfgDict("parameter",csv2cfg(cfgFile,sep=args.sep))
        io_tools.sysexit()
    else:
        # directly reading of xlsx for compatible systems
        # cfg  = cfgDict("parameter",xls2cfg(cfgFile,xlssheet=cfgSheet))
        io_tools.sysexit()

    # generate hdf Dictionary
    # hdf = hdf5Wrapper(hdfFile) # hdfFile  = './data/reference_data.h5' 
        
    # am Ende gibt es ein Objekt hdf und ein Dict cfg

    # hdfFile  = './data/reference_data.h5' 
    # if _pf == 'Linux': cfgFile = "./EAFPro_Parameter.csv"
    # else:              cfgFile = "./EAFPro_Parameter.xlsx"
    # cfgSheet = args.sheet
    # results_path = "./results/DH_results.h5"

    # hdfFile = './data/dummy_data.h5'
    # if _pf == 'Linux': cfgFile = "./EAFPro_Parameter.csv"
    # else:              cfgFile = "./EAFPro_Parameter.xlsx"
    # cfgSheet = args.sheet



    return retobj

# =============================================================================    
# =============================================================================
# Run the FastAPI application
if __name__ == "__main__":

    import uvicorn
    uvicorn.run("EAF_start_REST:eafapp", host="0.0.0.0", port=8000,reload=True)

    # original call
    # EAF_main.main()


