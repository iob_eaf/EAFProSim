package main

import (
	"C"
	"math"
)

//export CalculateVFS1
func CalculateVFS1(AA, BB, CC, DD, RR1, RR2, RRC, YY float64) float64 {

	// VF_S[7,1,2,1]=(1/np.pi/AA_7_1_2_1*
	// 	          (BB_7_1_2_1/2*(np.pi-np.arccos(RRC_7_1_2_1/RR2_7_1_2_1))-
	// 			   2*RRC_7_1_2_1*(np.arctan(YY_7_1_2_1)-np.arctan(BB_7_1_2_1**0.5))-
	// 			   0.5*np.arccos(RRC_7_1_2_1/RR1_7_1_2_1)+
	//               ((1+CC_7_1_2_1**2)*(1+DD_7_1_2_1**2))**0.5*np.arctan((((1+CC_7_1_2_1**2)*(YY_7_1_2_1**2-DD_7_1_2_1**2))/((1+DD_7_1_2_1**2)*(CC_7_1_2_1**2-YY_7_1_2_1**2)))**0.5)-
	//               ((1+(RR2_7_1_2_1+RRC_7_1_2_1)**2)*(1+(RR2_7_1_2_1-RRC_7_1_2_1)**2))**0.5*np.arctan((((1+(RR2_7_1_2_1+RRC_7_1_2_1)**2)*(RR2_7_1_2_1-RRC_7_1_2_1))/((1+(RR2_7_1_2_1-RRC_7_1_2_1)**2)*(RR2_7_1_2_1+RRC_7_1_2_1)))**0.5)-
	//               (RR2_7_1_2_1**2-RR1_7_1_2_1**2)*np.arctan(CC_7_1_2_1/DD_7_1_2_1*((YY_7_1_2_1**2-DD_7_1_2_1**2)/(CC_7_1_2_1**2-YY_7_1_2_1**2))**0.5)))

	term1 := BB / 2 * (math.Pi - math.Acos(RRC/RR2))
	term2 := -2 * RRC * (math.Atan(YY) - math.Atan(math.Sqrt(BB)))
	term3 := -0.5 * math.Acos(RRC/RR1)
	term4 := math.Sqrt((1+math.Pow(CC, 2))*(1+math.Pow(DD, 2))) * math.Atan(math.Sqrt(((1+math.Pow(CC, 2))*(math.Pow(YY, 2)-math.Pow(DD, 2)))/((1+math.Pow(DD, 2))*(math.Pow(CC, 2)-math.Pow(YY, 2)))))
	term5 := -math.Sqrt((1+math.Pow(RR2+RRC, 2))*(1+math.Pow(RR2-RRC, 2))) * math.Atan(math.Sqrt(((1+math.Pow(RR2+RRC, 2))*(RR2-RRC))/((1+math.Pow(RR2-RRC, 2))*(RR2+RRC))))
	term6 := -(math.Pow(RR2, 2) - math.Pow(RR1, 2)) * math.Atan(math.Sqrt(CC/DD*((math.Pow(YY, 2)-math.Pow(DD, 2))/(math.Pow(CC, 2)-math.Pow(YY, 2)))))

	VF_S := (1 / math.Pi / AA) * (term1 + term2 + term3 + term4 + term5 + term6)
	return VF_S
}

//export CalculateVFS2
func CalculateVFS2(AA, BB, CC, DD, RR1, RR2, RRC, YY float64) float64 {

	// VF_S[7,1,2,2]=(1/np.pi/AA_7_1_2_2*
	// 	           (AA_7_1_2_2/2*np.arccos(RRC_7_1_2_2/RR2_7_1_2_2)+
	// 			    BB_7_1_2_2/2*np.arccos(RRC_7_1_2_2/RR1_7_1_2_2)+
	//                 2*RRC_7_1_2_2*(np.arctan(YY_7_1_2_2)-np.arctan(AA_7_1_2_2**0.5)-np.arctan(BB_7_1_2_2**0.5))-
	//                 ((1+CC_7_1_2_2**2)*(1+DD_7_1_2_2**2))**0.5*np.arctan((((1+CC_7_1_2_2**2)*(YY_7_1_2_2**2-DD_7_1_2_2**2))/((1+DD_7_1_2_2**2)*(CC_7_1_2_2**2-YY_7_1_2_2**2)))**0.5)+
	//                 ((1+(RR1_7_1_2_2+RRC_7_1_2_2)**2)*(1+(RR1_7_1_2_2-RRC_7_1_2_2)**2))**0.5*np.arctan((((1+(RR1_7_1_2_2+RRC_7_1_2_2)**2)*(RR1_7_1_2_2-RRC_7_1_2_2))/((1+(RR1_7_1_2_2-RRC_7_1_2_2)**2)*(RR1_7_1_2_2+RRC_7_1_2_2)))**0.5)+
	//                 ((1+(RR2_7_1_2_2+RRC_7_1_2_2)**2)*(1+(RR2_7_1_2_2-RRC_7_1_2_2)**2))**0.5*np.arctan((((1+(RR2_7_1_2_2+RRC_7_1_2_2)**2)*(RR2_7_1_2_2-RRC_7_1_2_2))/((1+(RR2_7_1_2_2-RRC_7_1_2_2)**2)*(RR2_7_1_2_2+RRC_7_1_2_2)))**0.5)))

	term1 := AA / 2 * math.Acos(RRC/RR2)
	term2 := BB / 2 * math.Acos(RRC/RR1)
	term3 := 2 * RRC * (math.Atan(YY) - math.Atan(math.Sqrt(AA)) - math.Atan(math.Sqrt(BB)))
	term4 := -math.Sqrt((1+math.Pow(CC, 2))*(1+math.Pow(DD, 2))) * math.Atan(math.Sqrt(((1+math.Pow(CC, 2))*(math.Pow(YY, 2)-math.Pow(DD, 2)))/((1+math.Pow(DD, 2))*(math.Pow(CC, 2)-math.Pow(YY, 2)))))
	term5 := math.Sqrt((1+math.Pow(RR1+RRC, 2))*(1+math.Pow(RR1-RRC, 2))) * math.Atan(math.Sqrt(((1+math.Pow(RR1+RRC, 2))*(RR1-RRC))/((1+math.Pow(RR1-RRC, 2))*(RR1+RRC))))
	term6 := math.Sqrt((1+math.Pow(RR2+RRC, 2))*(1+math.Pow(RR2-RRC, 2))) * math.Atan(math.Sqrt(((1+math.Pow(RR2+RRC, 2))*(RR2-RRC))/((1+math.Pow(RR2-RRC, 2))*(RR2+RRC))))

	VF_S := (1 / math.Pi / AA) * (term1 + term2 + term3 + term4 + term5 + term6)
	return VF_S
}

// Build DLL
// go build -o vfs.dll -buildmode=c-shared -ldflags "-s -w" vfs.go

// leider nicht ohne weiteres einsetzbar mit Numba und auch nicht wesentlich schneller
// # =============================================================
// # in Python
// import ctypes

// # Load the DLL
// vfsGo = ctypes.CDLL('./vfs.dll')

// # Define the argument and result types
// vfsGo.CalculateVFS1.argtypes = [ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double]
// vfsGo.CalculateVFS1.restype  = ctypes.c_double

// # Define the argument and result types
// vfsGo.CalculateVFS2.argtypes = [ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double]
// vfsGo.CalculateVFS2.restype  = ctypes.c_double
// # =============================================================
// z.B. VF_S[7,1,2,1]=vfsGo.CalculateVFS1(AA_7_1_2_1,BB_7_1_2_1,CC_7_1_2_1,DD_7_1_2_1,RR1_7_1_2_1,RR2_7_1_2_1,RRC_7_1_2_1,YY_7_1_2_1)
// z.B. VF_S[7,1,2,3]=vfsGo.CalculateVFS2(AA_7_1_2_3,BB_7_1_2_3,CC_7_1_2_3,DD_7_1_2_3,RR1_7_1_2_3,RR2_7_1_2_3,RRC_7_1_2_3,YY_7_1_2_3)

func main() {}
