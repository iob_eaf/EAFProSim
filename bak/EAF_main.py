import argparse
import os
import time
import traceback
import warnings
from functools import partial
from multiprocessing import Pool, cpu_count
import sys
import numpy as np
from tqdm import tqdm
import platform

from EAF_sim_batch import EAF_sim_batch
from InputData import create_InputData
from parameter import parameter
from init_cond import init_cond
from data_input import data_input
from pre_processing import pre_processing
from EAF_save_results import EAF_save_results
from waitbar import logger
from mass_balance import mass_balance

from numba.typed import Dict
from numba.core import types

from io_tools import hdf5Wrapper, cfgDict, csv2cfg, xls2cfg,markSection

# =============================================================================
# standard wrapper for EAF_sim_batch
def EAF_parallel(charge_num, y, datasrc, show_pbar, logger, cfg=None, hdf=None):
    # redirect warnings
    warnings.showwarning = logger.redirect_warning
    
    # numba adaptations
    resh = Dict.empty(
        key_type=types.unicode_type,
        value_type=types.float64[:],
    )

    # try:
    M, cp, dh, p, c, Q, OpCh, ind, X, W, K, kd, comp, iv, DH_T, QM, V, eval = create_InputData(charge_num, datasrc)
    parameter(M=M, p=p, comp=comp, c=c, K=K, kd=kd, cfg=cfg)
    data_input(ind=ind, OpCh=OpCh, comp=comp, K=K, hdf=hdf)
    pre_processing(y=y, OpCh=OpCh, c=c, M=M, K=K, p=p, cp=cp, ind=ind, iv=iv, dh=dh, cfg=cfg)
    init_cond(y=y, OpCh=OpCh, M=M, p=p, comp=comp, c=c, K=K, ind=ind)

    dy = EAF_sim_batch(y=y, M=M, cp=cp, dh=dh, c=c, p=p, Q=Q, OpCh=OpCh, ind=ind, X=X, W=W, iv=iv,
                    K=K, kd=kd, comp=comp, DH_T=DH_T, QM=QM, V=V, eval=eval, show_pbar=show_pbar,resh=resh,cfg=cfg)

    # except Exception as ex:
    #    logger.redirect_exception(traceback.format_exc(), f"\nError occurred in solving charge {charge_num}\n")
    #    return None, charge_num
    
    return dy, charge_num
# =============================================================================

# =============================================================================
# original main routine
def main():

    markSection("EAF_START")    

    # =========================================================================
    # commandline parameters
    # INFO: the dummy data set contains three charges
    # INFO: the charge numbers are: 1, 2 and 3
    argparser = argparse.ArgumentParser()
    argparser.add_argument("-d", "--data", type=str, default="EAFProSim")
    argparser.add_argument("-fch", "--first_charge", type=int, default=1) # set the charge number of the first charge
    argparser.add_argument("-lch", "--last_charge", type=int, default=1) # set the charge number of the last charge; if only one charge is calculated first and last charge are the same
    argparser.add_argument("-rsp", "--results_path", type=str, default=r"results/results_01.hdf5")
    argparser.add_argument("-err", "--error_path", type=str, default=r"error.log")
    argparser.add_argument("-np", "--num_processes", type=int, default=int(cpu_count()/2))
    argparser.add_argument("-u", "--user", choices=["DH","default","FILE"],help="select type of user code",default='default')
    argparser.add_argument("-hdf", "--hdf", help="set hdf filename for FILE mode",default='./data/dummy_data.h5')
    argparser.add_argument("-cfg", "--cfg", help="set cfg base filename for FILE mode",default='./EAFPro_Parameter')
    argparser.add_argument("-she", "--sheet", help="set xlsx sheet if cfg is Excel",default='EAF_properties')
    argparser.add_argument("-sep", "--sep", help="set csv file separator",default=';')
    argparser.add_argument("-nn", "--nonumba", help="disable numba",action='store_true')

    args, unknown = argparser.parse_known_args()

    # doesn't work if numba imports have been executed before 
    # --> set on commandline: set NUMBA_DISABLE_JIT=1 or export NUMBA_DISABLE_JIT=1
    if args.nonumba:        
        # os.environ['NUMBA_DISABLE_JIT'] = '1' # if problems with stability
        print("NUMBA has to be disabled via commandline:  ...")
        print("    set NUMBA_DISABLE_JIT=1    or    export NUMBA_DISABLE_JIT=1")
        sys.exit()

    # =========================================================================
    # initialize data

    log = logger(args.error_path)
    y   = np.zeros(75)

    charge_list  = np.arange(args.first_charge, args.last_charge + 1)
    results_path = os.path.abspath(args.results_path)
    
    # =========================================================================
    # JUST READ PURE DATA HERE
    # read data from Excel and h5
    # read timeseries

    _pf = platform.system()
    if args.user=='DH':      # generate user data
        hdfFile  = './data/reference_data.h5' 
        if _pf == 'Linux': cfgFile = "./EAFPro_Parameter.csv"
        else:              cfgFile = "./EAFPro_Parameter.xlsx"
        cfgSheet = args.sheet
        results_path = "./results/DH_results.h5"

    elif args.user=="FILE":  # read only user data
        hdfFile  = args.hdf
        cfgFile  = args.cfg        
        cfgSheet = args.sheet
        
    else:                    # dummy_data
        hdfFile = './data/dummy_data.h5'
        if _pf == 'Linux': cfgFile = "./EAFPro_Parameter.csv"
        else:              cfgFile = "./EAFPro_Parameter.xlsx"
        cfgSheet = args.sheet
        
    if _pf == 'Linux':
        # read parameter: directly reading of xlsx leads to problems on LINUX - therefore csv
        cfg  = cfgDict("parameter",csv2cfg(cfgFile,sep=args.sep))
    else:
        # directly reading of xlsx for compatible systems
        cfg  = cfgDict("parameter",xls2cfg(cfgFile,xlssheet=cfgSheet))

    # generate hdf Dictionary
    hdf = hdf5Wrapper(hdfFile)

    # =========================================================================
    # USER CODE TO MODIFY DATA - START
    if args.user=='DH': # my own user code
        sys.path.append(os.path.abspath('../EAF_USER_Code'))        
        from EAF_UserCustomParameters import EAF_setUserCustomParameters        
        EAF_setUserCustomParameters(cfg,hdf,charge_list=charge_list)
    elif args.user == 'FILE':
        pass # take data a read from files
    else: # simple example for USER code
        from USER_customization import setUserCustomParameters
        setUserCustomParameters(cfg,hdf,charge_list=charge_list)
    
    # USER CODE TO MODIFY DATA - END
    # =========================================================================

    start_time = time.time()

    if len(charge_list) == 1:
        dy_charge_list = [EAF_parallel(charge_list[0], y=y, datasrc=args.data,
                                       show_pbar=True, logger=log, cfg=cfg, hdf=hdf)]
    else:
        dy_charge_list = []
        for chrg in charge_list:
            y = np.zeros(80)
            dy_charge_list.append(EAF_parallel(chrg, y=y, datasrc=args.data,
                                       show_pbar=True, logger=log, cfg=cfg, hdf=hdf))
    
    # remove charges and dy which threw error
    valid_dy_charges = [dy_charge for dy_charge in dy_charge_list if dy_charge[0] is not None]
    valid_dy_charges = list(map(list, zip(*valid_dy_charges)))
    valid_dy_list = valid_dy_charges[0]
    valid_charge_list = valid_dy_charges[1]
    ts, ys, results = map(list, zip(*valid_dy_list))

    print(f"Total no. of valid charges: {len(valid_charge_list)}/{len(charge_list)}")
    if len(valid_charge_list) != len(charge_list):
        print(f"Check {args.error_path} for charges with errors")

    # save result to h5 file
    EAF_save_results(results_path, args.data, valid_charge_list, ts=ts, ys=ys, results=results)
    
    mass_balance(dataFile=results_path, valid_charge_list=valid_charge_list)

    print(f"Total program time: {round(time.time() - start_time, 2)}s")

