# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 14:31:15 2021

@author: hay
"""
from tan_hyp import tan_hyp
import numpy as np
from numba import njit
import sys

@njit(cache=True)
def get_current_geometry(p,c,OpCh,K,ind,y):

    #Ergänzung if-Bedingung; EAF_scrap_fill erstmal nur lokal wie in Matlab gesetzt
    '''
    EAF_scrap_fill_level=0.700
    if y[ind["V_st_sol"]]>=p["V_EAF"]*EAF_scrap_fill_level:
        y[ind["V_st_sol"]]=p["V_EAF"]*EAF_scrap_fill_level-1e-10
    '''
    if y[ind["V_st_sol"]] >= p["V_scrap_init"]:
        y[ind["V_st_sol"]] = p["V_scrap_init"]-1e-10
    
    #erstes itpi
    last_elem = c["Geo_last_elem"]
    itpi = np.where(c["Geo_x1"][0:last_elem]<=y[ind["V_st_sol"]])[0][-1] #tuple that includes a numpy array with all indexes for which condition holds
    '''
    if itpi == (len(c["Geo_x1"][0:last_elem])-1):
        itpi = itpi - 1
        print('solid steel volume larger than anticipated')
    '''
    xx0 = y[ind["V_st_sol"]]-c["Geo_x1"][itpi]
    
    Geo_pp = c["m_geo_interp0"][itpi,:4]*xx0**3 + c["m_geo_interp1"][itpi,:4]*xx0**2 + c["m_geo_interp2"][itpi,:4]*xx0 + c["m_geo_interp3"][itpi,:4]
    
    p["h_hole"]  = Geo_pp[0] # height of cone/frustum [m]
    p["r_hole"]  = Geo_pp[1] # inner radius of cone/frustum [m]
    p["h_scrap"] = Geo_pp[3] # total scrap height above[m]
    
    #zweites itpi
    itpi_idx_array = np.where(c["Geo_x6"][0:last_elem]<=y[ind["m_st_liq"]]/p["rho_st_liq"]) #tuple that includes a numpy array with all indexes for which condition holds
    itpi = itpi_idx_array[0][-1]
    '''
    if itpi==(len(c["Geo_x6"][0:last_elem])-1):
        itpi=itpi-1
        print('liquid steel volume larger than anticipated')
    if np.isnan(itpi):
        itpi=0
        print('liquid steel volume smaller than anticipated')
    '''
    xx0 = (y[ind["m_st_liq"]]/p["rho_st_liq"])-c["Geo_x6"][itpi]
    

    p["h_melt"] = c["m_geo_interp0"][itpi,4]*xx0**3 + c["m_geo_interp1"][itpi,4]*xx0**2 + c["m_geo_interp2"][itpi,4]*xx0 + c["m_geo_interp3"][itpi,4]
    
      
    '''
    # export of additional simulation data not implemented yet
    #wird in resh aufgenommen: Beispiel
    R["itpi"] = float(itpi)
    R["power3"] = c["m_geo_interp0"][itpi,4]*xx0**3
    R["power2"] = c["m_geo_interp1"][itpi,4]*xx0**2
    R["power1"] = c["m_geo_interp2"][itpi,4]*xx0
    R["power0"] = c["m_geo_interp3"][itpi,4]
    R["xx0"] = xx0
    '''

    p["h_scrap"] = (tan_hyp((p["h_scrap"] - p["h_melt"]), 0.05, 400, 1) 
                   * (p["h_scrap"] - p["h_melt"]))  
    
    p["h_wall"] = (p["h_EAF_up"] - tan_hyp(p["h_scrap"] + p["h_melt"],
                    p["h_EAF_low"], 200, 1) * (p["h_scrap"] + p["h_melt"]
                    - p["h_EAF_low"]))
                                                
    p["h_wall_unc_exp"] = ((p["h_wall_unc"] - p["h_EAF_up"] + p["h_wall"])
                        * tan_hyp(p["h_wall"] + p["h_wall_unc"]
                        - p["h_EAF_up"], 0.005, 1e4, 1))
    
    arc_resistance = (OpCh["U_arc"] + 0.1)/(OpCh["I_arc"] * 1e3 + 1)
    
    h_slag_arc = (y[ind["h_slag"]] * tan_hyp(p["h_scrap"] - p["h_hole"], 
                  0.15,-50,1))
    
    p["h_arc"] = ((OpCh["U_arc"] + 1 - c["cathode_anode_fall"]
                * tan_hyp(OpCh["U_arc"], 12, 0.02,7) + h_slag_arc
                * (c["field_strength_slag"] - c["field_strength_gas"]))
                / c["field_strength_gas"] / 1e3)
    
    h_arc_max = ((OpCh["U_arc"] + 1 - c["cathode_anode_fall"]
                * tan_hyp(OpCh["U_arc"], 12, 0.02,7)) 
                / c["field_strength_slag"] / 1e3)
    
    p["h_arc"] = max(min(p["h_arc"],h_arc_max),p["h_arc_min"])
    
    p["r_arc"] = (np.sqrt(p["h_arc"] / (np.pi * c["arc_conductivity"]
                * arc_resistance)))
    
    
    p["h_el"] = (max((p["h_hole"] - p["h_scrap"] - p["h_melt"]
                      + p["h_EAF_up"] + p["h_EAF_low"] - p["h_arc"]),0.1))

   
    p["el_tip_ratio"] = min(0.95,max(0.05, p["h_el_tip"] / p["h_el"]))
    
    if p["h_EAF_up"] - p["h_wall"] - p["h_hole"] > p["h_burner"]:
        p["l_scrap_flame"] = p["l_flame"]
    elif p["h_EAF_up"] - p["h_wall"] < p["h_burner"]:
        p["l_scrap_flame"] = 0
    else:
        p["l_scrap_flame"] = min(p["r_EAF_up"] - p["r_hole"], p["l_flame"])
                                     
    K["wall_unc_exposed"] = (tan_hyp(p["h_wall"] + p["h_wall_unc"]
                                      - p["h_EAF_up"], 0.005, 1e4,1))
    
