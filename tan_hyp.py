# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 09:15:18 2021

@author: hay
"""
from numpy import tanh
from numba import jit, float64

@jit(float64(float64,float64,float64,float64), cache=True, nopython=True)
def tan_hyp(val,tan1,tan2,tan3):
    w=((tanh((val-tan1)*tan2)+1)*0.5)**tan3
    return w


@jit(float64[:](float64[:],float64,float64,float64), cache=True, nopython=True)
def tan_hyp_vect(val,tan1,tan2,tan3):
    w=((tanh((val-tan1)*tan2)+1)*0.5)**tan3
    return w