# ================================================================================

import json
import paho.mqtt.client as mqtt
import uuid
import base64
import time
from io import BytesIO
import os
import sys
import argparse
import threading

import io_tools as iot

# ================================================================================
import signal
__SIGINT__ = False

def sigint_handler(signal, frame):
    global __SIGINT__, mqtt_client
    print(f"SIGINT received. Exiting gracefully {os.getpid()}")
    __SIGINT__ = True
    mqtt_client.loop_stop()
    mqtt_client.disconnect()
    os.kill(os.getpid(),signal.SIGTERM)

signal.signal(signal.SIGINT, sigint_handler)

# ================================================================================

mqtt_broker_ip    = "localhost"
mqtt_broker_port  = 1883
RETURN_base_topic = "EAFPROSIM/response"
res_logfile       = "./results/results.txt"

# ================================================================================
def on_connect(client, userdata, flags, reason_code, properties):
    global RETURN_base_topic
    client.subscribe(RETURN_base_topic+"_"+str(topic_postfix)+"/#",qos=1)
    print(iot.CFY+f"client connected widh id '{client._client_id.decode()}' ..."+iot.CFS)
    print()
    print(iot.CFY+"# "+"="*64+iot.CFS)
    print(iot.CFY+"wait for first message ..."+iot.CFS)


# ================================================================================
# handle received data
def handle_message(topic,payload):    
    global jobs_finished, fileLock, max_h5_files

    try:
        if topic.endswith(("log","stdout","stderr")):

            print(iot.CFY+"EAFProsim OUTPUT:"+iot.CFS,str(payload)[:1024],"...",flush=True)

        elif topic.endswith(("results")):
            
            jobs_finished += 1

            response  = json.loads(payload)
            client_id = response.get("client_id")

            print(iot.CFY+"EAFProsim RESULT:"+iot.CFS,f"'{time.ctime()}' message on topic {topic}: {client_id} --> {response.keys()}")    

            # pruefen ob fehler vorliegt
            if response.get("error",True):
                print()
                print(iot.CFY+"ERROR:"+iot.CFS)
                print(response.get("data","no proper data ..."))

            # h5 content behandeln
            try:
                h5_buffer = response.get("data","no key 'data'")
                h5_obj = BytesIO(base64.b64decode(h5_buffer.encode('utf-8')))
                h5_obj.seek(0)
                hdf = iot.hdf5Wrapper(h5_obj)
                res = hdf.get("0/y/T_st_liq")

            except Exception as ex:
                print(iot.CFY+f"ERROR: h5 handling failed ..."+iot.CFS)

            with fileLock:
                try:
                    hdf.saveHDF5(f"results/MQTT_recv_res_hdf_{client_id}.h5",pattern="0/")
                except Exception as ex:
                    print(iot.CFR+f"failed to save hdf5: {iot.getTraceback(ex)}"+iot.CFS)

            # ergebnis speichern
            with fileLock:
                with open(res_logfile,"a",encoding='utf-8') as fp:
                                    
                    print(f"client_id:    {response.get('client_id',-1)}",file=fp)
                    print(f"time:         {time.ctime()}")
                    print(f"error:        {response.get('error',True)}",file=fp)                
                    print(f"data:         '{h5_buffer[:64]}...'",file=fp)                
                    print(f"t_tap_target: {response.get('t_tap_target',-2)} °C",file=fp)
                        
                    if res is not None:
                        print(f"t_tap_final:  {res[-1]-273:.1f} °C",file=fp)
                    else:
                        print(f"t_tap_final:  None ...",file=fp)
                    
                    print(file=fp,flush=True)
        
        else:
            print(iot.CFY+f"EAFProsim RESULT: unknown topic {topic}"+iot.CFS)

    except Exception as ex:
        print(iot.CFY+f"ERROR on_message:"+iot.CFS)
        print(iot.getTraceback(ex),flush=True)

# ================================================================================
# Callback-Funktion, die bei eingehender Nachricht auf dem Antwort-Topic aufgerufen wird
def on_message(client, userdata, message):

    global global_timer, jobs_finished, jobs_total
    
    try:
        print(iot.CFY+f"client got message ..."+iot.CFS)

        if time.time()-global_timer>60: # Unterbrechung eines Job-Batches
            print(iot.CFY+f"client: reset '{res_logfile}'"+iot.CFS)
            with open(res_logfile,"w",encoding='utf-8') as fp: 
                print(f"START '{time.ctime()}'",file=fp) # Datei zurücksetzen
        
        global_timer = time.time()

        # sequentiell, daher kein file Lock()
        handle_message(message.topic,message.payload)
        print(iot.CFY+f"handle message done ..."+iot.CFS)

        if jobs_finished == jobs_total:
            print(iot.CFG+f"{jobs_finished}/{jobs_total} jobs finished, exit ...")
            client.disconnect()
            sys.exit()

    except Exception as ex:
        print(f"ERROR: {iot.getTraceback(ex)}")

    print()
    print(iot.CFY+"# "+"="*64+iot.CFS)
    print(iot.CFY+f"wait for incoming message (finished {jobs_finished}/{jobs_total})..."+iot.CFS)


# ================================================================================
if __name__ == '__main__':
    
    argparser = argparse.ArgumentParser()
    argparser.add_argument("-n", "--numjobs", type=int, default=1)
    argparser.add_argument("-t", "--topicpostfix", default="")
    args, unknown = argparser.parse_known_args()    

    topic_postfix = args.topicpostfix
    fileLock      = threading.Lock()
    jobs_total    = args.numjobs
    jobs_finished = 0

    client_id = f"EAFProsim_OUTPUT_RECEIVER_{str(uuid.uuid4())}"
    mqtt_client = mqtt.Client(callback_api_version=mqtt.CallbackAPIVersion.VERSION2,
                              client_id=client_id)
    mqtt_client.on_message = on_message
    mqtt_client.on_connect = on_connect
    mqtt_client.connect(mqtt_broker_ip, port=mqtt_broker_port)

    global_timer = time.time()-60

    mqtt_client.loop_forever()
