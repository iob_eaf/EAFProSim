import numpy as np
import re, shutil
import argparse
import os
import io_tools as iot

'''
1. manually (!) export EAFPro_Parameter.xlsx to csv with defined separator (e.g. sep = ';' in Germany)
2. execute format_parameters_csv.py (filenames are hardcoded)
3. result is stored to EAFPro_Parameter.txt as readable formatted UTF-8 text file
4. this txt-file can be imported using io_tools.txt2cfg(...)
'''

from io_tools import xls2cfg, getTraceback

# =======================================================================

if __name__ == '__main__':
    
    print()

    argparser = argparse.ArgumentParser()
    argparser.add_argument("-xls", "--xls", required=True, help="set cfg base filename for FILE mode")
    argparser.add_argument("-dir", "--dir", help="set outfile path csv and txt",default='./')
    argparser.add_argument("-she", "--sheet", help="set xlsx sheet if cfg is Excel",default='EAF_properties')
    args, unknown = argparser.parse_known_args()

    # filenames
    xlsx    = args.xls
    txt     = xlsx.replace(".xlsx",".txt")
    txt_bak = txt+".bak"

    # backup existing file
    try:                     shutil.copy(txt,txt_bak)
    except Exception as ex:  print(getTraceback(ex))

    cfg = xls2cfg(xlsx,args.sheet,skip=3,namecol=3,valcol=4,unitcol=2,comcol=1,full=True,verbose=False)

    csv_items = [[key,val[0],val[1],val[2]] for key,val in cfg.items()]
    # for i,item in enumerate(csv_items): print(f"{i:3d}: {item}")

    # process for beautifying
    brack   = re.compile(r'\[\s*\]')
    brack2  = re.compile(r'\[\s*\-\s*\]')
    lgth = np.zeros(4,dtype=int)
    para = []
    for i, c in enumerate(csv_items):
        try:    val = float(c[1])
        except: val = None
        
        if str(c[1]).strip() == '' or val is None:
            line = ['#',c[3]]
        else:
            line    = c
            line[2] = brack2.sub('[ - ]',brack.sub('[  ]',line[2]))
            line    = [str(it).strip() for it in line]
            for i,it in enumerate(line): lgth[i] = max(lgth[i],len(it))
        
        if c[3] != '' and c[3] != 'None':  para.append(line)

    # write data to formatted txt
    seps  = [' = ',' # ',' # ','']
    empty = False
    with open(txt,"w",encoding='utf-8') as fp:
        print("# settings for EAF properties and simulation parameters",file=fp)
        print(f"# automatic conversion from '{xlsx}'",file=fp)
        print(file=fp)

        for i,p in enumerate(para):
            if p[0] == '': # empty line
                if not empty: fp.write("\n")
                empty = True
            elif p[0] == '#': # commented line
                empty = False
                if i<2:  fp.write("# %s\n" % p[1])
                else:    fp.write("\n# %s\n" % p[1])
            else: # data line
                empty = False
                formatted_string  = ''.join([str(value).ljust(length)+sep for value, length, sep in zip(p, lgth, seps)])
                fp.write(formatted_string+'\n')

    print(f"file '{txt}' generated from file '{xlsx}' ...")