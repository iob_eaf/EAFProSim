import pandas as pd
import numpy as np
import sys

def df2dict(data):
    cfg = {}
    for row in range(len(data)):
        try:
            cfg[data.loc[row,2]] = data.loc[row,3]
        except Exception as ex:
            print("conversion errow with '%s' ..." % str(data.loc[row,:]))
            sys.exit()
    return cfg

def parameter(M, p, comp, c, K, kd, paraFile='MQTT_00000005_inp.xlsx', cfg=None):#EAFPro_Parameter#DH_RandomPara
    
    if cfg is None:
        data = pd.read_excel(paraFile, sheet_name='EAF_properties', header=None)
        cfg = df2dict(data)

    p["h_EAF_up"] = cfg['p.h_EAF_up']
    p["h_wall_unc"] = cfg['p.h_wall_unc']
    p["r_EAF_low"] = cfg['p.r_EAF_low']
    p["r_EAF_up"] = cfg['p.r_EAF_up']
    p["h_arc_min"] = cfg['p.h_arc_min']
    p["h_el_tip"] = cfg['p.h_el_tip']
    p["r_hole_init"] = cfg['p.r_hole_init']
    p["r_el"] = cfg['p.r_el']
    p["n_el"] = cfg['p.n_el']
    p["r_heart"] = cfg['p.r_heart']
    p["m_water_el"] = cfg['p.m_water_el']
    p["h_el_cool"] = cfg['p.h_el_cool']
    p["lambda_roof"] = cfg['p.lambda_roof']
    p["lambda_wall"] = cfg['p.lambda_wall']
    p["pipe_thick"] = cfg['p.pipe_thick']
    p["slag_thick"] = cfg['p.slag_thick']
    p["h_burner"] = cfg['p.h_burner']
    p["l_flame"] = cfg['p.l_flame']
    p["rho_roof"] = cfg['p.rho_roof']
    p["rho_wall"] = cfg['p.rho_wall']
    p["d_roof"] = cfg['p.d_roof']
    p["d_wall"] = cfg['p.d_wall']
    p["d_refrac"] = cfg['p.d_refrac']
    p["T_vessel_out"] = cfg['p.T_vessel_out']
    p["rho_st_liq"] = cfg['p.rho_st_liq']
    p["rho_st_sol"] = cfg['p.rho_st_sol']
    p["rho_scrap_init"] = cfg['p.rho_st_sol']
    p["rho_el"] = cfg['p.rho_el']
    p["StartingTimeDelay"] = cfg['p.StartingTimeDelay']
    p["roof_opening_time"] = cfg['p.roof_opening_time']
    p["m_hot_heel"] = cfg['p.m_hot_heel']
    p["EAF_scrap_fill_level"] = cfg['p.EAF_scrap_fill_level']
    p["geo_fact"] = [cfg['p.geo_fact(1)'], cfg['p.geo_fact(2)']]
    p["m_tap"] = cfg['p.m_tap'] # ?

    p["h_EAF_low"] = p["r_EAF_up"] - p["r_EAF_low"]
    p["r_el_rep"] = np.sqrt(p["n_el"]) * p["r_el"]
    p["r_hole_init"] = max(p["r_hole_init"], p["r_el_rep"] + 1e-3)
    p["V_EAF"] = (np.pi * (p["r_EAF_up"] - p["r_EAF_low"]) / 3 * (p["r_EAF_up"] ** 2
            + p["r_EAF_up"] * p["r_EAF_low"] + p["r_EAF_low"] ** 2) + np.pi * p["r_EAF_up"] ** 2
            * p["h_EAF_up"])
    p["m_roof"] = np.pi * (p["r_EAF_up"] ** 2 - p["r_heart"] ** 2) * p["d_roof"] * p["rho_roof"]
    p["m_wall"] = (np.pi * 2 * p["r_EAF_up"] * (p["h_EAF_up"] - p["h_wall_unc"]) * p["d_wall"] * p["rho_wall"])
    p["m_wall_unc"] = ((np.pi * 2 * p["r_EAF_up"] * p["h_wall_unc"] + np.pi 
                 * (p["r_heart"] ** 2 - p["r_el_rep"] ** 2)) * p["d_wall"] * p["rho_wall"])
    p["A_vessel_side"] = ((2 **0.5) * (p["r_EAF_up"] - p["r_EAF_low"]) * np.pi 
                     *(p["r_EAF_low"] + p["r_EAF_up"] + (2 ** 0.5) * p["d_refrac"]) * 1.01)
    p["A_vessel_bottom"] = np.pi * p["r_EAF_low"] ** 2
    p["A_vessel_out"] = (np.pi * (p["r_EAF_up"] ** 2 + (p["h_EAF_low"] + p["d_refrac"]) ** 2) * 1.5)
    p["V_scrap_init"] = p["V_EAF"] * p["EAF_scrap_fill_level"]
    
    comp["C_char_lhv"] = cfg['comp.coal_lhv_temp(1)']#nur an 2 Stellen verwendet
    comp["C_inj_lhv"] = cfg['comp.injcoal_lhv']# nur an einer stelle verwendet

    comp["air_O2"] = cfg['comp.air_O2']
    comp["air_N2"] = cfg['comp.air_N2']
    comp["refrac_MgO"] = cfg['comp.refrac_MgO']
    comp["refrac_CaO"] = cfg['comp.refrac_CaO']
    comp["gas_CH4"] = cfg['comp.gas_CH4']
    comp["gas_N2"] = cfg['comp.gas_N2']
    comp["O2_O2"] = cfg['comp.O2_O2']
    comp["O2_N2"] = cfg['comp.O2_N2']
    comp["gas_H2"] = cfg['comp.gas_H2']

    c["T_melt_sl"] = cfg['c.T_melt_sl']
    c["T_melt_st"] = cfg['c.T_melt_st']
    c["T_undercool"] = cfg['c.T_undercool']
    c["T_ambient"] = cfg['c.T_ambient']
    c["slag_model"] = cfg['c.slag_model']
    c["melt_model"] = cfg['c.melt_model']
    c["Ep"] = np.array([cfg['c.Ep(1)'], cfg['c.Ep(2)'], cfg['c.Ep(3)'], cfg['c.Ep(4)'], cfg['c.Ep(5)'], cfg['c.Ep(6)'], cfg['c.Ep_uncooled_wall'], cfg['c.Ep(6)']])

    rho_CH4 = c["p_ambient"] * (M["CH4"] * comp["gas_CH4"] + M["N2"] * comp["gas_N2"]) / (c["R_gas"] * c["T_ambient"])
    rho_H2 = c["p_ambient"] * M["H2"] / (c["R_gas"] * c["T_ambient"])
    comp["burn_CH4"] = cfg['comp.burn_CH4']
    comp["burn_H2"] = cfg['comp.burn_H2']
    burn_CH4 = comp["burn_CH4"]
    burn_H2 = comp["burn_H2"]
    comp["burn_CH4"] = (burn_CH4*rho_CH4)/((burn_CH4*rho_CH4)+(burn_H2*rho_H2))
    comp["burn_H2"] = (burn_H2*rho_H2)/((burn_CH4*rho_CH4)+(burn_H2*rho_H2))

    K["therm"] = [cfg['K.therm(1)'], cfg['K.therm(2)'], cfg['K.therm(3)'], cfg['K.therm(4)'], cfg['K.therm(5)'], cfg['K.therm(6)'], cfg['K.therm(7)'], cfg['K.therm(8)'], cfg['K.therm(9)'], cfg['K.therm(10)'], cfg['K.therm(11)']]
    K["area"] = [cfg['K.area(1)'], cfg['K.area(2)'], cfg['K.area(3)'], cfg['K.area(4)'], cfg['K.area(5)']]
    K["wall"] = [cfg['K.wall(1)'], cfg['K.wall(2)'], cfg['K.wall(3)']]
    K["burn"] = cfg['K.burn']
    K["arc_power_distribution"] = [cfg['K.arc_power_distribution(1)'], cfg['K.arc_power_distribution(2)'], cfg['K.arc_power_distribution(3)'], 0.025]
    K["slag_influence"] = cfg['K.slag_influence']
    K["slag_height_fact"] = cfg['K.slag_height_fact']
    K["slag_T_reduc"] = cfg['c.slag_reduce']
    K["CO_slag_CO2"] = cfg['K.CO_slag_CO2']
    K["C_inj_diss"] = cfg['K.C_inj_diss']
    K["C_inj_char"] = cfg['K.C_inj_char']
    K["CO_C_CO2"] = cfg['K.C_CO_CO2']
    K["O2_lance_diss"] = cfg['K.O2_inj_diss']
    K["O2_post_CO"]= cfg['K.O2_post_CO']
    K["reac_fact"] = cfg['K.react_fact']
    K["reactor_melt_mass"] = cfg['K.reactor_melt_mass']
    K["leak_air_coal_burn"] = cfg['K.leak_air_coal_burn']
    K["xV_part_2"] = cfg['K.xV_part_2']
    K["xV_burn_1"] = cfg['K.xV_burn_1']
    K["xV_burn_2"] = cfg['K.xV_burn_2']
    K["xV_burn_3"] = cfg['K.xV_burn_3']
    K["xV_burn_4"] = cfg['K.xV_burn_4']
    K["xV_burn_5"] = cfg['K.xV_burn_5']
    K["pr"] = cfg['K.pr']
    K["convective_air"] = cfg['K.convective_air']
    K["refrac"] = cfg['K.refrac']
    K["alpha_el_cool"] = cfg['K.alpha_el_cool']
    K["FeO_O2"] = cfg['K.FeO_O2']
    K["FeO_CO2"] = cfg['K.FeO_CO2']
    K["FeO_O2_post"] = cfg['K.FeO_O2_post']
    K["conv_gas"] = cfg['K.conv_gas']
    K["C_inj_loss"] = cfg['K.inj_coal_loss']
    K["coal_burn_charging"] = cfg['K.coal_burn_charging']
    K["m_resolid"] = cfg['K.m_resolid']
    K["T_resolid"] = cfg['K.T_resolid']
    K["resolid_influence"] = cfg['K.resolid_influence']
    K["alpha_vessel_air"] = cfg['p.alpha_air']

    kd["O_1"] = cfg['kd.O_1']
    kd["C_char_burn"] = cfg['kd.C_char_burn']
    kd["C_char_reac"] = cfg['kd.C_char_reac']
    kd["C_char_diss"] = cfg['kd.C_char_diss'] * 6e-5
    kd["CO_post"] = cfg['kd.CO_post']
    kd["C_1"] = cfg['kd.C_1']
    kd["C_2"] = cfg['kd.C_2']
    kd["Si_1"] = cfg['kd.Si_1']
    kd["Si_2"] = cfg['kd.Si_2']
    kd["Si_3"] = cfg['kd.Si_3']
    kd["Mn_1"] = cfg['kd.Mn_1']
    kd["Mn_2"] = cfg['kd.Mn_2']
    kd["Mn_3"] = cfg['kd.Mn_3']
    kd["Cr_1"] = cfg['kd.Cr_1']
    kd["Cr_2"] = cfg['kd.Cr_2']
    kd["Cr_3"] = cfg['kd.Cr_3']
    kd["P_1"] = cfg['kd.P_1']
    kd["P_2"] = cfg['kd.P_2']
    kd["P_3"] = cfg['kd.P_3']
    kd["Fe_1"] = cfg['kd.Fe_1']
    kd["Fe_2"] = cfg['kd.Fe_2']
    kd["Fe_3"] = cfg['kd.Fe_3']
    kd["S"] = cfg['kd.S']
    kd["H2_post"] = cfg['kd.H2_post']
    kd["CH4_post"] = cfg['kd.CH4_post']
    kd["comb"] = cfg['kd.comb']
    kd["vol"] = cfg['kd.vol']
    kd["el_tip"] = cfg['kd.el_tip']
    kd["el_side"] = cfg['kd.el_side']
    kd["gas"] = np.array([0, cfg['kd.gas(2)'], cfg['kd.gas(3)'], cfg['kd.gas(4)']])
    kd["H2O_diss"] = cfg['kd.H2O_diss']