# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 12:11:19 2021

@author: hay
"""
from tan_hyp import tan_hyp
from equilibrium_mass_fractions import equilibrium_mass_fractions
from get_current_geometry import get_current_geometry
from get_current_op_chart import get_current_op_chart
from electrode_cooling import electrode_cooling
from burner_reactions import burner_reactions
from get_slag_height import get_slag_height
from InputData import update_mass_and_molar_fractions
from view_factor import view_factor
from radiosity import radiosity
from reaction_enthalpies import reaction_enthalpies
import numpy as np
from numba import njit

from write_results import write_results

# =============================================================
import time, sys
from colorama import Fore, init as colorinit
colorinit(autoreset=True)
'''
class globalParameter:
    solverTimerCum   = 0
    solverTimer      = 0
    solverCounter    = 0

__glob__ = globalParameter()
'''
#from line_profiler import LineProfiler

# =============================================================
#@profile
@njit(cache=True)
def EAF_diff_eq(t, y, ind, K, c, p, OpCh, iv, cp, M, comp, kd, Q, X, W, V, dh, QM, DH_T, eval, resh, pbar=False):

    #global __glob__
    # to be changed
    #if y[ind["m_O2"]] <= 0:
    #    y[ind["m_O2"]] = 0.00001

    #if (y[ind["m_O_reac"]] <= 0):
    #    y[ind["m_O_reac"]] = 0.00001

    K_O2_lance_gasphase = 0.0
    #pbar.update(int(t - pbar.n))

    #if any(y[0:79]<0):
        #a= 0
        #print("Fehler")
    '''
    __glob__.solverCounter += 1
    timer0 = time.time()
    
    if __glob__.solverTimer < 1:__glob__.solverTimer = time.time()
    if __glob__.solverCounter % 100 == 0:
        
        print(Fore.LIGHTGREEN_EX+f"{__glob__.solverCounter: 7d} EAF_diff_eq: {t:.4f} sec / "
                                f"{(time.time()-__glob__.solverTimer):.4f} sec / {__glob__.solverTimerCum:.2f} sec ... "
                                f"Temp: liq={(y[ind['T_st_liq']]-273.15):.2f} °C, sol={(y[ind['T_st_sol']]-273.15):.2f} °C")
    '''

    T_melt_st = ((1536 - 55 * 100
                    * y[ind["m_C_st_liq"]]
                    / y[ind["m_st_liq"]])
                    * tan_hyp(y[ind["m_C_st_liq"]]
                    / y[ind["m_st_liq"]]*100,0.8,-50,1)
                    + (1569.6 - 97 * 100
                    * y[ind["m_C_st_liq"]]
                    / y[ind["m_st_liq"]])
                    * tan_hyp(y[ind["m_C_st_liq"]]
                    / y[ind["m_st_liq"]]*100,0.8,50,1) + 273.15)

    p["rho_st_sol"] = (y[ind["m_st_sol"]] / (y[ind["V_st_sol"]]+1e-9))
    
    #p["V_gas"] = p["V_EAF"] - y[ind["m_st_sol"]]/p["rho_st_sol"] - y[ind["m_st_liq"]]/p["rho_st_liq"]

    # molar and mass fractions for liquid zones and gas
    #inp = get_mass_and_molar_fractions(inp,y)
    update_mass_and_molar_fractions(ind=ind, M=M, X=X, W=W, y=y, c=c, cp=cp)

    # calculate equilibrium mass fractions for reactions with slag and melt,
    # oxygen injection and carbon injection
    equilibrium_mass_fractions(y=y, c=c, ind=ind, X=X, W=W, M=M)  #return W

    # correction parameter to reduce heat losses from melt after significant
    # resolidification has occured
    resolid_correct_st_sol = (tan_hyp(((y[ind["m_resolid"]]
                            / min(y[ind["m_st_liq"]], K["m_resolid"]))
                            * K["resolid_influence"] + 1
                            - K["resolid_influence"]) * ( 1
                            - (y[ind["T_st_liq"]] - T_melt_st
                            + c["T_undercool"]) / K["T_resolid"]),
                            0.5, -6.5, 0.25))
    resolid_correct = tan_hyp(y[ind["m_resolid"]] / y[ind["m_st_liq"]],
                            0.2, -25, 0.5)
    # has to be considered for heat transfer equations!!! different structure than in Matlab to avoid k.therm= K.therm*x
    K["therm"][2] = 0.01 * (y[ind["m_st_sol"]]/p["m_scrap_init"])

    # specific surface area for contact between melt and scrap
    K["area"][0] = (1e-4 + 0.175 * np.exp(-4 * (y[ind["m_st_sol"]]
                / p["m_scrap_init"]) ** 0.45))

    # thermal efficiency of post combustion
    K["post"] = K["burn"] * (0.35 + 0.65 * np.tanh(1300 / (y[ind["T_st_sol"]]
                - 273) - 1))


    dry_offgas_percentage = (y[ind["m_CH4"]] + y[ind["m_CO"]] + y[ind["m_CO2"]] + y[ind["m_N2"]] + y[ind["m_O2"]] + y[ind["m_H2"]]) / y[ind["m_gas"]] #1
    get_current_op_chart(OpCh=OpCh, iv=iv, comp=comp, c=c, M=M, Q=Q, t=t, dop = dry_offgas_percentage)

    """ get current geometry"""
    get_current_geometry(p=p,c=c,OpCh=OpCh,K=K,ind=ind,y=y)

    """ get electrode cooling"""
    electrode_cooling(p, OpCh["P_arc"], iv, Q["el_cooling_heat"], cp["H2O_liq"], M["H2O"], K["alpha_el_cool"] , dh["H2O_100"], y[ind["T_el"]])


    A_vessel_melt = ((p["h_melt"] / p["h_EAF_low"]) * p["A_vessel_side"]
                    + p["A_vessel_bottom"])



    roof_constants = (p["lambda_roof"] / (p["pipe_thick"] * OpCh["m_water_roof"]
                    * cp["H2O_liq"] / M["H2O"] * 1e3) * np.pi
                    * (p["r_EAF_up"] ** 2 - p["r_heart"] ** 2))

    wall_constants = (p["lambda_wall"] / ((p["pipe_thick"]
                    + p["slag_thick"]) * OpCh["m_water_wall"]
                    * cp["H2O_liq"] / M["H2O"] * 1e3) * np.pi
                    * 2 * p["r_EAF_up"]
                    * (p["h_EAF_up"]  - p["h_EAF_low"] ))

    iv["C_inj_av"] = OpCh["m_C_inj"] * (1 - K["C_inj_loss"])


    iv["C_conv_gas"] = (min(OpCh["m_conv_gas"] * comp["air_O2"] / M["O2"]
                            * 2 * M["C"], iv["C_inj_av"]))

    iv["C_inj_av"] = iv["C_inj_av"] - iv["C_conv_gas"]


    """Mass change solids and liquids (alls [kg/s])"""

    # various rates of change for each species from different reactions etc.
    # initialize empty matrix for rates of change
    x = np.zeros((16,12))
    diffusion = np.zeros(9)
    # initialise array for total rates of change for each differential variable
    dy=y*0

    # Rate of change of combustible materials
    dy[ind["m_comb_scrap"]] = (-kd["comb"] * y[ind["m_comb_scrap"]] ** 0.5
                                * tan_hyp(y[ind["m_comb_scrap"]],2,0.8,10)
                                * (1.5 + tan_hyp(y[ind["m_O2"]],0.3,1,1)
                                + tan_hyp(OpCh["P_arc"],5,0.4,1.4)))

    # Rate of change of volatile PKS components
    dy[ind["m_vol_coal"]] = (-kd["vol"] * y[ind["m_vol_coal"]] ** 0.75
                            * tan_hyp(y[ind["m_vol_coal"]],0.05,50,5)
                            * (1-(y[ind["V_st_sol"]] / p["V_scrap_init"])
                            ** 2 + 0.1))

    A_el = 2 * np.pi * p["r_el"] * p["h_el"] * p["n_el"]

    # change of Electrode mass [kg/s]
    dy[ind["m_el"]] = (-(kd["el_tip"] * OpCh["I_arc"] ** 2 + kd["el_side"]
                        * A_el) / 3600
                        * tan_hyp(y[ind["m_O2"]],0.01,100,20))

    # rate of refractory consumption [ks/s], assumed density of 2e3 kg/m?
    m_refrac_wear = (K["refrac"] / 3600 * (A_vessel_melt
                    + y[ind["h_slag"]] / p["h_melt"]
                    * (A_vessel_melt - p["A_vessel_bottom"])))*2e3

    #m_leak_air = OpCh["m_leakair"]
    m_leak_air = (-K["pr"] * (y[ind["p_furnace"]]-K["pr4"]) * tan_hyp(
                y[ind["p_furnace"]],K["pr1"],-K["pr2"],K["pr3"])
                + K["convective_air"])

    dy[ind["m_leak_air"]] = (m_leak_air - y[ind["m_leak_air"]]) * 50

    # mass flow rate of leak air entering EAF;
    #m_leak_air = (y[ind["m_leak_air"]] + m_leak_air) / 2

    K["melt_exposed"]= abs((0.5*np.tanh(5*(y[ind["h_slag"]]-p["h_scrap"]+p["h_hole"])+3*(p["r_hole"]-p["r_hole_init"])
        /(p["r_EAF_low"]+p["h_melt"]-p["r_hole_init"]))+0.5)*(1-((y[ind["V_st_sol"]]/p["V_scrap_init"])**2)*tan_hyp((y[ind["m_st_sol"]]/y[ind["m_st_liq"]]),0.1,25,1)))

    #R["melt_exposed_before_thyp"] = K["melt_exposed"]


    """reaction change rates steel and carbon [kg/s]"""

    #  Rate of change of carbon (C)

    # burning rate of charged coal to CO (g)
    x[1][1] = ((-kd["C_char_burn"] * (K["melt_exposed"] ** 4 + 0.05)
                * y[ind["m_coal"]] ** 0.5 *((y[ind["m_O2"]] / M["O2"]
                / p["V_gas"]) ** 0.5) * tan_hyp(y[ind["m_O2"]],
                0.001,75,30) * tan_hyp(y[ind["T_gas"]],500,0.006,2)
                - comp["air_O2"] * m_leak_air * K["leak_air_coal_burn"])
                * tan_hyp(y[ind["m_coal"]],0.5,75,30))

    # Charged C decarburization: becoming available for slag reactions (b)
    x[1][2] = (-kd["C_char_reac"] * (1.01 - y[ind["V_st_sol"]]
                / p["V_scrap_init"]) ** 3 * y[ind["m_coal"]] ** 0.75
                * tan_hyp(y[ind["m_coal"]],0.5,0.5,20))

    # charged C dissolving
    x[1][3] = (-kd["C_char_diss"] * y[ind["m_coal"]] ** 0.55
                * y[ind["T_st_liq"]] * tan_hyp(y[ind["m_coal"]],0.5,75,30))

    # carbon from dissociation of combustibles
    x[1][4] = -dy[ind["m_comb_scrap"]] / M["C9H20"] * M["C"] * 9

    # add coal becoming available for slag reactions to C_inj
    C_inj_eff = iv["C_inj_av"] - x[1][2]

    # Decrease C_inj available for slag reactions if FeO in slag is low
    K_C_inj_char = ( min( 1 -K["C_inj_diss"],
                            max(5 / W["FeO"], K["C_inj_char"])))
    #K["C_inj_char"] = ( min( 1 -K["C_inj_diss"],
    #                        max(5 / W["FeO"], K["C_inj_char"])))
    K_CO_slag_CO2 = K["CO_slag_CO2"] * tan_hyp(W["FeO"], 35,0.075,1)
    #K["CO_slag_CO2"] = K["CO_slag_CO2"] * tan_hyp(W["FeO"], 35,0.075,1)


    O_reac_av = tan_hyp(y[ind["m_O_reac"]], 0.02,1500,1)

    diffusion[1] = kd["O_1"] * (W["O"] - W["O_reac"]) * (y[ind["m_O_st_liq"]]/(0.1+y[ind["m_O_st_liq"]]))

    """ Rate of change of dissolved carbon (C)"""
    # Diffusion of dissolved C to interface
    diffusion[2] = kd["C_1"] * (W["C"] - W["C_reac"]) * (y[ind["m_C_st_liq"]]/(0.1+y[ind["m_C_st_liq"]]))

    # Dissolved C oxidation to CO; Reaction with dissolved O (g)
    x[2][1] = (-kd["C_1"] * (W["C_reac"] - W["C_eq"]) * O_reac_av * K["reac_fact"])

    # constraint to avoid negative masses depending on direction of reaction
    x[2][1] = (x[2][1] * ((1 - np.sign(x[2][1]))
            * tan_hyp(y[ind["m_C_reac"]], 0.05,500,1) * (y[ind["m_C_st_liq"]]/(0.1+y[ind["m_C_st_liq"]]))) / 2)

    # Dissolved C oxidation to CO; Reaction with injcected O (g)
    x[2][2] = (-kd["C_2"] * (W["C"] - W["C_eq_O"]) * OpCh["m_O2_lance"]
                * (1 - K["CO_C_CO2"]))

    # Dissolved C increase due to dissolving of charged C
    x[2][3] = -x[1][3]

    # Dissolved C increase due to dissolving of injected C
    x[2][4] = C_inj_eff * K["C_inj_diss"]

    # Dissolved C oxidation to CO2; Reaction with injected O (i)
    x[2][5] = x[2][2] / (1 - K["CO_C_CO2"]) * K["CO_C_CO2"]

    K_O2 = np.zeros(8)

    K_O2[1] = (-x[2][2] / M["C"] * M["O"]) / (OpCh["m_O2_lance"] +1e-6)

    K_O2[2] = (-x[2][5] / M["C"] * M["O2"]) / (OpCh["m_O2_lance"] +1e-6)

    """Rate of change of silicon (Si)"""
    # Diffusion of Si to interface
    diffusion[3] = kd["Si_1"] * (W["Si"] - W["Si_reac"]) * (y[ind["m_Si_st_liq"]]/(0.1+y[ind["m_Si_st_liq"]]))

    # Si oxidation to SiO2; Reaction with dissovled O (c)
    x[3][1] = (-kd["Si_1"] * (W["Si_reac"] - W["Si_eq"]) #* O_reac_av
                * K["reac_fact"])

    # constraint to avoid negative masses depending on direction of reaction
    x[3][1] = (x[3][1] * ((1 + np.sign(x[3][1]))
                * tan_hyp(y[ind["m_SiO2_sl_liq"]], 0.05,50,5)
                + (1 - np.sign(x[3][1]))
                * tan_hyp(y[ind["m_Si_reac"]], 0.05,500,1) * (y[ind["m_Si_st_liq"]]/(0.1+y[ind["m_Si_st_liq"]])) * O_reac_av) / 2)

    # Si oxidation to SiO2,; Reaction with injected O (c)
    x[3][2] = (-kd["Si_2"] * (W["Si"] - W["Si_eq_O"]) * OpCh["m_O2_lance"]
                * tan_hyp(y[ind["m_Si_st_liq"]], 0.05,50,5)) 

    # Si from SiO2 reduction with injected carbon (d)
    x[3][3] = (kd["Si_3"]  * W["Si_eq_C"] * C_inj_eff
                * tan_hyp(y[ind["m_SiO2_sl_liq"]],0.05,50,5))

    K_O2[3] = (-x[3][2] / M["Si"] * 2 *M["O"]) / (OpCh["m_O2_lance"] + 1e-6)

    K_C = np.zeros(9)

    K_C[3] = (x[3][3] / M["Si"] * 2 * M["C"]) / (C_inj_eff + 1e-6)

    """Rate of change of manganese (Mn)"""
    diffusion[4] = kd["Mn_1"] * (W["Mn"] - W["Mn_reac"]) * (y[ind["m_Mn_st_liq"]]/(0.1+y[ind["m_Mn_st_liq"]]))

    # Diffusion of Mn to interface
    x[4][1] = (-kd["Mn_1"] * (W["Mn_reac"] - W["Mn_eq"]) #* O_reac_av
                * K["reac_fact"])

    # Mn oxidation to MnO; Reaction with dissovled O (e)
    x[4][1] = (x[4][1] * ((1 + np.sign(x[4][1]))
                * tan_hyp(y[ind["m_MnO_sl_liq"]], 0.05,50,5)
                + (1 - np.sign(x[4][1]))
                * tan_hyp(y[ind["m_Mn_reac"]], 0.05,500,1) * (y[ind["m_Mn_st_liq"]]/(0.1+y[ind["m_Mn_st_liq"]])) * O_reac_av) / 2)

    # Mn oxidation to MnO; Reaction with dissovled O (e)
    x[4][2] = (-kd["Mn_2"] * (W["Mn"] - W["Mn_eq_O"]) * OpCh["m_O2_lance"]
                * tan_hyp(y[ind["m_Mn_st_liq"]], 0.05,50,5))

    # Mn from MnO reduction with injected carbon (f)
    x[4][3] = (kd["Mn_3"]  * W["Mn_eq_C"] * C_inj_eff
                * tan_hyp(y[ind["m_MnO_sl_liq"]],0.05,50,5))

    K_O2[4] = (-x[4][2] / M["Mn"] * M["O"]) / (OpCh["m_O2_lance"] + 1e-6)

    K_C[4] = (x[4][3] / M["Mn"] * M["C"]) / (C_inj_eff + 1e-6)

    """Rate of change of chromium (Cr)"""
    # Diffusion of Cr to interface
    diffusion[5] = kd["Cr_1"] * (W["Cr"] - W["Cr_reac"]) * (y[ind["m_Cr_st_liq"]]/(0.1+y[ind["m_Cr_st_liq"]]))

    # Cr oxidation to CrO3; Reaction with dissovled O (j)
    x[5][1] = (-kd["Cr_1"] * (W["Cr_reac"] - W["Cr_eq"]) #* O_reac_av
                * K["reac_fact"])
    # constraint to avoid negative masses depending on direction of reaction
    x[5][1] = (x[5][1] * ((1 + np.sign(x[5][1]))
                * tan_hyp(y[ind["m_Cr2O3_sl_liq"]], 0.05,50,5)
                + (1 - np.sign(x[5][1]))
                * tan_hyp(y[ind["m_Cr_reac"]], 0.05,500,1) * (y[ind["m_Cr_st_liq"]]/(0.1+y[ind["m_Cr_st_liq"]])) * O_reac_av) / 2)

    # Cr oxidation to CrO3; Reaction with injected O (j)
    x[5][2] = (-kd["Cr_2"] * (W["Cr"] - W["Cr_eq_O"]) * OpCh["m_O2_lance"]
                * tan_hyp(y[ind["m_Cr_st_liq"]], 0.1,10,5))

    # Cr from Cr2O3 reduction with injected carbon (k)
    x[5][3] = (kd["Cr_3"]  * W["Cr_eq_C"] * C_inj_eff
                * tan_hyp(y[ind["m_Cr2O3_sl_liq"]],0.05,50,5))

    K_O2[5] = (-x[5][2] / (2*M["Cr"]) *3* M["O"]) / (OpCh["m_O2_lance"]
                +1e-6) 

    K_C[5] = (x[5][3] / M["Cr"]* M["C"] * 3/2 ) / (C_inj_eff + 1e-6) 

    """Rate of change of phosphorus (P)"""
    # Diffusion of P to interface
    diffusion[6] = kd["P_1"] * (W["P"] - W["P_reac"]) * (y[ind["m_P_st_liq"]]/(0.1+y[ind["m_P_st_liq"]]))

    # P oxidation to P2O5; Reaction with dissovled O (l)
    x[6][1] = (-kd["P_1"] * (W["P_reac"] - W["P_eq"]) #* O_reac_av
                * K["reac_fact"])

    # constraint to avoid negative masses depending on direction of reaction
    x[6][1] = (x[6][1] * ((1 + np.sign(x[6][1]))
                * tan_hyp(y[ind["m_P2O5_sl_liq"]], 0.05,50,5)
                + (1 - np.sign(x[6][1]))
                * tan_hyp(y[ind["m_P_reac"]], 0.05,500,1) * (y[ind["m_P_st_liq"]]/(0.1+y[ind["m_P_st_liq"]])) * O_reac_av) / 2)

    # P oxidation to P2O5; Reaction with injected O (l)
    x[6][2] = (-kd["P_2"] * (W["P"] - W["P_eq_O"]) * OpCh["m_O2_lance"]
                * tan_hyp(y[ind["m_P_st_liq"]], 0.05,50,5))

    # P from P2O5 reduction with injected carbon (m)
    x[6][3] = (kd["P_3"]  * W["P_eq_C"] * C_inj_eff
                * tan_hyp(y[ind["m_P2O5_sl_liq"]],0.05,50,5))


    K_O2[6] = (-x[6][2] / M["P"] / 2 * 5 * M["O"]) / (OpCh["m_O2_lance"]
                                                        + 1e-6)

    K_C[6] = (x[6][3] / M["P"] / 2 * 5 * M["C"]) / (C_inj_eff + 1e-6)


    """Rate of change of iron"""
    # Diffusion of Fe to interface
    diffusion[7] = kd["Fe_1"] * (W["Fe"] - W["Fe_reac"])

    # Fe oxidation to FeO; Reaction with dissovled O (a)
    x[7][1] = (-kd["Fe_1"] * (W["Fe_reac"] - W["Fe_eq"]) #* O_reac_av
                * K["reac_fact"])

    # constraint to avoid negative masses depending on direction of reaction
    x[7][1] = (x[7][1] * ((1 + np.sign(x[7][1]))
                * tan_hyp(y[ind["m_FeO_sl_liq"]], 0.05,50,5)
                + (1 - np.sign(x[7][1]))
                * tan_hyp(y[ind["m_Fe_reac"]], 0.05,500,1) * O_reac_av) / 2)

    # Fe oxidation to FeO; Reaction with injected O (a)
    x[7][2] = (-kd["Fe_2"] * (W["Fe"] - W["Fe_eq_O"]) * OpCh["m_O2_lance"])

    # Fe from FeO reduction with injected carbon (b)
    x[7][3] = (kd["Fe_3"]  * W["Fe_eq_C"] * C_inj_eff
                * tan_hyp(y[ind["m_FeO_sl_liq"]],10,2,1))

    K_O2[7] = (-x[7][2] / M["Fe"] *  M["O"]) / (OpCh["m_O2_lance"] + 1e-6)


    K_C[7] = (x[7][3] / M["Fe"]  * M["C"]) / (C_inj_eff + 1e-6)

    """normalization of reactions with C_inj and O2_inj"""
    # normalization of O2_lance consuming reactions to match injected O2 rate
    x[2:8,2] = x[2:8,2] / (sum(K_O2) + 1e-10) * (1 - K["O2_lance_diss"] - K_O2_lance_gasphase)

    # including correction for O2 partially dissolving in melt (CO2 production)
    x[2][5] = x[2][5] / (sum(K_O2) + 1e-10) * (1 - K["O2_lance_diss"] - K_O2_lance_gasphase)

    # normalization of C_inj consuming reactions to match injected C rate
    x[3:8,3] = (x[3:8,3] / (sum(K_C) + 1e-10) * (1 + K_CO_slag_CO2)
                * (1 - K["C_inj_diss"] - K_C_inj_char))

    """Rate of change of dissolved Oxygen (O)"""
    # oxygen from reaction with dissolved C
    x[8][1] = x[2][1] * M["O"] / M["C"]
    # oxygen from reaction with Si
    x[8][2] = x[3][1] * M["O"] / M["Si"] * 2
    # oxygen from reaction with Mn
    x[8][3] = x[4][1] * M["O"] / M["Mn"]
    # oxygen from reaction with Cr
    x[8][4] = x[5][1] * M["O"] / M["Cr"] * 3 / 2
    # oxygen from reaction with P
    x[8][5] = x[6][1] * M["O"] / M["P"] * 5 / 2
    # oxygen from reaction with Fe
    x[8][6] = x[7][1] * M["O"] / M["Fe"]

    # oxygen from reaction S+CaO --> CaS + O
    x[8][8] = kd["S"] * (W["S_reac"] - W["S_eq"])
    # constraint to avoid negative masses
    x[8][8] = (x[8][8] * ((1 + np.sign(x[8][8]))
                * tan_hyp(y[ind["m_CaO_sl_liq"]], 0.05,50,5)
                * tan_hyp(y[ind["m_S_reac"]], 0.05,500,1)
                + (1 - np.sign(x[8][8]))
                * tan_hyp(y[ind["m_CaS_sl_liq"]], 0.05,50,5) * O_reac_av) / 2)
    # oxygen dissolving from injected O2
    x[8][9] = OpCh["m_O2_lance"] * K["O2_lance_diss"]

    # Diffusion of Sulphur to interface [kg/s]
    diffusion[8] = kd["S"] * (W["S"] - W["S_reac"]) * (y[ind["m_S_st_liq"]]/(0.1+y[ind["m_S_st_liq"]]))

    """rates of change at interface"""
    # rate of change of oxygen at interface [kg/s]
    dy[ind["m_O_reac"]]  = diffusion[1] + sum(x[8][1:9])
    # rate of change of C at interface [kg/s]
    dy[ind["m_C_reac"]]  = diffusion[2] + x[2][1]
    # rate of change of Si at interface [kg/s]
    dy[ind["m_Si_reac"]] = diffusion[3] + x[3][1]
    # rate of change of Mn at interface [kg/s]
    dy[ind["m_Mn_reac"]] = diffusion[4] + x[4][1]
    # rate of change of Cr at interface [kg/s]
    dy[ind["m_Cr_reac"]] = diffusion[5] + x[5][1]
    # rate of change of P at interface [kg/s]
    dy[ind["m_P_reac"]]  = diffusion[6] + x[6][1]
    # rate of change of Fe at interface [kg/s]
    dy[ind["m_Fe_reac"]] = diffusion[7] + x[7][1]
    # rate of change of S at interface [kg/s]
    dy[ind["m_S_reac"]]  = diffusion[8] - x[8][8] / M["O"] * M["S"]

    # adjusting with mass flow from or to reactor to keep total reactor mass constant
    reac_mass_change = sum(dy[ind["m_O_reac"]:ind["m_S_reac"]+1])
    flow_direction = 1 - tan_hyp(reac_mass_change,0,500,1)
    dy[ind["m_O_reac"]] = (dy[ind["m_O_reac"]] - reac_mass_change
                            * (flow_direction * W["O"]
                            + (1 - flow_direction) * W["O_reac"]) / 100)

    dy[ind["m_C_reac"]] = (dy[ind["m_C_reac"]] - reac_mass_change
                            * (flow_direction * W["C"]
                            + (1 - flow_direction) * W["C_reac"]) / 100)

    dy[ind["m_Si_reac"]] = (dy[ind["m_Si_reac"]] - reac_mass_change
                            * (flow_direction * W["Si"]
                            + (1 - flow_direction) * W["Si_reac"]) / 100)

    dy[ind["m_Mn_reac"]] = (dy[ind["m_Mn_reac"]] - reac_mass_change
                            * (flow_direction * W["Mn"]
                            + (1 - flow_direction) * W["Mn_reac"]) / 100)

    dy[ind["m_Cr_reac"]] = (dy[ind["m_Cr_reac"]] - reac_mass_change
                            * (flow_direction * W["Cr"]
                            + (1 - flow_direction) * W["Cr_reac"]) / 100)

    dy[ind["m_P_reac"]] = (dy[ind["m_P_reac"]] - reac_mass_change
                            * (flow_direction * W["P"]
                            + (1 - flow_direction) * W["P_reac"]) / 100)

    dy[ind["m_Fe_reac"]] = (dy[ind["m_Fe_reac"]] - reac_mass_change
                            * (flow_direction * W["Fe"]
                            + (1 - flow_direction) * W["Fe_reac"]) / 100)

    dy[ind["m_S_reac"]] = (dy[ind["m_S_reac"]] - reac_mass_change
                            * (flow_direction * W["S"]
                            + (1 - flow_direction) * W["S_reac"]) / 100)

    """Burners and gas regions for gas reactions"""
    burner_reactions(p=p,c=c,ind=ind,M=M,K=K,OpCh=OpCh,iv=iv,kd=kd,comp=comp,y=y)

    """mass change rates gases (all [kg/s])"""

    """Rate of change of C in EAF"""
    # C from Boduard reaction
    x[1][5] = iv["dz_gas"][2][7] * M["C"]

    # C from heterogenrous water gas reaction
    x[1][6] = iv["dz_gas"][3][7] * M["C"]

    # C from injected coal not reacting or dissolving
    x[1][7] = C_inj_eff * K_C_inj_char

    dy[ind["m_coal"]] = sum(x[1][:])

    """Rate of change of CO"""
    # CO extraction through off-gas vents (-)
    x[9][1] = (- OpCh["m_offgas"] * y[ind["m_CO"]] / y[ind["m_gas"]])

    # CO from oxidation of C (+)
    x[9][2] = ((M["CO"]/M["C"]) * (C_inj_eff
                * (1 - K["C_inj_diss"] - K_C_inj_char)
                * ( 1- K_CO_slag_CO2) -x[2][1] - x[2][2]
                + iv["C_conv_gas"]))

    # CO post combustion with O2 (-)
    x[9][3] = ((-kd["CO_post"] * y[ind["m_CO"]]
                * ((y[ind["m_O2"]] / M["O2"] / p["V_gas"]) ** 0.5))
                * tan_hyp(y[ind["m_O2"]],0.001,75,30)
                - 2* (M["CO"] / M["O2"]) * OpCh["m_O2_post"]
                * K["O2_post_CO"] * tan_hyp(y[ind["m_CO"]],0.001,75,30))

    # CO extraction through hatches (-)
    x[9,4] = (-(K["pr"] * (y[ind["p_furnace"]] + K["pr4"]) * y[ind["m_CO"]]
                * tan_hyp(y[ind["m_CO"]],0.003,600,10)) / y[ind["m_gas"]]
                * tan_hyp(y[ind["p_furnace"]],-K["pr1"],K["pr2"],K["pr3"]))

    # CO from homogeneous water-gas-shift reaction
    x[9][5] = iv["dz_gas"][1][0] * M["CO"]

    #CO from electrode oxidation, oxidation of coal and volatile coal components
    x[9][6] = (-(M["CO"] / M["C"]) * dy[ind["m_el"]]
                - (M["CO"] / M["C"]) * x[1][1] + (-dy[ind["m_vol_coal"]]
                / (1 - comp["C_char_C_fix"]) * comp["C_char_C_vol"]
                + (C_inj_eff + x[1,2] + iv["C_conv_gas"]) * comp["C_inj_C_vol"]
                / comp["C_inj_C_fix"]) * M["CO"] / M["C"])

    # CO from Boudouard reaction
    x[9][7] = iv["dz_gas"][2][0] * M["CO"]

    # CO from heterogeneous water-gas-shift reaction
    x[9][9] = iv["dz_gas"][3][0] * M["CO"]

    """% Rate of change of CO2"""
    #  CO2 extraction through off-gas vents (-)
    x[10][1] = (- OpCh["m_offgas"] * y[ind["m_CO2"]] / y[ind["m_gas"]])

    # CO2 from CO post combustion with leak air and injected O2(+)
    x[10][2] = - x[9][3] * M["CO2"] / M["CO"]

    # CO2 from homogeneous water-gas-shift reaction
    x[10][3] = iv["dz_gas"][1][1] * M["CO2"]

    # CO2 from Boduard reaction
    x[10][5] = iv["dz_gas"][2][1] * M["CO2"]

    # CO2 extraction through hatches (-)
    x[10][7] = (-(K["pr"] * (y[ind["p_furnace"]] + K["pr4"])
                * y[ind["m_CO2"]]
                * tan_hyp(y[ind["m_CO2"]],0.003,600,10)) / y[ind["m_gas"]]
                * tan_hyp(y[ind["p_furnace"]],-K["pr1"],K["pr2"],K["pr3"]))

    # CO2 from dissolved C oxidation (+)
    x[10][8] = - x[2][5] * M["CO2"] / M["C"]

    # CO2 from Reaction of CO with slag
    x[10][9] = C_inj_eff*(1- K["C_inj_diss"]-K_C_inj_char)*K_CO_slag_CO2*M["CO2"]/M["C"]

    """% Rate of change of N2"""
    # N2 extraction through off-gas vents (-)
    x[11][1] =  (- OpCh["m_offgas"] * y[ind["m_N2"]] / y[ind["m_gas"]]
                * tan_hyp(y[ind["m_N2"]],0.002,600,10))

    # N2 extraction through off-gas vents (-)
    x[11][2] = comp["air_N2"] * m_leak_air

    # N2 blown out through hatches
    x[11][3] = (-(K["pr"] * (y[ind["p_furnace"]] + K["pr4"])
                * y[ind["m_N2"]]
                * tan_hyp(y[ind["m_N2"]],0.003,600,10)) / y[ind["m_gas"]]
                * tan_hyp(y[ind["p_furnace"]],-K["pr1"],K["pr2"],K["pr3"]))

    # N2 as part of injected gases and coal
    x[11][4] = (OpCh["m_CH4_burn"] / comp["gas_CH4"] * comp["gas_N2"]
                -dy[ind["m_vol_coal"]] / (1 - comp["C_char_C_fix"])
                * comp["C_char_N2"] + (OpCh["m_O2_post"]
                + OpCh["m_O2_lance"] + OpCh["m_O2_CH4_burn"] + OpCh["m_O2_H2_burn"])
                / comp["O2_O2"]*comp["O2_N2"] + (iv["C_inj_av"]
                + iv["C_conv_gas"]) * comp["C_inj_N2"] / comp["C_inj_C_fix"])

    # N2 from coal conveyor gas (air)  (+)
    x[11][5] = comp["air_N2"] * OpCh["m_conv_gas"]

    """% Rate of change of O2"""
    # O2 extraction through off-gas vents (-)
    x[12][1] = (- OpCh["m_offgas"] * y[ind["m_O2"]] / y[ind["m_gas"]]
                * tan_hyp(y[ind["m_O2"]],0.002,600,10))

    # Mass O2 sucked in with leak air (+)
    x[12][2] = comp["air_O2"] * m_leak_air + OpCh["m_O2_lance"] * K_O2_lance_gasphase

    # Residual O2 from CO post combustion with leak air and injected O2 (-)
    x[12][4] = OpCh["m_O2_post"] + M["O"] / M["CO"] * x[9][3]

    # O2 for electrode oxidation (-)
    x[12][5] = dy[ind["m_el"]] *M["O"] / M["C"]

    # oxidation of coal (-) to CO; was 14O2 because was combusted to CO2
    x[12][6] = x[1][1] * M["O"] / M["C"]

    # O2 blown out through hatches
    x[12][7] = (-(K["pr"] * (y[ind["p_furnace"]] + K["pr4"])
                * y[ind["m_O2"]]
                * tan_hyp(y[ind["m_O2"]],0.003,600,10)) / y[ind["m_gas"]]
                * tan_hyp(y[ind["p_furnace"]],-K["pr1"],K["pr2"],K["pr3"]))

    """Rate of change of H2"""
    # H2 extraction through off-gas vents
    x[13][1] = (- OpCh["m_offgas"] * y[ind["m_H2"]] / y[ind["m_gas"]]
                * tan_hyp(y[ind["m_H2"]],0.002,600,10))

    # H2 blown out through hatches
    x[13][2] = (-(K["pr"] * (y[ind["p_furnace"]] + K["pr4"])
                * y[ind["m_H2"]]
                * tan_hyp(y[ind["m_H2"]],0.003,600,10)) / y[ind["m_gas"]]
                * tan_hyp(y[ind["p_furnace"]],-K["pr1"],K["pr2"],K["pr3"]))

    # H2 from hetergogeneous water-gas-shift reaction
    x[13][4] = (-kd["H2_post"] * y[ind["m_H2"]]
                * (y[ind["m_O2"]] / M["O2"] / p["V_gas"]) ** 0.5
                * tan_hyp(y[ind["m_O2"]],0.04,70,20))

    # H2 from homogeneous water-gas-shift reaction
    x[13][5] = iv["dz_gas"][3][4] * M["H2"]

    # H2 from dissociation of combustibles
    x[13][6] = iv["dz_gas"][1][4] * M["H2"]

    # H2 from volatile combustibles components
    x[13][7] = - dy[ind["m_comb_scrap"]] / M["C9H20"] * M["H2"] * 10
    # H2 from volatile Coal components
    x[13][8] = (-dy[ind["m_vol_coal"]] / (1 - comp["C_char_C_fix"])
                * comp["C_char_H2"] + (iv["C_inj_av"] + iv["C_conv_gas"])
                * comp["C_inj_H2"] / comp["C_inj_C_fix"])

    x[13][9] = -iv["burner"][5] * M["H2"]

    """Rate of change of H2O"""

    # H2O extraction through off-gas-vents
    x[14][1] = (- OpCh["m_offgas"] * y[ind["m_H2O"]] / y[ind["m_gas"]]
                * tan_hyp(y[ind["m_H2O"]],0.002,600,10))
    # H2O blown out through hatches
    x[14][2] = (-(K["pr"] * (y[ind["p_furnace"]] + K["pr4"])
                * y[ind["m_H2O"]]
                * tan_hyp(y[ind["m_H2O"]],0.003,600,10)) / y[ind["m_gas"]]
                * tan_hyp(y[ind["p_furnace"]],-K["pr1"],K["pr2"],K["pr3"]))
    # H2O from electrode cooling
    x[14][3] = iv["m_water_in"]
    # H2O from heterogeneous water-gas-shift reaction
    x[14][8] = iv["dz_gas"][3][5] * M["H2O"]
    # H2O homogeneous water-gas-shift reaction
    x[14][9] = iv["dz_gas"][1][5] * M["H2O"]
    # H2O from Coal
    x[14][10] = (-dy[ind["m_vol_coal"]] / (1 - comp["C_char_C_fix"])
                * comp["C_char_H2O"] + (iv["C_inj_av"] + iv["C_conv_gas"])
                * comp["C_inj_H2O"] / comp["C_inj_C_fix"])

    """Rate of change of CH4"""

    # CH4 extracted through off gas vents
    x[15][1] = (- OpCh["m_offgas"] * y[ind["m_CH4"]] / y[ind["m_gas"]]
                * tan_hyp(y[ind["m_CH4"]],0.002,600,10))
    # CH4 blown out through hatches
    x[15][2] = (-(K["pr"] * (y[ind["p_furnace"]] + K["pr4"])
                * y[ind["m_CH4"]]
                * tan_hyp(y[ind["m_CH4"]],0.003,600,10)) / y[ind["m_gas"]]
                * tan_hyp(y[ind["p_furnace"]],-K["pr1"],K["pr2"],K["pr3"]))
    # CH4 injected through CH4 burners
    x[15][3] = OpCh["m_CH4_burn"]
    # CH4 + 2O2 = CO2 + 2H2O
    x[15][4] = -iv["burner"][0] * M["CH4"]
    # CH4 + 3/2O2 = CO + 2H2O
    x[15][5] = -iv["burner"][1] * M["CH4"]
    # CH4 + O2 = CO2 + 2H2
    x[15][6] = -iv["burner"][2] * M["CH4"]
    #CH4 post combustion in region 2     CH4 + 3/2O2 -> CO + 2H2O
    x[15][8] = (- kd["CH4_post"] * y[ind["m_CH4"]] * ((y[ind["m_O2"]]
                / M["O2"] / p["V_gas"]) ** 1.5
                * tan_hyp(y[ind["m_CH4"]],0.0002,2000,13))
                * (1.5-(y[ind["m_vol_coal"]] / (y[ind["m_coal"]]
                + y[ind["m_vol_coal"]] + 1))))

    # CO from CH4 burners
    x[9][8] = (-x[15][5] - x[15][8]) / M["CH4"] * M["CO"]
    # CO2 from CH4 burners
    x[10][4] = -x[15][4] / M["CH4"] * M["CO2"]
    # CO2 from CH4 burners
    x[10][6] = -x[15][6] / M["CH4"] * M["CO2"]
    # residual O2 from CH4 burners
    x[12][3] = (OpCh["m_O2_CH4_burn"] + x[15][4] / M["CH4"] * M["O2"] * 2
                + x[15][5] / M["CH4"] * M["O2"] * 3 / 2
                + x[15][6] / M["CH4"] *M["O2"])

    x[12][11] = OpCh["m_O2_H2_burn"] + x[13][9] / M["H2"] * M["O2"] * 0.5
    # O2 consumption for CH4 post combustion
    x[12][8] = x[15][8] * M["O2"] / 2 * 3 / M["CH4"]
    # O2 for H2 post combustion
    x[12][9] = x[13][4] * M["O2"] / 2 / M["H2"]
    # H2 from incomplete CH4 combustion from burners
    x[13][3] = -x[15][6] / M["CH4"] * M["H2"] * 2
    # H2O from CH4 post-combustion in region 2
    x[14][4] = -x[15][8] / M["CH4"] * 2 * M["H2O"]
    # H2O from CH4 burners
    x[14][6] = (-x[15][4] - x[15][5]) / M["CH4"] * M["H2O"] * 2
    # H2O from H2 post-combustion
    x[14][7] = -x[13][4] / M["H2"] * M["H2O"]

    x[14][11] = x[13][9] / M["H2"] * M["H2O"]

    # dissociation of water
    x[14][5] = (-kd["H2O_diss"] * y[ind["m_H2O"]]
                * tan_hyp(y[ind["m_H2O"]],0.39,20,3)
                * tan_hyp(OpCh["P_arc"],15,0.3,1))

    x[13][8] = x[13][8] - x[14][5] / M["H2O"] * M["H2"]

    x[12][10] = -x[14][5] / M["H2O"] * M["O"]


    """Reaction of O2 and CO2 from burners with scrap producing FeO"""

    #ratio of residual burner O2 used for FeO production [-]
    K_FeO_O2 = (K["FeO_O2"] * p["l_scrap_flame"] / p["l_flame"]
                * tan_hyp(y[ind["m_Fe_st_sol"]],50,1,10))

    # ratio of CO2 from burners used for FeO production [-]
    K_FeO_CO2 = (K["FeO_CO2"] * p["l_scrap_flame"] / p["l_flame"]
                * tan_hyp(y[ind["m_Fe_st_sol"]],50,1,10))

    iv["m_FeO_burn_O2"] = K_FeO_O2 * (x[12][3] + x[12][11]) * M["FeO"] / M["O"]


    iv["m_FeO_burn_CO2"] = K_FeO_CO2 * (x[10][4] + x[10][6]) * M["FeO"] / M["CO2"]

    # oxidation of scrap (Fe only) with oxygen postcombustion O2
    iv["m_FeO_post_O2"] = (K["FeO_O2_post"] * OpCh["m_O2_post"] * p["h_scrap"]
                / p["h_EAF_up"] * M["FeO"] / M["O"] * tan_hyp(y[ind["m_Fe_st_sol"]],50,1,10))

    # FeO production from CO2 and O2 from burners [Kg/s]
    iv["m_FeO_sum"] = iv["m_FeO_burn_O2"] + iv["m_FeO_burn_CO2"] + iv["m_FeO_post_O2"]

    # CO from CO2 + Fe = FeO + CO  [kg/s]
    x[9][10] = K_FeO_CO2 * (x[10][4] + x[10][6]) * M["CO"] / M["CO2"]

    # postcombustion O2 correction for FeO production
    x[12][5] = x[12][5] - iv["m_FeO_post_O2"] / M["FeO"] * M["O"]

    # residual O2 from burners  [Kg/s] correction for FeO production
    x[12][3] = x[12][3] * (1 - K_FeO_O2)

    x[12][11] = x[12][11] * (1 - K_FeO_O2)

    # CO2 from CH4 burners [Kg/s] correction for FeO production
    x[10][4] = x[10][4] * (1 - K_FeO_CO2)

    # CO2 from CH4 burners [Kg/s] correction for FeO production
    x[10][6] = x[10][6] * (1 - K_FeO_CO2)


    """%% coefficients, surfaces, arc Energy input, radiosity"""

    get_slag_height(X, p, M, V, c, y[ind["h_slag"]], x[9,2])

    slag_calculate = ((y[ind["m_sl_liq"]] / p["rho_sl_liq"])
                    / (np.pi * (p["r_EAF_up"] +
                    tan_hyp(p["h_melt"],p["h_EAF_low"],-100,1)
                    * (p["r_EAF_low"] + p["h_melt"] - p["r_EAF_up"])) ** 2))

    dy[ind["h_slag"]] = (slag_calculate + p["Vg"] * p["KS"]
                            * K["slag_height_fact"] * (slag_calculate / 0.1)
                            - y[ind["h_slag"]]) * 0.15


    K["melt_exposed"]= abs((0.5*np.tanh(5*(y[ind["h_slag"]]-p["h_scrap"]+p["h_hole"])+3*(p["r_hole"]-p["r_hole_init"])
                        /(p["r_EAF_low"]+p["h_melt"]-p["r_hole_init"]))+0.5)*(1-((y[ind["V_st_sol"]]/p["V_scrap_init"])**2)*tan_hyp((y[ind["m_st_sol"]]/y[ind["m_st_liq"]]),0.1,25,1)))
    
    K["melt_exposed"] = (K["melt_exposed"] * (resolid_correct + (1-resolid_correct) * tan_hyp(OpCh["P_arc"],5,5,1)))


    K["slag"] = (tan_hyp(p["h_EAF_up"] + p["h_EAF_low"] - p["h_melt"]
                - p["h_el"] - p["h_arc"],y[ind["h_slag"]],-20,1)
                * min(1,max(0,-(p["h_EAF_up"] + p["h_EAF_low"] - p["h_melt"]
                - p["h_el"] - p["h_arc"] - y[ind["h_slag"]]) / p["h_arc"])))

    K["slag"] = K["slag"] * (0.5 + 0.5 * tan_hyp(p["h_scrap"],0.5,-10,1))


    """Surfaces"""

    A = np.zeros(9)
    A2 = np.zeros(4)

    # Roof surface [m2]
    A[0] = np.pi * (p["r_EAF_up"] ** 2 - p["r_heart"] ** 2)

    # Wall surface [m2]
    A[1] = np.pi * 2 * p["r_EAF_up"] * p["h_wall"]

    # surface ring around hole
    A2[0] = np.pi * (p["r_EAF_up"] ** 2 - p["r_hole"] ** 2)

    # wall of hole
    A2[1] = 2 * np.pi * p["r_hole"] * p["h_hole"]

    # bottom surface of hole surrounding electrode
    A2[2] = (np.pi * (p["r_hole"] ** 2 - p["r_el_rep"] ** 2)
                * (1 - K["melt_exposed"]))

    # total scrap surface [m?]
    A[2] = sum(A2)

    # virtual surface between scrap and electrode
    A2[3] = np.pi *(p["r_hole"] ** 2 - p["r_el_rep"] ** 2)

    # liquid scrap surface [m2]
    A[3] = np.pi * p["r_hole"] ** 2

    # arc surface
    A[4] = 2 * np.pi * p["r_arc"] * p["h_arc"]

    # surface of upper part of electrode
    A[5] = 2 * np.pi * p["r_el_rep"] * p["h_el"] *(1 - p["el_tip_ratio"])

    # surface of uncooled wall segment
    A[6] = 2 * np.pi * p["r_EAF_up"] * p["h_wall_unc_exp"]

    # surface of hot electrode tip
    A[7] = (np.pi * p["r_el_rep"] ** 2 + 2 * np.pi * p["r_el_rep"]
            * p["h_el"] * p["el_tip_ratio"])

    #surface of roof heart
    A[8] = np.pi * (p["r_heart"] ** 2 - p["r_el_rep"] **2)

    """Energy distribution from electric arc"""

    #% Heat transfer from arc to steel (solid and liquid scrap) through conduction [MW]
    Q["arc"] = ((K["arc_power_distribution"][0] + K["slag"]
                * K["arc_power_distribution"][2] * K["slag_influence"])
                * OpCh["P_arc"])

    # Heat transfer from arc to gas [MW]
    Q["arc_gas"] = K["arc_power_distribution"][1] * OpCh["P_arc"]

    # Heat transfer from arc through radiation [MW]
    Q["arc_rad"] = (K["arc_power_distribution"][2] * OpCh["P_arc"]
                    * (1 - K["slag"] * K["slag_influence"]))

    # Dissipation in electrode [MW]
    Q["arc_diss"] = K["arc_power_distribution"][3] * OpCh["P_arc"]

    view_factor(p=p, K=K, iv=iv, A=A, A3=A2)
    radiosity(p=p, c=c, X=X, iv=iv, ind=ind, Q=Q, K=K, y=y, A=A)

    #  correcting melt surface with exposure coefficient
    A[3] = np.pi * p["r_hole"] ** 2 * K["melt_exposed"]
    #  correcting wall surface for uncooled wall segment
    A[1] = np.pi * 2 * p["r_EAF_up"] * p["h_wall"]
    # adding roof heart and uncooled wall (total uncooled refractory surface)
    A[6] = 2 * np.pi * p["r_EAF_up"] * p["h_wall_unc_exp"]
    # remove surface nine (was roof heart)
    A[8] = np.pi * (p["r_heart"] ** 2 - p["r_el_rep"] **2)

    A[1] = A[1] - A[6]

    A[6] = A[6] + A[8]

    #A = np.delete(A,8)

    # irradiation on all surfaces
    #G = (np.matmul(iv["VF"],(iv["J"] * iv["transm"]))
    #     + iv["eps_gas"] * c["sigma_sb"] * y[ind["T_gas"]] ** 4)
    G = (np.dot(iv["VF"],(iv["J"] * iv["transm"]))
        + iv["eps_gas"] * c["sigma_sb"] * y[ind["T_gas"]] ** 4)

    Q["rad"] = A[0:8] * (iv["J"] - G) / 1e6

    Q["gas_rad"] = (sum(((np.ones(8) * iv["eps_gas"]
                * c["sigma_sb"] * y[ind["T_gas"]] ** 4)
                - np.dot(iv["VF"],(iv["J"] * iv["abs_gas"])))*A[0:8])/1e6)
    
    Q["balance"] = sum(Q["rad"]) - Q["rad"][4] + Q["gas_rad"]


    """Radiative heat transfer"""

    # Heat transfer to roof (radiative) [MW]
    Q["roof_rad"]      = Q["rad"][0]
    # Heat transfer to wall (radiative) [MW]
    Q["wall_rad"]      = Q["rad"][1]
    # Heat transfer to solid scrap (radiative) [MW]
    Q["st_sol_rad"]    = Q["rad"][2] - (Q["arc_rad"] + Q["balance"])
    #Heat transfer to liquid scrap (radiative) [MW]
    Q["st_liq_rad"]    = Q["rad"][3]
    # calculated arc Radiation must be equal to Q_arc_RAD
    Q["arc_rad_test"]  = Q["rad"][4]
    # Heat transfer to electrode shaft (radiative) [MW]
    Q["el_shaft_rad"]  = Q["rad"][5]
    # Heat transfer to uncooled wall segment [MW]
    Q["wall_unc_rad"]  = Q["rad"][6]
    # Heat transfer to electrode tip (radiative) [MW]
    Q["el_tip_rad"]    = Q["rad"][7]
    # Heat transfer to complete electrode (radiative) [MW]
    Q["el_rad"] = Q["el_shaft_rad"] + Q["el_tip_rad"]


    """heat fluxes from chemical reactions"""

    # reaction_enthalpies;
    reaction_enthalpies(DH_T=DH_T, M=M, dh=dh, cp=cp, ind=ind, c=c, iv=iv, comp=comp, y=y, dy=dy, x=x, K_FeO_CO2=K_FeO_CO2)

    # heat transfer into gas from chemical reactions [MW]
    Q["gas_chem"] = ((DH_T["q"] + DH_T["r"] + DH_T["t"] + DH_T["z"]
                        + DH_T["o"] + DH_T["p"]) / 1e3)

    #Heat transfer from burners to steel(solid and liquid scrap) [MW]
    Q["burn_st_sol"] = DH_T["n"] * K["post"] /1e3

    # Heat transfer from burners to gas [MW]
    Q["burn_gas"] = DH_T["n"] * (1 - K["post"]) /1e3

    # Heat transfer from CO post combustion to steel(solid and liquid scrap) [MW]
    Q["CO_post"] = DH_T["h"] / 1e3

    # Heat transfer into liquid steel from chemical reactions and gasification of volatile PKS components [MW]
    Q["st_liq_chem"] = ((DH_T["a"] + DH_T["b"] + DH_T["c"] + DH_T["d"]
                    + DH_T["e"] + DH_T["f"] + DH_T["g"] + DH_T["i"]
                    + DH_T["j"] + DH_T["k"] + DH_T["l"] + DH_T["m"]
                    + DH_T["s"] + DH_T["u"] + DH_T["v"] + DH_T["y"]) / 1e3)

    # Heat transfer into liquid slag from FeO produced by burners [MW]
    Q["sl_liq_chem"] = ((DH_T["w"] + DH_T["x"]) / 1e3
                    * y[ind["T_st_sol"]] / c["T_melt_st"])

    # Heat transfer into solid scrap from FeO produced by burners [MW]
    Q["st_sol_chem"] = ((DH_T["w"] + DH_T["x"]) / 1e3
                    * (1-y[ind["T_st_sol"]] / c["T_melt_st"]))


    """Heat transfer between zones"""

    #  Volume of lSc
    V_st_sol_st_liq   = (p["h_melt"] / 3 * np.pi * (p["r_EAF_low"] ** 2
                    * (p["r_EAF_low"] + p["h_melt"]) * p["r_EAF_low"]
                    + (p["r_EAF_low"] + p["h_melt"] ) ** 2))

    # masses for calculation of contact area between zones

    # Smaller one between mass of solid scrap z(13) and liquid scrap z(14) [kg]
    m_st_sol_st_liq = min(V_st_sol_st_liq * p["rho_st_sol"], y[ind["m_st_sol"]])
    # Mass of solid scrap covered by melt
    m_st_sol_sl_liq = min(y[ind["m_st_sol"]],y[ind["m_sl_liq"]])
    # Smaller one between mass of solid scrap and solid slag [kg]
    m_st_sol_sl_sol = min(y[ind["m_st_sol"]],y[ind["m_sl_sol"]])
    # Smaller one between mass of liquid scrap and liquid slag [kg]
    m_st_liq_sl_liq = min(y[ind["m_st_liq"]],y[ind["m_sl_liq"]])
    # Smaller one between mass of liquid scrap and solid slag [kg]
    m_st_liq_sl_sol = min(y[ind["m_st_liq"]],y[ind["m_sl_sol"]])
    # Smaller one between mass of liquid slag and solid slag [kg]
    m_sl_liq_sl_sol = min(y[ind["m_sl_sol"]],y[ind["m_sl_liq"]])

    # heat transfers between zones

    #  Heat transfer between solid scrap and liquid scrap [MW]
    Q["st_sol_st_liq"] = (m_st_sol_st_liq * K["therm"][0] * K["area"][0]
                        * (y[ind["T_st_liq"]] -y[ind["T_st_sol"]]) / 1e3
                        * resolid_correct_st_sol)

    # Heat transfer between solid scrap and solid slag [MW]
    Q["st_sol_sl_sol"] = (m_st_sol_sl_sol * K["therm"][1] * K["area"][1]
                        * (y[ind["T_st_sol"]] - y[ind["T_sl_sol"]]) / 1e3)

    #  Heat transfer between solid scrap and liquid slag [MW]
    Q["st_sol_sl_liq"] = (m_st_sol_sl_liq * K["therm"][2] * K["area"][2]
                        * (y[ind["T_st_sol"]] -y[ind["T_sl_liq"]]) / 1e3
                        * resolid_correct)

    # Heat transfer between solid scrap and gas [MW]
    Q["st_sol_gas"] = (y[ind["V_st_sol"]]/p["V_scrap_init"]
                            * K["therm"][3] * (y[ind["T_st_sol"]]
                            - y[ind["T_gas"]]) / 1e3)*(1 - K["melt_exposed"])

    # Heat transfer between solid scrap and wall [MW]
    Q["st_sol_wall"] = (K["wall"][0] * (y[ind["T_st_sol"]]
                        - y[ind["T_wall"]]) * (1 - np.exp(-y[ind["V_st_sol"]]
                        / p["V_scrap_init"])) / 1e3)

    # Heat transfer between solid scrap and uncooled wall segment [MW]
    Q["st_sol_wall_unc"] = (K["therm"][9] * (y[ind["T_st_sol"]]
                            - y[ind["T_wall_unc"]]) * 2 * np.pi * p["r_EAF_up"]
                            * (p["h_wall_unc"]- p["h_wall_unc_exp"]) / 1e3)

    # Heat transfer between liquid scrap and solid slag [MW]
    Q["st_liq_sl_sol"]   = (m_st_liq_sl_sol * K["therm"][4] * K["area"][3]
                            * (y[ind["T_st_liq"]]-y[ind["T_sl_sol"]]) / 1e3
                            * (1 + K["slag"]) * resolid_correct)

    # Heat transfer between liquid scrap and liquid slag [MW]
    Q["st_liq_sl_liq"]   = (m_st_liq_sl_liq * K["therm"][5] * K["area"][4]
                            * (y[ind["T_st_liq"]] - y[ind["T_sl_liq"]])
                            / 1e3 * resolid_correct)

    # Heat transfer between liquid scrap and gas [MW]
    Q["st_liq_gas"]   = (K["therm"][6] * (y[ind["T_st_liq"]]
                    - y[ind["T_gas"]]) * (0.15 + 0.85
                    * K["melt_exposed"]) / 1e3 * resolid_correct)

    # Heat transfer from liquid scrap through vessel to surrounding air [MW]
    Q["st_liq_vessel"] = (K["alpha_vessel_air"] * p["A_vessel_out"]
                    * (p["T_vessel_out"] - c["T_ambient"])
                    / 1e6 * resolid_correct)

    # Heat tansfer from solid slag through vessel to surrounding air [MW]
    Q["sl_liq_vessel"] = 0

    # Heat transfer between liquid slag and solid slag [MW]
    Q["sl_liq_sl_sol"]   = (m_sl_liq_sl_sol * K["therm"][4] * K["area"][3]
                            * (y[ind["T_sl_liq"]] - y[ind["T_sl_sol"]]) / 1e3 * K["slag"] / 5)

    # Heat transfer between liquid slag and gas [MW]
    Q["sl_liq_gas"]   = (y[ind["m_sl_liq"]] * K["therm"][7]
                        * (y[ind["T_sl_liq"]] - y[ind["T_gas"]])
                        * K["melt_exposed"] / 1e3)

    # Heat tansfer from solid slag through vessel to surrounding air [MW]
    Q["sl_sol_vessel"] = 0

    # Heat transfer between gas and roof [MW]
    Q["gas_roof"] = (K["wall"][1] * (y[ind["T_gas"]] - y[ind["T_roof"]])
                        * A[0] / 1e3)

    # Heat transfer between gas and wall [MW]
    Q["gas_wall"] = (K["wall"][2] * (y[ind["T_gas"]] - y[ind["T_wall"]])
                        * A[1] / 1e3)

    # heat transfer between gas and uncooled wall segemnt [MW]
    Q["gas_wall_unc"] = (K["wall"][2] * (y[ind["T_gas"]]
                        - y[ind["T_wall_unc"]]) * A[6] /1e3)

    # Heat transfer into roof [MW]
    Q["roof_water"] = (np.pi* (p["r_EAF_up"]  ** 2 - p["r_heart"] ** 2)
                    * (y[ind["T_roof"]]-y[ind["T_roof_water"]])
                    * p["lambda_roof"] / p["pipe_thick"] / 1e6)

    # Heat transfer into wall [MW]
    Q["wall_water"] = (np.pi * 2 * p["r_EAF_up"] * (p["h_EAF_up"]
                    - p["h_EAF_low"]) * (y[ind["T_wall"]]
                    - y[ind["T_wall_water"]]) * p["lambda_wall"]
                    / (p["pipe_thick"] + p["slag_thick"]) / 1e6)

    # electrode cooling through water [MW]
    Q["el_cooling"] = (Q["el_cooling_heat"] / 1e3
                        + iv["m_steam"] * dh["H2O_100"])

    # heat transfer from electrode to gas [MW]
    Q["el_gas"]     = (K["therm"][8] * A[5] / (1 - p["el_tip_ratio"])
                        * (y[ind["T_gas"]] - y[ind["T_el"]]) / 1e3)

    # heat transfer between uncooled wall segment and outsider air [MW]
    Q["wall_unc_air"] = (K["alpha_vessel_air"] * (p["h_wall_unc"]* 2
                        * np.pi * (p["r_EAF_up"] + p["d_refrac"]) + np.pi
                        * (p["r_heart"] - p["r_el_rep"]))
                        * (p["T_vessel_out"] - c["T_ambient"]) / 1e6)

    #heat transfer between uncooled wall and melt [MW]
    Q["wall_unc_st_liq"] = ((y[ind["T_wall_unc"]]-y[ind["T_st_liq"]]) * 2
                            * np.pi * p["r_EAF_up"] * p["h_wall_unc"] ** 0.2
                            * K["therm"][10] / 1e3
                            * tan_hyp(p["h_wall_unc"],5e-3,5e3,1))


    """net heat transfers into zones"""

    # Heat transfer into roof [MW]
    Q["roof"] = (-Q["roof_rad"] + Q["gas_roof"] - Q["roof_water"])

    # Heat transfer into wall [MW]
    Q["wall"] = (-Q["wall_rad"] + Q["gas_wall"] - Q["wall_water"]
                + Q["st_sol_wall"])

    # Heat transfer to electrodes [MW]
    Q["el"] = (-Q["el_rad"] - Q["el_cooling"] + Q["el_gas"]
                + Q["arc_diss"])

    # Heat transfer in solid scrap zone [MW]
    Q["st_sol"] = ((Q["arc"] + Q["burn_st_sol"] + K["post"] * Q["CO_post"])
                * (1 - K["melt_exposed"]) + Q["st_sol_st_liq"]
                - Q["st_sol_sl_liq"] - Q["st_sol_sl_sol"] - Q["st_sol_gas"]
                - Q["st_sol_wall"] - Q["st_sol_rad"] + Q["st_sol_chem"]
                - Q["st_sol_wall_unc"])

    # Heat transfer in liquid scrap zone [MW]
    Q["st_liq"] = ((Q["arc"] + Q["burn_st_sol"] + K["post"] * Q["CO_post"])
                * K["melt_exposed"] + Q["st_liq_chem"] - Q["st_sol_st_liq"]
                - Q["st_liq_sl_sol"] - Q["st_liq_sl_liq"] - Q["st_liq_gas"]
                - Q["st_liq_vessel"] - Q["st_liq_rad"] + Q["wall_unc_st_liq"])

    # Heat transfer in solid slag zone [MW]
    Q["sl_sol"] = (Q["st_sol_sl_sol"] + Q["st_liq_sl_sol"]
                    - Q["sl_sol_vessel"] + Q["sl_liq_sl_sol"])

    # Heat transfer in solid slag zone [MW]
    Q["sl_liq"] = (Q["st_liq_sl_liq"] + Q["st_sol_sl_liq"] - Q["sl_liq_gas"]
                    - Q["sl_liq_vessel"] - Q["sl_liq_sl_sol"] + Q["sl_liq_chem"])

    # Heat transfer in gas zone [MW]
    Q["gas"] = (-Q["gas_rad"] + Q["gas_chem"] + Q["arc_gas"]
                + (1 - K["post"]) * Q["CO_post"] - Q["gas_wall_unc"]
                + Q["burn_gas"] + Q["st_sol_gas"] + Q["st_liq_gas"]
                + Q["sl_liq_gas"]- Q["gas_wall"] - Q["gas_roof"]
                - Q["el_gas"])

    # Heat transfer to water cooling [MW]
    Q["water"] = Q["roof_water"] + Q["wall_water"]

    # Heat transfer through vessel, uncooled wall and roof heart surrounding air [MW]
    Q["vessel"] = (Q["st_liq_vessel"] + Q["sl_liq_vessel"]
                    + Q["sl_sol_vessel"] + Q["wall_unc_air"])
    # heat transfer into uncooled wall segment [MW]
    Q["wall_unc"] = (-Q["wall_unc_rad"] + Q["gas_wall_unc"]
                        + Q["st_sol_wall_unc"] - Q["wall_unc_air"]
                        - Q["wall_unc_st_liq"])

    """solidification of lSc and lSl, division of transferred heat for melting and heating of sSc and sSl"""

    # negative energy which has to be compensated through solidification
    #Q["resolid_st"]  = (tan_hyp(y[ind["T_st_liq"]],T_melt_st
    #                    - c["T_undercool"],-0.05,4) * Q["st_liq"] * tan_hyp(Q["st_liq"] *1e3,-0.5,-5,2))
    Q["resolid_st"]  = ( tan_hyp(y[ind["T_st_liq"]],T_melt_st,-0.05,4) *
                          -(T_melt_st - y[ind["T_st_liq"]]) * cp["st_liq"]/M["Fe"] *y[ind["m_st_liq"]])/1000/10


    # positive mass change: amount of mass which solidifies
    m_resolid_st      = (-(Q["resolid_st"] * 1e3 * M["Fe"])
                            / (c["lambda_st"] - cp["st_sol"]
                            * (y[ind["T_st_sol"]]-y[ind["T_st_liq"]])))


    heat_division_slag = (y[ind["T_sl_sol"]] / c["T_melt_sl"]) ** 0.25

    Q_scrap_focused  =(max((Q["arc"] + Q["burn_st_sol"] + K["post"] * Q["CO_post"])
                    * (1 - K["melt_exposed"]) - Q["st_sol_rad"] + Q["st_sol_chem"],0))

    Q_scrap_diffuse = (max(K["post"] * Q["CO_post"] * (1 - K["melt_exposed"])
                + Q["st_sol_st_liq"] - Q["st_sol_sl_liq"] - Q["st_sol_gas"],0))

    heat_division_scrap= ((Q_scrap_focused + Q_scrap_diffuse 
                        * (y[ind["T_st_sol"]] / c["T_melt_st"]) ** 0.15)
                            /(Q_scrap_focused + Q_scrap_diffuse + 1E-9))

    Q["st_liq"] = (Q["st_liq"] + Q["st_sol"]
                * (1 - tan_hyp(y[ind["m_st_sol"]],2.5,50,1)))

    Q["st_sol"] = Q["st_sol"] * tan_hyp(y[ind["m_st_sol"]],2.5,50,1)

    Q["sl_liq"]  = (Q["sl_liq"] + Q["sl_sol"]
                * (1 - tan_hyp(y[ind["m_sl_sol"]],0.1,100,1)))

    Q["sl_sol"] = Q["sl_sol"] * tan_hyp(y[ind["m_sl_sol"]],0.5,100,1)
    
    m_dri = sum(OpCh["mass_dri_inj"])
    Q["dri"] = m_dri * OpCh["T_dri"] * cp["st_sol"] / M["Fe"] / 1e3

    heat_division_scrap = heat_division_scrap * tan_hyp(Q["st_sol"],0,100000,1)
    heat_division_slag = heat_division_slag * tan_hyp(Q["sl_sol"],0,100000,1)

    """Rate of temperature and mass changes and relative pressure"""

    # Roof (inside) temperature change rate [K/s]
    dy[ind["T_roof"]] = ((Q["roof"] * 2e6 / (p["m_roof"] *cp["roof"] * 1e3)
                            +(OpCh["m_water_roof_roc"] * (y[ind["T_roof"]]
                            - y[ind["T_roof_water"]]) * roof_constants
                            / OpCh["m_water_roof"] - OpCh["T_water_roof_roc"])
                            /(1 + roof_constants)) / (1 + roof_constants
                            / (1 + roof_constants)))

    # Roof (outside and cooling water) temperature change rate [K/s]
    dy[ind["T_roof_water"]]  = ((OpCh["T_water_roof_roc"] + roof_constants
                            * (dy[ind["T_roof"]] - OpCh["m_water_roof_roc"]
                            *(y[ind["T_roof"]] - y[ind["T_roof_water"]])
                            /OpCh["m_water_roof"])) / (1 + roof_constants))

    # Wall (inside) temperature change rate [K/s]
    dy[ind["T_wall"]] = ((Q["wall"] * 2e6 / (p["m_wall"] *cp["wall"] * 1e3)
                            +(OpCh["m_water_wall_roc"] * (y[ind["T_wall"]]
                            - y[ind["T_wall_water"]]) * wall_constants
                            / OpCh["m_water_wall"] - OpCh["T_water_wall_roc"])
                            /(1 + wall_constants)) / (1 + wall_constants
                            / (1 + wall_constants)))

    # Wall (outside and cooling water) temperature change rate [K/s]
    dy[ind["T_wall_water"]]  = ((OpCh["T_water_wall_roc"] + wall_constants
                            * (dy[ind["T_wall"]] - OpCh["m_water_wall_roc"]
                            *(y[ind["T_wall"]] - y[ind["T_wall_water"]])
                            /OpCh["m_water_wall"])) / (1 + wall_constants))

    # temperature of uncooled wall segment
    dy[ind["T_wall_unc"]] = (2e3 * Q["wall_unc"]
                            / ((p["m_wall_unc"] + 1) * cp["wall"]))

    # Electrode temperature change rate [K/s]
    dy[ind["T_el"]] = Q["el"] * 1e3 / (y[ind["m_el"]] * cp["C"] / M["C"])

    # rate at with solid scrap is melting 
    melt_rate_scrap = (Q["st_sol"] * heat_division_scrap) / (c["lambda_st"] + cp["st_sol"] * (c["T_melt_st"] -y[ind["T_st_sol"]])) *1e3 * M["Fe"]

    # Solid scrap mass transfer rate [kg/s]
    dy[ind["m_st_sol"]]   = (- melt_rate_scrap
                             + m_resolid_st 
                             - iv["m_FeO_sum"] * M["Fe"] / M["FeO"]  #/ (y[ind["m_Fe_st_sol"]] / y[ind["m_st_sol"]])*(y[ind["m_st_sol"]] - y[ind["m_resolid"]])/ y[ind["m_st_sol"]]
                             #+ y[ind["m_resolid"]] / y[ind["m_st_sol"]]
                             + OpCh["m_dust"] * comp["dust_C"]
                             + OpCh["mass_dri_inj"][ind["comp_O"]] + OpCh["mass_dri_inj"][ind["comp_C"]] + OpCh["mass_dri_inj"][ind["comp_Si"]] 
                             + OpCh["mass_dri_inj"][ind["comp_Mn"]] + OpCh["mass_dri_inj"][ind["comp_Cr"]] + OpCh["mass_dri_inj"][ind["comp_P"]] 
                             + OpCh["mass_dri_inj"][ind["comp_S"]] + OpCh["mass_dri_inj"][ind["comp_Fe"]])
                             #+ OpCh["m_dri"] * (comp["dri_O"] + comp["dri_C"] + comp["dri_Si"] 
                             #                   + comp["dri_Mn"] + comp["dri_Cr"] + comp["dri_P"]
                             #                   + comp["dri_S"] + comp["dri_Fe"]))   

    # Mass transfer to liquid steel [kg/s]
    m_st_sol_st_liq = ( melt_rate_scrap
                       * (y[ind["m_O_st_sol"]] + y[ind["m_C_st_sol"]]
                          + y[ind["m_Si_st_sol"]] + y[ind["m_Mn_st_sol"]]
                          + y[ind["m_Cr_st_sol"]] + y[ind["m_P_st_sol"]]
                          + y[ind["m_Fe_st_sol"]] + y[ind["m_S_st_sol"]]) / y[ind["m_st_sol"]]
                          - m_resolid_st)

    # amount of substance/moles transfered to from lSc to lSl (negative)
    #m_st_sol_sl_liq = ( melt_rate_scrap
    #                   * (y[ind["m_FeO_st_sol"]] + y[ind["m_CaO_st_sol"]]
    #                      + y[ind["m_MgO_st_sol"]] + y[ind["m_Al2O3_st_sol"]])
    #                      / y[ind["m_st_sol"]] + iv["m_FeO_sum"])
    #m_st_sol_sl_liq = iv["m_FeO_sum"]

    sum_m_st_sl = (sum(x[3,:]) + sum(x[4,:]) + sum(x[5,:]) + sum(x[6,:])
                + sum(x[7,:]) - x[7,4] + sum(x[8,:])- x[8,8] / M["O"] * M["S"])

    # Liquid scrap mass transfer rate [kg/s]
    dy[ind["m_st_liq"]]  = (m_st_sol_st_liq + sum(x[2,:]) + sum_m_st_sl)  # + x[8,1] + x[8,9])


    melt_rate_slag = Q["sl_sol"] * heat_division_slag / (c["lambda_sl"] + cp["sl_sol"] * (c["T_melt_sl"] - y[ind["T_sl_sol"]])) * 1e3 * M["sl_sol"]
    # Solid slag mass change rate [kg/s]
    dy[ind["m_sl_sol"]]  = (- melt_rate_slag 
                            + OpCh["m_dust"] * (comp["dust_CaO"] + comp["dust_MgO"] + comp["dust_Al2O3"] + comp["dust_SiO2"] + comp["dust_MnO"]
                            + comp["dust_Cr2O3"] + comp["dust_P2O5"] + comp["dust_CaS"] + comp["dust_FeO"])
                            + OpCh["mass_slagformer_inj"][ind["comp_CaO"]] + OpCh["mass_slagformer_inj"][ind["comp_MgO"]] + OpCh["mass_slagformer_inj"][ind["comp_Al2O3"]]
                            + OpCh["mass_slagformer_inj"][ind["comp_SiO2"]] + OpCh["mass_slagformer_inj"][ind["comp_MnO"]] + OpCh["mass_slagformer_inj"][ind["comp_Cr2O3"]]
                            + OpCh["mass_slagformer_inj"][ind["comp_P2O5"]] + OpCh["mass_slagformer_inj"][ind["comp_CaS"]] + OpCh["mass_slagformer_inj"][ind["comp_FeO"]])

    # Mass transfer to liquid slag
    sum_m_sl = (-(M["SiO2"] / M["Si"]) * sum(x[3,:]) 
                - (M["MnO"] / M["Mn"]) * sum(x[4,:])
                - (M["Cr2O3"] / (2 * M["Cr"])) * sum(x[5,:])
                - (M["P2O5"] / (2 * M["P"])) * sum(x[6,:]) 
                - M["FeO"] / M["Fe"] * (sum(x[7,:])- x[7,4]) 
                + x[8,8] / M["O"] * (M["S"] - M["O2"]/2))


    # Liquid slag mass transfer rate [kg/s]
    dy[ind["m_sl_liq"]]  = (melt_rate_slag + sum_m_sl + m_refrac_wear + iv["m_FeO_sum"])


    # mass change of injected lime as part of solid slag [kg/s]  # dy[ind["m_st_sol"]]
    dy[ind["m_resolid"]] = (m_resolid_st - melt_rate_scrap * y[ind["m_resolid"]] / y[ind["m_st_sol"]])  #dy[ind["m_st_sol]?
                            

    #melt_rate_scrap = dy[ind["m_st_sol"]] - m_resolid_st
    '''
    dy[ind["m_FeO_st_sol"]] = (melt_rate_scrap * y[ind["m_FeO_st_sol"]]
                                / y[ind["m_st_sol"]])
    dy[ind["m_CaO_st_sol"]] = (melt_rate_scrap * y[ind["m_CaO_st_sol"]]
                                / y[ind["m_st_sol"]])
    dy[ind["m_MgO_st_sol"]] = (melt_rate_scrap * y[ind["m_MgO_st_sol"]]
                                / y[ind["m_st_sol"]])
    dy[ind["m_Al2O3_st_sol"]] = (melt_rate_scrap * y[ind["m_Al2O3_st_sol"]]
                                / y[ind["m_st_sol"]])
    '''
    dy[ind["m_O_st_sol"]] = (- melt_rate_scrap * y[ind["m_O_st_sol"]] / y[ind["m_st_sol"]]
                             + m_resolid_st * y[ind["m_O_st_liq"]] / y[ind["m_st_liq"]]
                             + OpCh["mass_dri_inj"][ind["comp_O"]]) #+ OpCh["m_dri"] * comp["dri_O"])
    dy[ind["m_C_st_sol"]] = (- melt_rate_scrap * y[ind["m_C_st_sol"]] / y[ind["m_st_sol"]]
                             + m_resolid_st * y[ind["m_C_st_liq"]] / y[ind["m_st_liq"]]
                             + OpCh["mass_dri_inj"][ind["comp_C"]] #+ OpCh["m_dri"] * comp["dri_C"]
                             + OpCh["m_dust"] * comp["dust_C"])
    dy[ind["m_Si_st_sol"]] = (- melt_rate_scrap * y[ind["m_Si_st_sol"]] / y[ind["m_st_sol"]]
                              + m_resolid_st * y[ind["m_Si_st_liq"]] / y[ind["m_st_liq"]]
                              + OpCh["mass_dri_inj"][ind["comp_Si"]]) #+ OpCh["m_dri"] * comp["dri_Si"])
    dy[ind["m_Mn_st_sol"]] = (- melt_rate_scrap * y[ind["m_Mn_st_sol"]] / y[ind["m_st_sol"]]
                              + m_resolid_st * y[ind["m_Mn_st_liq"]] / y[ind["m_st_liq"]]
                              + OpCh["mass_dri_inj"][ind["comp_Mn"]]) #+ OpCh["m_dri"] * comp["dri_Mn"])
    dy[ind["m_Cr_st_sol"]] = (- melt_rate_scrap * y[ind["m_Cr_st_sol"]] / y[ind["m_st_sol"]]
                              + m_resolid_st * y[ind["m_Cr_st_liq"]] / y[ind["m_st_liq"]]
                              + OpCh["mass_dri_inj"][ind["comp_Cr"]] )#+ OpCh["m_dri"] * comp["dri_Cr"])
    dy[ind["m_P_st_sol"]] = (- melt_rate_scrap * y[ind["m_P_st_sol"]] / y[ind["m_st_sol"]]
                             + m_resolid_st * y[ind["m_P_st_liq"]] / y[ind["m_st_liq"]]
                             + OpCh["mass_dri_inj"][ind["comp_P"]] )#+ OpCh["m_dri"] * comp["dri_P"])
    dy[ind["m_Fe_st_sol"]] = (- melt_rate_scrap * y[ind["m_Fe_st_sol"]] / y[ind["m_st_sol"]]
                              + m_resolid_st * y[ind["m_Fe_st_liq"]] / y[ind["m_st_liq"]]
                              + OpCh["mass_dri_inj"][ind["comp_Fe"]] #+ OpCh["m_dri"] * comp["dri_Fe"]
                              - iv["m_FeO_sum"] * M["Fe"] / M["FeO"])
    dy[ind["m_S_st_sol"]] = (- melt_rate_scrap * y[ind["m_S_st_sol"]] / y[ind["m_st_sol"]]
                             + m_resolid_st * y[ind["m_S_st_liq"]] / y[ind["m_st_liq"]]
                             + OpCh["mass_dri_inj"][ind["comp_S"]] )#+ OpCh["m_dri"] * comp["dri_S"])

    # change rate of species in liquid scrap

    # Mass of Dissolved O in the bath
    dy[ind["m_O_st_liq"]] = (sum(x[8,:])
                             + melt_rate_scrap * y[ind["m_O_st_sol"]] / y[ind["m_st_sol"]]
                             - m_resolid_st * y[ind["m_O_st_liq"]] / y[ind["m_st_liq"]])
    # Mass of Dissolved C in the bath
    dy[ind["m_C_st_liq"]] = (sum(x[2,:])
                             + melt_rate_scrap * y[ind["m_C_st_sol"]] / y[ind["m_st_sol"]]
                             - m_resolid_st * y[ind["m_C_st_liq"]] / y[ind["m_st_liq"]])
    # Mass of Dissolved Si
    dy[ind["m_Si_st_liq"]] = (sum(x[3,:])
                              + melt_rate_scrap * y[ind["m_Si_st_sol"]] / y[ind["m_st_sol"]]
                              - m_resolid_st * y[ind["m_Si_st_liq"]] / y[ind["m_st_liq"]])
    # Mass of Dissolved Mn
    dy[ind["m_Mn_st_liq"]] = (sum(x[4,:])
                              + melt_rate_scrap * y[ind["m_Mn_st_sol"]] / y[ind["m_st_sol"]]
                              - m_resolid_st * y[ind["m_Mn_st_liq"]] / y[ind["m_st_liq"]])
    # Mass of Dissolved Cr
    dy[ind["m_Cr_st_liq"]] = (sum(x[5,:])
                              + melt_rate_scrap * y[ind["m_Cr_st_sol"]] / y[ind["m_st_sol"]]
                              - m_resolid_st * y[ind["m_Cr_st_liq"]] / y[ind["m_st_liq"]])
    # Mass of Dissolved P
    dy[ind["m_P_st_liq"]] = (sum(x[6,:])
                             + melt_rate_scrap * y[ind["m_P_st_sol"]] / y[ind["m_st_sol"]]
                             - m_resolid_st * y[ind["m_P_st_liq"]] / y[ind["m_st_liq"]])
    # Mass of dissolved S
    dy[ind["m_S_st_liq"]] = (-x[8,8] * M["S"] / M["O"]
                             + melt_rate_scrap * y[ind["m_S_st_sol"]] / y[ind["m_st_sol"]]
                             - m_resolid_st * y[ind["m_S_st_liq"]] / y[ind["m_st_liq"]])
    # Mass of Iron Fe
    dy[ind["m_Fe_st_liq"]] = (sum(x[7,:])
                              + melt_rate_scrap * y[ind["m_Fe_st_sol"]] / y[ind["m_st_sol"]]
                              - m_resolid_st * y[ind["m_Fe_st_liq"]] / y[ind["m_st_liq"]])
    
    
    dy[ind["m_CaO_sl_sol"]] = (- melt_rate_slag * y[ind["m_CaO_sl_sol"]] / y[ind["m_sl_sol"]] 
                               + OpCh["mass_slagformer_inj"][ind["comp_CaO"]]
                               + OpCh["m_dust"] * comp["dust_CaO"])
    dy[ind["m_MgO_sl_sol"]] = (- melt_rate_slag * y[ind["m_MgO_sl_sol"]] / y[ind["m_sl_sol"]] 
                               + OpCh["mass_slagformer_inj"][ind["comp_MgO"]]
                               + OpCh["m_dust"] * comp["dust_MgO"])
    dy[ind["m_Al2O3_sl_sol"]] = (- melt_rate_slag * y[ind["m_Al2O3_sl_sol"]] / y[ind["m_sl_sol"]]
                                + OpCh["mass_slagformer_inj"][ind["comp_Al2O3"]]
                                + OpCh["m_dust"] * comp["dust_Al2O3"])
    dy[ind["m_SiO2_sl_sol"]] = (- melt_rate_slag * y[ind["m_SiO2_sl_sol"]] / y[ind["m_sl_sol"]]
                                + OpCh["mass_slagformer_inj"][ind["comp_SiO2"]]
                                + OpCh["m_dust"] * comp["dust_SiO2"])
    dy[ind["m_MnO_sl_sol"]] = (- melt_rate_slag * y[ind["m_MnO_sl_sol"]] / y[ind["m_sl_sol"]]
                                + OpCh["mass_slagformer_inj"][ind["comp_MnO"]]
                                + OpCh["m_dust"] * comp["dust_MnO"])
    dy[ind["m_Cr2O3_sl_sol"]] = (- melt_rate_slag * y[ind["m_Cr2O3_sl_sol"]] / y[ind["m_sl_sol"]]
                                + OpCh["mass_slagformer_inj"][ind["comp_Cr2O3"]]
                                + OpCh["m_dust"] * comp["dust_Cr2O3"])
    dy[ind["m_P2O5_sl_sol"]] = (- melt_rate_slag * y[ind["m_P2O5_sl_sol"]] / y[ind["m_sl_sol"]]
                                + OpCh["mass_slagformer_inj"][ind["comp_P2O5"]]
                                + OpCh["m_dust"] * comp["dust_P2O5"])
    dy[ind["m_CaS_sl_sol"]] = (- melt_rate_slag * y[ind["m_CaS_sl_sol"]] / y[ind["m_sl_sol"]]
                               + OpCh["mass_slagformer_inj"][ind["comp_CaS"]]
                               + OpCh["m_dust"] * comp["dust_CaS"])
    dy[ind["m_FeO_sl_sol"]] = (- melt_rate_slag * y[ind["m_FeO_sl_sol"]] / y[ind["m_sl_sol"]]
                               + OpCh["mass_slagformer_inj"][ind["comp_FeO"]]
                               + OpCh["m_dust"] * comp["dust_FeO"])

    # change rates of species in liquid slag

    # Rate of change of CaO
    dy[ind["m_CaO_sl_liq"]] = (melt_rate_slag * y[ind["m_CaO_sl_sol"]]
                            / y[ind["m_sl_sol"]] - x[8,8] / M["O"]
                            * M["CaO"] + m_refrac_wear * comp["refrac_CaO"])
                            #- melt_rate_scrap * y[ind["m_CaO_st_sol"]]
                            #  / y[ind["m_st_sol"]])
    # Rate of change of MgO
    dy[ind["m_MgO_sl_liq"]] = (melt_rate_slag * y[ind["m_MgO_sl_sol"]]
                            / y[ind["m_sl_sol"]] + m_refrac_wear
                            * comp["refrac_MgO"])# - melt_rate_scrap
                            #* y[ind["m_MgO_st_sol"]]/ y[ind["m_st_sol"]])
    # Rate of change of Al2O3
    dy[ind["m_Al2O3_sl_liq"]] = (melt_rate_slag * y[ind["m_Al2O3_sl_sol"]]
                            / y[ind["m_sl_sol"]])# - melt_rate_scrap
                            #* y[ind["m_Al2O3_st_sol"]] / y[ind["m_st_sol"]])
    # Rate of change of siliconoxide (SiO2)
    dy[ind["m_SiO2_sl_liq"]] = (melt_rate_slag * y[ind["m_SiO2_sl_sol"]]
                            / y[ind["m_sl_sol"]] - sum(x[3,:]) / M["Si"]
                            * M["SiO2"])# - melt_rate_scrap
                            #* y[ind["m_SiO2_st_sol"]] / y[ind["m_st_sol"]])
    #  Rate of change of manganese oxide (MnO)
    dy[ind["m_MnO_sl_liq"]] = (melt_rate_slag * y[ind["m_MnO_sl_sol"]]
                            / y[ind["m_sl_sol"]] - sum(x[4,:]) / M["Mn"]
                            * M["MnO"])
    #  Rate of change of chromium oxide (Cr2O3)
    dy[ind["m_Cr2O3_sl_liq"]] = (melt_rate_slag * y[ind["m_Cr2O3_sl_sol"]]
                            / y[ind["m_sl_sol"]] - sum(x[5,:]) / 2 / M["Cr"]
                            * M["Cr2O3"])
    # Rate of change of phosphorus oxide (P2O5)
    dy[ind["m_P2O5_sl_liq"]] = (melt_rate_slag * y[ind["m_P2O5_sl_sol"]]
                            / y[ind["m_sl_sol"]] - sum(x[6,:]) / 2 / M["P"]
                            * M["P2O5"])
    dy[ind["m_CaS_sl_liq"]] = (melt_rate_slag * y[ind["m_CaS_sl_sol"]]
                            / y[ind["m_sl_sol"]] + x[8,8] / M["O"]
                            * M["CaS"])
    # Rate of change of iron oxide (FeO)
    dy[ind["m_FeO_sl_liq"]] = (melt_rate_slag * y[ind["m_FeO_sl_sol"]]
                            / y[ind["m_sl_sol"]] - M["FeO"] / M["Fe"]
                            * (sum(x[7,:]) - x[7,4]) + iv["m_FeO_sum"])
                            # - melt_rate_scrap * y[ind["m_FeO_st_sol"]] / y[ind["m_st_sol"]]

    # gas mass change rates

    # Rate of change of CO
    dy[ind["m_CO"]]  = sum(x[9,:])
    # Rate of change of CO2
    dy[ind["m_CO2"]] = sum(x[10,:])
    # Rate of change of N2
    dy[ind["m_N2"]]  = sum(x[11,:])
    # Rate of change of O2
    dy[ind["m_O2"]]  = sum(x[12,:])
    # Rate of change of H2
    dy[ind["m_H2"]]  = sum(x[13,:])
    # Rate of change of H2O
    dy[ind["m_H2O"]] = sum(x[14,:])
    # Rate of change of CH4
    dy[ind["m_CH4"]] = sum(x[15,:])
    # total gas mass change
    dy[ind["m_gas"]] = (dy[ind["m_CO"]] + dy[ind["m_CO2"]] + dy[ind["m_N2"]]
                            + dy[ind["m_O2"]] + dy[ind["m_H2"]] +dy[ind["m_H2O"]]
                            + dy[ind["m_CH4"]])

    # change of solid scrap bulk volume
    dy[ind["V_st_sol"]] = (- melt_rate_scrap / (y[ind["m_st_sol"]] / y[ind["V_st_sol"]])
                           + m_resolid_st / p["rho_st_liq"]
                           - iv["m_FeO_sum"] * M["Fe"] / M["FeO"] / (y[ind["m_st_sol"]] / y[ind["V_st_sol"]])
                           + m_dri / p["rho_st_sol"]
                           + OpCh["m_dust"] / p["rho_st_sol"]
                           - ((y[ind["T_st_sol"]] / c["T_melt_st"]) * y[ind["m_st_liq"]]
                              / (y[ind["m_st_liq"]] + y[ind["m_st_sol"]]))
                              * tan_hyp(min([y[ind["V_st_sol"]],p["rho_st_liq"]-50-p["rho_st_sol"]]),1,10,1)
                              * 0.25 * tan_hyp(Q["st_sol"]/40,1,0.25,1))

    # Gas Volume in EAF [m]; is conctant as the scraps bulk volume has gas integrated;
    # solid scrap has the same density as liquid steel melt, only the bulk volume with gas included is different

    # values for the mass balance
    eval["CO_extraction"] = - x[9][1] - x[9][4]
    eval["CO2_extraction"] = - x[10][1] - x[10][7]
    eval["N2_extraction"] = - x[11][1] - x[11][3]
    eval["O2_extraction"] = - x[12][1] - x[12][7]
    eval["H2_extraction"] = - x[13][1] - x[13][2]
    eval["H2O_extraction"] = - x[14][1] - x[14][2]
    eval["CH4_extraction"] = - x[15][1] - x[15][2]
    eval["air_extraction"] =  x[11][2] + x[12][2]
    eval["refractory_material"] = - m_refrac_wear
    eval["a"] = x[7][1] + x[7][2]
    eval["b"] = x[7][3]
    eval["c"] = x[3][1] + x[3][2]
    eval["d"] = x[3][3]
    eval["e"] = x[4][1] + x[4][2]
    eval["f"] = x[4][3]
    eval["g"] = x[1][1] + x[2][1] + x[2][2]
    eval["h"] = x[9][3]
    eval["i"] = x[2][5]
    eval["j"] = x[5][1]+x[5][2]
    eval["k"] = x[5][3]
    eval["l"] = x[6][1]+x[6][2]
    eval["m"] = x[6][3]
    eval["n_1"] = x[15][4]
    eval["n_2"] = x[15][5]+x[15][8]
    eval["n_3"] = x[15][6]
    eval["q"] = x[9][5]
    eval["r"] = x[10][5]
    eval["s"] = x[14][8]
    eval["t"] = x[13][4]
    eval["u"] = x[8][9]
    eval["v"] = x[10][9]
    eval["w"] = x[12][3]
    eval["x"] = x[10][4] + x[10][6]
    eval["z"] = x[14][5]
    eval["m_resolid_st"] = m_resolid_st


    """Mixing enthalpies"""
    # all species assumed at current zone temperatures for chemical reactions,
    # so temperature changes when changing between zones already accounted for
    # in reaction enthalpies, here only heating of gases from external sources
    # and mixing at constant temperatures correcting for different heat
    # capacities + solidification

    # heating of injected coal to melt temperature (only fraction that takes part in reactions)
    QM["C_inj"] = (iv["C_inj_av"]  * (1 - K_C_inj_char)
                * ( c["T_ambient"]-y[ind["T_st_liq"]]) * cp["C"] / M["C"])

    # heating of charged coal dissolving to melt temperature
    QM["C_diss"] = (-x[1,3] * (c["T_ambient"] - y[ind["T_st_liq"]])
                    * cp["C"] / M["C"])

    # heating of lanced oxygen (including residual N2) to melt temperature
    QM["O2_lance"] = (OpCh["m_O2_lance"] * (c["T_ambient"] - y[ind["T_st_liq"]])
                        * (cp["O2"] / M["O2"] + cp["N2"] / M["N2"]
                        * comp["O2_N2"] / comp["O2_O2"]))

    # heating of postcombustion oxygen (including residual N2) to gas temperature
    QM["O2_post"] = (OpCh["m_O2_post"] * (c["T_ambient"] - y[ind["T_gas"]])
                    * (cp["O2"] / M["O2"] + cp["N2"] / M["N2"]
                        * comp["O2_N2"] / comp["O2_O2"]))

    # heating of burner gases (including residual N2) to gas temperature
    QM["burner"] = ((OpCh["m_CH4_burn"] * (cp["CH4"] / M["CH4"] + cp["N2"]
                    / M["N2"] * comp["gas_N2"] / comp["gas_CH4"])
                    + OpCh["m_O2_CH4_burn"] * (cp["O2"] / M["O2"] + cp["N2"]
                    / M["N2"] * comp["O2_N2"] / comp["O2_O2"])
                    + OpCh["m_H2_burn"] * (cp["H2"] / M["H2"] + cp["N2"]
                    / M["N2"] * comp["gas_N2"] / comp["gas_H2"])
                    + OpCh["m_O2_H2_burn"] * (cp["O2"] / M["O2"] + cp["N2"]
                    / M["N2"] * comp["O2_N2"] / comp["O2_O2"]))
                    * (c["T_ambient"] - y[ind["T_gas"]]))

    # heating of leak air to gas temperature
    QM["leak_air"] = (-m_leak_air * (comp["air_O2"] * cp["O2"] / M["O2"]
                    + comp["air_N2"] * cp["N2"] / M["N2"])
                        * (y[ind["T_gas"]]-c["T_ambient"]))

    # heating and evaporation of water from electrode cooling
    QM["leak_H2O"] = (-iv["m_water_in"] * (dh["H2O_100"] * 1e3
                    + cp["H2O_gas"] / M["H2O"] * (y[ind["T_gas"]] - 373.15)))

    # heating of conveyor gas (air) to gas temperature
    QM["conv_gas"] = (-OpCh["m_conv_gas"] * (comp["air_O2"] * cp["O2"]
                    / M["O2"] + comp["air_N2"] * cp["N2"] / M["N2"])
                    * (y[ind["T_gas"]]-c["T_ambient"]))

    # addition of solidified Fe from melt to scrap zone
    QM["resolid"] = (m_resolid_st * (cp["st_liq"] - cp["st_sol"]) / M["Fe"]
                    * (y[ind["T_st_sol"]] - c["T_ref"]))

    # addition of slagformers to slag zone
    QM["slag_form"] = ((c["T_ambient"] - y[ind["T_sl_sol"]]) *
                        OpCh["m_slag_form"] * cp["sl_sol"] / M["sl_sol"])
    
    # addition of dust to slag zone except C content
    QM["dust"] = ((c["T_ambient"] - y[ind["T_sl_sol"]]) * cp["sl_sol"] / M["sl_sol"] *
                        OpCh["m_dust"] * (comp["dust_FeO"] + comp["dust_SiO2"]))
    
    # addition of DRI to scrap zone + C content from dust injection
    QM["dri"] = ((c["T_ambient"] - y[ind["T_st_sol"]]) * cp["st_sol"] / M["Fe"] *
                        (OpCh["m_dust"] * comp["dust_C"] 
                         + OpCh["mass_dri_inj"][ind["comp_O"]] + OpCh["mass_dri_inj"][ind["comp_C"]] + OpCh["mass_dri_inj"][ind["comp_Si"]] 
                         + OpCh["mass_dri_inj"][ind["comp_Mn"]] + OpCh["mass_dri_inj"][ind["comp_Cr"]] + OpCh["mass_dri_inj"][ind["comp_P"]] 
                         + OpCh["mass_dri_inj"][ind["comp_S"]] + OpCh["mass_dri_inj"][ind["comp_Fe"]]))
                         #+ OpCh["m_dri"] * ((comp["dri_O"] + comp["dri_C"] + comp["dri_Si"]
                         #                 + comp["dri_Mn"] + comp["dri_Cr"] + comp["dri_P"]
                         #                 + comp["dri_S"] + comp["dri_Fe"]))))

    #melting and heating of additional scrap for burner FeO reactions
    QM["FeO_burners"] = (-iv["m_FeO_sum"] * M["Fe"] / M["FeO"]
                            / (y[ind["m_Fe_st_sol"]]/y[ind["m_st_sol"]]) / M["Fe"]
                            * (c["lambda_st"] + cp["st_sol"] * (c["T_melt_st"]
                            - y[ind["T_st_sol"]])))

    # mixing enthalpy for dissolved O rate of change
    QM["st_liq_O"]  = ((dy[ind["m_O_st_liq"]] + m_resolid_st * y[ind["m_O_st_liq"]] / y[ind["m_st_liq"]])
                       * (y[ind["T_st_liq"]] - c["T_ref"]) * (cp["O2"] / M["O2"] - cp["st_liq"] / M["Fe"]))
    # mixing enthalpy for dissolved C rate of change
    QM["st_liq_C"]  = ((dy[ind["m_C_st_liq"]] + m_resolid_st * y[ind["m_C_st_liq"]] / y[ind["m_st_liq"]])
                       * (y[ind["T_st_liq"]] - c["T_ref"]) * (cp["C"] / M["C"] - cp["st_liq"] / M["Fe"]))
    # mixing enthalpy for Si rate of change
    QM["st_liq_Si"] = ((dy[ind["m_Si_st_liq"]] + m_resolid_st * y[ind["m_Si_st_liq"]] / y[ind["m_st_liq"]])
                       * (y[ind["T_st_liq"]] - c["T_ref"]) * (cp["Si"] / M["Si"] - cp["st_liq"] / M["Fe"]))
    # mixing enthalpy for Mn rate of change
    QM["st_liq_Mn"] = ((dy[ind["m_Mn_st_liq"]] + m_resolid_st * y[ind["m_Mn_st_liq"]] / y[ind["m_st_liq"]])
                       * (y[ind["T_st_liq"]] - c["T_ref"]) * (cp["Mn"] / M["Mn"] - cp["st_liq"] / M["Fe"]))
    # mixing enthalpy for Cr rate of change
    QM["st_liq_Cr"] = ((dy[ind["m_Cr_st_liq"]] + m_resolid_st * y[ind["m_Cr_st_liq"]] / y[ind["m_st_liq"]])
                       * (y[ind["T_st_liq"]] - c["T_ref"]) * (cp["Cr"] / M["Cr"] - cp["st_liq"] / M["Fe"]))
    # mixing enthalpy for P rate of change
    QM["st_liq_P"]  = ((dy[ind["m_P_st_liq"]] + m_resolid_st * y[ind["m_P_st_liq"]] / y[ind["m_st_liq"]])
                       * (y[ind["T_st_liq"]] - c["T_ref"]) * (cp["P"] / M["P"] - cp["st_liq"] / M["Fe"]))
    # mixing enthalpy for Fe rate of change
    QM["st_liq_Fe"] = ((dy[ind["m_Fe_st_liq"]] + m_resolid_st * y[ind["m_Fe_st_liq"]] / y[ind["m_st_liq"]])
                        * (y[ind["T_st_liq"]] - c["T_ref"]) * (cp["Fe"] / M["Fe"] - cp["st_liq"] / M["Fe"]))

    # mixing enthalpy for S rate of change
    QM["st_liq_S"]  = ((dy[ind["m_S_st_liq"]] + m_resolid_st * y[ind["m_S_st_liq"]]
                            / y[ind["m_st_liq"]]) * (y[ind["T_st_liq"]] - c["T_ref"])
                        * (cp["S"] / M["S"] - cp["st_liq"] / M["Fe"]))
    # mixing enthalpy for CaO rate of change
    QM["sl_liq_CaO"]   = ((dy[ind["m_CaO_sl_liq"]] - m_refrac_wear * comp["refrac_CaO"])
                            *(y[ind["T_sl_liq"]] - c["T_ref"]) *
                            (cp["CaO"] / M["CaO"] - cp["sl_liq"] / M["sl_liq"]))
    # mixing enthalpy for MgO rate of change
    QM["sl_liq_MgO"]   = ((dy[ind["m_MgO_sl_liq"]] - m_refrac_wear * comp["refrac_MgO"])
                            * (y[ind["T_sl_liq"]] - c["T_ref"])
                            * (cp["MgO"] / M["MgO"] - cp["sl_liq"] / M["sl_liq"]))
    # mixing enthalpy for Al2O3 rate of change
    QM["sl_liq_Al2O3"] = (dy[ind["m_Al2O3_sl_liq"]] * (y[ind["T_sl_liq"]] - c["T_ref"])
                            * (cp["Al2O3"] / M["Al2O3"] - cp["sl_liq"] / M["sl_liq"]))
    # mixing enthalpy for SiO2 rate of change
    QM["sl_liq_SiO2"]  = (dy[ind["m_SiO2_sl_liq"]] * (y[ind["T_sl_liq"]]-c["T_ref"])
                            *(cp["SiO2"] / M["SiO2"] - cp["sl_liq"] / M["sl_liq"]))
    # mixing enthalpy for MnO rate of change
    QM["sl_liq_MnO"]   = (dy[ind["m_MnO_sl_liq"]] * (y[ind["T_sl_liq"]] - c["T_ref"])
                            *(cp["MnO"] / M["MnO"] - cp["sl_liq"] / M["sl_liq"]))
    # mixing enthalpy for Cr2O3 rate of change
    QM["sl_liq_Cr2O3"] = (dy[ind["m_Cr2O3_sl_liq"]] * (y[ind["T_sl_liq"]] - c["T_ref"])
                            * (cp["Cr2O3"] / M["Cr2O3"] - cp["sl_liq"] / M["sl_liq"]))
    # mixing enthalpy for P2O5 rate of change
    QM["sl_liq_P2O5"]  = (dy[ind["m_P2O5_sl_liq"]] * (y[ind["T_sl_liq"]] - c["T_ref"])
                            * (cp["P2O5"] / M["P2O5"] - cp["sl_liq"] / M["sl_liq"]))
    # mixing enthalpy for FeO rate of change
    QM["sl_liq_FeO"]   = (dy[ind["m_FeO_sl_liq"]] * (y[ind["T_sl_liq"]] - c["T_ref"])
                            * (cp["FeO"] / M["FeO"] - cp["sl_liq"] / M["sl_liq"]))
    # mixing enthalpy for CaS rate of change
    QM["sl_liq_CaS"]   = (dy[ind["m_CaS_sl_liq"]] * (y[ind["T_sl_liq"]] - c["T_ref"])
                            * (cp["CaS"] / M["CaS"] - cp["sl_liq"] / M["sl_liq"]))

    # mixing enthalpy for CO rate of change
    QM["gas_CO"]  = (dy[ind["m_CO"]] * (y[ind["T_gas"]] - c["T_ref"])
                        * (cp["CO"] / M["CO"] - cp["gas"] / M["gas"]))
    # mixing enthalpy for CO2 rate of change
    QM["gas_CO2"] = (dy[ind["m_CO2"]] * (y[ind["T_gas"]] - c["T_ref"])
                        * (cp["CO2"] / M["CO2"] - cp["gas"] / M["gas"]))
    # mixing enthalpy for N2 rate of change
    QM["gas_N2"]  = (dy[ind["m_N2"]] * (y[ind["T_gas"]] - c["T_ref"])
                        * (cp["N2"] / M["N2"] - cp["gas"] / M["gas"]))
    # mixing enthalpy for O2 rate of change
    QM["gas_O2"]  = (dy[ind["m_O2"]] * (y[ind["T_gas"]] - c["T_ref"])
                        * (cp["O2"] / M["O2"] - cp["gas"] / M["gas"]))
    # mixing enthalpy for H2 rate of change
    QM["gas_H2"]  = (dy[ind["m_H2"]] * (y[ind["T_gas"]] - c["T_ref"])
                        *(cp["H2"] / M["H2"] - cp["gas"] / M["gas"]))
    # mixing enthalpy for H2O rate of change
    QM["gas_H2O"] = (dy[ind["m_H2O"]] * (y[ind["T_gas"]] - c["T_ref"])
                        *(cp["H2O_gas"] / M["H2O"] - cp["gas"] / M["gas"]))
    # mixing enthalpy for CH4 rate of change
    QM["gas_CH4"] = (dy[ind["m_CH4"]] * (y[ind["T_gas"]] - c["T_ref"])
                        *(cp["CH4"] / M["CH4"] - cp["gas"] / M["gas"]))

    heat_cap_elem_st_sol_st_liq = [y[ind["m_O_st_sol"]] * cp["O2"] / M["O2"],
                                y[ind["m_C_st_sol"]] * cp["C"] / M["C"],
                                y[ind["m_Si_st_sol"]] * cp["Si"] / M["Si"],
                                y[ind["m_Mn_st_sol"]] * cp["Mn"] / M["Mn"],
                                y[ind["m_Cr_st_sol"]] * cp["Cr"] / M["Cr"],
                                y[ind["m_P_st_sol"]] * cp["P"] / M["P"],
                                y[ind["m_Fe_st_sol"]] * cp["Fe"] / M["Fe"], # cp["Fe"] # cp["st_sol"]
                                y[ind["m_S_st_sol"]] * cp["S"] / M["S"]]

    heat_cap_st_sol_st_liq = sum(heat_cap_elem_st_sol_st_liq) / y[ind["m_st_sol"]]


    heat_cap_elem_st_sol_sl_liq = [y[ind["m_O_st_sol"]]*cp["FeO"]/M["FeO"],
                                y[ind["m_O_st_sol"]]*cp["CaO"]/M["CaO"],
                                y[ind["m_O_st_sol"]]*cp["MgO"]/M["MgO"],
                                y[ind["m_O_st_sol"]]*cp["Al2O3"]/M["Al2O3"],
                                y[ind["m_O_st_sol"]]*cp["SiO2"]/M["SiO2"]]

    heat_cap_st_sol_sl_liq = sum(heat_cap_elem_st_sol_sl_liq) / y[ind["m_st_sol"]]


    heat_cap_elem_sl_sol_sl_liq =  np.array([y[ind["m_CaO_sl_sol"]] * cp["CaO"]/M["CaO"],
                                            y[ind["m_MgO_sl_sol"]] * cp["MgO"]/M["MgO"],
                                            y[ind["m_Al2O3_sl_sol"]] * cp["Al2O3"]/M["Al2O3"],
                                            y[ind["m_SiO2_sl_sol"]] * cp["SiO2"]/M["SiO2"],
                                            y[ind["m_MnO_sl_sol"]] * cp["MnO"]/M["MnO"],
                                            y[ind["m_Cr2O3_sl_sol"]] * cp["Cr2O3"]/M["Cr2O3"],
                                            y[ind["m_P2O5_sl_sol"]] * cp["P2O5"]/M["P2O5"],
                                            y[ind["m_FeO_sl_sol"]] * cp["FeO"]/M["FeO"],
                                            y[ind["m_CaS_sl_sol"]] * cp["CaS"]/M["CaS"]],dtype=np.float64)

    heat_cap_sl_sol_sl_liq = sum(heat_cap_elem_sl_sol_sl_liq / y[ind["m_sl_sol"]])

    # mixing enthalpy for melting of scrap (species leaving scrap zone at melting temperature)
    QM["st_sol_melt"] = (- melt_rate_scrap #(dy[ind["m_st_sol"]] - m_resolid_st)
                         * (c["T_melt_st"] - c["T_ref"])
                         * (heat_cap_st_sol_st_liq + heat_cap_st_sol_sl_liq
                            - cp["st_sol"] / M["Fe"]))
    # mixing enthalpy for melting of scrap, heating molten scrap to melt temperature
    QM["st_liq_melt"] = (- melt_rate_scrap #(dy[ind["m_st_sol"]] - m_resolid_st)
                         * heat_cap_st_sol_st_liq
                         * (y[ind["T_st_liq"]] - c["T_melt_st"]))
    # mixing enthalpy for melting of slag (species leaving scrap zone at melting temperature)
    QM["sl_sol_melt"] = (-melt_rate_slag
                         * (heat_cap_sl_sol_sl_liq - cp["sl_sol"] / M["sl_sol"])
                         * (c["T_melt_sl"] - c["T_ref"]))
    # mixing enthalpy for melting of slag, heating molten slag to lSl temperature
    QM["sl_liq_melt"] = (- melt_rate_slag * heat_cap_sl_sol_sl_liq
                         * (y[ind["T_sl_liq"]] - c["T_melt_sl"]))
                        #(dy[ind["m_st_sol"]] - m_resolid_st)* heat_cap_st_sol_sl_liq* (y[ind["T_sl_liq"]] - c["T_melt_st"])

    leak_fact=0.5*tan_hyp(y[ind["T_gas"]],600,1,1)

    #total mixing enthalpy gas zone [MW]
    QM["gas"] = (((QM["O2_post"] + QM["burner"]) * (1-K["post"])
                + QM["leak_H2O"] + QM["leak_air"] * leak_fact) / 1e3)

    # total mixing enthalpy solid scrap zone [MW]
    QM["st_sol"] = ((QM["resolid"] + QM["st_sol_melt"]
                + QM["FeO_burners"] + (QM["O2_post"] + QM["burner"])
                * K["post"]) / 1e3)

    # total mixing enthalpy solid slag zone [MW]
    QM["sl_sol"] = (QM["slag_form"] + QM["dust"] + QM["sl_sol_melt"]) / 1e3

    # energy to correct for different heat capacities;
    # should not change temperature of gas phase will therefore be taken from lSc
    QM["gas_cp_change"] = (QM["gas_CO"] + QM["gas_CO2"] + QM["gas_O2"]
                        + QM["gas_N2"] + QM["gas_H2"] + QM["gas_H2O"]
                        + QM["gas_CH4"])

    # total mixing enthalpy melt zone [MW]
    QM["st_liq"] = ((QM["gas_cp_change"] + QM["conv_gas"] + QM["C_inj"]
                + QM["O2_lance"] + QM["C_diss"] + QM["st_liq_melt"]
                + QM["st_liq_O"] + QM["st_liq_C"] + QM["st_liq_Si"]
                + QM["st_liq_Mn"] + QM["st_liq_Cr"]
                + QM["st_liq_P"] + QM["st_liq_Fe"] + QM["st_liq_S"]
                + QM["leak_air"] * (1-leak_fact) + QM["dri"]) / 1e3)

    # total mixing enthalpy of liquid slag zone [MW]
    QM["sl_liq"] = ((QM["sl_liq_melt"] + QM["sl_liq_CaO"] + QM["sl_liq_MgO"]
                + QM["sl_liq_Al2O3"] + QM["sl_liq_SiO2"]
                + QM["sl_liq_MnO"] + QM["sl_liq_Cr2O3"] + QM["sl_liq_P2O5"]
                + QM["sl_liq_FeO"] + QM["sl_liq_CaS"]) / 1e3)

    """Mixing enthalpy dependend temperatures rates of change and pressure rate of change"""
    
    # Solid scrap temperature change rate [K/s]
    dy[ind["T_st_sol"]]   = (((Q["st_sol"] * (1 - heat_division_scrap)) + Q["dri"]) * 1e3#
                            /(y[ind["m_st_sol"]] * cp["st_sol"] / M["Fe"]))

    # Liquid scrap temperature change rate [K/s]
    dy[ind["T_st_liq"]]   = ((Q["st_liq"] - Q["resolid_st"] + QM["st_liq"]
                            + QM["st_sol"] + QM["sl_liq"] + QM["sl_sol"] + Q["aluminium"])
                            * 1e3 / (y[ind["m_st_liq"]] * cp["st_liq"] / M["Fe"]))

    # Solid slag temperature change rate [K/s]
    dy[ind["T_sl_sol"]]  = (Q["sl_sol"] * (1 - heat_division_slag)
                            * 1e3/(y[ind["m_sl_sol"]] * cp["sl_sol"]
                                    / M["sl_sol"]))

    # Liquid slag temperature change rate [K/s]
    dy[ind["T_sl_liq"]]   = (Q["sl_liq"] * 1e3 / (y[ind["m_sl_liq"]]
                            * cp["sl_liq"] / M["sl_liq"]))

    # Gas temperature change rate [K/s]
    dy[ind["T_gas"]]  = ((Q["gas"] + QM["gas"]) * 1e3 / (y[ind["m_gas"]]
                        * (cp["gas"] / M["gas"])))

    # change of relative Pressure inside the EAF [N/ms]; z(40) [Pa]
    dy[ind["p_furnace"]]  = (c["R_gas"] * y[ind["T_gas"]] / p["V_gas"] *
                            (dy[ind["m_CO"]] / M["CO"]
                                + dy[ind["m_CO2"]] / M["CO2"]
                                + dy[ind["m_N2"]] / M["N2"]
                                + dy[ind["m_O2"]] / M["O2"]
                                + dy[ind["m_H2"]] / M["H2"]
                                + dy[ind["m_H2O"]] / M["H2O"]
                                + dy[ind["m_CH4"]] / M["CH4"])
                                + c["R_gas"] * dy[ind["T_gas"]] / p["V_gas"]
                                *(y[ind["m_CO"]] / M["CO"]
                                + y[ind["m_CO2"]] / M["CO2"]
                                + y[ind["m_N2"]] / M["N2"]
                                + y[ind["m_O2"]] / M["O2"]
                                + y[ind["m_H2"]] / M["H2"]
                                + y[ind["m_H2O"]] / M["H2O"]
                                + y[ind["m_CH4"]] / M["CH4"]))
                                #+ c["R_gas"] * y[ind["T_gas"]] / (-dy[ind["V_st_sol"]]) # dV_gas = -dy[ind["V_st_sol"]]
                                #*(y[ind["m_CO"]] / M["CO"]
                                #+ y[ind["m_CO2"]] / M["CO2"]
                                #+ y[ind["m_N2"]] / M["N2"]
                                #+ y[ind["m_O2"]] / M["O2"]
                                #+ y[ind["m_H2"]] / M["H2"]
                                #+ y[ind["m_H2O"]] / M["H2O"]
                                #+ y[ind["m_CH4"]] / M["CH4"]))
    if (OpCh["current_t"] == 0) or (np.round(OpCh["current_t"]) < np.round(t)):#np.round(OpCh["current_t"]) < np.round(t)
        OpCh["current_t"] = t
        write_results(resh=resh, y=y, p=p, Q=Q, OpCh=OpCh, X=X, W=W, comp=comp, c=c, cp=cp, DH_T=DH_T, dh=dh, QM=QM, M=M, K=K, iv=iv, eval=eval, ind=ind)
    
    #for i in range(1,79):
    #    if y[i] < 0 and dy[i] < 0:
    #if any(y[0:79]<0):
    #    print("Fehler")

    #if any(np.isnan(dy)) or any(np.isinf(dy)):
    #   print("quatsch")
    #if np.any(y[0:79]<0):
    #    print(f"Fehler@{t}")
    #if y[ind["m_vol_coal"]]<0:
    #    print(f"Fehler@{t}")
    
    #__glob__.solverTimerCum += time.time() - timer0

    return dy