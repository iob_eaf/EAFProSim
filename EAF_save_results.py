import os.path
import numpy as np
import h5py
from io import BytesIO
import io_tools as iot
import sys

def save_arrays(group, arr, name):
    arr_group = group.create_group(name)
    for idx, data in enumerate(arr):
        arr_group.create_dataset(str(idx), data=data, compression="gzip", compression_opts=9)

'''
def save_object(p_group, obj, group_name):
    group = p_group.create_group(str(group_name))
    if type(obj) == dict:
        att_dict = obj
    else:
        att_dict = obj.__dict__

    for att_name, att in att_dict.items():
        if type(att) in [int, float, np.ndarray, np.float_, int_, list, str]:
            # print(str(att_name))
            if type(att) == list and len(att) > 1 and type(att[0]) == np.ndarray:
                # list of 3d arrays and ragged ndarrays are saved individually
                if len(att[0].shape) > 2 or len(np.array(att).shape) == 1:
                    ragged_group = group.create_group(str(att_name))
                    for item_idx, item in enumerate(att):
                        ragged_group.create_dataset(str(item_idx), data=item, compression="gzip")
                else:
                    group.create_dataset(str(att_name), data=att, compression="gzip")
            else:
                group.create_dataset(str(att_name), data=att, compression="gzip")
        else:
            save_object(group, att, att_name)


def convert_obj_to_dict(inp_dict, obj):
    if type(obj) == dict:
        att_dict = obj
    else:
        att_dict = obj.__dict__

    for att_name, att in att_dict.items():
        if type(att) in [int, float, np.ndarray, np.float_, int_, list, str]:
            if att_name not in inp_dict:
                inp_dict[att_name] = [att]
            else:
                inp_dict[att_name].append(att)
        else:
            if att_name not in inp_dict:
                inp_dict[att_name] = {}
            convert_obj_to_dict(inp_dict[att_name], att)


def save_charge_result(charge_group, result, name):
    # merge array of inp objects to a single inp dictionary
    inp_dict = {}
    for idx, inp in enumerate(result):
        convert_obj_to_dict(inp_dict, inp)

    # save inp dictionary to hdf5 file
    save_object(charge_group, inp_dict, name)
    return
'''
def EAF_save_results(path, dataset, charge_list, ts, ys, results):
    
    """
    Saves results from EAF simulation for each charge

    :param path: Path to HDF5 file or None if store in memory
    :param dataset: Name of the dataset as string
    :param charge_list: A list of charge numbers
    :param ts: A list of time arrays for each charge, obtained from EAF_sim_batch
    :param ys: A list of y arrays for each charge, obtained from EAF_sim_batch
    :param results: A list of arrays of InputData objects for each charge
    """
    default_params = {
        'compression': 'gzip',
        'compression_opts': 9
    }
    
    if path is None:
        print("Save EAF simulation results to memory")
        file_obj = BytesIO()
    else:
        print("Save EAF simulation results to file")
        path_dir = os.path.dirname(path)
        if not os.path.isdir(path_dir):
            os.makedirs(path_dir)
        file_obj = path

    with h5py.File(file_obj,"w") as hf:
        # save dataset name
        hf.create_dataset("data", data=dataset)

        for charge_idx, charge_num in enumerate(charge_list):
            charge_group = hf.create_group(str(charge_num))
            
            y    = charge_group.create_group("y")
            p    = charge_group.create_group("p")
            Q    = charge_group.create_group("Q")
            OpCh = charge_group.create_group("OpCh")
            X    = charge_group.create_group("X")
            W    = charge_group.create_group("W")
            comp = charge_group.create_group("comp")
            DH_T = charge_group.create_group("DH_T")
            QM   = charge_group.create_group("QM")
            M    = charge_group.create_group("M")
            iv   = charge_group.create_group("iv")
            eval = charge_group.create_group("eval")
            c    = charge_group.create_group("c")
            cp   = charge_group.create_group("cp")
            dh   = charge_group.create_group("dh")
            K    = charge_group.create_group("K")
            
            # save resh result
            for key in results[charge_idx]:
                a = key.split('.')
                if a[0] == 'p':
                    p.create_dataset(a[1], data=results[charge_idx][key], **default_params)
                elif a[0] == 't':
                    charge_group.create_dataset(a[0], data=results[charge_idx][key], **default_params)
                elif a[0] == 'y':
                    y.create_dataset(a[1], data=results[charge_idx][key], **default_params)
                elif a[0] == 'Q':
                    Q.create_dataset(a[1], data=results[charge_idx][key], **default_params)
                elif a[0] == 'OpCh':
                    OpCh.create_dataset(a[1], data=results[charge_idx][key], **default_params)
                elif a[0] == 'X':
                    X.create_dataset(a[1], data=results[charge_idx][key], **default_params)
                elif a[0] == 'W':
                    W.create_dataset(a[1], data=results[charge_idx][key], **default_params)
                elif a[0] == 'comp':
                    comp.create_dataset(a[1], data=results[charge_idx][key], **default_params)
                elif a[0] == 'DH_T':
                    DH_T.create_dataset(a[1], data=results[charge_idx][key], **default_params)
                elif a[0] == 'QM':
                    QM.create_dataset(a[1], data=results[charge_idx][key], **default_params)
                elif a[0] == 'M':
                    M.create_dataset(a[1], data=results[charge_idx][key], **default_params)
                elif a[0] == 'iv':
                    iv.create_dataset(a[1], data=results[charge_idx][key], **default_params)
                elif a[0] == 'eval':
                    eval.create_dataset(a[1], data=results[charge_idx][key], **default_params)
                elif a[0] == 'c':
                    c.create_dataset(a[1], data=results[charge_idx][key], **default_params)
                elif a[0] == 'cp':
                    cp.create_dataset(a[1], data=results[charge_idx][key], **default_params)
                elif a[0] == 'dh':
                    dh.create_dataset(a[1], data=results[charge_idx][key], **default_params)
                elif a[0] == 'K':
                    K.create_dataset(a[1], data=results[charge_idx][key], **default_params)
                
                #print(key)
            #charge_result = results[charge_idx]
            #save_charge_result(charge_group, charge_result, "result")
            
            #dri[np.where(elements=='O')].item()

    return file_obj
