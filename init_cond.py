import numpy as np
import copy
from io_tools import checkAscending,sysexit
import sys
from melting_geometry import melting_geometry
from scipy.interpolate import PchipInterpolator
from scipy.integrate import trapezoid

def init_cond(y, OpCh, M, p, comp, c, cp, K, ind):

    if OpCh["basket_number"] == 0:
        #comp_air = np.array([0.0001, 0.001, 0.79, 0.2088889, 0.0000001, 0.00001, 0.000001])
        i = int(OpCh["basket_number"])
    
    elif OpCh["basket_number"] > 0:
        #comp_air = np.array([y[ind["m_CO"]]/y[ind["m_gas"]], y[ind["m_CO2"]]/y[ind["m_gas"]], y[ind["m_N2"]]/y[ind["m_gas"]], y[ind["m_O2"]]/y[ind["m_gas"]],
        #                     y[ind["m_H2"]]/y[ind["m_gas"]], y[ind["m_H2O"]]/y[ind["m_gas"]], y[ind["m_CH4"]]/y[ind["m_gas"]]])
        i = int(OpCh["basket_number"] - 1)
    
    # Water cooling and wall initial Temperatures [K]
    y[ind["T_roof"]] = OpCh["T_roof_init"][i]
    y[ind["T_wall"]] = OpCh["T_wall_init"][i]
    y[ind["T_roof_water"]] = OpCh["T_roof_H2O_init"][i]
    y[ind["T_wall_water"]] = OpCh["T_wall_H2O_init"][i]
    
    # Solid scrap zone temperature [K]
    y[ind["T_st_sol"]] = ((y[ind["T_st_sol"]] * y[ind["m_st_sol"]] + c["T_ambient"] * OpCh["m_st_sol_basket"][i] ) /
                        (y[ind["m_st_sol"]] + OpCh["m_st_sol_basket"][i]))
    # Solid slag zone temperature [K]
    y[ind["T_sl_sol"]] = (y[ind["T_sl_sol"]] * y[ind["m_sl_sol"]] + c["T_ambient"] * OpCh["m_slag"][i]) / (
                    y[ind["m_sl_sol"]] + OpCh["m_slag"][i])
    
    
    # Solid scrap mass [kg]
    y[ind["m_O_st_sol"]] = y[ind["m_O_st_sol"]] + OpCh["mass_scrap"][i,ind["comp_O"]]
    y[ind["m_C_st_sol"]] = y[ind["m_C_st_sol"]] + OpCh["mass_scrap"][i,ind["comp_C"]]
    y[ind["m_Si_st_sol"]] = y[ind["m_Si_st_sol"]] + OpCh["mass_scrap"][i,ind["comp_Si"]]
    y[ind["m_Mn_st_sol"]] = y[ind["m_Mn_st_sol"]] + OpCh["mass_scrap"][i,ind["comp_Mn"]]
    y[ind["m_Cr_st_sol"]] = y[ind["m_Cr_st_sol"]] + OpCh["mass_scrap"][i,ind["comp_Cr"]]
    y[ind["m_P_st_sol"]] = y[ind["m_P_st_sol"]] + OpCh["mass_scrap"][i,ind["comp_P"]]
    y[ind["m_Fe_st_sol"]] = y[ind["m_Fe_st_sol"]] + OpCh["mass_scrap"][i,ind["comp_Fe"]]
    y[ind["m_S_st_sol"]] = y[ind["m_S_st_sol"]] + OpCh["mass_scrap"][i,ind["comp_S"]]
    # combustable oil etc
    burn_at_charging_perc = 0.50 #0.35
    y[ind["m_comb_scrap"]] = y[ind["m_comb_scrap"]] + OpCh["mass_scrap"][i,ind["comp_Combustables"]]*(1-burn_at_charging_perc)
    y[ind["m_st_sol"]] = sum(y[ind["m_O_st_sol"]:ind["m_S_st_sol"]+1])
    
    Hu_volatiles = ((11.9 + 9.9) /2) *3.6 #MJ
    y[ind["T_st_sol"]] = y[ind["T_st_sol"]] + (OpCh["mass_scrap"][i,ind["comp_Combustables"]]*burn_at_charging_perc*0.5*Hu_volatiles)*1000/(cp["st_sol"]*y[ind["m_st_sol"]]/M["Fe"])

    
    if OpCh["m_st_sol_basket"][i] / p["rho_scrap_init"] / p["V_EAF"] > 0.5 or y[ind["V_st_sol"]] + OpCh["m_st_sol_basket"][i] / p["rho_scrap_init"] > p["V_scrap_init"]:
        rho_st_sol_fill = OpCh["m_st_sol_basket"][i] / (p["V_scrap_init"] - y[ind["V_st_sol"]] - y[ind["m_st_liq"]]/p["rho_st_liq"])
        if rho_st_sol_fill>2000:
            raise Warning("calculated density of scrap exceeds 2000kg/m3")
        #elif rho_st_sol_fill<500:
        #    raise Warning("calculated density of scrap below 500kg/m3")

        # scrap density within reasonable range
        rho_st_sol_fill = max(min(rho_st_sol_fill, 2000), 500)#p["rho_st_sol"]
        # set new initial density for scrap zone
        p["rho_scrap_init"] = rho_st_sol_fill

    #adjust Volume of Scrap
    y[ind["V_st_sol"]] = y[ind["V_st_sol"]] + OpCh["m_st_sol_basket"][i] / p["rho_scrap_init"]

       
    if OpCh["basket_number"] == 0:
        #comp_gas = np.array([0.0001, 0.001, 0.79, 0.2088889, 0.0000001, 0.00001, 0.000001])
        #M_gas = M["air"]
        p["r_hole"] = p["r_hole_init"]
        #p["m_scrap_init"] = y[ind["m_st_sol"]]
        #p["V_scrap_init"] = p["m_scrap_init"] / p["rho_scrap_init"]
    '''
    elif OpCh["basket_number"] > 0:
        #comp_gas = np.array([y[ind["m_CO"]]/y[ind["m_gas"]], y[ind["m_CO2"]]/y[ind["m_gas"]], y[ind["m_N2"]]/y[ind["m_gas"]], y[ind["m_O2"]]/y[ind["m_gas"]],
        #                     y[ind["m_H2"]]/y[ind["m_gas"]], y[ind["m_H2O"]]/y[ind["m_gas"]], y[ind["m_CH4"]]/y[ind["m_gas"]]])
        # exchange with atmosphere after opening furnace roof
        comp_gas = np.array([0.0001, 0.001, 0.79, 0.2088889, 0.0000001, 0.00001, 0.000001])
        M_gas = M["air"]
        if p["m_scrap_init"] < y[ind["m_st_sol"]] or p["V_scrap_init"] < y[ind["V_st_sol"]]:
            p["m_scrap_init"] = y[ind["m_st_sol"]]
            #p["V_scrap_init"] = y[ind["V_st_sol"]]
        p["V_scrap_init"] = p["m_scrap_init"] / p["rho_scrap_init"]
    '''

    comp_gas = np.array([0.0001, 0.001, 0.79, 0.2088889, 0.0000001, 0.00001, 0.000001])
    M_gas = M["air"]
    p["V_scrap_init"] = y[ind["V_st_sol"]]
    p["m_scrap_init"] = y[ind["m_st_sol"]]
    p["rho_scrap_init"] = p["m_scrap_init"]/p["V_scrap_init"]
    
    # Mass of Gas Zone
    #y[ind["m_gas"]] = (c["ap"] * (p["V_EAF"] - (y[ind["m_st_sol"]] + y[ind["m_st_liq"]]) / p["rho_st_liq"]) * M["air"] / 
    #                        (c["R_gas"] * y[ind["T_gas"]]))
    #y[ind["m_CO"]:ind["m_CH4"]+1] = comp_air * y[ind["m_gas"]]
    

    # gas volume freeboard
    #p["V_gas"] = p["V_EAF"] - (y[ind["m_st_sol"]]+y[ind["m_st_liq"]])/p["rho_st_liq"]
    
    # gas volume freeboard
    p["V_gas"] = p["V_EAF"] - y[ind["m_st_sol"]]/p["rho_scrap_init"] - y[ind["m_st_liq"]]/p["rho_st_liq"]
    y[ind["m_gas"]] = (c["ap"] * p["V_gas"] * M_gas / (c["R_gas"] * y[ind["T_gas"]]))
    y[ind["m_CO"]:ind["m_CH4"]+1] = comp_gas * y[ind["m_gas"]]
    
    
    # if significant volume has been charged (indicating charging of basket, not only through 5th hole)
    #if OpCh["m_st_sol_basket"][i] / p["rho_scrap_init"] > p["V_EAF"] * 0.05:

    # relative pressure
    y[ind["p_furnace"]] = 0
    # Gas zone temperature [K]
    y[ind["T_gas"]] = c["T_ambient"] + 800
    # initial mass of gases [kg]
    y[ind["m_gas"]] = (c["ap"] * p["V_gas"] * M["air"] / (c["R_gas"] * y[ind["T_gas"]]))
    y[ind["m_CO"]:ind["m_CH4"]+1] = comp_gas * y[ind["m_gas"]]

    # metal/slag reactor initial masses with bulk metal composition
    y[ind["m_O_reac"]:ind["m_S_reac"]+1] = copy.deepcopy(y[ind["m_O_st_liq"]:ind["m_S_st_liq"]+1]) / y[ind["m_st_liq"]] * K["reactor_melt_mass"]

        # initial mass of gases [kg]
        # Composition of air
        #y[ind["m_CO"]] = 0.0001 * y[ind["m_gas"]]
        #y[ind["m_CO2"]] = 0.001 * y[ind["m_gas"]]
        #y[ind["m_N2"]] = 0.79 * y[ind["m_gas"]]
        #y[ind["m_O2"]] = 0.2089 * y[ind["m_gas"]]
        #y[ind["m_H2"]] = 0.0000001 * y[ind["m_gas"]]
        #y[ind["m_H2O"]] = 0.00001 * y[ind["m_gas"]]
        #y[ind["m_CH4"]] = 0.000001 * y[ind["m_gas"]]
    
    comp["C_char_C_fix"] = OpCh["mass_coal"][i,ind["comp_C"]]/OpCh["m_coal"][i]
    comp["C_char_C_vol"] = OpCh["mass_coal"][i,ind["comp_Combustables"]]/OpCh["m_coal"][i]
    comp["C_char_H2"] = 0
    comp["C_char_N2"] = 0
    comp["C_char_O2"] = OpCh["mass_coal"][i,ind["comp_O"]]/OpCh["m_coal"][i]
    comp["C_char_H2O"] = OpCh["mass_coal"][i,ind["comp_H2O"]]/OpCh["m_coal"][i]
    
    # charged coal mass and composition for new basket
    y[ind["m_coal"]] = y[ind["m_coal"]] + OpCh["m_coal"][i] * comp["C_char_C_fix"]
    C_char_C_fix_temp = comp["C_char_C_fix"]
    comp["C_char_C_vol"] = ((comp["C_char_C_vol"] / (1 - comp["C_char_C_vol"]) * y[ind["m_vol_coal"]] + comp["C_char_C_vol"] * OpCh["m_coal"][i]) /
                (y[ind["m_vol_coal"]] / (1 - comp["C_char_C_fix"]) + OpCh["m_coal"][i]))
    comp["C_char_O2"] = ((comp["C_char_O2"] / (1 - comp["C_char_C_fix"]) * y[ind["m_vol_coal"]] + comp["C_char_O2"] * OpCh["m_coal"][i]) /
                (y[ind["m_vol_coal"]] / (1 - comp["C_char_C_fix"]) + OpCh["m_coal"][i]))
    comp["C_char_H2"] = ((comp["C_char_H2"] / (1 - comp["C_char_C_fix"]) * y[ind["m_vol_coal"]] + comp["C_char_H2"] * OpCh["m_coal"][i]) /
                (y[ind["m_vol_coal"]] / (1 - comp["C_char_C_fix"]) + OpCh["m_coal"][i]))
    comp["C_char_N2"] = ((comp["C_char_N2"] / (1 - comp["C_char_C_fix"]) * y[ind["m_vol_coal"]] + comp["C_char_N2"] * OpCh["m_coal"][i]) /
                (y[ind["m_vol_coal"]] / (1 - comp["C_char_C_fix"]) + OpCh["m_coal"][i]))
    comp["C_char_H2O"] = ((comp["C_char_H2O"] / (1 - comp["C_char_C_fix"]) * y[ind["m_vol_coal"]] + comp["C_char_H2O"] * OpCh["m_coal"][i]) /
                (y[ind["m_vol_coal"]] / (1 - comp["C_char_C_fix"]) + OpCh["m_coal"][i]))
    #comp["C_char_lhv"] = ((comp["C_char_lhv"] / (1 - comp["C_char_C_fix"]) * y[ind["m_vol_coal"]] + comp["C_char_lhv"] * OpCh["m_coal"][i]) /
    #            (y[ind["m_vol_coal"]] / (1 - comp["C_char_C_fix"]) + OpCh["m_coal"][i]))
    comp["C_char_C_fix"] = ((comp["C_char_C_fix"] / (1 - comp["C_char_C_fix"]) * y[ind["m_vol_coal"]] + comp["C_char_C_fix"] * OpCh["m_coal"][i]) /
                (y[ind["m_vol_coal"]] / (1 - comp["C_char_C_fix"]) + OpCh["m_coal"][i]))

    # mass of volatile coal components [kg]
    y[ind["m_vol_coal"]] = y[ind["m_vol_coal"]] + OpCh["m_coal"][i] * (1 - C_char_C_fix_temp)

    # calculating solid Slag composition from charged chalk and dolomite masses
    #elements that probably come from scrap as well
    y[ind["m_CaO_sl_sol"]]     = y[ind["m_CaO_sl_sol"]] + OpCh["mass_scrap"][i,ind["comp_CaO"]]
    y[ind["m_MgO_sl_sol"]]     = y[ind["m_MgO_sl_sol"]] + OpCh["mass_scrap"][i,ind["comp_MgO"]]
    y[ind["m_Al2O3_sl_sol"]]   = y[ind["m_Al2O3_sl_sol"]] + OpCh["mass_scrap"][i,ind["comp_Al2O3"]]
    y[ind["m_SiO2_sl_sol"]]    = y[ind["m_SiO2_sl_sol"]] + OpCh["mass_scrap"][i,ind["comp_SiO2"]]
    y[ind["m_MnO_sl_sol"]]     = y[ind["m_MnO_sl_sol"]] + OpCh["mass_scrap"][i,ind["comp_MnO"]]
    y[ind["m_Cr2O3_sl_sol"]]   = y[ind["m_Cr2O3_sl_sol"]] + OpCh["mass_scrap"][i,ind["comp_Cr2O3"]]
    y[ind["m_P2O5_sl_sol"]]    = y[ind["m_P2O5_sl_sol"]] + OpCh["mass_scrap"][i,ind["comp_P2O5"]]
    y[ind["m_FeO_sl_sol"]]     = y[ind["m_FeO_sl_sol"]] + OpCh["mass_scrap"][i,ind["comp_FeO"]]
    y[ind["m_CaS_sl_sol"]]     = y[ind["m_CaS_sl_sol"]] + OpCh["mass_scrap"][i,ind["comp_CaS"]]

    # Solid slag mass [kg]
    y[ind["m_sl_sol"]] = sum(y[ind["m_CaO_sl_sol"]:ind["m_CaS_sl_sol"]+1])#m_FeO_sl_sol
    '''
    # if significant amount of scrap is charged assume furnce was filled to capacity
    if OpCh["m_st_sol_basket"][i] / p["rho_scrap_init"] / p["V_EAF"] > 0.5 or y[ind["V_st_sol"]] + OpCh["m_st_sol_basket"][i] / p["rho_scrap_init"] > p["V_scrap_init"]:
        rho_st_sol_fill = OpCh["m_st_sol_basket"][i] / (p["V_scrap_init"] - y[ind["V_st_sol"]])
        # scrap density within reasonable range
        rho_st_sol_fill = max(min(rho_st_sol_fill, 900), p["rho_st_sol"])
        # set new initial density for scrap zone
        p["rho_scrap_init"] = rho_st_sol_fill
    '''
    #y[ind["V_st_sol"]] = y[ind["V_st_sol"]] + OpCh["m_st_sol_basket"][i] / p["rho_scrap_init"]


    #if OpCh["basket_number"] == 0:
    #    p["r_hole"] = p["r_hole_init"]
    #    p["m_scrap_init"] = y[ind["m_st_sol"]]
    #    p["V_scrap_init"] = p["m_scrap_init"] / p["rho_scrap_init"]
    
    #elif OpCh["basket_number"] > 0:
    #    if p["m_scrap_init"] < y[ind["m_st_sol"]] or p["V_scrap_init"] < y[ind["V_st_sol"]]:
    #        p["m_scrap_init"] = y[ind["m_st_sol"]]
    #        p["V_scrap_init"] = y[ind["V_st_sol"]]
    #    p["V_scrap_init"] = p["m_scrap_init"] / p["rho_scrap_init"]
    
    num_species = 28
    opch_time = OpCh["OpCh_data"][np.where(OpCh["OpCh_data"][:,11]==i+1)][:,0]
    opch_slagformers = OpCh["OpCh_data"][np.where(OpCh["OpCh_data"][:,11]==i+1)][:,26:26+num_species]
    opch_dri = OpCh["OpCh_data"][np.where(OpCh["OpCh_data"][:,11]==i+1)][:,26+num_species:26+2*num_species]
    opch_dust = OpCh["OpCh_data"][np.where(OpCh["OpCh_data"][:,11]==i+1)][:,25]
    injected_mass = trapezoid(np.sum(opch_slagformers, axis=1),opch_time) + trapezoid(np.sum(opch_dri, axis=1),opch_time) + trapezoid(opch_dust,opch_time)
    m_geo = melting_geometry(p, y[ind["V_st_sol"]], y[ind["m_st_sol"]], y[ind["m_st_liq"]], injected_mass)

    #sortieren
    m_geo_sorted = m_geo[np.argsort(m_geo[:, 0])]

    # Interpolation m_geo
    m_geo_array0 = np.zeros((m_geo_sorted.shape[0]-1,5))
    m_geo_array1 = np.zeros((m_geo_sorted.shape[0]-1,5))
    m_geo_array2 = np.zeros((m_geo_sorted.shape[0]-1,5))
    m_geo_array3 = np.zeros((m_geo_sorted.shape[0]-1,5))
    rws = [1,2,3,8]
    for ij in range (0,4):
        pcobj = PchipInterpolator(m_geo_sorted[:,0], m_geo_sorted[:,rws[ij]])
        m_geo_array0[:,ij] = pcobj.c[0]
        m_geo_array1[:,ij] = pcobj.c[1]
        m_geo_array2[:,ij] = pcobj.c[2]
        m_geo_array3[:,ij] = pcobj.c[3]

    res5 = checkAscending(m_geo[:,5],id='m_geo[:,5]')[0]
    res7 = checkAscending(m_geo[:,7],id='m_geo[:,7]')[0]
    if res5 or res7: sysexit("geo check failed ...")

    pcobj = PchipInterpolator(m_geo[:,5], m_geo[:,7])
    m_geo_array0[:,4] = pcobj.c[0]
    m_geo_array1[:,4] = pcobj.c[1]
    m_geo_array2[:,4] = pcobj.c[2]
    m_geo_array3[:,4] = pcobj.c[3]

    sz_m_geo = len(m_geo)
    c["m_geo_interp0"][0:sz_m_geo-1,:] = m_geo_array0
    c["m_geo_interp1"][0:sz_m_geo-1,:] = m_geo_array1
    c["m_geo_interp2"][0:sz_m_geo-1,:] = m_geo_array2
    c["m_geo_interp3"][0:sz_m_geo-1,:] = m_geo_array3
    c["Geo_x1"][0:sz_m_geo] = m_geo_sorted[:,0]
    c["Geo_x6"][0:sz_m_geo] = m_geo[:,5]
    c["Geo_last_elem"] = sz_m_geo-1
