"""
MQTT based execution server for EAFProsim - expects local running Eclipse-Mosquitto or other MQTT broker

- main program is executed as separate process, i.e. arbitrary number of processes can be
  executed in parallel depending on system CPU and memory
- all data has to be provided via MQTT and all output is catched into return object
"""

# ================================================================================
# general imports
import os, sys
import json
import paho.mqtt.client as mqtt
import multiprocessing as mp
import threading
import time
import numpy as np

# import pickle
# import base64

# ================================================================================
# EAFProsim imports
import io_tools as iot
# from EAF_start import EAF_parallel
# from EAF_save_results import EAF_save_results
# from mass_balance import mass_balance

from EAF_MQTT_do_simulation import do_simulation
# ================================================================================
# signal handler for CTRL-C on running paho-mqtt
import signal
__SIGINT__ = False
def sigint_handler(sig, frame):
    global __SIGINT__, mqtt_client
    print(f"SIGINT received. Exiting on user request pid={os.getpid()}")
    __SIGINT__ = True
    try: mqtt_client.loop_stop()
    except Exception as ex: print(ex)

    try: mqtt_client.disconnect()
    except Exception as ex: print(ex)

    try: os.kill(os.getpid(),signal.SIGTERM)
    except Exception as ex: print(ex)

    try: sys.exit()
    except Exception as ex: print(ex)

    sig.signal(sig.SIGINT, sigint_handler)

signal.signal(signal.SIGINT, sigint_handler)

# ================================================================================
# mosquitto broker settings (or other MQTT broker)

mqtt_broker_ip   = "localhost"  # local MQTT broker
mqtt_broker_port = 1883         # local MQTT port

# job topic for EAFProsim, return topic defined dynamically via message
EAFProsim_topic  = "EAFPROSIM/runconcurrent" 

# logging for incoming jobs and local errors
data_dir  = "./data"
res_dir   = "./results"
job_log   = data_dir+"/mqtt_jobs.log"
error_log = data_dir+"/mqtt_err.log"

# ================================================================================
def mysleep(duration=1.0,intervall=0.1):
    global mqtt_client
    timer = time.time()
    while time.time()-timer<duration: 
        mqtt_client.loop(timeout=intervall)
        time.sleep(intervall)

# # ================================================================================
# # worker für threads damit Bibliotheken pre-loaded werden können
# def worker_initializer(shared_worker_id, shared_sigint, lock):
#     global my_worker_id, my_sigint
#     my_sigint = shared_sigint
#     with lock:
#         with shared_worker_id.get_lock():
#             my_worker_id = f"worker_{shared_worker_id.value:02d}"
#             print(iot.CFS+f"start pool member {my_worker_id} ...")
#             shared_worker_id.value += 1

# # tasks aus queue verarbeiten
# def process_tasks(task_queue):
#     global my_worker_id, my_sigint
#     while True:
        
#         if my_sigint.value == 1:  break

#         data = task_queue.get()
                
#         if data is not None:
#             print(f"{my_worker_id}: new job ...")
#             on_process_request(None,None,data)
#         else:
#             time.sleep(1)

# # incoming request - do nothing, just enqueue
# def on_message(client, userdata, msg):
#     userdata.put(msg.payload)

# # ================================================================================
# # execute the actual simulation
# # this runs as SEPARATE PARALLEL PROCESS !!
# # no global data can be used !
# def on_thread_connect(client, userdata, flags, reason_code, properties):
#     print("do_simulation: client connected to MQTT ...")    
# def on_thread_disconnect(client, userdata, flags, reason_code, properties):
#     print("do_simulation: client disconnected from MQTT ...")    

# # ================================================================================
# def do_simulation(client_id,response_topic,data,config,lock):
#     """
#     concurrent wrapper for calling EAF_start

#     Parameters:
#     - client_id (str):      id of client, arbitrary but for 
#     - response_topic (str): main topic to send stderr, stdout, log and results
#     - data (dict):          dictionary with job data 
#     - lock (Lock):          lock for concurrent access to info files

#     Returns:
#     - via MQTT:             stderr, stdout, log and results
#     """
    
#     config = type("",(),config)

#     with lock: iot.outsep("do_simulation",start=True)

#     with lock: print(f"do_simulation: MQTT - {config.mqtt_broker_ip}:{config.mqtt_broker_port}",flush=True)

#     # connect to broker - send also data back on Exception
#     local_client = mqtt.Client(callback_api_version=mqtt.CallbackAPIVersion.VERSION2,
#                          client_id=f"EAFProsim_Client_{client_id}") # must be unique !!
#     local_client.on_connect    = on_thread_connect
#     local_client.on_disconnect = on_thread_disconnect
#     local_client.connect(config.mqtt_broker_ip, port=config.mqtt_broker_port)
#     print(f"do_simulation: connection established ...",flush=True)

#     try:

#         # ========================================
#         # prepare io

#         response_topic = response_topic.strip("/") # make sure right formatting of topic
        
#         # logger
#         with lock: print(f"do_simulation: redirect logger to MQTT")
#         log, err = iot.getMqttLogger(mqtt_client=local_client,
#                                      topic=response_topic+"/log") # redirect logger to MQTT
#         if err: 
#             with lock: print(err.msg)

#         # redirect print zu mqtt
#         with lock: print(f"do_simulation: redirect stdout and stderr to MQTT")
#         sys.stdout = iot.MqttRedirector(mqtt_client=local_client,topic=response_topic+"/stdout")
#         sys.sterr  = iot.MqttRedirector(mqtt_client=local_client,topic=response_topic+"/stderr")

#         # topic für ergebnisse
#         results_topic = response_topic+"/results"

#         # ========================================
#         # process data here
#         # basically adapted EAF_start.py
#         # ========================================

#         y_results = np.zeros(75)
#         charge    = data['charge']

#         # ========================================
#         # here the magic happens

#         # process hdf5 from io.bytesIO() file type
#         with lock: print(f"do_simulation: client {client_id} start processing hdf ...",file=sys.__stdout__)
#         h5_obj = pickle.loads(base64.b64decode(data['hdf'].encode('utf-8')))
#         hdf    = iot.hdf5Wrapper(h5_obj)
        
#         # config file dictionary als generisches Dict, neues cfgDict erzeugen
#         with lock: print(f"do_simulation: client {client_id} start processing cfg ...",file=sys.__stdout__)
#         cfg = iot.cfgDict("parameter",json.loads(data['cfg']))

#         # save if demanded
#         if data['store'] == True:
#             with lock: print("do_simulation: save hdf from hdf5Wrapper() ...")
#             hdf.saveHDF5(f"{config.data_dir}/MQTT_inp_{client_id}.h5")      # dir should com via data[]

#             with lock: print("do_simulation: save cfg from cfgDict() wrapper ...")
#             cfg.saveExcel(f"{config.data_dir}/MQTT_inp_{client_id}.xlsx") # dir should com via data[]

#         # ========================================
#         # now run EAF_parallel
            
#         with lock: print(f"do_simulation: call EAF_parallel ...",file=sys.__stdout__)

#         start_time  = time.time()
#         data_source = f"EAFProsim_{client_id}"

#         ys, _ = EAF_parallel(
#                             charge,
#                             y         = y_results, 
#                             datasrc   = data_source,
#                             show_pbar = False, 
#                             logger    = log, 
#                             cfg       = cfg, 
#                             hdf       = hdf)
#         with lock: print(f"do_simulation: call EAF_parallel done ...",file=sys.__stdout__)

#         if ys is None: 
#             with lock: print(f"RESULT not valid",file=sys.__stdout__)
#             error  = True
#             h5_ret = base64.b64encode(json.dumps({"result":"Heat result not valid"})).decode('utf-8')
        
#         else:
#             # ========================================
#             # TODO: here some more magic happens
            
#             ts, ys, results = ys
#             # resDict = Dict.empty(key_type=types.int64,value_type=types.float64)            
#             # resDict[0] = results
            
#             h5_obj = EAF_save_results(None, data_source, [0], ts=ts, ys=ys, results=[results])
#             if data['store'] == True:
#                 with lock: print("do_simulation: save results from io.bytesIO() ...")
#                 h5_obj.seek(0)
#                 with open(f"{config.res_dir}/MQTT_res_{client_id}.h5", 'wb') as file: 
#                     file.write(h5_obj.getvalue())

#             h5_obj.seek(0)
#             mass_balance(dataFile=h5_obj, valid_charge_list=[0])

#             with lock: print(f"Total program time: {round(time.time() - start_time, 2)}s")

#             h5_obj.seek(0)
#             h5_ret = base64.b64encode(h5_obj.getvalue()).decode('utf-8')
#             error  = False

#         # ========================================
#         # process data here done
#         # ========================================

#         # ========================================
#         # prepare result data

#         return_object         = {"client_id":client_id,
#                                  "error":error,
#                                  "t_tap_target": data.get('t_tap_target',-1),
#                                  "m_tap_target": data.get('m_tap_target',-1),
#                                  "user_data":    data.get('user_data',None)}
#         return_object['data'] = h5_ret

#         # ========================================
#         # return result data

#         with lock: 
#             print(f"do_simulation: send {iot.CFG+client_id+iot.CFS} results back on topic {results_topic} (error: {error})...",file=sys.__stdout__)

#         data_json = json.dumps(return_object)
#         local_client.publish(results_topic, data_json,qos=1)
#         with lock: print(f"do_simulation: send done ...",file=sys.__stdout__)
#         with lock:
#             with open(config.job_log,"a") as fp:
#                 print(f"{time.ctime()}: job {client_id} finished and published",file=fp)
        
#         time.sleep(1.0)

#         # reset io streams
#         sys.stdout = sys.__stdout__
#         sys.stderr = sys.__stderr__        
    
#     except Exception as ex:
#         print(f"do_simulation: {iot.CFR+client_id+iot.CFS} caused Exception ...")
#         data_json = json.dumps({"client_id": client_id,
#                                 "error":     True,
#                                 "data":      iot.getTraceback(ex)})
#         local_client.publish(results_topic, data_json,qos=1)
#         with lock:
#             with open(config.job_log,"a") as fp:
#                 print(f"{time.ctime()}: job {client_id} finished with EXCEPTION, no data returned",file=fp)

#     local_client.disconnect()
#     with lock: iot.outsep("do_simulation",end=True)

# ================================================================================
# manage simulations

# eigentliche vorbereitung der simulation
def on_process_request(client, userdata, message):
    """
    manage incoming mqtt simulation requests

    Parameters:
    - standard on_message

    Returns:
    - nothing
    """

    global started_processes, global_timer, job_log, error_log, process_lock

    # initialise logfiles
    if time.time()-global_timer>60:
        with process_lock:
            with open(job_log,"w") as fp: 
                print(f"START {time.ctime()}",file=fp)
                print(file=fp)
            with open(error_log,"w") as fp: 
                print(f"START {time.ctime()}",file=fp)
                print(file=fp)
    global_timer = time.time()

    try:
        t0 = time.time()
        bytes     = message.payload
        with process_lock:
            print(f"on_process_request: incoming bytes {iot.CFY+str(len(bytes))+iot.CFS} bytes, {bytes[:42]}...")            

        data_json = bytes.decode('utf-8') # all data for simulation as json
        data      = json.loads(data_json) # all data as dictionary
        t1 = time.time()

        client_id      = data['client_id']       # get client id
        response_topic = data["response_topic"]  # extract response topic
                
        with process_lock:
            print(f"on_process_request: got job {iot.CFY+client_id+iot.CFS} and returns to {response_topic}")
            with open(job_log,"a") as fp:
                print(f"{time.ctime()}: new job {client_id}, response topic {response_topic}, bytes {len(bytes)}",file=fp)
        t2 = time.time()

        # ============================================================
        # run as separate process
        config = {'mqtt_broker_ip':   mqtt_broker_ip,
                  'mqtt_broker_port': mqtt_broker_port,
                  'EAFProsim_topic':  EAFProsim_topic,
                  'data_dir':         data_dir,
                  'res_dir':          res_dir,
                  'job_log':          job_log,
                  'error_log':        error_log}

        with process_lock:
            print(f"on_process_request: start parallel process on client {client_id}")            
        process = mp.Process(target=do_simulation,args=(client_id,response_topic,data,config,process_lock))
        t3 = time.time()
        process.start()
        t4 = time.time()

        # ============================================================

        with process_lock:
            started_processes.append({
                                    "client_id":      client_id,
                                    "pid":            process.pid,
                                    "timeout":        300,
                                    "start_time":     time.time(),
                                    "process":        process,
                                    "response_topic": response_topic,
                                    "setup":          f"{t1-t0:.1f}s-{t2-t0:.1f}s-{t3-t0:.1f}s-{t4-t0:.1f}s"
                                    })
            
    except Exception as ex:
        with process_lock:
            print(f"on_process_request: ERROR {iot.getTraceback(ex)} ...")

# ================================================================================
# kill processes on timeout

def observe_processes(process_lock,file_lock):
    """
    kill parallel processes on timeout

    Parameters:
    - process_lock (Lock):  Lock() from multiprocessing, mainly for print
    - file_lock (Lock):     Lock() from processing, for concurrent file access

    Returns:
    - nothing
    """

    global __SIGINT__, mqtt_client

    time.sleep(0.1)
    print("process observer started ...")

    last_head = time.time()

    while True:
        
        # enable receiving MQTT message
        time.sleep(5) #mysleep(5)

        if time.time() - last_head > 60:
            print()
            print(f"EAF_MQTT_Server.py: waiting for new jobs for EAFProSim")
            print()
            last_head = time.time()
        
        try:
            
            with process_lock:

                print(f"{time.ctime()} ... "\
                      +iot.CFM+f"check "\
                      +iot.CFY+f"{len(started_processes)}"\
                      +iot.CFM+f" running processes "\
                      +iot.CFS+f"(SIGINT:{__SIGINT__},PID:{os.getpid()}) ...")

                now = time.time()

                for i in range(len(started_processes) - 1, -1, -1):                    

                    last_head = time.time()

                    proc_info = started_processes[i]
                    duration  = now - proc_info['start_time']
                    
                    length = int(np.log10(proc_info['timeout'])+1)
                    print(f"    check {proc_info['client_id']}: pid={proc_info['pid']:6d}, duration={duration:{length+3}.2f} sec, timeout={proc_info['timeout']} sec, setup={proc_info['setup']} ...")

                    if (duration > proc_info['timeout']) or __SIGINT__:

                        if proc_info['process'].is_alive():
                            
                            print(f"    --> process timeout for client_id {proc_info['client_id']} with pid {proc_info['pid']}")
                            proc_info['process'].terminate()
                            proc_info['process'].join()

                        if proc_info['process'].exitcode != 0:
                            print(iot.CFY+f"="*64+iot.CFS)
                            print(iot.CFY+f"process {proc_info['client_id']} crashed"+iot.CFS)
                            print(iot.CFY+f"="*64+iot.CFS)
                            send_crash_info(proc_info['client_id'],proc_info['response_topic'])

                        del started_processes[i]
                    
                    else:
                        if not proc_info['process'].is_alive():
                            print(f"    --> clean up finished process {proc_info['client_id']} with pid {proc_info['pid']}")
                            if proc_info['process'].exitcode != 0:
                                print(iot.CFR+f"="*64+iot.CFS)
                                print(iot.CFR+f"process {proc_info['client_id']} crashed"+iot.CFS)
                                print(iot.CFR+f"="*64+iot.CFS)                                
                                with file_lock:
                                    with open(error_log,"a") as fp:
                                        print(f"process for client_id {proc_info['client_id']} crashed",file=fp)

                            del started_processes[i]

        except Exception as ex:

            print(f"observe_processes: ERROR {ex} ...")

        if __SIGINT__: sys.exit()


# ================================================================================
def on_main_connect(client, userdata, flags, reason_code, properties):
    global EAFProsim_topic
    print(f"main program connected to MQTT")
    print(f"subscribe to '{EAFProsim_topic}")
    mqtt_client.subscribe(EAFProsim_topic,qos=1)

# ================================================================================
def send_crash_info(client_id,topic):
    return_object = {"client_id":   client_id,
                     "error":       "EAFProSim crashed",
                     "t_tap_target": -1,
                     "m_tap_target": -1,
                     "user_data":    None}
    return_object['data'] = None

    data_json = json.dumps(return_object)

# ================================================================================
# # ... zu kompliziert
# def setup_worker_pool(num_processes=42):
    
#     # ============================================================
#     # n parallele pools mit pre-loaded libraries

#     shared_worker_id = mp.Value('i', 0)
#     shared_sigint    = mp.Value('i', 0)
#     task_queue       = mp.Queue()

#     pool = mp.Pool(processes=num_processes,initializer=worker_initializer,initargs=(shared_worker_id, shared_sigint, process_lock))
#     pool.close()

#     # ============================================================
#     # Use workers to process tasks from the queue
#     workers = [mp.Process(target=process_tasks, args=(task_queue,)) for _ in range(num_processes)]
#     for worker in workers: worker.start()

#     return pool, workers, task_queue


# ================================================================================

if __name__ == "__main__":

    iot.outsep("START MQTT-Client")

    # ============================================================
    # globale variablen
    global_timer = time.time()-60
    process_lock      = mp.Lock()
    file_lock         = mp.Lock()
    started_processes = []

    # ============================================================
    # nomen est omen
    # print("setup worker pool ...")
    # pool, workers, task_queue = setup_worker_pool(num_processes=42)    
    on_message = on_process_request

    # ============================================================
    # setup mqtt receiver client
    mqtt_client = mqtt.Client(callback_api_version=mqtt.CallbackAPIVersion.VERSION2,
                         client_id=f"EAFProsim_Simulator") # ,userdata=task_queue)
    mqtt_client.connect(mqtt_broker_ip, port=mqtt_broker_port)    

    mqtt_client.on_connect = on_main_connect
    mqtt_client.on_message = on_message # on_process_request
    
    observe_thread = threading.Thread(target=observe_processes,args=(process_lock,file_lock))
    observe_thread.start()

    print(f"run process {os.getpid()}")
    
    mqtt_client.loop_forever()
