"""
Created around 13.12.2021 

@author: Ralph Rauhut (HiWi von Lilly Schüttensack)
"""

from tan_hyp import tan_hyp_vect
from numba import njit
import numpy as np

    # cell model (Parameters from Gaye, Algorithm by Graham 2008)
    # energy of formation and cell interaction (both woo and eoo) for P-Mg and
    # P-Mn are unknown, set to 0 so no interaction considered, resulting error
    # considered small for Mn and Mg because of low amounts of P2O5 expected
    # and acceptable for P2O5 

@njit(cache=True)
def cell_model(mole_fraction_slag, R_gas, T_liquid_scrap):

    """
    algorithm as per Graham
    results or example given by Graham match the published results
    several conversions from j to cal, remove where possible
    very small error compared to current implementation when only calculating
    GM high and using GM_high-GM for activity calculation instead of
    GM_high-GM_low, up to 50% faster
    no difference when either only changing one species at a time or imposing
    opposite change on one other species for balance 
    (any species can be used for opposite change, saves one iteration of GM_high and GM_low calculation,
    activity calculation has to be adjusted accordingly)
    single species cannot be calculated, activity will have to be set to manually,
    lack of interaction parameters for P2O5 means, that activity can only be
    calculated, if either CaO or Al2O3 are present in addition to P2O5
    """
    ## Precalculation
    test = 0 #% determines weather both direction (higher and lower contents) or only higher contents are used when determining dG/dX_i; both directions takes twice as long
    # test = 1  
    
    mole_fraction=mole_fraction_slag/np.sum(mole_fraction_slag) #incase inaccuracy
    mole_fraction=mole_fraction*tan_hyp_vect(mole_fraction,1e-4,500000,1)+(1-tan_hyp_vect(mole_fraction,1e-4,500000,1))*0.5e-4
    ## activities
    # mixing enthalpies

    GM=mixing_enthalpy(mole_fraction,R_gas,T_liquid_scrap) #mixing enthalpy for current composition

    delta_c = 1e-5 #from Graham page 93 (sensitivity small, 0.5e-5 or 2e-5 give very small error compared to 1e-5)

    idx = np.where(mole_fraction > 0)[0][0]
    GM_high=np.zeros(len(mole_fraction))
    GM_low= np.zeros(len(mole_fraction))
    
    for i in range(idx+1,8):
        if mole_fraction[i] == 0:
            continue
        else:
            mole_fraction_high = mole_fraction.copy()  # initialising new molar compositions for increased and decreased amount of each species
            mole_fraction_low = mole_fraction.copy() 
            idx2_high=(np.where(mole_fraction_high>0))[0][0]
            mole_fraction_high[idx2_high]=mole_fraction_high[idx2_high]-delta_c # change first species 
            mole_fraction_high[i]=mole_fraction_high[i]+delta_c #opposite change for current species
            idx2_low=(np.where(mole_fraction_low>0))[0][0]
            mole_fraction_low[idx2_low]=mole_fraction_low[idx2_low]+delta_c # same in other direction
            mole_fraction_low[i]=mole_fraction_low[i]-delta_c
            GM_high[i]=mixing_enthalpy(mole_fraction_high,R_gas,T_liquid_scrap) # calculate new mixing enthalpies with changed composition
            if test==1:
                GM_low[i] = mixing_enthalpy(mole_fraction_low,R_gas,T_liquid_scrap)

# ----------------------------------------------------------------------------
#select values for relevant species (~=0 in current composition)
    if test == 1:
        GM_low = GM_low[np.not_equal(GM_low,0)]
    GM_high = GM_high [np.not_equal(GM_high,0)]
    mole_fraction1 = mole_fraction[np.not_equal(mole_fraction,0)]
    dGM = np.zeros(len(mole_fraction1))
    if test==1:
        dGM[1:] = (GM_high-GM_low)/2/delta_c
    else:
        dGM[1:] = (GM_high-GM)/delta_c


# ----------------------------------------------------------------------------
#activities for all species ~=0 in current composition (others set to 0)
    ln_a = np.zeros(len(mole_fraction1))
    for i in range(0,len(mole_fraction1)):
        ln_a[i]=GM # initialise with mixing enthalpy for current composition
        for j in range(1,len(mole_fraction1)): # adjust with dGM for each species leaving out the first, which is accounted for by opposite change for each species
            if i==j:
                ln_a[i]=ln_a[i]+(1-mole_fraction1[j])*dGM[j]
            else:
                ln_a[i]=ln_a[i]-mole_fraction1[j]*dGM[j]
    
    ln_a=ln_a/R_gas/T_liquid_scrap
    a=np.exp(ln_a*4.1868) #convert to J
    a_all=np.zeros(len(mole_fraction))
    a_all[np.not_equal(mole_fraction,0)] = a # set activities for species ~=0 in current composition (others remain 0)
    
    return a_all

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#

## calculation of mixing enthalpy
@njit(cache=True)
def mixing_enthalpy(X,R_gas,T_liquid_scrap):

    #[W.P2O5 W.SiO2 W.Cr2O3 W.Al2O3 W.FeO W.MnO W.MgO W.CaO+W.CaS*M.CaS/M.CaO

    #               P Si   Cr      Al      Fe     Mn     Mg     Ca
    WOO = np.array([[0, 0,    0, -11300,     0,     0,     0, -17590],#  P
                    [0, 0, 4500,   2000,  1500, -4500, -8000, -12500],#  Si
                    [0, 0,    0,   1920, 10000, -3660,  -820,  -2960],#  Cr
                    [0, 0,    0,      0,  -400, -1200, -3500,  -8500],#  Al
                    [0, 0,    0,      0,     0,  -500,  2000,  -3000],#  Fe
                    [0, 0,    0,      0,     0,     0,     0,   -500],#  Mn
                    [0, 0,    0,      0,     0,     0,     0,   1000],#  Mg
                    [0, 0,    0,      0,     0,     0,     0,      0]])#  Ca


    #               P  Si     Cr     Al     Fe    Mn      Mg     Ca
    EOO = np.array([[0, 0,     0,     0,     0,     0,     0,     0], # P
                    [0, 0, -1020, -3000,  2100, -1000,  1200, -4500],# Si
                    [0, 0,     0,  -350,     0,     0, -2300,   860],# Cr
                    [0, 0,     0,     0, -2300, -3200, -7000, -5500],# Al
                    [0, 0,     0,     0,     0,     0,     0,   500],# Fe
                    [0, 0,     0,     0,     0,     0,     0,  -800],# Mn
                    [0, 0,     0,     0,     0,     0,     0,     0],# Mg
                    [0, 0,     0,     0,     0,     0,     0,     0]])# Ca
    


    #   0   1   2   3   4   5   6   7
    #   P   Si  Cr  Al  Fe  Mn  Mg  Ca
    nu1=np.array([2,1,2,2,1,1,1,1]) # stochiometric coefficient metal in oxide
    nu2= np.array([5,2,3,3,1,1,1,1]) # stochimetric coefficient oxygen in oxide

    # energies of cell formation correction with X dependencies
    WOO[2,3] = WOO[2,3] + 760 * X[2]     #Cr
    WOO[3,7] = WOO[3,7] + 3000 * X[3]    #Al
    WOO_new = WOO + np.flipud(np.rot90(WOO))
    WOO_new = WOO_new * 4.1868 #convert cal to J

    EOO[3,7] = EOO[3,7] - 5000 * X[3]    #Al
    EOO[1,7] = EOO[1,7] + 7500 * X[1]    #Si
    EOO[1,6] = EOO[1,6] + 3000 * X[1]    #Si
    EOO[1,4] = EOO[1,4] + 1000 * X[1]    #Si
    EOO[1,5] = EOO[1,5] + 5200 * X[1]    #Si
    EOO[2,3] = EOO[2,3] + 280 * X[2]     #Cr
    EOO_new = EOO + np.flipud(np.rot90(EOO))
    EOO_new=EOO_new * 4.1868 #convert cal to J

    
    #--------------------------------------------------------
    V = X*nu2 # cation vector (Vi)
    Vk = np.sum(V) #anion vector (Vk)
    D = np.zeros(len(V)) # oxygen available (Di)
    for i in range(0,len(V)):
        D[i] = sum(V[i:])

    D[V==0]=0 # exclude species =0 in current composition
    D1=D[D>1][0] # find D for first species in current composition

    Q=np.zeros(len(V)) #Qik vector
    for i in range(0,len(V)):
        for k in range(i+1,len(V)):
            Q[i]=Q[i]+nu2[k]*X[k]/D1*EOO_new[i,k]/R_gas/T_liquid_scrap
    #---------------------------------------------------------

    Qexp=np.exp(Q)
    WOOexp=np.exp(-WOO_new/R_gas/T_liquid_scrap)
    
    Qexp = np.expand_dims(Qexp, axis=1)

    P=WOOexp*Qexp*Qexp.T # Pijk matrix
    for i in range(0,len(V)):
        P[i,i]=1

    P[V==0,:]=0
    P[:,V==0]=0

    #----------------------------------------------------------
    # initialise Yj and Yk

    Y=nu2*X/np.sqrt(D1)
    #Yk=np.ones(len(Y)) # unused variable Yk?

    Z=0 # Z start at 0
    dZ=0.1 # Stepsize for increasing z
    P1=P
    #P1[P1==0]=1 # set 0 to 1 to allow for log(P) to give 0 in next step
    # logic indexing on 2D array not supported in numba
    for kk1 in range(P.shape[0]):
        for kk2 in range(P.shape[1]):
            if P1[kk1,kk2] == 0:
                P1[kk1,kk2] = 1 
    logP1=np.log(P1)

    while Z<1: # approximation of Y vectors
        PZ= np.power(P,Z)
        B1 = -Y * np.sum(PZ*logP1*Y,1)
        A = np.transpose(PZ*Y)
        for i in range(0,len(V)):
            suma=0
            for k in range (0,len(V)):
                suma=suma+PZ[i,k]*Y[k]
            A[i,i] = Y[i] + suma
    
        for kk1 in range(P.shape[0]):
            for kk2 in range(P.shape[1]):
                if P[kk1,kk2] == 0:                  
                    A[kk1,kk2] = 0

        for kk1 in range(V.shape[0]):
            if V[kk1] == 0:
                B1[kk1] = 0

        vect = A.flatten()
        vect = vect[vect!=0]
        sizer = int(np.sqrt(np.shape(vect)[0]))
        A_solve = np.reshape(vect,(sizer,sizer))
        B1_solve = B1[B1!=0]
        A_solve = np.linalg.inv(A_solve)

        dY1 = np.dot(A_solve,B1_solve)
        dY1[V==0] = 0

        B2 = np.zeros(len(V))
        for i in range(0,len(V)):
            B2[i] = 0
            for j in range(0,len(V)):
                B2[i] = B2[i]-PZ[i,j]*(2*dY1[i]*dY1[j]+2*(dY1[i]*Y[j]+Y[i]*dY1[j])*logP1[i,j]+Y[i]*Y[j]*(logP1[i,j])**2)

        B2_solve = B2[B2!=0]
        dY2 = np.dot(A_solve,B2_solve)
        dY2[V==0] = 0
            
        Y = Y+dZ*dY1+dZ**2*dY2/2
        Z = Z+dZ

    
#---------------------------------------
    epsilon = 1 #initialize variables
    A_corr = np.zeros(A.shape)
    B_corr = np.zeros(V.shape)
    Y = np.expand_dims(Y, axis=1)
    while abs(epsilon) > 1e-15 : #optimization algorithm for refined Y-vectors (epsilon as per Graham)
        for i in range (0,len(V)):
            B_corr[i]=V[i]
            for j in range (0,len(V)):
                A_corr[i,j]=Y[i,0]*P[i,j]
                if i==j:
                    for k in range (0,len(V)):
                        A_corr[i,j]=A_corr[i,j]+Y[k,0]*P[i,k]        
                B_corr[i]=B_corr[i]-Y[i,0]*Y[j,0]*P[i,j]
        A_corr_solve=np.reshape(vect,(sizer,sizer)) 
        B_corr_solve=B_corr[V!=0] 
        A_corr_solve=np.linalg.inv(A_corr_solve) 
        Y_corr = np.dot(A_corr_solve,B_corr_solve)
        Y_corr[V==0] = 0

        for l in range(0,len(Y)):
            Y[l,0]= Y[l,0]+Y_corr[l] 

        R=P*Y*np.transpose(Y) 

        H = np.array([np.max(np.abs(V-np.sum(R,1))), np.abs(Vk-np.sum(R))]) # error in oxygen and anion mass balance
        epsilon = np.max(H)
        

    #---------------------------------------
    #initialise variables
    E_form=0
    E_int=0
    ln_p=0
    ln_u=0
    ln_u_star=0


    R0 = R.flatten()
    R1 = np.reshape(R0[R0!=0],(sizer,sizer))
    WOO1 = WOO_new.flatten()
    WOO1 = np.reshape(WOO1[R0!=0],(sizer,sizer))/4.1868
    #EOO1=np.reshape(EOO_new[np.not_equal(R,0)],(sizer,sizer))/4.1868

    Q1=np.exp(Q[V!=0])
    V1=V[V!=0]
    D2=D[D!=0]
    nu21=nu2[V!=0]
    nu11=nu1[V!=0]
    nu31=nu11/nu21

    #calculate total energy and degeneracy
    for i in range(0,len(V1)):
        ln_u=ln_u+2*V1[i]*np.log(V1[i]/D1)
        E_int=E_int+R_gas*T_liquid_scrap*2*R1[i,i]*np.log(Q1[i])/4.1868
        if np.not_equal(i,len(V1)-1):
            ln_p=ln_p+nu31[i]*(D2[i]*np.log(D2[i]/V1[i])-D2[i+1]*np.log(D2[i+1]/V1[i]))   
        for j in range(0,len(V1)):
                E_form=E_form+WOO1[i,j]*R1[i,j]
                ln_u_star=ln_u_star-R1[i,j]*np.log(R1[i,j]/D1)

    
    ln_sigma=-(E_form+E_int)*4.1868/R_gas/T_liquid_scrap+ln_p+ln_u+ln_u_star
    GM= -R_gas*T_liquid_scrap*ln_sigma/4.1868  # free energy of mixing
    return GM

