"""
Created on Tue Mar 30 11:39:21 2021

@author: schuettensack
"""
import numpy as np
from calcVF import calcVF
from VF_melt import VF_melt

from tan_hyp import tan_hyp
from numba import njit

@njit(cache=True)
def view_factor(p, K, iv, A, A3):
    '''
    View factors for melting geometry based on Hernandez (cylinder
                                                          based)

    0 roof
    1 wall
    2 solid scrap
    3 liquid scrap
    4 arc
    5 electrode shaft
    6 uncooled wall segment
    7 electrode tip (segment of shaft and horizontal surface on tip)
    8 roof heart (uncoole roof)
    '''
    VF_S    = np.zeros((9,9,9,9))
    
    VF      = np.zeros((9,9))


    VF[0][0]=0                                                                 # View Factor from roof to roof
    VF[4][4]=0                                                                 # View Factor from arc to arc
    VF[0][8]=0                                                                 # View Factor water cooled roof to roof heart
    VF[8][0]=0                                                                 # View Factor from roof heart to watercooled roof
    VF[8][8]=0                                                                 # View Factor roof heart to itself

    A8 = np.ones(2)
    A8[1] = p["r_el_rep"]**2*np.pi # horizontal surface area of electrode tip
    A8[0]=A[7]-A8[1] # vertical surfrace area of electrode tip

    # virtual view factors

    # VF from ring between scrap and electrode to roof surface, segmented to
    # account for roof heart
    #     http://www.thermalradiation.net/sectionc/C-53.html
    RR2_2_3_0 = np.ones(2)
    RR1_2_3_0 = p["r_hole"]/p["h_wall"]
    RR2_2_3_0[0] = p["r_EAF_up"]/p["h_wall"]
    RR2_2_3_0[1] = p["r_heart"]/p["h_wall"]
    RRC_2_3_0 = p["r_el_rep"]/p["h_wall"]
    AA_2_3_0=RR1_2_3_0**2-RRC_2_3_0**2


    for gh in range(0,2):
        BB_2_3_0=RR2_2_3_0[gh]**2-RRC_2_3_0**2
        CC_2_3_0=RR2_2_3_0[gh]+RR1_2_3_0
        DD_2_3_0=RR2_2_3_0[gh]-RR1_2_3_0
        YY_2_3_0=AA_2_3_0**0.5+BB_2_3_0**0.5
        VF_S[2,3,0,gh+1]=(1/np.pi/AA_2_3_0*(AA_2_3_0/2*np.arccos(RRC_2_3_0/RR2_2_3_0[gh])+BB_2_3_0/2*np.arccos(RRC_2_3_0/RR1_2_3_0)+
                            2*RRC_2_3_0*(np.arctan(YY_2_3_0)-np.arctan(AA_2_3_0**0.5)-np.arctan(BB_2_3_0**0.5))-
                            ((1+CC_2_3_0**2)*(1+DD_2_3_0**2))**0.5*np.arctan((((1+CC_2_3_0**2)*(YY_2_3_0**2-DD_2_3_0**2))/((1+DD_2_3_0**2)*(CC_2_3_0**2-YY_2_3_0**2)))**0.5)+
                            ((1+(RR1_2_3_0+RRC_2_3_0)**2)*(1+(RR1_2_3_0-RRC_2_3_0)**2))**0.5*np.arctan((((1+(RR1_2_3_0+RRC_2_3_0)**2)*(RR1_2_3_0-RRC_2_3_0))/((1+(RR1_2_3_0-RRC_2_3_0)**2)*(RR1_2_3_0+RRC_2_3_0)))**0.5)+
                            ((1+(RR2_2_3_0[gh]+RRC_2_3_0)**2)*(1+(RR2_2_3_0[gh]-RRC_2_3_0)**2))**0.5*np.arctan((((1+(RR2_2_3_0[gh]+RRC_2_3_0)**2)*(RR2_2_3_0[gh]-RRC_2_3_0))/((1+(RR2_2_3_0[gh]-RRC_2_3_0)**2)*(RR2_2_3_0[gh]+RRC_2_3_0)))**0.5)))
        # VF from ring to ring with intercepting cylinder, segmented roof with
        # inner ring for roof heart
    
    VF_S[2,3,0,0]=VF_S[2,3,0,1]-VF_S[2,3,0,2] # only to outer roof ring (water cooled area)

    #----------------------------------------------------------------------------------------------------------------------
    # http://www.thermalradiation.net/sectionc/C-53.html
    # VF from electrode bottom to top surface of hole (virtual, 2,3)
    RR2_7_1_2_3 = p["r_hole"]/max(1e-10,p["h_arc"]-p["h_hole"])
    RR1_7_1_2_3 = p["r_el_rep"]/max(1e-10,p["h_arc"]-p["h_hole"])
    RRC_7_1_2_3 = p["r_arc"]/max(1e-10,p["h_arc"]-p["h_hole"])
    AA_7_1_2_3=RR1_7_1_2_3**2-RRC_7_1_2_3**2
    BB_7_1_2_3=RR2_7_1_2_3**2-RRC_7_1_2_3**2
    CC_7_1_2_3=RR2_7_1_2_3+RR1_7_1_2_3
    DD_7_1_2_3=RR2_7_1_2_3-RR1_7_1_2_3
    YY_7_1_2_3=AA_7_1_2_3**0.5+BB_7_1_2_3**0.5
    
    VF_S[7,1,2,3]=(1/np.pi/AA_7_1_2_3*(AA_7_1_2_3/2*np.arccos(RRC_7_1_2_3/RR2_7_1_2_3)+BB_7_1_2_3/2*np.arccos(RRC_7_1_2_3/RR1_7_1_2_3)+
        2*RRC_7_1_2_3*(np.arctan(YY_7_1_2_3)-np.arctan(AA_7_1_2_3**0.5)-np.arctan(BB_7_1_2_3**0.5))-
        ((1+CC_7_1_2_3**2)*(1+DD_7_1_2_3**2))**0.5*np.arctan((((1+CC_7_1_2_3**2)*(YY_7_1_2_3**2-DD_7_1_2_3**2))/((1+DD_7_1_2_3**2)*(CC_7_1_2_3**2-YY_7_1_2_3**2)))**0.5)+
        ((1+(RR1_7_1_2_3+RRC_7_1_2_3)**2)*(1+(RR1_7_1_2_3-RRC_7_1_2_3)**2))**0.5*np.arctan((((1+(RR1_7_1_2_3+RRC_7_1_2_3)**2)*(RR1_7_1_2_3-RRC_7_1_2_3))/((1+(RR1_7_1_2_3-RRC_7_1_2_3)**2)*(RR1_7_1_2_3+RRC_7_1_2_3)))**0.5)+
        ((1+(RR2_7_1_2_3+RRC_7_1_2_3)**2)*(1+(RR2_7_1_2_3-RRC_7_1_2_3)**2))**0.5*np.arctan((((1+(RR2_7_1_2_3+RRC_7_1_2_3)**2)*(RR2_7_1_2_3-RRC_7_1_2_3))/((1+(RR2_7_1_2_3-RRC_7_1_2_3)**2)*(RR2_7_1_2_3+RRC_7_1_2_3)))**0.5)))
    
    # http://www.thermalradiation.net/sectionc/C-53.html
    # VF from electrode bottom to total surface from arc to wall at top of hole
    RR2_7_1_2_0=p["r_hole"]/max(1e-10,p["h_arc"]-p["h_hole"])
    RR1_7_1_2_0=p["r_el_rep"]/max(1e-10,p["h_arc"]-p["h_hole"])
    RRC_7_1_2_0=p["r_arc"]/max(1e-10,p["h_arc"]-p["h_hole"])
    AA_7_1_2_0=RR1_7_1_2_0**2-RRC_7_1_2_0**2
    BB_7_1_2_0=RR2_7_1_2_0**2-RRC_7_1_2_0**2
    CC_7_1_2_0=RR2_7_1_2_0+RR1_7_1_2_0
    DD_7_1_2_0=RR2_7_1_2_0-RR1_7_1_2_0
    YY_7_1_2_0=AA_7_1_2_0**0.5+BB_7_1_2_0**0.5
    
    VF_S[7,1,2,0]=(1/np.pi/AA_7_1_2_0*(AA_7_1_2_0/2*np.arccos(RRC_7_1_2_0/RR2_7_1_2_0)+BB_7_1_2_0/2*np.arccos(RRC_7_1_2_0/RR1_7_1_2_0)+
        2*RRC_7_1_2_0*(np.arctan(YY_7_1_2_0)-np.arctan(AA_7_1_2_0**0.5)-np.arctan(BB_7_1_2_0**0.5))-
        ((1+CC_7_1_2_0**2)*(1+DD_7_1_2_0**2))**0.5*np.arctan((((1+CC_7_1_2_0**2)*(YY_7_1_2_0**2-DD_7_1_2_0**2))/((1+DD_7_1_2_0**2)*(CC_7_1_2_0**2-YY_7_1_2_0**2)))**0.5)+
        ((1+(RR1_7_1_2_0+RRC_7_1_2_0)**2)*(1+(RR1_7_1_2_0-RRC_7_1_2_0)**2))**0.5*np.arctan((((1+(RR1_7_1_2_0+RRC_7_1_2_0)**2)*(RR1_7_1_2_0-RRC_7_1_2_0))/((1+(RR1_7_1_2_0-RRC_7_1_2_0)**2)*(RR1_7_1_2_0+RRC_7_1_2_0)))**0.5)+
        ((1+(RR2_7_1_2_0+RRC_7_1_2_0)**2)*(1+(RR2_7_1_2_0-RRC_7_1_2_0)**2))**0.5*np.arctan((((1+(RR2_7_1_2_0+RRC_7_1_2_0)**2)*(RR2_7_1_2_0-RRC_7_1_2_0))/((1+(RR2_7_1_2_0-RRC_7_1_2_0)**2)*(RR2_7_1_2_0+RRC_7_1_2_0)))**0.5)))
    
    VF_S[7,1,2,0]=VF_S[7,1,2,0]-VF_S[7,1,2,3] # substract virtual VF 2,3
    # if height is close to zero VF_S[7,1,2,3] will be equal to VF_S[7,1,2,0]
    # so that total VF_S[7,1,2,0] after substraction will become zero
    # --------------------------------------------------------------------------------------------------------------------------------------
    
    # http://www.thermalradiation.net/sectionc/C-55.html
    # electrode horizontal to vertical scrap surface
    # cylinder with height of arc 
    RR1_7_1_2_1=p["r_el_rep"]/p["h_arc"]
    RR2_7_1_2_1=p["r_hole"]/p["h_arc"]
    RRC_7_1_2_1=p["r_arc"]/p["h_arc"]
    AA_7_1_2_1=RR1_7_1_2_1**2-RRC_7_1_2_1**2
    BB_7_1_2_1=RR2_7_1_2_1**2-RRC_7_1_2_1**2
    CC_7_1_2_1=RR2_7_1_2_1+RR1_7_1_2_1
    DD_7_1_2_1=RR2_7_1_2_1-RR1_7_1_2_1
    YY_7_1_2_1=AA_7_1_2_1**0.5+BB_7_1_2_1**0.5
    
    VF_S[7,1,2,1]=(1/np.pi/AA_7_1_2_1*(BB_7_1_2_1/2*(np.pi-np.arccos(RRC_7_1_2_1/RR2_7_1_2_1))-2*RRC_7_1_2_1*(np.arctan(YY_7_1_2_1)-np.arctan(BB_7_1_2_1**0.5))-0.5*np.arccos(RRC_7_1_2_1/RR1_7_1_2_1)+
        ((1+CC_7_1_2_1**2)*(1+DD_7_1_2_1**2))**0.5*np.arctan((((1+CC_7_1_2_1**2)*(YY_7_1_2_1**2-DD_7_1_2_1**2))/((1+DD_7_1_2_1**2)*(CC_7_1_2_1**2-YY_7_1_2_1**2)))**0.5)-
        ((1+(RR2_7_1_2_1+RRC_7_1_2_1)**2)*(1+(RR2_7_1_2_1-RRC_7_1_2_1)**2))**0.5*np.arctan((((1+(RR2_7_1_2_1+RRC_7_1_2_1)**2)*(RR2_7_1_2_1-RRC_7_1_2_1))/((1+(RR2_7_1_2_1-RRC_7_1_2_1)**2)*(RR2_7_1_2_1+RRC_7_1_2_1)))**0.5)-
        (RR2_7_1_2_1**2-RR1_7_1_2_1**2)*np.arctan(CC_7_1_2_1/DD_7_1_2_1*((YY_7_1_2_1**2-DD_7_1_2_1**2)/(CC_7_1_2_1**2-YY_7_1_2_1**2))**0.5)))
    
    # http://www.thermalradiation.net/sectionc/C-55.html
    # electrode horizontal to vertical scrap surface
    # cylinder with height of arc minus height of hole
    RR1_7_1_2_4=p["r_el_rep"]/max(1e-5,(p["h_arc"]-p["h_hole"]))
    RR2_7_1_2_4=p["r_hole"]/max(1e-5,(p["h_arc"]-p["h_hole"]))
    RRC_7_1_2_4=p["r_arc"]/max(1e-5,(p["h_arc"]-p["h_hole"]))
    AA_7_1_2_4=RR1_7_1_2_4**2-RRC_7_1_2_4**2
    BB_7_1_2_4=RR2_7_1_2_4**2-RRC_7_1_2_4**2
    CC_7_1_2_4=RR2_7_1_2_4+RR1_7_1_2_4
    DD_7_1_2_4=RR2_7_1_2_4-RR1_7_1_2_4
    YY_7_1_2_4=AA_7_1_2_4**0.5+BB_7_1_2_4**0.5
    
    VF_S[7,1,2,4]=(1/np.pi/AA_7_1_2_4*(BB_7_1_2_4/2*(np.pi-np.arccos(RRC_7_1_2_4/RR2_7_1_2_4))-2*RRC_7_1_2_4*(np.arctan(YY_7_1_2_4)-np.arctan(BB_7_1_2_4**0.5))-0.5*np.arccos(RRC_7_1_2_4/RR1_7_1_2_4)+
        ((1+CC_7_1_2_4**2)*(1+DD_7_1_2_4**2))**0.5*np.arctan((((1+CC_7_1_2_4**2)*(YY_7_1_2_4**2-DD_7_1_2_4**2))/((1+DD_7_1_2_4**2)*(CC_7_1_2_4**2-YY_7_1_2_4**2)))**0.5)-
        ((1+(RR2_7_1_2_4+RRC_7_1_2_4)**2)*(1+(RR2_7_1_2_4-RRC_7_1_2_4)**2))**0.5*np.arctan((((1+(RR2_7_1_2_4+RRC_7_1_2_4)**2)*(RR2_7_1_2_4-RRC_7_1_2_4))/((1+(RR2_7_1_2_4-RRC_7_1_2_4)**2)*(RR2_7_1_2_4+RRC_7_1_2_4)))**0.5)-
        (RR2_7_1_2_4**2-RR1_7_1_2_4**2)*np.arctan(CC_7_1_2_4/DD_7_1_2_4*((YY_7_1_2_4**2-DD_7_1_2_4**2)/(CC_7_1_2_4**2-YY_7_1_2_4**2))**0.5)))
    
    VF_S[7,1,2,1]=VF_S[7,1,2,1]-VF_S[7,1,2,4] # to hole vertical surface as difference between the two cylinders
    #------------------------------------------------------------------------------------------------------------------------------------------------
    # http://www.thermalradiation.net/sectionc/C-53.html
    # VF from electrode bottom to top surface of hole (virtual, 2,3)
    RR2_7_1_2_2=p["r_hole"]/p["h_arc"]
    RR1_7_1_2_2=p["r_el_rep"]/p["h_arc"]
    RRC_7_1_2_2=p["r_arc"]/p["h_arc"]
    AA_7_1_2_2=RR1_7_1_2_2**2-RRC_7_1_2_2**2
    BB_7_1_2_2=RR2_7_1_2_2**2-RRC_7_1_2_2**2
    CC_7_1_2_2=RR2_7_1_2_2+RR1_7_1_2_2
    DD_7_1_2_2=RR2_7_1_2_2-RR1_7_1_2_2
    YY_7_1_2_2=AA_7_1_2_2**0.5+BB_7_1_2_2**0.5
    
    VF_S[7,1,2,2]=(1/np.pi/AA_7_1_2_2*(AA_7_1_2_2/2*np.arccos(RRC_7_1_2_2/RR2_7_1_2_2)+BB_7_1_2_2/2*np.arccos(RRC_7_1_2_2/RR1_7_1_2_2)+
        2*RRC_7_1_2_2*(np.arctan(YY_7_1_2_2)-np.arctan(AA_7_1_2_2**0.5)-np.arctan(BB_7_1_2_2**0.5))-
        ((1+CC_7_1_2_2**2)*(1+DD_7_1_2_2**2))**0.5*np.arctan((((1+CC_7_1_2_2**2)*(YY_7_1_2_2**2-DD_7_1_2_2**2))/((1+DD_7_1_2_2**2)*(CC_7_1_2_2**2-YY_7_1_2_2**2)))**0.5)+
        ((1+(RR1_7_1_2_2+RRC_7_1_2_2)**2)*(1+(RR1_7_1_2_2-RRC_7_1_2_2)**2))**0.5*np.arctan((((1+(RR1_7_1_2_2+RRC_7_1_2_2)**2)*(RR1_7_1_2_2-RRC_7_1_2_2))/((1+(RR1_7_1_2_2-RRC_7_1_2_2)**2)*(RR1_7_1_2_2+RRC_7_1_2_2)))**0.5)+
        ((1+(RR2_7_1_2_2+RRC_7_1_2_2)**2)*(1+(RR2_7_1_2_2-RRC_7_1_2_2)**2))**0.5*np.arctan((((1+(RR2_7_1_2_2+RRC_7_1_2_2)**2)*(RR2_7_1_2_2-RRC_7_1_2_2))/((1+(RR2_7_1_2_2-RRC_7_1_2_2)**2)*(RR2_7_1_2_2+RRC_7_1_2_2)))**0.5)))
    VF_S[7,1,3,0]=VF_S[7,1,2,2]*K["melt_exposed"] # melt surface if exposed
    VF_S[7,1,2,2]=VF_S[7,1,2,2]*(1-K["melt_exposed"]) # scrap otherwise
    
    #--------------------------------------------------------------------------------------------------------------------------------
    # http://www.thermalradiation.net/sectionc/C-55.html
    # electrode horizontal to complete wall
    RR1_7_1_1_0=p["r_el_rep"]/max(1e-5,p["h_wall"]-p["h_el"])
    RR2_7_1_1_0=p["r_EAF_up"]/max(1e-5,p["h_wall"]-p["h_el"])
    RRC_7_1_1_0=p["r_arc"]/max(1e-5,p["h_wall"]-p["h_el"])
    AA_7_1_1_0=RR1_7_1_1_0**2-RRC_7_1_1_0**2
    BB_7_1_1_0=RR2_7_1_1_0**2-RRC_7_1_1_0**2
    CC_7_1_1_0=RR2_7_1_1_0+RR1_7_1_1_0
    DD_7_1_1_0=RR2_7_1_1_0-RR1_7_1_1_0
    YY_7_1_1_0=AA_7_1_1_0**0.5+BB_7_1_1_0**0.5
    
    VF_S[7,1,1,0]=(1/np.pi/AA_7_1_1_0*(BB_7_1_1_0/2*(np.pi-np.arccos(RRC_7_1_1_0/RR2_7_1_1_0))-2*RRC_7_1_1_0*(np.arctan(YY_7_1_1_0)-np.arctan(BB_7_1_1_0**0.5))-0.5*np.arccos(RRC_7_1_1_0/RR1_7_1_1_0)+
        ((1+CC_7_1_1_0**2)*(1+DD_7_1_1_0**2))**0.5*np.arctan((((1+CC_7_1_1_0**2)*(YY_7_1_1_0**2-DD_7_1_1_0**2))/((1+DD_7_1_1_0**2)*(CC_7_1_1_0**2-YY_7_1_1_0**2)))**0.5)-
        ((1+(RR2_7_1_1_0+RRC_7_1_1_0)**2)*(1+(RR2_7_1_1_0-RRC_7_1_1_0)**2))**0.5*np.arctan((((1+(RR2_7_1_1_0+RRC_7_1_1_0)**2)*(RR2_7_1_1_0-RRC_7_1_1_0))/((1+(RR2_7_1_1_0-RRC_7_1_1_0)**2)*(RR2_7_1_1_0+RRC_7_1_1_0)))**0.5)-
        (RR2_7_1_1_0**2-RR1_7_1_1_0**2)*np.arctan(CC_7_1_1_0/DD_7_1_1_0*((YY_7_1_1_0**2-DD_7_1_1_0**2)/(CC_7_1_1_0**2-YY_7_1_1_0**2))**0.5)))

    
    # http://www.thermalradiation.net/sectionc/C-55.html
    # electrode horizontal to uncooled wall
    RR1_7_1_6_0=p["r_el_rep"]/max(min(p["h_wall_unc_exp"],p["h_wall"]-p["h_el"]),1e-5)
    RR2_7_1_6_0=p["r_EAF_up"]/max(min(p["h_wall_unc_exp"],p["h_wall"]-p["h_el"]),1e-5)
    RRC_7_1_6_0=p["r_arc"]/max(min(p["h_wall_unc_exp"],p["h_wall"]-p["h_el"]),1e-5)
    AA_7_1_6_0=RR1_7_1_6_0**2-RRC_7_1_6_0**2
    BB_7_1_6_0=RR2_7_1_6_0**2-RRC_7_1_6_0**2
    CC_7_1_6_0=RR2_7_1_6_0+RR1_7_1_6_0
    DD_7_1_6_0=RR2_7_1_6_0-RR1_7_1_6_0
    YY_7_1_6_0=AA_7_1_6_0**0.5+BB_7_1_6_0**0.5
    
    VF_S[7,1,6,0]=(1/np.pi/AA_7_1_6_0*(BB_7_1_6_0/2*(np.pi-np.arccos(RRC_7_1_6_0/RR2_7_1_6_0))-2*RRC_7_1_6_0*(np.arctan(YY_7_1_6_0)-np.arctan(BB_7_1_6_0**0.5))-0.5*np.arccos(RRC_7_1_6_0/RR1_7_1_6_0)+
        ((1+CC_7_1_6_0**2)*(1+DD_7_1_6_0**2))**0.5*np.arctan((((1+CC_7_1_6_0**2)*(YY_7_1_6_0**2-DD_7_1_6_0**2))/((1+DD_7_1_6_0**2)*(CC_7_1_6_0**2-YY_7_1_6_0**2)))**0.5)-
        ((1+(RR2_7_1_6_0+RRC_7_1_6_0)**2)*(1+(RR2_7_1_6_0-RRC_7_1_6_0)**2))**0.5*np.arctan((((1+(RR2_7_1_6_0+RRC_7_1_6_0)**2)*(RR2_7_1_6_0-RRC_7_1_6_0))/((1+(RR2_7_1_6_0-RRC_7_1_6_0)**2)*(RR2_7_1_6_0+RRC_7_1_6_0)))**0.5)-
        (RR2_7_1_6_0**2-RR1_7_1_6_0**2)*np.arctan(CC_7_1_6_0/DD_7_1_6_0*((YY_7_1_6_0**2-DD_7_1_6_0**2)/(CC_7_1_6_0**2-YY_7_1_6_0**2))**0.5)))
    
    VF_S[7,1,1,0]=VF_S[7,1,1,0]-VF_S[7,1,6,0] # wall reduced by uncooled wall
    VF_8=np.array([0.0,VF_S[7,1,1,0],sum(VF_S[7,1,2,0:3]),VF_S[7,1,3,0],0.0,0.0,VF_S[7,1,6,0]*K["wall_unc_exposed"],0.0,0.0])


    VF_rot=calcVF(p,K["melt_exposed"],p["el_tip_ratio"])

    VF[4,:]=VF_rot[0,:]
    VF[:,4]=VF[4,:].T*A[4]/(A[:].T+1e-10) # reciproke VFs from all surfaces to arc
    
    
    ## VF_5_0 VF_5_1 VF_5_2 VF_5_3 VF_5_4 VF_5_5 VF_5_6 VF_5_7 VF_0_5 VF_1_5 VF_2_5 VF_3_5 VF 4_5 VF_6_5 VF_7_5
    # VF_7_0 VF_7_1 VF_7_2 VF_7_3 VF_7_4 VF_7_5 VF_7_6 VF_7_7 VF_0_7 VF_1_7 VF_2_7 VF_3_7 VF 4_7 VF_5_7 VF_6_7
    VF[5,:]=VF_rot[1,:]
    VF[7,:]=VF_rot[2,:]
    VF[7,:]=(VF[7,:]*A8[0]+VF_8*A8[1])/A[7] # add VFs from horizontal electrode surface
    VF[7,4]=VF[4,7]*A[4]/A[7] # reciproke VF from arc to horizontal electrode surface
    VF[:,5]=VF[5,:].T*A[5]/(A[:].T+1e-10) # reciproke VFs from all surfaces to electrode shaft
    VF[:,7]=VF[7,:].T*A[7]/(A[:].T+1e-10) # reciproke VFs from all surfaces to electrode tip
    
    
    ## VF_3_0 VF_0_3 VF_3_8 VF_8_3 VF_3_1 VF_1_3 VF_6_3 VF_3_6
    # from melt to wall and roof
    VF_melt_rw=VF_melt(p) # calculation of VFs from horizonal surface at bottom of hole to wall and roof segments
    
    #VF_melt_rw=VF_melt_fast(vfgeo_h,vfgeo_r) # calculation of VFs from horizonal surface at bottom of hole to wall and roof segments
    # view factors from melt to watercooled roof and roof heart
    # as the area is melt when the hole has reached
    # the melt surface and scrap otherwise
    
    VF[3,0]= VF_melt_rw[3,0] * K["melt_exposed"]
    VF[0,3] = VF[3,0]*A[3]/A[0]                                                # roof surface to liquid scrap surface
    
    VF[3,8]=VF_melt_rw[3,8]*K["melt_exposed"]*tan_hyp(A[8],0.01,2e3,1)
    # liquid scrap surface to roof surface
    VF[8,3] = VF[3,8]*A[3]/(A[8]+1e-10)
    
    VF[3,1]=VF_melt_rw[3,1]*K["melt_exposed"]
    
    VF[3,6]=VF_melt_rw[3,6]*K["melt_exposed"]*K["wall_unc_exposed"]
    
    VF[6,3] = VF[3,6]*A[3]/(A[6]+1e-10) # uncooled wall surface to melt surface
    
    VF[1,3] = VF[3,1]*A[3]/(A[1]-A[6]) # wall surface to liquid srap surface
    
    
    ## VF_1_1 VF_6_6 VF_1_6 VF_6_1
    
    
    # wall to itself as inside surface of cylinder with coaxial cylinder
    # http://www.thermalradiation.net/sectionc/C-91.html
    # height of wall corrected for uncooled wall segment
    RR1_1_1= p["r_el_rep"]/(p["h_wall"]-p["h_wall_unc_exp"]*K["wall_unc_exposed"])
    RR2_1_1= p["r_EAF_up"]/(p["h_wall"]-p["h_wall_unc_exp"]*K["wall_unc_exposed"])
    
    VF[1,1]=((1/np.pi/RR2_1_1)*(np.pi*(RR2_1_1- RR1_1_1)+np.arccos(RR1_1_1/RR2_1_1)-
        np.sqrt(1+4*RR2_1_1**2)*np.arctan(np.sqrt((1+4*RR2_1_1**2)*(RR2_1_1**2-RR1_1_1**2))/RR1_1_1)+
        2*RR1_1_1*np.arctan(2*np.sqrt(RR2_1_1**2-RR1_1_1**2))))              # wall to wall

    
    # uncooled wall to itelf as inside surface of cylinder with coaxial cylinder
    # http://www.thermalradiation.net/sectionc/C-91.html
    RR1_6_6= p["r_el_rep"]/(p["h_wall_unc_exp"]*K["wall_unc_exposed"]+1e-10)
    RR2_6_6= p["r_EAF_up"]/(p["h_wall_unc_exp"]*K["wall_unc_exposed"]+1e-10)
    
    VF[6,6]=((1/np.pi/RR2_6_6)*(np.pi*(RR2_6_6- RR1_6_6)+np.arccos(RR1_6_6/RR2_6_6)-
        np.sqrt(1+4*RR2_6_6**2)*np.arctan(np.sqrt((1+4*RR2_6_6**2)*(RR2_6_6**2-RR1_6_6**2))/RR1_6_6)+
        2*RR1_6_6*np.arctan(2*np.sqrt(RR2_6_6**2-RR1_6_6**2))))              # wall to wall
    
    # total wall to itself
    # http://www.thermalradiation.net/sectionc/C-91.html
    RR1_16_16 = p["r_el_rep"]/p["h_wall"]
    RR2_16_16 = p["r_EAF_up"]/p["h_wall"]
    
    VF_wall=((1/np.pi/RR2_16_16)*(np.pi*(RR2_16_16- RR1_16_16)+np.arccos(RR1_16_16/RR2_16_16)-
        np.sqrt(1+4*RR2_16_16**2)*np.arctan(np.sqrt((1+4*RR2_16_16**2)*(RR2_16_16**2-RR1_16_16**2))/RR1_16_16)+
        2*RR1_16_16*np.arctan(2*np.sqrt(RR2_16_16**2-RR1_16_16**2))))        # wall to wall
    # uncooled wall to cooled wall from VF algebra (see Tso and Mahulikar,
    # 1999)
    VF[6,1]=(A[1]*VF_wall-(A[1]-A[6])*VF[1,1]-A[6]*VF[6,6])/2/(A[6]+1e-10)
    VF[1,6]=VF[6,1]*A[6]/(A[1]-A[6]+1e-10)
    
    ## VF_1_0 VF_0_1 VF_6_0 VF_0_6 VF_1_8 VF_8_1 VF_6_8 VF_8_6
    # VF roof (watercooled and heart) to wall (uncooled and cooled sections)
    #      http://www.thermalradiation.net/sectionc/C-55.html
    
    h_rel=np.zeros(7)
    h_rel[1] = p["h_wall"] - p["h_wall_unc_exp"]                                 # height for watercooled wall
    #h_rel[1] = p["h_wall"] - p.h_wall_unc_exp
    h_rel[6] = p["h_wall"]                                                      # height for total wall
    r_rel = np.zeros(9)
    r_rel[0] = p["r_EAF_up"]                                                    # radius for total roof
    r_rel[8] = p["r_heart"]                                                     # radius for roof heart
    
    for qw in range(1,7,5):
        for er in range(0,9,8):
            RR1_16_08=r_rel[er]/h_rel[qw]
            RR2_16_08=p["r_EAF_up"]/h_rel[qw]
            RRC_16_08=p["r_el_rep"]/h_rel[qw]
            AA_16_08=RR1_16_08**2-RRC_16_08**2
            BB_16_08=RR2_16_08**2-RRC_16_08**2
            CC_16_08=RR2_16_08+RR1_16_08
            DD_16_08=RR2_16_08-RR1_16_08
            YY_16_08=AA_16_08**0.5+BB_16_08**0.5
            
            VF_S[er,0,qw,0]=(1/np.pi/AA_16_08*(BB_16_08/2*(np.pi-np.arccos(RRC_16_08/RR2_16_08))-2*RRC_16_08*(np.arctan(YY_16_08)-np.arctan(BB_16_08**0.5))-0.5*np.arccos(RRC_16_08/RR1_16_08)+
                ((1+CC_16_08**2)*(1+DD_16_08**2))**0.5*np.arctan((((1+CC_16_08**2)*(YY_16_08**2-DD_16_08**2))/((1+DD_16_08**2)*(CC_16_08**2-YY_16_08**2)))**0.5)-
                ((1+(RR2_16_08+RRC_16_08)**2)*(1+(RR2_16_08-RRC_16_08)**2))**0.5*np.arctan((((1+(RR2_16_08+RRC_16_08)**2)*(RR2_16_08-RRC_16_08))/((1+(RR2_16_08-RRC_16_08)**2)*(RR2_16_08+RRC_16_08)))**0.5)-
                (RR2_16_08**2-RR1_16_08**2)*np.arctan(CC_16_08/(DD_16_08+1e-10)*((YY_16_08**2-DD_16_08**2)/(CC_16_08**2-YY_16_08**2))**0.5)))
    
    # substract roof heart from total roof weighted with surface areas to get
    # watercooled roof to watercooled wall and total wall
    VF_S[0,0,1,0]=(VF_S[0,0,1,0]*(A[0]+A[8])-VF_S[8,0,1,0]*A[8])/A[0]
    VF_S[0,0,6,0]=(VF_S[0,0,6,0]*(A[0]+A[8])-VF_S[8,0,6,0]*A[8])/A[0]
    
    # substract watercooled wall from total wall to get VFs to uncooled wall
    VF_S[0,0,6,0]=VF_S[0,0,6,0]-VF_S[0,0,1,0]
    VF_S[8,0,6,0]=VF_S[8,0,6,0]-VF_S[8,0,1,0]
    
    # VFs from roof sections to wall sections
    VF[0,1]=VF_S[0,0,1,0]
    VF[8,1]=VF_S[8,0,1,0]
    VF[0,6]=VF_S[0,0,6,0]
    VF[8,6]=VF_S[8,0,6,0]
    # reciproke VFs from wall sections to roof sections
    VF[1,0]=VF[0,1]*A[0]/(A[1]-A[6])
    VF[1,8]=VF[8,1]*A[8]/(A[1]-A[6])
    VF[6,0]=VF[0,6]*A[0]/(A[6]+1e-10)
    VF[6,8]=VF[8,6]*A[8]/(A[6]+1e-10)
    
    
    ## VF_2_2
    
    # vertical scrap surface to itself as inside surface of cylinder with coaxial cylinder
    # http://www.thermalradiation.net/sectionc/C-91.html
    # height of wall corrected for uncooled wall segment
    RR1_2_2= p["r_el_rep"]/(p["h_hole"]+1e-10)
    RR2_2_2= p["r_hole"]/(p["h_hole"]+1e-10)
    
    VF_S[2,1,2,1]=((1/np.pi/RR2_2_2)*(np.pi*(RR2_2_2- RR1_2_2)+np.arccos(RR1_2_2/RR2_2_2)-
        np.sqrt(1+4*RR2_2_2**2)*np.arctan(np.sqrt((1+4*RR2_2_2**2)*(RR2_2_2**2-RR1_2_2**2))/RR1_2_2)+
        2*RR1_2_2*np.arctan(2*np.sqrt(RR2_2_2**2-RR1_2_2**2))))              # vertical scrap to itself
    
    VF_S[2,2,2,1]=VF_S[3,0,2,1]*(1-K["melt_exposed"])                         # horizontal scrap to itself
    
    VF[2,2]=(VF_S[2,1,2,1]*A3[1]+VF_S[2,2,2,1]*A3[2])/A[2]                     # total view factor of scrap to itself
    
    ## VF_3_3 VF_3_2 VF_2_3 VF_0_2 VF_2_0 VF_2_8 VF_8_2 VF_1_6 VF_6_1 from summation rule
    VF[3,2] = (1-sum(VF[3,:]))*K["melt_exposed"]                              # remaining to scrap if melt is uncovered
    VF[2,3] = VF[3,2]*A[3]/(A[2]+1e-6)                                         # scrap to melt
    # melt to itself (only relevant when uncovered, for balance)
    VF[3,3] = (1-sum(VF[3,:]))*(1-K["melt_exposed"])
    
    # uncooled roof (heart) and watercooled roof sections to scrap and vice versa
    VF[8,2] = 1-sum(VF[8,:])
    VF[0,2] = 1-sum(VF[0,:])
    VF[2,8] = VF[8,2]*A[8]/(A[2]+1e-10)
    VF[2,0] = VF[0,2]*A[0]/(A[2]+1e-10)
    
    VF[1,2]=1-sum(VF[1,:])
    VF[2,1]=VF[1,2]*(A[1]-A[6])/(A[2]+1e-10)
    
    VF[6,2]=(1-sum(VF[6,:]))*K["wall_unc_exposed"]
    VF[2,6]=VF[6,2]*A[6]/(A[2]+1e-10)
    
    
    VF[6,6]=VF[6,6]+(1-sum(VF[6,:]))*(1-K["wall_unc_exposed"]) # so that VF[6,6] is 1 if uncooled wall is not exposed (necessary for numerical reasons, only relevant if K["wall_unc_exposed!=1)
    # uncooled wall segment to cooled wall segment from sum of view factors
    
    # so that VF[8,8] is 1 if uncooled roof surface is zero (necessary for numerical reasons)
    # and all other VF[8,:]=0
    VF[8,:]=VF[8,:]*tan_hyp(A[8]/(A[0]+1e-10),0.003,10000,1)
    VF[:,8]=VF[:,8]*tan_hyp(A[8]/(A[0]+1e-10),0.003,10000,1)
    VF[8,8]=1-tan_hyp(A[8]/(A[0]+1e-10),0.003,10000,1)
    
    ## Add surfaces 8 and 6 to avoid additional zone and correct surface areas
    VF[:,6]=VF[:,6]+VF[:,8] # add all VFs to uncooled wall and roof heart
    VF[6,:]=(VF[6,:]*A[6]+VF[8,:]*A[8])/(A[6]+A[8]) # add surface weighted view factors from uncooled wall and roof heart
    
    # remove view factors to and from roof heart
    #VF=np.delete(VF,8,axis=0)
    #VF=np.delete(VF,8,axis=1)
    VF_c = VF[:8,:8]

    A[3] = A[3]*K["melt_exposed"]                                         # correct A[3] for covered/uncovered melt
    A[1]=A[1]-A[6]                                                             # correct A[1] to account for uncooled wall section
    A[6]=A[6]+A[8]                                                             # add roof heart surface and uncooled wall surface
    #A=np.delete(A,8,axis=0)                                                    # remove surface 8 (roof heart)
    A_c = A[:8]
    ## normalising all VF to get sum(VF[i,:]) close to 1 and  A = VF'*A;
    #if VF.any()<0 or VF.any()>1 or VF.imag.any()!=0 or np.isnan(VF.any()):
        #stop=1
    
    VF_c[:8] = (VF_c[:8, :].T / np.sum(VF_c[:8, :], axis=1))
    # normalising VF to get sum(VF_i_j*A_j)==A_i
    # Important for Q_i calculation (energy balance)
    VF_transp = VF_c
    A_test = VF_transp.dot(A_c)
    VF_transp[0,:] = VF_transp[0,:]*(A_c[0]/(A_test[0]+1e-10))
    VF_transp[1,:] = VF_transp[1,:]*(A_c[1]/A_test[1])
    VF_transp[2,:] = VF_transp[2,:]*(A_c[2]/(A_test[2]+1e-10))
    VF_transp[3,:] = VF_transp[3,:]*(A_c[3]/(A_test[3]+1e-10))
    VF_transp[4,:] = VF_transp[4,:]*(A_c[4]/A_test[4])
    VF_transp[5,:] = VF_transp[5,:]*(A_c[5]/A_test[5])
    VF_transp[6,:] = VF_transp[6,:]*((A_c[6]+1e-10)/(A_test[6]+1e-10))
    VF_transp[7,:] = VF_transp[7,:]*(A_c[7]/(A_test[7]+1e-10))
    
    iv["VF"] = np.absolute(VF_transp.T)
    

# https://web.archive.org/web/20190820120322/http://www.thermalradiation.net/
# permanent link to archived version of thermalradiation webpage from Agust
# 2019
