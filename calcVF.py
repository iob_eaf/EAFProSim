# -*- coding: utf-8 -*-
"""
Created on Wed Mar 31 13:10:55 2021

@author: schuettensack
"""
import numpy as np
import math
import numpy.matlib
from numba import njit

@njit(cache=True) #, fastmath=True
def calcVF(p,schmelzIDX,segm_perc):
    
    #vertices
    p_vec=np.zeros((10,2))
    p_vec[0] = [p["r_EAF_up"]-p["r_arc"],0]                                          # contact point of arc and melt (at arc radius)
    p_vec[1] = [p["r_EAF_up"]-p["r_hole"],0]                                         # contact point of inside of scrap with melt (at radius of hole)
    p_vec[2][0] = p_vec[1][0]
    p_vec[2][1] = p["h_hole"]                                         # top of inside surface of hole (at radius of hole)
    p_vec[3] = [0,p["h_hole"]]                                                    # top of scrap surface at wall (at wall radius)
    p_vec[4] = [0,p["h_EAF_low"]+p["h_wall_unc"]-p["h_melt"]-p["h_scrap"]+p["h_hole"]]        # top of uncooled wall at wall radius (actual uncooled wall, not just exposed section)
    p_vec[5] = [0,p["h_el"]+p["h_arc"]]                                              # corner of roof and wall
    p_vec[6] = [p["r_EAF_up"]-p["r_heart"],p_vec[5][1]]                              # contact point of roof heart and water cooled roof 
    p_vec[7] = [p["r_EAF_up"]-p["r_el_rep"],p_vec[5][1]]                             # contact point of electrode and roof heart
    p_vec[8] = [p["r_EAF_up"]-p["r_el_rep"],p["h_arc"]]                                 # outside corner of electrode tip (at bottom of electrode and radius of electrode)
    p_vec[9] = [p_vec[0][0],p["h_arc"]]                                           # contact point of arc and electrode tip
    
    VF = np.zeros((3,9))
    num_elements = 100
    elements_arr = np.arange(1, num_elements+1)

    p_arc=np.zeros((num_elements,2))
    p_arc[:,0] = p["r_EAF_up"]-p["r_arc"]
    p_arc[:,1] = (p["h_arc"]/num_elements) * (elements_arr - 0.5)
    p_el=np.zeros((num_elements,2))
    p_el[:,0] = p["r_EAF_up"]-p["r_el_rep"]
    p_el[:,1] = p["h_arc"] + (p["h_el"]/num_elements) * (elements_arr - 0.5)

    vec =np.zeros((2*10*num_elements,2))
    for i in range(0,num_elements):
        for k in range(0,10):
            vec[i*10+k,:] = p_vec[k,:] - p_arc[i,:]
    for i in range(0,num_elements):
        for k in range(0,10):
            vec[num_elements*10+i*10+k,:] = p_vec[k,:] - p_el[i,:]

    angle = np.arctan(np.absolute(vec[:,0]/vec[:,1]))

    for i in range(0,vec.shape[0]):
        if vec[i,1]>0:
            angle[i] = np.pi - angle[i]

    #calculation viewfactor arc
    VFF=np.zeros((9,num_elements))
    sz1 = 9*num_elements
    sz2 = 10*num_elements
    erg = np.zeros(sz1)
    for i in range(0,num_elements):
        erg[7+i*9] = np.pi - angle[8+i*10]
        erg[0+i*9] = angle[1+i*10]

    bounds = np.stack((angle[1:sz2:10],angle[8:sz2:10])).T
    phi = np.zeros(num_elements)
    phi = np.minimum(angle[2:sz2:10], bounds[:, 1])
    erg[1:sz1:9] = phi - bounds[:, 0]

    bounds[:,0] = phi.copy()
    
    for kk in range(2, 7):
        phi = np.minimum(angle[kk + 1:sz2:10], bounds[:, 1]) - bounds[:, 0]
        phi[phi < 0] = 0
        erg[kk:sz1:9] = phi
        bounds[:, 0] = bounds[:, 0] + phi

    erg = np.cumsum(erg)
    for i in range(0,num_elements):
        for k in range(0,9):
            erg[i*9+k] = erg[i*9+k] - i*np.pi

    targ_vect=np.arange(1,num_elements*10+1)
    targ_vect = np.delete(targ_vect, np.arange(0, targ_vect.size, 10))

    #div_vect=np.insert(erg, np.arange(0,num_elements*9,9), 0.0)
    div_vect = np.zeros(np.shape(erg)[0]+num_elements)
    for i in range(0,num_elements):
        div_vect[1+i*10:(i+1)*10] = erg[0+i*9:(i+1)*9]
    int_vect= (div_vect-np.sin(div_vect)*np.cos(div_vect))/np.pi
    int_div=np.append(int_vect[1:],[1.0])-int_vect

    
    VFF=np.delete(int_div, np.arange(9, int_div.size, 10)).reshape((num_elements,9)).T
    VF_arc = np.sum(VFF,axis=1)/num_elements
    
    VF[0,0] = VF_arc[5]
    VF[0,1] = VF_arc[4]
    VF[0,2] = VF_arc[1]+VF_arc[2]+VF_arc[0]*(1-schmelzIDX)
    VF[0,3] = schmelzIDX*VF_arc[0]
    VF[0,6] = VF_arc[3]
    VF[0,7] = VF_arc[7]
    VF[0,8] = VF_arc[6]
    
    
    # calculation VF electrode
    angle=np.delete(angle, np.arange(0,10*num_elements))
    VFF=np.zeros((9,num_elements))
    erg = np.zeros(sz1)
    bounds = np.zeros((num_elements,2))
    bounds[:,1] = np.pi
    
    for kk in range(0, 9):
        phi = np.minimum(angle[kk+1:sz2:10], bounds[:, 1]) - bounds[:, 0]
        phi[phi < 0] = 0
        erg[kk:sz1:9] = phi
        bounds[:, 0] = bounds[:, 0] + phi

    #erg = np.cumsum(erg)-(np.repeat(np.array(range(0,num_elements)).T,9, axis=0)*np.pi)
    erg = np.cumsum(erg)
    for i in range(0,num_elements):
        for k in range(0,9):
            erg[i*9+k] = erg[i*9+k] - i*np.pi

    targ_vect=np.arange(1,num_elements*10+1)
    targ_vect = np.delete(targ_vect, np.arange(0, targ_vect.size, 10))
    #div_vect=np.insert(erg, list(range(0,num_elements*9,9)), 0.0)
    div_vect = np.zeros(np.shape(erg)[0]+num_elements)
    for i in range(0,num_elements):
        div_vect[1+i*10:(i+1)*10] = erg[0+i*9:(i+1)*9]
    int_vect= (div_vect-np.sin(div_vect)*np.cos(div_vect))/np.pi
    int_div=np.append(int_vect[1:],[1.0])-int_vect

    
    VFF=np.delete(int_div, np.arange(9, int_div.size, 10)).reshape((num_elements,9)).T                        
    
    num_segment = round(segm_perc*num_elements)
    
    VF_elu = np.sum(VFF[:,0:num_segment],axis=1)/num_segment
    VF_elo = np.sum(VFF[:,num_segment:num_elements],axis=1)/(num_elements-num_segment)
    VF_el = np.stack((VF_elo,VF_elu)).T
    
    VF[1:3,0] = VF_el[5,:]
    VF[1:3,1] = VF_el[4,:]
    VF[1:3,2] = VF_el[1,:]+VF_el[2,:]+VF_el[0,:]*(1-schmelzIDX)
    VF[1:3,3] = schmelzIDX*VF_el[0,:]
    VF[1:3,4] = VF_el[8,:]
    VF[1:3,6] = VF_el[3,:]
    VF[1:3,8] = VF_el[6,:]
    
    return VF
